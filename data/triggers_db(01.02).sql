DROP TRIGGER IF EXISTS insert_film_info;
CREATE TRIGGER insert_film_info
AFTER INSERT ON film_user FOR EACH ROW
BEGIN
  DECLARE durationCorrection INT;
 	SET durationCorrection = (SELECT runtime FROM film WHERE id = NEW.film_id);
    IF NEW.status = 'WATCHED' THEN
      update film SET reviews = reviews + 1 WHERE id = NEW.film_id;
  	  IF durationCorrection IS NOT NULL THEN
  	  update user SET duration = duration + durationCorrection WHERE id = NEW.user_id;
  	  END IF;
    END IF;

    IF NEW.like_value = 'DISLIKED' THEN 
        update film SET dislikes = dislikes + 1 WHERE id = NEW.film_id;
    ELSEIF NEW.like_value = 'LIKED' THEN
        update film SET likes = likes + 1 WHERE id = NEW.film_id;
    END IF;
      update film SET rating_custom = (10 * likes / (likes + dislikes)) WHERE id = NEW.film_id;
END;

DROP TRIGGER IF EXISTS update_films;
CREATE TRIGGER update_films
AFTER UPDATE ON film_user FOR EACH ROW
BEGIN
  DECLARE durationCorrection INT;
  IF 
    NEW.status <> OLD.status
  THEN
 	SET durationCorrection = (SELECT runtime FROM film WHERE id = NEW.film_id);
    IF NEW.status = 'WATCHED' THEN
      update film SET reviews = reviews + 1 WHERE id = NEW.film_id;
	  IF durationCorrection IS NOT NULL THEN
	  update user SET duration = duration + durationCorrection WHERE id = NEW.user_id;
	  END IF;
    ELSEIF OLD.status = 'WATCHED' THEN
      update film SET reviews = reviews - 1 WHERE id = NEW.film_id;
	  IF durationCorrection IS NOT NULL THEN
	  update user SET duration = duration - durationCorrection WHERE id = NEW.user_id;
	  END IF;
    END IF;
  END IF;
  IF 
    NEW.like_value <> OLD.like_value
  THEN
    IF OLD.like_value = 'LIKED' THEN
       update film SET likes = likes - 1 WHERE id = NEW.film_id;
    END IF; 
    IF OLD.like_value = 'DISLIKED' THEN 
       update film SET dislikes = dislikes - 1 WHERE id = NEW.film_id;
    END IF;
    IF NEW.like_value = 'DISLIKED' THEN 
       update film SET dislikes = dislikes + 1 WHERE id = NEW.film_id;
    ELSEIF NEW.like_value = 'LIKED' THEN
        update film SET likes = likes + 1 WHERE id = NEW.film_id;
    END IF;
    update film SET rating_custom = (10 * likes / (likes + dislikes)) WHERE id = NEW.film_id;
  END IF;
END