package com.epam.lab.autoupdater.converter;

import com.epam.lab.dto.ImdbFilm;
import com.epam.lab.i18n.Translater;
import com.epam.lab.model.Film;

import java.text.NumberFormat;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class FilmConverter {

    private Film film;
    private ImdbFilm imdbFilm;

    public FilmConverter(Film film, ImdbFilm imdbFilm) {
        this.film = film;
        this.imdbFilm = imdbFilm;
    }

    public void convert() {
        String title = DataParser.parseText(imdbFilm.getTitle());
        Integer year = DataParser.parseInteger(imdbFilm.getYear());
        String released = DataParser.parseDate(imdbFilm.getReleased());
        Integer runtime = DataParser.parseRuntime(imdbFilm.getRuntime());
        String director = DataParser.parseText(imdbFilm.getDirector());
        String writer = DataParser.parseText(imdbFilm.getWriter());
        String actors = DataParser.parseText(imdbFilm.getActors());
        String plot = DataParser.parseText(imdbFilm.getPlot());
        Double ratingImdb = DataParser.parseDouble(imdbFilm.getImdbRating());
        String imdbId = imdbFilm.getImdbID();
        String poster = imdbFilm.getPoster();
        film.setTitle(title);
        film.setYear(year);
        film.setReleased(released);
        film.setRuntime(runtime);
        film.setDirector(director);
        film.setWriter(writer);
        film.setActors(actors);
        film.setPlot(plot);
        film.setRatingImdb(ratingImdb);
        film.setImdbId(imdbId);
        film.setPoster(poster);
        if (title != null) {
            film.setTitleUk(Translater.translate(title));
        }
        if (plot != null) {
            film.setTitleUk(Translater.translate(plot));
        }
    }

    public void update() {
        String title = DataParser.parseText(imdbFilm.getTitle());
        Integer year = DataParser.parseInteger(imdbFilm.getYear());
        String released = DataParser.parseDate(imdbFilm.getReleased());
        Integer runtime = DataParser.parseRuntime(imdbFilm.getRuntime());
        String director = DataParser.parseText(imdbFilm.getDirector());
        String writer = DataParser.parseText(imdbFilm.getWriter());
        String actors = DataParser.parseText(imdbFilm.getActors());
        String plot = DataParser.parseText(imdbFilm.getPlot());
        Double ratingImdb = DataParser.parseDouble(imdbFilm.getImdbRating());
        String poster = imdbFilm.getPoster();
        if (title != null && !title.equals(film.getTitle())) {
            film.setTitle(title);
            film.setTitleUk(Translater.translate(title));
        }
        if (year != null && !year.equals(film.getYear())) {
            film.setYear(year);
        }
        if (released != null && !released.equals(film.getReleased())) {
            film.setReleased(released);
        }
        if (runtime != null && !runtime.equals(film.getRuntime())) {
            film.setRuntime(runtime);
        }
        if (director != null && !director.equals(film.getDirector())) {
            film.setDirector(director);
        }
        if (writer != null && !writer.equals(film.getWriter())) {
            film.setWriter(writer);
        }
        if (actors != null && !actors.equals(film.getActors())) {
            film.setActors(actors);
        }
        if (plot != null && !plot.equals(film.getPlot())) {
            film.setPlot(plot);
            film.setPlotUk(Translater.translate(plot));
        }
        if (ratingImdb != null && !ratingImdb.equals(film.getRatingImdb())) {
            film.setRatingImdb(ratingImdb);
        }
        if (!poster.equals(film.getPoster())) {
            film.setPoster(poster);
        }
    }

    public static class DataParser {

        public static Integer parseInteger(String number) {
            try {
                return Integer.parseInt(number);
            } catch (NumberFormatException e) {
                return null;
            }
        }

        public static Double parseDouble(String number) {
            try {
                return Double.parseDouble(number);
            } catch (NumberFormatException e) {
                return null;
            }
        }

        public static String parseDate(String date) {
            try {
                return LocalDate.parse(date, DateTimeFormatter.ofPattern("dd MMM yyyy"))
                        .format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            } catch (DateTimeParseException e) {
                return null;
            }
        }

        public static Integer parseRuntime(String runtime) {
            try {
                return Integer.parseInt(String.valueOf(NumberFormat.getInstance().parse(runtime)));
            } catch (ParseException e) {
                return null;
            }
        }

        public static String[] parseList(String list) {
            if (list.equals("N/A")) {
                return null;
            } else {
                return list.split(", ");
            }
        }

        public static String parseText(String text) {
            if (text.equals("N/A")) {
                return null;
            } else {
                return text.replaceAll("\"", "\\\\\"");
            }
        }

    }
}
