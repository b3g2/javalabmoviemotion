package com.epam.lab.autoupdater.parser;

import com.epam.lab.autoupdater.converter.FilmConverter;
import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dto.ImdbFilm;
import com.epam.lab.i18n.Translater;
import com.epam.lab.model.*;
import com.google.gson.Gson;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FilmsParser {

    private final Pattern FILM_PATTERN = Pattern.compile("/title/tt\\d+/");
    private final Pattern ID_PATTERN = Pattern.compile("/tt\\d+/");

    public void downloadPage(URL url, String fileName, boolean append) throws IOException {
        HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
        httpcon.addRequestProperty("User-Agent", "Mozilla/4.76");
        InputStream is = httpcon.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        BufferedWriter writer = new BufferedWriter(new FileWriter(new File(fileName), append));

        String line;
        while ((line = reader.readLine()) != null) {
            writer.write(line);
            writer.newLine();
        }
    }

    public void parsePageForFilmsIds(String pageFileName, String idsFileName, boolean append) {
        try (
                BufferedReader reader = new BufferedReader(new FileReader(new File(pageFileName)));
                BufferedWriter writer = new BufferedWriter(new FileWriter(new File(idsFileName), append))
        ) {
            String line;
            while ((line = reader.readLine()) != null) {
                String id = getFilmId(line);
                if (id != null) {
                    writer.write(id);
                    writer.newLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private String getFilmId(String line) {
        String id = null;
        Matcher matcher = FILM_PATTERN.matcher(line);
        if (matcher.find()) {
            String filmHref = line.substring(matcher.start(), matcher.end());
            Matcher m = ID_PATTERN.matcher(filmHref);
            if (m.find()) {
                id = filmHref.substring(m.start() + 1, m.end() - 1);
            }
        }
        return id;
    }

    public void getFilmsByIds(String idsFileName, String filmsFileName, boolean append) {
        try (
                BufferedReader reader = new BufferedReader(new FileReader(new File(idsFileName)));
                BufferedWriter writer = new BufferedWriter(new FileWriter(new File(filmsFileName), append))
        ) {
            String line;
            String film;
            while ((line = reader.readLine()) != null) {
                film = getFilmFromApi(line);
                if (film != null) {
                    writer.append(film);
                    writer.newLine();
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getFilmFromApi(String imdbId) throws IOException {
        URL url = new URL("http://www.omdbapi.com/?i=" + imdbId + "&plot=full");
        HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
        httpcon.addRequestProperty("User-Agent", "Mozilla/4.76");
        InputStream is = httpcon.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        String line = reader.readLine();
        if (line.contains("Title")) {
            return line;
        } else {
            return null;
        }
    }

    public void addFilmsToDatabase(String filmsFileName) {
        Gson gson = new Gson();
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(filmsFileName)))) {
            String line;
            while ((line = reader.readLine()) != null) {
                addFilm(gson.fromJson(line, ImdbFilm.class));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void addFilm(ImdbFilm imdbFilm) {
        GeneralDAOService daoService = GeneralDAOService.getInstance();
        Film film = GeneralDAOService.getInstance().get("imdb_id = \"" + imdbFilm.getImdbID() + "\"", Film.class);
        if (imdbFilm.getType().equals("movie") && film == null) {
            film = new Film();
            new FilmConverter(film, imdbFilm).convert();
            daoService.add(film);
            film = daoService.get(film);
            //updates genres
            String[] genres = FilmConverter.DataParser.parseList(imdbFilm.getGenre());
            updateGenres(film.getId(), genres);
            //updates countries
            String[] countries = FilmConverter.DataParser.parseList(imdbFilm.getCountry());
            updateCountries(film.getId(), countries);
        }
    }

    public void updateFilm(ImdbFilm imdbFilm) {
        GeneralDAOService daoService = GeneralDAOService.getInstance();
        Film film = GeneralDAOService.getInstance().get("imdb_id = \"" + imdbFilm.getImdbID() + "\"", Film.class);
        if (imdbFilm.getType().equals("movie") && film != null) {
            new FilmConverter(film, imdbFilm).update();
            daoService.update("id = \"" + film.getId() + "\"", film);
            //updates genres
            String[] genres = FilmConverter.DataParser.parseList(imdbFilm.getGenre());
            updateGenres(film.getId(), genres);
            //updates countries
            String[] countries = FilmConverter.DataParser.parseList(imdbFilm.getCountry());
            updateCountries(film.getId(), countries);
        }
    }

    private void updateGenres(Integer filmId, String[] genres) {
        GeneralDAOService daoService = GeneralDAOService.getInstance();
        if (genres != null) {
            for (String genreName : genres) {
                Genre genre = daoService.get("name = \"" + genreName + "\"", Genre.class);
                if (genre == null) {
                    daoService.add(new Genre(genreName, Translater.translate(genreName)));
                    genre = daoService.get("name = \"" + genreName + "\"", Genre.class);
                }
                if (daoService.get("id = \"" + genre.getId() + "\"", Genre.class) == null) {
                    FilmGenre filmGenre = new FilmGenre(filmId, genre.getId());
                    daoService.add(filmGenre);
                }
            }
        }
    }

    private void updateCountries(Integer filmId, String[] countries) {
        GeneralDAOService daoService = GeneralDAOService.getInstance();
        if (countries != null) {
            for (String countryName : countries) {
                Country country = daoService.get("name = \"" + countryName + "\"", Country.class);
                if (country == null) {
                    daoService.add(new Country(countryName, Translater.translate(countryName)));
                    country = daoService.get("name = \"" + countryName + "\"", Country.class);
                }
                if (daoService.get("id = \"" + country.getId() + "\"", Country.class) == null) {
                    FilmCountry filmCountry = new FilmCountry(filmId, country.getId());
                    daoService.add(filmCountry);
                }
            }
        }
    }

//    public String getImdbTrailer(String imdbId) throws IOException {
//        URL url = new URL("http://m.imdb.com/title/" + imdbId + "/videogallery");
//        HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
//        httpcon.addRequestProperty("User-Agent", "Mozilla/4.76");
//        InputStream is = httpcon.getInputStream();
//        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
//        StringBuilder htmlBuilder = new StringBuilder();
//        String line;
//        while ((line = reader.readLine()) != null) {
//            htmlBuilder.append(line);
//        }
//        XMLParser html = new XMLParser(htmlBuilder.toString());
//        return html.find("video").attr("src");
//    }
}
