package com.epam.lab.autoupdater.updater;

import com.epam.lab.autoupdater.parser.FilmsParser;
import com.epam.lab.dto.ImdbFilm;
import com.epam.lab.dto.SimpleFilm;
import com.epam.lab.factory.ServiceFactory;
import com.google.gson.Gson;

import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class FilmsUpdater {

    public void updateAll() throws IOException {

    }

    public void updateNew() throws IOException {
        final String pagePath = "./mm_films_data/page.dat";
        final String idsPath = "./mm_films_data/ids.dat";
        final String filmsPath = "./mm_films_data/films.dat";

        new File("./mm_films_data").mkdir();
        new File(pagePath).delete();
        new File(idsPath).delete();
        new File(filmsPath).delete();

        LocalDate date = LocalDate.now();
        FilmsParser parser = new FilmsParser();
        for (int i = 0; i < 12; i++) {
            String s = date.format(DateTimeFormatter.ofPattern("yyyy-MM"));
            URL url = new URL("http://www.imdb.com/movies-coming-soon/" + s + "/?ref_=cs_dt_pv");
            parser.downloadPage(url, pagePath, true);
            date = date.plusWeeks(1);
        }
        parser.parsePageForFilmsIds(pagePath, idsPath, false);
        parser.getFilmsByIds(idsPath, filmsPath, false);
        parser.addFilmsToDatabase(filmsPath);
    }

    public void updateExisted() throws IOException {
        FilmsParser parser = new FilmsParser();
        Gson gson = new Gson();
        List<SimpleFilm> films = ServiceFactory.getDataService().getFilmsIds();
        for (SimpleFilm film : films) {
            String filmJson = parser.getFilmFromApi(film.getImdbId());
            ImdbFilm imdbFilm = gson.fromJson(filmJson, ImdbFilm.class);
            parser.updateFilm(imdbFilm);
        }
    }

}
