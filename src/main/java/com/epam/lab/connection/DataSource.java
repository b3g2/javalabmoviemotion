package com.epam.lab.connection;

import java.sql.Connection;
import java.sql.SQLException;

public class DataSource {

	private static JdbcConnectionPool pool = new JdbcConnectionPool();

	/*
	 * This method is used to get a connection from the pool connections
	 */
	public static Connection getConnection() throws SQLException {
		return pool.getConnectionFromPool();
	}

	/*
	 * This method is used to return the connection to the pool connections
	 */
	public static void returnConnection(Connection connection) {
		pool.returnConnectionToPool(connection);
	}
}