package com.epam.lab.connection;

import org.apache.log4j.Logger;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class JdbcConnectionPool {

	private static final Logger LOG = Logger.getLogger(JdbcConnectionPool.class);
	private static final Integer DB_MAX_CONNECTIONS = 50;
	private static Properties properties = new Properties();
	private List<Connection> availableConnections = new ArrayList<>();

	/*
	 * Download configuration file connection to the MySQL database
	 */
	static {
		try {
			properties.load(JdbcConnectionPool.class.getClassLoader().getResourceAsStream("db-config.properties"));
		} catch (IOException e) {
			LOG.error("db-config.properties file load error" + e);
		}
	}

	public JdbcConnectionPool() {
		initializeConnectionPool();
	}

	private void initializeConnectionPool() {
		while (!checkIfConnectionPoolIsFull()) {
			availableConnections.add(createNewConnectionForPool());
		}
	}

	private synchronized boolean checkIfConnectionPoolIsFull() {
		return availableConnections.size() >= DB_MAX_CONNECTIONS;
	}

	/*
	 * Creating a connection
	 */
	private Connection createNewConnectionForPool() {
		try {
			Class.forName(properties.getProperty("driverClass"));
			try {
				return DriverManager.getConnection(properties.getProperty("url"),
						properties.getProperty("userName"), properties.getProperty("password"));
			} catch (SQLException e) {
				LOG.error("SQL Exception in JdbcConnectionPool " + e);
			}

		} catch (ClassNotFoundException e) {
			LOG.error("Class Not Found Exception " + e);
		}
		return null;
	}

	/*
	 * This method is used to get a connection from the pool connections
	 */
	public synchronized Connection getConnectionFromPool() {
		Connection connection = null;
		if (availableConnections.size() > 0) {
			connection = availableConnections.get(0);
			availableConnections.remove(0);
		}
		return connection;
	}

	/*
	 * This method is used to return the connection to the pool connections
	 */
	public synchronized void returnConnectionToPool(Connection connection) {
		if (! checkIfConnectionPoolIsFull()) {
			availableConnections.add(connection);
		}
	}
}