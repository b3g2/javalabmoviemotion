package com.epam.lab.dao;

import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;
import com.epam.lab.dao.GeneralDAOService.JoinResultMap;
import com.epam.lab.transformer.GeneralTransformer;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

public final class GeneralDAO {

    private static GeneralDAO instance = null;
    private GeneralTransformer transformer;

    private GeneralDAO() {
        if (transformer == null) {
            transformer = GeneralTransformer.getInstance();
        }
    }

    public static GeneralDAO getInstance() {
        if (instance == null) {
            instance = new GeneralDAO();
        }
        return instance;
    }

    public Object getAll(Connection connection, Class clazz) throws SQLException {
        return getList(connection, null, clazz);
    }

    /**
     * SELECT * FROM <i>tableName</i> WHERE <b>condition</b>
     *
     * @return list of objects that match condition
     */
    public Object getList(Connection connection, String condition, Class clazz) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement(createSelectQuery(condition, clazz));
        ResultSet rs = stmt.executeQuery();
        return transformer.transformToList(rs, clazz);
    }

    public Object getList(Connection connection, String condition, int from, int amount, Class clazz) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement(createSelectQuery(condition, from, amount, clazz));
        ResultSet rs = stmt.executeQuery();
        return transformer.transformToList(rs, clazz);
    }

    /**
     * SELECT * FROM <i>tableName</i> LIMIT <b>offset</b>, <b>count</b>
     *
     * @return list of objects with limit
     */
    public Object getList(Connection connection, Integer offset, Integer count, Class clazz) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement(createSelectQuery(offset, count, clazz));
        ResultSet rs = stmt.executeQuery();
        return transformer.transformToList(rs, clazz);
    }

    /**
     * SELECT * FROM <i>tableName</i> WHERE <b>condition</b>
     *
     * @return first object that match condition
     */
    public Object get(Connection connection, String condition, Class clazz) throws SQLException {
        PreparedStatement stmt = connection.prepareStatement(createSelectQuery(condition, clazz));
        ResultSet rs = stmt.executeQuery();
        return transformer.transformToObject(rs, clazz);
    }

    public Object get(Connection connection, Object object) throws SQLException, IllegalAccessException {
        PreparedStatement stmt = connection.prepareStatement(createSelectQuery(object));
        ResultSet rs = stmt.executeQuery();
        return transformer.transformToObject(rs, object.getClass());
    }

    public int update(Connection connection, Object oldObject, Object newObject) throws SQLException, IllegalAccessException {
        PreparedStatement stmt = connection.prepareStatement(createUpdateQuery(oldObject, newObject));
        return stmt.executeUpdate();
    }

    public int update(Connection connection, String condition, String set, Class clazz) throws SQLException, IllegalAccessException {
        PreparedStatement stmt = connection.prepareStatement(createUpdateQuery(condition, set, clazz));
        return stmt.executeUpdate();
    }

    public int update(Connection connection, String condition, Object updatedObject) throws SQLException, IllegalAccessException {
        PreparedStatement stmt = connection.prepareStatement(createUpdateQuery(condition, updatedObject));
        return stmt.executeUpdate();
    }

    public int add(Connection connection, Object object) throws SQLException, IllegalAccessException {
        PreparedStatement stmt;
        stmt = connection.prepareStatement(createInsertQuery(object));
        return stmt.executeUpdate();
    }

    /**
     * DELETE FROM <i>tableName</i> WHERE <b>condition</b>
     */
    public int delete(Connection connection, String condition, Class clazz) throws SQLException, IllegalAccessException {
        PreparedStatement stmt = connection.prepareStatement(createDeleteQuery(condition, clazz));
        return stmt.executeUpdate();
    }

    public int delete(Connection connection, Object object) throws SQLException, IllegalAccessException {
        PreparedStatement stmt = connection.prepareStatement(createDeleteQuery(object));
        return stmt.executeUpdate();
    }

    private String createSelectQuery(String condition, Class clazz) {
        return "SELECT * FROM " + transformer.getTableName(clazz)
                + (condition != null ? " WHERE " + condition : "");
    }

    private String createSelectQuery(String condition, int from, int amount, Class clazz) {
        String query = "SELECT * FROM " + transformer.getTableName(clazz) +
                " WHERE " + condition +
                " LIMIT " + (from >= 0 ? from + ", " : "") + amount;
        return processStatement(query);
    }


    private String createSelectQuery(Integer offset, Integer count, Class clazz) {
        return "SELECT * FROM " + transformer.getTableName(clazz)
                + (offset != null && count != null ? " LIMIT " + offset + ", " + count : "");
    }

    private String createSelectQuery(Object object) throws IllegalAccessException {
        final StringBuilder query = new StringBuilder("SELECT * FROM ");
        // get table name
        query.append(object.getClass().getDeclaredAnnotation(TableName.class).value()).append(" WHERE");
        // get all columns
        final List<Field> fields = transformer.getColumns(object.getClass().getDeclaredFields());
        // form part of string with columns values to update (SET)
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = field.get(object);
            boolean notNull = field.getDeclaredAnnotation(NotNull.class) != null;
            if (notNull && value == null)
                continue;
            query.append(" ").append(transformer.getColumnName(field)).append(value != null ? " = " : " IS ")
                    .append("\"").append(value).append("\" AND ");
        }
        // delete last 'AND '
        return processStatement(query.delete(query.length() - 5, query.length()).toString());
    }

    private String createUpdateQuery(String condition, String set, Class clazz) throws IllegalAccessException {
        String query = "UPDATE " + transformer.getTableName(clazz)
                + " SET " + set + (condition != null ? " WHERE " + condition : "");
        return processStatement(query);
    }

    private String createUpdateQuery(Object oldObject, Object newObject) throws IllegalAccessException {
        final StringBuilder query = new StringBuilder("UPDATE ");
        // get table name
        query.append(oldObject.getClass().getDeclaredAnnotation(TableName.class).value()).append(" SET");
        // get all columns
        final List<Field> fields = transformer.getColumns(oldObject.getClass().getDeclaredFields());
        // form part of string with columns values to update (SET)
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = field.get(newObject);
            boolean notNull = field.getDeclaredAnnotation(NotNull.class) != null;
            if (notNull && value == null)
                continue;
            query.append(" ").append(transformer.getColumnName(field)).append(" = ").append("\"")
                    .append(field.get(newObject)).append("\", ");
        }
        // delete last ', '
        query.delete(query.length() - 2, query.length()).append(" WHERE");
        // form part of string with condition (WHERE)
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = field.get(oldObject);
            boolean notNull = field.getDeclaredAnnotation(NotNull.class) != null;
            if (notNull && value == null)
                continue;
            query.append(" ").append(transformer.getColumnName(field)).append(value != null ? " = " : " IS ")
                    .append("\"").append(value).append("\" AND ");
        }
        // delete last 'AND '
        return processStatement(query.delete(query.length() - 5, query.length()).toString());
    }

    private String createUpdateQuery(String condition, Object updatedObject) throws IllegalAccessException {
        final StringBuilder query = new StringBuilder("UPDATE ");
        // get table name
        query.append(updatedObject.getClass().getDeclaredAnnotation(TableName.class).value()).append(" SET");
        // get all columns
        final List<Field> fields = transformer.getColumns(updatedObject.getClass().getDeclaredFields());
        // form part of string with columns values to update (SET)
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = field.get(updatedObject);
            boolean notNull = field.getDeclaredAnnotation(NotNull.class) != null;
            if (notNull && value == null)
                continue;
            query.append(" ").append(transformer.getColumnName(field)).append(" = ").append("\"")
                    .append(field.get(updatedObject)).append("\", ");
        }
        // delete last ', ' and append WHERE condition
        query.delete(query.length() - 2, query.length()).append(" WHERE ").append(condition);
        return processStatement(query.toString());
    }

    private String createInsertQuery(Object object) throws IllegalAccessException {
        final StringBuilder query = new StringBuilder("INSERT INTO ");
        // get table name
        query.append(object.getClass().getDeclaredAnnotation(TableName.class).value()).append(" (");
        // get all columns
        final List<Field> fields = transformer.getColumns(object.getClass().getDeclaredFields());
        // form part of string with columns to insert (INTO table_name (...,
        // ...., ...))
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = field.get(object);
            boolean notNull = field.getDeclaredAnnotation(NotNull.class) != null;
            boolean pk = field.getDeclaredAnnotation(PrimaryKey.class) != null;
            if (!pk && notNull && value == null)
                continue;
            query.append(transformer.getColumnName(field)).append(", ");
        }
        // replace last ', '
        query.delete(query.length() - 2, query.length()).append(") VALUES (");
        // form part of string with columns values (VALUES)
        for (Field field : fields) {
            Object value = field.get(object);
            boolean notNull = field.getDeclaredAnnotation(NotNull.class) != null;
            boolean pk = field.getDeclaredAnnotation(PrimaryKey.class) != null;
            if (!pk && notNull && value == null)
                continue;

            query.append("\"").append(value).append("\"").append(", ");

        }
        // replace last ', '
        // replace 'null' by NULL
        // replace 'true' by true
        // replace 'false' by false
        return processStatement(query.delete(query.length() - 2, query.length()).append(")").toString());
    }

    private String createDeleteQuery(String condition, Class clazz) {
        return "DELETE FROM " + transformer.getTableName(clazz)
                + (condition != null ? " WHERE " + condition : "");
    }

    private String createDeleteQuery(Object object) throws IllegalAccessException {
        final StringBuilder query = new StringBuilder("DELETE FROM ");
        // get table name
        query.append(object.getClass().getDeclaredAnnotation(TableName.class).value()).append(" WHERE");
        // get all columns
        final List<Field> fields = transformer.getColumns(object.getClass().getDeclaredFields());
        // form part of string with condition (WHERE)
        for (Field field : fields) {
            field.setAccessible(true);
            Object value = field.get(object);
            boolean notNull = field.getDeclaredAnnotation(NotNull.class) != null;
            if (notNull && value == null)
                continue;
            query.append(" ").append(transformer.getColumnName(field)).append(value != null ? " = " : " IS ")
                    .append("\"").append(value).append("\" AND ");
        }
        // delete last 'AND '
        return processStatement(query.delete(query.length() - 5, query.length()).toString());
    }

    public JoinResultMap getJoin(Connection connection, String whereCondition, String groupByCondition, String orderByCondition, Object... joinVarargs) throws SQLException {
        Class[] joinClasses = new Class[(joinVarargs.length + 1) / 2];
        String[] joinConditions = new String[(joinVarargs.length + 1) / 2];
        processJoinVarargs(joinClasses, joinConditions, joinVarargs);
        PreparedStatement stmt = connection.prepareStatement(createJoinQuery(whereCondition, groupByCondition, orderByCondition, joinClasses, joinConditions));
        ResultSet rs = stmt.executeQuery();
        return transformer.transformToJoinedArray(rs, joinClasses);
    }

    private void processJoinVarargs(Class[] joinClasses, String[] joinConditions, Object[] joinVarargs) {
        int i;
        int k;
        for (i = 0, k = 0; i < joinVarargs.length - 2; i += 2, k++) {
            joinClasses[k] = (Class) joinVarargs[i];
            joinConditions[k] = (String) joinVarargs[i + 1];
        }
        joinClasses[k] = (Class) joinVarargs[i];
        joinConditions[k] = null;
    }

    private String createJoinQuery(String whereCondition, String groupBy, String orderBy, Class[] joinClasses, String[] joinConditions) {
        final StringBuilder query = new StringBuilder(createPrefixSelectCondition(joinClasses)).append(" FROM ");
        final String from = transformer.getTableName(joinClasses[0]);
        query.append(from);
        for (int i = 0; i < joinClasses.length; i++) {
            String condition = joinConditions[i];
            if (condition != null) {
                String join = transformer.getTableName(joinClasses[i + 1]);
                query.append(" JOIN ").append(join).append(" ON ").append(condition);
            }
        }
        if (whereCondition != null) {
            query.append(" WHERE ").append(whereCondition);
        }
        if (groupBy != null) {
            query.append(" GROUP BY ").append(groupBy);
        }
        if (orderBy != null) {
            query.append(" ORDER BY ").append(orderBy);
        }
        return query.toString();
    }

    private String createPrefixSelectCondition(Class[] classes) {
        StringBuilder builder = new StringBuilder("SELECT ");
        for (Class clazz : classes) {
            String tableName = transformer.getTableName(clazz);
            // get all columns
            List<Field> fields = transformer.getColumns(clazz.getDeclaredFields());
            // form part of string with condition (WHERE)
            for (Field field : fields) {
                String columnName = transformer.getColumnName(field);
                builder.append(tableName).append(".").append(columnName).append(" AS ")
                        .append(tableName).append("_").append(columnName).append(", ");
            }
        }
        // delete last ', '
        return builder.delete(builder.length() - 2, builder.length()).toString();
    }

    private String processStatement(String statement) {
        return statement.replaceAll("\"true\"", "true").replaceAll("\"false\"", "false")
                .replaceAll("\"null\"", "NULL");
    }

}
