package com.epam.lab.dao;

import com.epam.lab.connection.DataSource;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public final class GeneralDAOService {
    private GeneralDAO dao;
    private static GeneralDAOService instance;

    private GeneralDAOService() {
        if (dao == null) {
            dao = GeneralDAO.getInstance();
        }
    }

    public static GeneralDAOService getInstance() {
        if (instance == null) {
            instance = new GeneralDAOService();
        }
        return instance;
    }

    public JoinResultMap getJoin(Object... joinVarargs) {
        return getJoin(null, null, null, joinVarargs);
    }

    public JoinResultMap getJoin(String whereCondition, Object... joinVarargs) {
        return getJoin(whereCondition, null, null, joinVarargs);
    }

    /**
     * SELECT *
     * FROM <b>joinVarargs[0]</b>
     * [JOIN <b>joinVarargs[2]</b> ON <b>joinVarargs[1]</b>]
     * [JOIN <b>joinVarargs[n]</b> ON <b>joinVarargs[n-1]</b>]
     * [WHERE <b>whereCondition</b>]
     * [GROUP BY <b>groupByCondition</b>]
     * [ORDER BY <b>orderByCondition</b>]
     *
     * @return array of list of joined models
     */
    public JoinResultMap getJoin(String whereCondition, String groupByCondition, String orderByCondition, Object... joinVarargs) {
        JoinResultMap result = null;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = dao.getJoin(connection, whereCondition, groupByCondition, orderByCondition, joinVarargs);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                DataSource.returnConnection(connection);
            }
        }
        return result;
    }

    public <T> T get(String condition, Class<T> clazz) {
        T result = null;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = (T) dao.get(connection, condition, clazz);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                DataSource.returnConnection(connection);
            }
        }
        return result;
    }

    public <T> T get(T object) {
        T result = null;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = (T) dao.get(connection, object);
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                DataSource.returnConnection(connection);
            }
        }
        return result;
    }

    public <T> List<T> getList(String condition, Class<T> clazz) {
        List<T> result = null;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = (List<T>) dao.getList(connection, condition, clazz);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                DataSource.returnConnection(connection);
            }
        }
        return result;
    }

    public <T> List<T> getList(String condition, int from, int amount, Class<T> clazz) {
        List<T> result = null;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = (List<T>) dao.getList(connection, condition, from, amount, clazz);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                DataSource.returnConnection(connection);
            }
        }
        return result;
    }

    public <T> List<T> getList(Integer offset, Integer count, Class<T> clazz) {
        List<T> result = null;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = (List<T>) dao.getList(connection, offset, count, clazz);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                DataSource.returnConnection(connection);
            }
        }
        return result;
    }

    public <T> List<T> getAll(Class<T> clazz) {
        List<T> result = null;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = (List<T>) dao.getAll(connection, clazz);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                DataSource.returnConnection(connection);
            }
        }
        return result;
    }

    public <T> int add(T object) {
        int result = 0;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = dao.add(connection, object);
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            DataSource.returnConnection(connection);
        }
        return result;
    }

    public <T> int update(T oldObject, T newObject) {
        int result = 0;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = dao.update(connection, oldObject, newObject);
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            DataSource.returnConnection(connection);
        }
        return result;
    }

    public <T> int update(String condition, String set, Class<T> clazz) {
        int result = 0;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = dao.update(connection, condition, set, clazz);
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            DataSource.returnConnection(connection);
        }
        return result;
    }

    public <T> int update(String condition, T updatedObject) {
        int result = 0;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = dao.update(connection, condition, updatedObject);
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            DataSource.returnConnection(connection);
        }
        return result;
    }

    public <T> int delete(String condition, Class<T> clazz) {
        int result = 0;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = dao.delete(connection, condition, clazz);
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            if (connection != null) {
                DataSource.returnConnection(connection);
            }
        }
        return result;
    }

    public <T> int delete(T object) {
        int result = 0;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            result = dao.delete(connection, object);
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            DataSource.returnConnection(connection);
        }
        return result;
    }

    public <T> int deleteEach(List<T> objects) {
        int result = 0;
        Connection connection = null;
        try {
            connection = DataSource.getConnection();
            for (T object : objects) {
                result += dao.delete(connection, object);
            }
        } catch (SQLException | IllegalAccessException e) {
            e.printStackTrace();
        } finally {
            DataSource.returnConnection(connection);
        }
        return result;
    }

    public static class JoinResultMap {

        private Map<Class, List> map;

        public JoinResultMap() {
            map = new HashMap<>();
        }

        public JoinResultMap(int capacity) {
            map = new HashMap<>(capacity);
        }

        public <T> List<T> get(Class<T> clazz) {
            return (List<T>) map.get(clazz);
        }

        public <T> List<T> put(Class<T> clazz, List<T> list) {
            return (List<T>) map.put(clazz, list);
        }

        public int size() {
            return map.size();
        }

        public boolean isEmpty() {
            return map.isEmpty();
        }

        public <T> int listSize(Class<T> clazz) {
            List list = map.get(clazz);
            if (list != null) {
                return list.size();
            } else {
                return 0;
            }
        }

        public int listSize() {
            int listSize = 0;
            Iterator<Map.Entry<Class, List>> iterator = map.entrySet().iterator();
            List list;
            if (iterator.hasNext() && (list = iterator.next().getValue()) != null) {
                listSize = list.size();
            }
            return listSize;
        }

    }
}
