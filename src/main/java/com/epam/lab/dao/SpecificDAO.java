package com.epam.lab.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.epam.lab.connection.DataSource;
import com.epam.lab.model.Film;
import com.epam.lab.model.User;
import com.epam.lab.transformer.GeneralTransformer;

public class SpecificDAO {

	private static final String SEARCH_YEARS = "SELECT year FROM film GROUP BY year DESC";
	private static final String FILM_COUNT = "SELECT  count(*)  FROM film";

	public List<String> getAllYears() {
		Connection connection = null;
		List<String> years = new ArrayList<>();
		try {
			connection = DataSource.getConnection();
			try (PreparedStatement prepareStatement = connection.prepareStatement(SEARCH_YEARS)) {
				ResultSet rs = prepareStatement.executeQuery();
				while (rs.next()) {
					years.add(rs.getString(1));
				}
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				DataSource.returnConnection(connection);
			}
		}
		return years;
	}

	public List<Film> getFilmsWithOptions(String query) {
		Connection connection = null;
		List<Film> films = null;
		try {
			connection = DataSource.getConnection();
			try (PreparedStatement prepareStatement = connection.prepareStatement(query)) {
				ResultSet rs = prepareStatement.executeQuery();
				Object transformToList = GeneralTransformer.getInstance().transformToList(rs, Film.class);
				films = (List<Film>) transformToList;
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				DataSource.returnConnection(connection);
			}
		}
		return films;
	}

	public List<User> getUsersWithOptions(String query) {
		Connection connection = null;
		List<User> users = null;
		try {
			connection = DataSource.getConnection();
			try (PreparedStatement prepareStatement = connection.prepareStatement(query)) {
				ResultSet rs = prepareStatement.executeQuery();
				Object transformToList = GeneralTransformer.getInstance().transformToList(rs, User.class);
				users = (List<User>) transformToList;
				rs.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (connection != null) {
				DataSource.returnConnection(connection);
			}
		}
		return users;
	}
}
