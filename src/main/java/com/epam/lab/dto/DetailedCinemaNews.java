package com.epam.lab.dto;

import com.epam.lab.model.Cinema;
import com.epam.lab.model.CinemaNews;
import com.epam.lab.model.User;

public class DetailedCinemaNews implements Comparable<DetailedCinemaNews> {
	private CinemaNews mainInfo;
	private User user;
	private Cinema cinema;
	
	public DetailedCinemaNews(CinemaNews mainInfo, User user, Cinema cinema) {
		super();
		this.mainInfo = mainInfo;
		this.user = user;
		this.cinema = cinema;
	}
	
	public CinemaNews getMainInfo() {
		return mainInfo;
	}
	public void setMainInfo(CinemaNews mainInfo) {
		this.mainInfo = mainInfo;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Cinema getCinema() {
		return cinema;
	}
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}

	@Override
	public int compareTo(DetailedCinemaNews o) {
		return mainInfo.compareTo(o.mainInfo);
	}	
}
