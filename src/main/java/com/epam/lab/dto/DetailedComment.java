package com.epam.lab.dto;

import com.epam.lab.model.Comment;
import com.epam.lab.model.Film;
import com.epam.lab.model.User;

public class DetailedComment implements Comparable<DetailedComment> {
	private Comment comment;
	private String postedTime;
	private User commentator;
	private Film film;

	public DetailedComment(Comment comment, User commentator, Film film) {
		this.comment = comment;
		this.commentator = commentator;
		this.film = film;
		setPostedTime();
	}

	public DetailedComment(Comment comment, User commentator) {
		super();
		this.comment = comment;
		this.commentator = commentator;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public User getCommentator() {
		return commentator;
	}

	public void setCommentator(User commentator) {
		this.commentator = commentator;
	}

	public Film getFilm() {
		return film;
	}

	public void setFilm(Film film) {
		this.film = film;
	}
	
	public void setPostedTime() {
		String timePosted = comment.getAddedDateTime();
		postedTime = timePosted.substring(0, timePosted.length()-2);
	}

	public String getPostedTime() {
		return postedTime;
	}

	@Override
	public int compareTo(DetailedComment o) {
		return comment.compareTo(o.getComment());
	}
}
