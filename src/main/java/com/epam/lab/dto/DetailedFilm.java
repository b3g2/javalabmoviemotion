package com.epam.lab.dto;

import java.util.List;

import com.epam.lab.model.Country;
import com.epam.lab.model.Film;
import com.epam.lab.model.Genre;

public class DetailedFilm {
	private Film film;
	private List<Genre> genres;
	private List<Country> countries;
	
	public DetailedFilm() {
		super();
	}

	public DetailedFilm(Film film, List<Genre> genres, List<Country> countries) {
		super();
		this.film = film;
		this.genres = genres;
		this.countries = countries;
	}

	public Film getFilm() {
		return film;
	}

	public void setFilm(Film film) {
		this.film = film;
	}

	public List<Genre> getGenres() {
		return genres;
	}

	public void setGenres(List<Genre> genres) {
		this.genres = genres;
	}

	public List<Country> getCountries() {
		return countries;
	}

	public void setCountries(List<Country> countries) {
		this.countries = countries;
	}	
}