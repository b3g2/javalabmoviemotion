package com.epam.lab.dto;

import com.epam.lab.model.Cinema;
import com.epam.lab.model.FilmCinema;

public class DetailedShowing {
	private FilmCinema showingPart;
	private Cinema cinemaPart;	
	public DetailedShowing() {
		super();
	}	
	public DetailedShowing(FilmCinema showingPart, Cinema cinemaPart) {
		super();
		this.showingPart = showingPart;
		this.cinemaPart = cinemaPart;
	}	
	public FilmCinema getShowingPart() {
		return showingPart;
	}
	public void setShowingPart(FilmCinema showingPart) {
		this.showingPart = showingPart;
	}
	public Cinema getCinemaPart() {
		return cinemaPart;
	}
	public void setCinemaPart(Cinema cinemaPart) {
		this.cinemaPart = cinemaPart;
	}	
}
