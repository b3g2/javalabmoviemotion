package com.epam.lab.dto;

import com.epam.lab.model.User;

public class DetailedWatcher {
	private User user;
	private Integer watchedFilms;
	private String genre;
	
	public DetailedWatcher() {
		super();
	}
	
	public DetailedWatcher(User user, Integer watchedFilms, String genre) {
		super();
		this.user = user;
		this.watchedFilms = watchedFilms;
		this.genre = genre;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((user == null) ? 0 : user.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		DetailedWatcher other = (DetailedWatcher) obj;
		if (user == null) {
			if (other.user != null)
				return false;
		} else if (!user.equals(other.user))
			return false;
		return true;
	}

	public User getUser() {
		return user;
	}	
	public void setUser(User user) {
		this.user = user;
	}
	public Integer getWatchedFilms() {
		return watchedFilms;
	}
	public void setWatchedFilms(Integer watchedFilms) {
		this.watchedFilms = watchedFilms;
	}
	public String getGenre() {
		return genre;
	}
	public void setGenre(String genre) {
		this.genre = genre;
	}	
}
