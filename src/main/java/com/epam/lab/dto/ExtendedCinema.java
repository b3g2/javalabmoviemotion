package com.epam.lab.dto;

import com.epam.lab.model.Cinema;

public class ExtendedCinema {
	private Cinema cinema;
	private int total;
	private int delete;
	public int getDelete() {
		return delete;
	}
	public void setDelete(int delete) {
		this.delete = delete;
	}
	public Cinema getCinema() {
		return cinema;
	}
	public void setCinema(Cinema cinema) {
		this.cinema = cinema;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
    public ExtendedCinema()
    {
    	
    }
}
