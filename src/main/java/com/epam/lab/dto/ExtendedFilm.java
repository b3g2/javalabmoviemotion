package com.epam.lab.dto;

import com.epam.lab.model.Film;

public class ExtendedFilm {
	private Film film;
	private String dateOfStart;
	private String dateOfFinish;
	private double minPrice;
	private double maxPrice;
	private String state;

	public ExtendedFilm() {
	}

	public ExtendedFilm(Film film, String dateOfStart, String dateOfFinish, double minPrice, double maxPrice,
			String state) {
		this.film = film;
		this.dateOfStart = dateOfStart;
		this.dateOfFinish = dateOfFinish;
		this.minPrice = minPrice;
		this.maxPrice = maxPrice;
		this.state = state;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public Film getFilm() {
		return film;
	}

	public double getMinPrice() {
		return minPrice;
	}

	public void setMinPrice(double minPrice) {
		this.minPrice = minPrice;
	}

	public double getMaxPrice() {
		return maxPrice;
	}

	public void setMaxPrice(double maxPrice) {
		this.maxPrice = maxPrice;
	}

	public void setFilm(Film film) {
		this.film = film;
	}

	public String getDateOfStart() {
		return dateOfStart;
	}

	public void setDateOfStart(String dateOfStart) {
		this.dateOfStart = dateOfStart;
	}

	public String getDateOfFinish() {
		return dateOfFinish;
	}

	public void setDateOfFinish(String dateOfFinish) {
		this.dateOfFinish = dateOfFinish;
	}
}
