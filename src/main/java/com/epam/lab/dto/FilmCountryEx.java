package com.epam.lab.dto;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;

@TableName("film_country")
public class FilmCountryEx {

    private static final long serialVersionUID = 1L;

    @PrimaryKey
    @NotNull
    @ColumnName("id")
    private Integer id;
    
    @NotNull
    @ColumnName("film_id")
    private Integer filmId;

    @NotNull
    @ColumnName("country_id")
    private Integer countryId;

    private String countryName;

    public FilmCountryEx() {
    }

    public FilmCountryEx(Integer filmId, Integer countryId) {
        this.filmId = filmId;
        this.countryId = countryId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getCountryId() {
        return countryId;
    }

    public void setCountryId(Integer countryId) {
        this.countryId = countryId;
    }

    public Integer getFilmId() {
        return filmId;
    }

    public void setFilmId(Integer filmId) {
        this.filmId = filmId;
    }

    public String getCountryName() {
        return countryName;
    }

    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }
}
