package com.epam.lab.dto;

import com.epam.lab.model.Film;
import com.epam.lab.model.FilmUser;

public class FilmWithCurrentUser {
    private Film film;
    private FilmUser filmUser;

    public FilmWithCurrentUser() {
    }

    public FilmWithCurrentUser(Film film, FilmUser filmUser) {
        this.film = film;
        this.filmUser = filmUser;
    }

    public Film getFilm() {
        return film;
    }

    public void setFilm(Film film) {
        this.film = film;
    }

    public FilmUser getFilmUser() {
        return filmUser;
    }

    public void setFilmUser(FilmUser filmUser) {
        this.filmUser = filmUser;
    }
}
