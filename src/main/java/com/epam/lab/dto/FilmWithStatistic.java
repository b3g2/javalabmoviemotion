package com.epam.lab.dto;

import com.epam.lab.model.Comment;
import com.epam.lab.model.Film;

public class FilmWithStatistic {
	private Film film;
	private Comment comment;
	private Integer sheduledCount;
	private Integer favouriteCount;
	private Integer commentCount;

	public FilmWithStatistic() {
	}

	public FilmWithStatistic(Film film, Integer sheduledCount, Integer favouriteCount, Integer commentCount) {
		this.film = film;
		this.sheduledCount = sheduledCount;
		this.favouriteCount = favouriteCount;
		this.commentCount = commentCount;
	}

	public Film getFilm() {
		return film;
	}

	public void setFilm(Film film) {
		this.film = film;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public Integer getSheduledCount() {
		return sheduledCount;
	}

	public void setSheduledCount(Integer sheduledCount) {
		this.sheduledCount = sheduledCount;
	}

	public Integer getFavouriteCount() {
		return favouriteCount;
	}

	public void setFavouriteCount(Integer favouriteCount) {
		this.favouriteCount = favouriteCount;
	}
	
	public Integer getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}
}
