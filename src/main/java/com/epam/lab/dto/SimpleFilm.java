package com.epam.lab.dto;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;

import java.io.Serializable;

@TableName("film")
public class SimpleFilm implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKey
    @NotNull
    @ColumnName("id")
    private Integer id;

    @NotNull
    @ColumnName("imdb_id")
    private String imdbId;

    public SimpleFilm() {
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getImdbId() {
        return imdbId;
    }

    public void setImdbId(String imdbId) {
        this.imdbId = imdbId;
    }
}