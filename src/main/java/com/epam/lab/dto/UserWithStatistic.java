package com.epam.lab.dto;

import com.epam.lab.model.Comment;
import com.epam.lab.model.User;

public class UserWithStatistic {
	private User user;
	private Comment comment;
	private Integer scheduledCount;
	private Integer likeCount;
	private Integer dislikeCount;
	private Integer favouriteCount;
	private Integer commentCount;
	private Integer daysWatchedCount;
	private Integer hoursWatchedCount;
	private Integer minsWatchedCount;

	public UserWithStatistic() {
	}

	public UserWithStatistic(User user) {
		this.user = user;
		setStatistic();
	}

	public UserWithStatistic(User user, Integer sheduledCount, Integer likeCount, Integer dislikeCount,
			Integer favouriteCount, Integer commentCount) {
		this.user = user;
		this.scheduledCount = sheduledCount;
		this.likeCount = likeCount;
		this.dislikeCount = dislikeCount;
		this.favouriteCount = favouriteCount;
		this.commentCount = commentCount;
		setStatistic();
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Comment getComment() {
		return comment;
	}

	public void setComment(Comment comment) {
		this.comment = comment;
	}

	public Integer getSheduledCount() {
		return scheduledCount;
	}

	public void setSheduledCount(Integer sheduledCount) {
		this.scheduledCount = sheduledCount;
	}

	public Integer getLikeCount() {
		return likeCount;
	}

	public void setLikeCount(Integer likeCount) {
		this.likeCount = likeCount;
	}

	public Integer getDislikeCount() {
		return dislikeCount;
	}

	public void setDislikeCount(Integer dislikeCount) {
		this.dislikeCount = dislikeCount;
	}

	public Integer getFavouriteCount() {
		return favouriteCount;
	}

	public void setFavouriteCount(Integer favouriteCount) {
		this.favouriteCount = favouriteCount;
	}

	public Integer getCommentCount() {
		return commentCount;
	}

	public void setCommentCount(Integer commentCount) {
		this.commentCount = commentCount;
	}

	private void setStatistic() {
		setDaysWatchedCount();
		setHoursWatchedCount();
		setMinsWatchedCount();
	}

	public void setDaysWatchedCount() {
		this.daysWatchedCount = (this.user.getDuration() / 60) / 24;
	}

	public Integer getDaysWatchedCount() {
		return daysWatchedCount;
	}

	public void setHoursWatchedCount() {
		this.hoursWatchedCount = this.user.getDuration() / 60 - getDaysWatchedCount() * 24;
	}

	public Integer getHoursWatchedCount() {
		return hoursWatchedCount;
	}

	public void setMinsWatchedCount() {
		this.minsWatchedCount = this.user.getDuration() - (getDaysWatchedCount() * 24 + getHoursWatchedCount()) * 60;
	}

	public Integer getMinsWatchedCount() {
		return minsWatchedCount;
	}
}
