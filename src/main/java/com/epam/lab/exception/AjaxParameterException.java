package com.epam.lab.exception;

public class AjaxParameterException extends MovieMotionRuntimeException {
    public AjaxParameterException() {
    }

    public AjaxParameterException(String message) {
        super(message);
    }

    public AjaxParameterException(String message, Throwable cause) {
        super(message, cause);
    }

    public AjaxParameterException(Throwable cause) {
        super(cause);
    }

    public AjaxParameterException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
