package com.epam.lab.exception;

public class CloneObjectException extends MovieMotionRuntimeException {
    public CloneObjectException() {
    }

    public CloneObjectException(String message) {
        super(message);
    }

    public CloneObjectException(String message, Throwable cause) {
        super(message, cause);
    }

    public CloneObjectException(Throwable cause) {
        super(cause);
    }

    public CloneObjectException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
