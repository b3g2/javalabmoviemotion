package com.epam.lab.exception;

public class MovieMotionRuntimeException extends RuntimeException {
    public MovieMotionRuntimeException() {
    }

    public MovieMotionRuntimeException(String message) {
        super(message);
    }

    public MovieMotionRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }

    public MovieMotionRuntimeException(Throwable cause) {
        super(cause);
    }

    public MovieMotionRuntimeException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
