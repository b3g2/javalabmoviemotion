package com.epam.lab.factory;

import com.epam.lab.service.DataService;
import com.epam.lab.service.EmailService;
import com.epam.lab.service.UserService;

public class ServiceFactory {
    public static UserService getUserService() {
        return new UserService();
    }

    public static DataService getDataService() {
        return new DataService();
    }

    public static EmailService getEmailService() {
        return new EmailService();
    }
}
