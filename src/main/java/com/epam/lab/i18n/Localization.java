package com.epam.lab.i18n;

import java.io.UnsupportedEncodingException;
import java.util.Locale;
import java.util.ResourceBundle;

public class Localization {
    private ResourceBundle resourceBundle;

    public Localization(String name, String lang) {
        resourceBundle = ResourceBundle.getBundle(name, new Locale.Builder().setLanguage(lang).build());
    }

    public String get(String key) {
        String s = null;
        try {
            s = new String(resourceBundle.getString(key).getBytes("ISO-8859-1"), "UTF-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return s;
    }

    public String getLanguage() {
        return resourceBundle.getLocale().getLanguage();
    }
}
