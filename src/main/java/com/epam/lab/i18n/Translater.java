package com.epam.lab.i18n;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.model.Country;
import com.epam.lab.model.Genre;
import com.memetix.mst.language.Language;
import com.memetix.mst.translate.Translate;
import org.apache.log4j.Logger;

import java.util.List;

public class Translater {

	private GeneralDAOService daoService = GeneralDAOService.getInstance();
	private static final Logger LOG = Logger.getLogger(Translater.class);

	public Translater() {
		
	}

	public static String translate(String s) {
		String s_uk = null;
		Translate.setClientId("MovieMotion");
		Translate.setClientSecret("sSdgJZABgomyzrsUiwRQlxHnOPaCbVuLlVm2FMM5wdU=");
		try {
			s_uk = Translate.execute(s, Language.ENGLISH, Language.UKRAINIAN);
		} catch (Exception e) {
			LOG.error("Problems of translation in Translater.class" + e);
		}
		s_uk = s_uk.replaceAll("[\\'\\`]", "\\\\'");
		
		return s_uk;
	}

	public void TranslateGenre() {

		String name_uk = null;
		List<Genre> genres = daoService.getAll(Genre.class);

		for (Genre genre : genres) {
			try {
				name_uk = Translate.execute(genre.getName(), Language.ENGLISH, Language.UKRAINIAN);
			} catch (Exception e) {
				e.printStackTrace();
			}
			GeneralDAOService.getInstance().update("id=" + genre.getId(), "name_uk = '" + name_uk + "'", Genre.class);
		}
	}

	public void TranslateCountry() {

		String name_uk = null;
		List<Country> countries = daoService.getAll(Country.class);

		for (Country country : countries) {
			try {
				name_uk = Translate.execute(country.getName(), Language.ENGLISH, Language.UKRAINIAN);
			} catch (Exception e) {
				e.printStackTrace();
			}
			GeneralDAOService.getInstance().update("id=" + country.getId(), "name_uk = '" + name_uk + "'",
					Country.class);
		}
	}
}
