package com.epam.lab.model;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;

import java.io.Serializable;

@TableName("cinema")
public class Cinema implements Serializable, Comparable<Cinema> {

	private static final long serialVersionUID = 1L;

	@PrimaryKey
	@NotNull
	@ColumnName("id")
	private Integer id;
	@NotNull
	@ColumnName("name")
	private String cinemaName;
	@NotNull
	@ColumnName("city")
	private String city;
	@NotNull
	@ColumnName("address")
	private String address;
	@NotNull
	@ColumnName("country")
	private String country;
	@NotNull
	@ColumnName("state")
	private String state;
	@ColumnName("site")
	private String site;

	public Cinema() {
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCinemaName() {
		return cinemaName;
	}

	public void setCinemaName(String cinemaName) {
		this.cinemaName = cinemaName;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}
	
	public String getSite() {
		return site;
	}

	public void setSite(String site) {
		this.site = site;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	@Override
	public int compareTo(Cinema o) {
		return city.compareTo(o.city);
	}
}
