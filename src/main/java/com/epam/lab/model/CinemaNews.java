package com.epam.lab.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;
import com.epam.lab.util.ServerUtil;

@TableName("cinema_news")
public class CinemaNews implements Comparable<CinemaNews> {
	@PrimaryKey
	@NotNull
	@ColumnName("id")
	private Integer id;
	@NotNull
	@ColumnName("user_id")
	private Integer userId;
	@NotNull
	@ColumnName("cinema_id")
	private Integer cinemaId;
	@NotNull
	@ColumnName("news")
	private String content;
	@NotNull
	@ColumnName("added_date_time")
	private String addedDateTime;
	@ColumnName("header")
	private String header;
	@ColumnName("picture_url")
	private String pictureURL;

	public String getPictureURL() {
		return pictureURL;
	}

	public void setPictureURL(String pictureURL) {
		this.pictureURL = pictureURL;
	}

	public String getHeader() {
		return header;
	}

	public void setHeader(String header) {
		this.header = header;
	}

	public CinemaNews() {
	}

	public CinemaNews(Integer userId, Integer cinemaId, String content, String header, String pictureURL) {
		super();
		this.userId = userId;
		this.cinemaId = cinemaId;
		this.content = content;
		this.addedDateTime = LocalDateTime.now().format(ServerUtil.DATE_TIME_PATTERN);
		this.header = header;
		this.pictureURL = pictureURL;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getCinemaId() {
		return cinemaId;
	}

	public void setCinemaId(Integer cinemaId) {
		this.cinemaId = cinemaId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		int gotLength = addedDateTime.length();
		if (addedDateTime.charAt(gotLength - 2) == '.') {
			addedDateTime = addedDateTime.substring(0, gotLength - 5);
		}
		this.addedDateTime = addedDateTime;
	}

	@Override
	public int compareTo(CinemaNews o) {
		return o.addedDateTime.compareTo(addedDateTime);
	}

}
