package com.epam.lab.model;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;

import java.io.Serializable;

@TableName("cinema_worker")
public class CinemaWorker extends User implements Serializable {

	private static final long serialVersionUID = 1L;

	@PrimaryKey
	@NotNull
	@ColumnName("id")
	private Integer cinemaWorkerId;
	@NotNull
	@ColumnName("phone")
	private String phone;
	@NotNull
	@ColumnName("cinema_id")
	private Integer cinemaId;
	@NotNull
	@ColumnName("user_id")
	private Integer user_id;

	public CinemaWorker() {
	}

	public Integer getCinemaWorkerId() {
		return cinemaWorkerId;
	}

	public void setCinemaWorkerId(Integer cinemaWorkerId) {
		this.cinemaWorkerId = cinemaWorkerId;
	}

	public String getPhone() {
		return phone;
	}

	public void setPhone(String phone) {
		this.phone = phone;
	}

	public Integer getCinemaId() {
		return cinemaId;
	}

	public void setCinemaId(Integer cinemaId) {
		this.cinemaId = cinemaId;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}
}
