package com.epam.lab.model;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;
import com.epam.lab.util.ServerUtil;

import java.io.Serializable;
import java.time.LocalDateTime;

@TableName("comment")
public class Comment implements Serializable, Comparable<Comment> {

	private static final long serialVersionUID = 1L;

	@PrimaryKey
	@NotNull
	@ColumnName("id")
	private Integer id;
	@NotNull
	@ColumnName("film_id")
	private Integer filmId;
	@NotNull
	@ColumnName("user_id")
	private Integer userId;
	@NotNull
	@ColumnName("content")
	private String content;
	@NotNull
	@ColumnName("added_date_time")
	private String addedDateTime;
	
	public Comment() {
	}
	
	public Comment(Integer userId, Integer filmId, String content) {
		super();
		this.filmId = filmId;
		this.userId = userId;
		this.content = content;
		this.addedDateTime = LocalDateTime.now().format(ServerUtil.DATE_TIME_PATTERN);
	}

	public String getAddedDateTime() {
		return addedDateTime;
	}

	public void setAddedDateTime(String addedDateTime) {
		this.addedDateTime = addedDateTime;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFilmId() {
		return filmId;
	}

	public void setFilmId(Integer filmId) {
		this.filmId = filmId;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	@Override
	public int compareTo(Comment o) {
		return o.addedDateTime.compareTo(addedDateTime);
	}
}