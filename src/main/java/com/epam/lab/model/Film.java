package com.epam.lab.model;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;

import java.io.Serializable;

@TableName("film")
public class Film implements Serializable {

	private static final long serialVersionUID = 1L;

	@PrimaryKey
	@NotNull
	@ColumnName("id")
	private Integer id;
	
	@NotNull
	@ColumnName("title")
	private String title;
	
	@ColumnName("title_uk")
	private String titleUk;
	
	@ColumnName("year")
	private Integer year;
	
	@ColumnName("released")
	private String released;
	
	@ColumnName("runtime")
	private Integer runtime;
	
	@ColumnName("director")
	private String director;
	
	@ColumnName("writer")
	private String writer;
	
	@ColumnName("actors")
	private String actors;
	
	@ColumnName("plot")
	private String plot;
	
	@ColumnName("plot_uk")
	private String plotUk;
	
	@ColumnName("rating_imdb")
	private Double ratingImdb;
	
	@ColumnName("rating_custom")
	private Double ratingCustom;
	
	@NotNull
	@ColumnName("imdb_id")
	private String imdbId;
	
	@ColumnName("poster")
	private String poster;
	
	@NotNull
	@ColumnName("reviews")
	private Integer reviews;
	
	@NotNull
	@ColumnName("likes")
	private Integer likes;
	
	@NotNull
	@ColumnName("dislikes")
	private Integer dislikes;
	
	@NotNull
	@ColumnName("verified")
	private Boolean verified;

	public Film() {
	}

	public Film(String title, Integer year, String released, Integer runtime, String director, String writer, String actors, String plot, Double ratingImdb, String imdbId, String poster) {
		this.title = title;
		this.year = year;
		this.released = released;
		this.runtime = runtime;
		this.director = director;
		this.writer = writer;
		this.actors = actors;
		this.plot = plot;
		this.ratingImdb = ratingImdb;
		this.imdbId = imdbId;
		this.poster = poster;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleUk() {
		return titleUk;
	}

	public void setTitleUk(String titleUk) {
		this.titleUk = titleUk;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getReleased() {
		return released;
	}

	public void setReleased(String released) {
		this.released = released;
	}

	public Integer getRuntime() {
		return runtime;
	}

	public void setRuntime(Integer runtime) {
		this.runtime = runtime;
	}

	public String getDirector() {
		return director;
	}

	public void setDirector(String director) {
		this.director = director;
	}

	public String getWriter() {
		return writer;
	}

	public void setWriter(String writer) {
		this.writer = writer;
	}

	public String getActors() {
		return actors;
	}

	public void setActors(String actors) {
		this.actors = actors;
	}

	public String getPlot() {
		return plot;
	}

	public void setPlot(String plot) {
		this.plot = plot;
	}

	public String getPlotUk() {
		return plotUk;
	}

	public void setPlotUk(String plotUk) {
		this.plotUk = plotUk;
	}

	public Double getRatingImdb() {
		return ratingImdb;
	}

	public void setRatingImdb(Double ratingImdb) {
		this.ratingImdb = ratingImdb;
	}

	public Double getRatingCustom() {
		return ratingCustom;
	}

	public void setRatingCustom(Double ratingCustom) {
		this.ratingCustom = ratingCustom;
	}

	public String getImdbId() {
		return imdbId;
	}

	public void setImdbId(String imdbId) {
		this.imdbId = imdbId;
	}

	public String getPoster() {
		return poster;
	}

	public void setPoster(String poster) {
		this.poster = poster;
	}

	public Integer getReviews() {
		return reviews;
	}

	public void setReviews(Integer reviews) {
		this.reviews = reviews;
	}

	public void setVerified(Boolean verified) {
		this.verified = verified;
	}

    public Boolean getVerified() {
        return verified;
    }

    public Integer getLikes() {
        return likes;
    }

    public void setLikes(Integer likes) {
        this.likes = likes;
    }

    public Integer getDislikes() {
        return dislikes;
    }

    public void setDislikes(Integer dislikes) {
        this.dislikes = dislikes;
    }
    
    

	@Override
	public String toString() {
		return "Film [id=" + id + ", title=" + title + ", year=" + year +"]\n";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((imdbId == null) ? 0 : imdbId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Film other = (Film) obj;
		if (imdbId == null) {
			if (other.imdbId != null)
				return false;
		} else if (!imdbId.equals(other.imdbId))
			return false;
		return true;
	}
    
}
