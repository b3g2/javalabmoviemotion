package com.epam.lab.model;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;

@TableName("film_genre")
public class FilmGenre {

    private static final long serialVersionUID = 1L;

    @PrimaryKey
    @NotNull
    @ColumnName("id")
    private Integer id;

    @NotNull
    @ColumnName("film_id")
    private Integer filmId;

    @NotNull
    @ColumnName("genre_id")
    private Integer genreId;

    public FilmGenre() {
    }

    public FilmGenre(Integer filmId, Integer genreId) {
        this.filmId = filmId;
        this.genreId = genreId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getGenreId() {
        return genreId;
    }

    public void setGenreId(Integer genreId) {
        this.genreId = genreId;
    }

    public Integer getFilmId() {
        return filmId;
    }

    public void setFilmId(Integer filmId) {
        this.filmId = filmId;
    }
}
