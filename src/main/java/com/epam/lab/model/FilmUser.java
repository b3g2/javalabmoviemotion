package com.epam.lab.model;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;

import java.io.Serializable;

@TableName("film_user")
public class FilmUser implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKey
    @NotNull
    @ColumnName("id")
    private Integer id;
    @NotNull
    @ColumnName("film_id")
    private Integer filmId;
    @NotNull
    @ColumnName("user_id")
    private Integer userId;
    @NotNull
    @ColumnName("status")
    private String status;
    @NotNull
    @ColumnName("favourite")
    private Boolean favourite;
    @NotNull
    @ColumnName("like_value")
    private String likeValue;

    public FilmUser() {
    }
    
    public FilmUser(Integer filmId, Integer userId) {
		super();
		this.filmId = filmId;
		this.userId = userId;
	}

	public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFilmId() {
        return filmId;
    }

    public void setFilmId(Integer filmId) {
        this.filmId = filmId;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public Boolean getFavourite() {
        return favourite;
    }

    public void setFavourite(Boolean favourite) {
        this.favourite = favourite;
    }

	public String getLikeValue() {
		return likeValue;
	}

	public void setLikeValue(String likeValue) {
		this.likeValue = likeValue;
	}

}
