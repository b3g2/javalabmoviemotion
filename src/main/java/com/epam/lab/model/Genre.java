package com.epam.lab.model;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;

import java.io.Serializable;

@TableName("genre")
public class Genre implements Serializable {

    private static final long serialVersionUID = 1L;

    @PrimaryKey
    @NotNull
    @ColumnName("id")
    private Integer id;

    @NotNull
    @ColumnName("name")
    private String name;

    @ColumnName("name_uk")
    private String nameUK;

    public Genre() {
    }

    public Genre(String name) {
        this.name = name;
    }

    public Genre(String name, String nameUK) {
        this.name = name;
        this.nameUK = nameUK;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNameUK() {
        return nameUK;
    }

    public void setNameUK(String nameUa) {
        this.nameUK = nameUa;
    }

    public String getName() {
        return name;
    }

    public void setName(String nameEa) {
        this.name = nameEa;
    }

	@Override
	public String toString() {
		return id.toString();
	}

}
