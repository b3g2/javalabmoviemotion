package com.epam.lab.model;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;

@TableName("mailing")
public class Mailing {
    @PrimaryKey
    @NotNull
    @ColumnName("id")
    private Integer id;
    @NotNull
    @ColumnName("email")
    private String email;

    public Mailing() {
    }

    public Mailing(String email) {
        this.email = email;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
