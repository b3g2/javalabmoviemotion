package com.epam.lab.model;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;

import java.io.Serializable;

@TableName("statistic")
public class Statistic implements Serializable {

	private static final long serialVersionUID = -6835367888405560184L;

	@PrimaryKey
	@NotNull
	@ColumnName("id")
	private Integer id;
	@NotNull
	@ColumnName("user_id")
	private Integer userId;
	@NotNull
	@ColumnName("edited_items")
	private Integer editedItems;
	@NotNull
	@ColumnName("added_premieres")
	private Integer addedPremieres;
	@NotNull
	@ColumnName("ban_count")
	private Integer banCount;
	@NotNull
	@ColumnName("unban_count")
	private Integer unbanCount;
	@NotNull
	@ColumnName("added_moderators")
	private Integer addedModerators;
	@NotNull
	@ColumnName("added_admins")
	private Integer addedAdmins;
	@NotNull
	@ColumnName("removed_powers")
	private Integer removedPowers;
	@NotNull
	@ColumnName("added_showing")
	private Integer addedShowing;
	@NotNull
	@ColumnName("edited_showing")
	private Integer editedShowing;

	public Statistic() {
	}

	public Statistic(Integer userId) {
		this.userId = userId;
	}

	public void incEditedItems() {
		editedItems++;
	}

	public void incAddedPremieres() {
		addedPremieres++;
	}

	public void incAddedShowing() {
		addedShowing++;
	}

	public void incEditedShowing() {
		editedShowing++;
	}

	public void incBanCount() {
		banCount++;
	}

	public void incUnbanCount() {
		unbanCount++;
	}

	public void incAddedModerators() {
		addedModerators++;
	}

	public void incAddedAdmins() {
		addedAdmins++;
	}

	public void incRemovedPowers() {
		removedPowers++;
	}

	public Integer getUserId() {
		return userId;
	}

	public void setUserId(Integer userId) {
		this.userId = userId;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEditedItems() {
		return editedItems;
	}

	public void setEditedItems(Integer editedItems) {
		this.editedItems = editedItems;
	}

	public Integer getAddedPremieres() {
		return addedPremieres;
	}

	public void setAddedPremieres(Integer addedPremieres) {
		this.addedPremieres = addedPremieres;
	}

	public Integer getBanCount() {
		return banCount;
	}

	public void setBanCount(Integer banCount) {
		this.banCount = banCount;
	}

	public Integer getUnbanCount() {
		return unbanCount;
	}

	public void setUnbanCount(Integer unbanCount) {
		this.unbanCount = unbanCount;
	}

	public Integer getAddedModerators() {
		return addedModerators;
	}

	public void setAddedModerators(Integer addedModerators) {
		this.addedModerators = addedModerators;
	}

	public Integer getAddedAdmins() {
		return addedAdmins;
	}

	public void setAddedAdmins(Integer addedAdmins) {
		this.addedAdmins = addedAdmins;
	}

	public Integer getRemovedPowers() {
		return removedPowers;
	}

	public void setRemovedPowers(Integer removedPowers) {
		this.removedPowers = removedPowers;
	}

	public Integer getAddedShowing() {
		return addedShowing;
	}

	public void setAddedShowing(Integer addedShowing) {
		this.addedShowing = addedShowing;
	}

	public Integer getEditedShowing() {
		return editedShowing;
	}

	public void setEditedShowing(Integer editedShowing) {
		this.editedShowing = editedShowing;
	}
}
