package com.epam.lab.model;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.NotNull;
import com.epam.lab.annotation.PrimaryKey;
import com.epam.lab.annotation.TableName;

import java.io.Serializable;

@TableName("user")
public class User implements Serializable {

	private static final long serialVersionUID = 1L;

	@PrimaryKey
	@NotNull
	@ColumnName("id")
	private Integer id;

	@NotNull
	@ColumnName("fname")
	private String firstName;

	@NotNull
	@ColumnName("lname")
	private String lastName;

	@NotNull
	@ColumnName("role")
	private String role;

	@NotNull
	@ColumnName("duration")
	private Integer duration;

	@NotNull
	@ColumnName("email")
	private String email;

	@ColumnName("password")
	private String password;

	@ColumnName("photo_url")
	private String photoURL;

	@NotNull
	@ColumnName("language")
	private String language;

	@ColumnName("session_id")
	private Integer sessionId;

	@ColumnName("temp_pass")
	private String tempPassword;

	@ColumnName("temp_pass_expiry")
	private String tempPasswordExpiry;

	@NotNull
	@ColumnName("ban")
	private Boolean ban;

	@ColumnName("access_level")
	private Integer accessLevel;

	@NotNull
	@ColumnName("mailing")
	private Boolean mailing;

	public User() {
	}

	public User(String firstName, String lastName, String email) {
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getRole() {
		return role;
	}

	public void setRole(String role) {
		this.role = role;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getPhotoURL() {
		return photoURL;
	}

	public void setPhotoURL(String photoURL) {
		this.photoURL = photoURL;
	}

	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	public Integer getSessionId() {
		return sessionId;
	}

	public void setSessionId(Integer sessionId) {
		this.sessionId = sessionId;
	}

	public String getTempPassword() {
		return tempPassword;
	}

	public void setTempPassword(String tempPassword) {
		this.tempPassword = tempPassword;
	}

	public String getTempPasswordExpiry() {
		return tempPasswordExpiry;
	}

	public void setTempPasswordExpiry(String tempPasswordExpiry) {
		this.tempPasswordExpiry = tempPasswordExpiry;
	}

	public Boolean getBan() {
		return ban;
	}

	public void setBan(Boolean ban) {
		this.ban = ban;
	}

	public Integer getAccessLevel() {
		return accessLevel;
	}

	public void setAccessLevel(Integer accessLevel) {
		this.accessLevel = accessLevel;
	}

	public Boolean getMailing() {
		return mailing;
	}

	public void setMailing(Boolean mailing) {
		this.mailing = mailing;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((email == null) ? 0 : email.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		User other = (User) obj;
		if (email == null) {
			if (other.email != null)
				return false;
		} else if (!email.equals(other.email))
			return false;
		return true;
	}
}
