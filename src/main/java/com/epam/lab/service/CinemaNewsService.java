package com.epam.lab.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dao.GeneralDAOService.JoinResultMap;
import com.epam.lab.dto.DetailedCinemaNews;
import com.epam.lab.model.Cinema;
import com.epam.lab.model.CinemaNews;
import com.epam.lab.model.User;
import com.google.common.base.Strings;

public class CinemaNewsService {
	private static GeneralDAOService generalDAOService;
	public static final Integer NEWS_PER_PAGE = 12;
	static{
		generalDAOService = GeneralDAOService.getInstance();
	}

	public Integer addNews(Integer userId, Integer cinemaId, String content, String header, String pictureURL) {
        int addedAmount = generalDAOService.add(new CinemaNews(userId, cinemaId, content, header, pictureURL));
        return addedAmount;        
	}

	public List<DetailedCinemaNews> getNewsByCityAndCinemaName(String city, String name, Integer page) {
		String clause=formNewsExtractingClauseByCityAndName(city, name, page);
		JoinResultMap detailedData = generalDAOService.getJoin(clause,User.class,"user.id=cinema_news.user_id",CinemaNews.class,"cinema.id=cinema_news.cinema_id",Cinema.class);
		List<User> newsMakers = detailedData.get(User.class);
		List<Cinema> cinemas = detailedData.get(Cinema.class);
		List<CinemaNews> cinemaNews = detailedData.get(CinemaNews.class);
	
		List<DetailedCinemaNews> news = new ArrayList<>();		
		for(int i=0;i<cinemaNews.size();++i){
			news.add(new DetailedCinemaNews(cinemaNews.get(i), newsMakers.get(i), cinemas.get(i)));
		}
		Collections.sort(news);
        return news;
	}

	public List<DetailedCinemaNews> getNews(Integer userId, Integer cinemaId, Integer page) {
		String clause=formNewsExtractingClause(userId, cinemaId, page);
		List<CinemaNews> cinemaNews = generalDAOService.getList(clause, CinemaNews.class);
		List<DetailedCinemaNews> detailedNews = new ArrayList<>();
		for(CinemaNews news:cinemaNews){
			Cinema cinema = generalDAOService.get("id="+news.getCinemaId(), Cinema.class);
			User user = generalDAOService.get("id="+news.getUserId(), User.class);
			detailedNews.add(new DetailedCinemaNews(news, user, cinema));
		}

		Collections.sort(detailedNews);
        return detailedNews;    
        
//        String clause=formNewsExtractingClause(userId, cinemaId, page);
//		JoinResultMap detailedData = generalDAOService.getJoin(clause,User.class,"user.id=cinema_news.user_id",CinemaNews.class,"cinema.id=cinema_news.cinema_id",Cinema.class);
//		
//		List<User> newsMakers = detailedData.get(User.class);
//		List<Cinema> cinemas = detailedData.get(Cinema.class);
//		List<CinemaNews> cinemaNews = detailedData.get(CinemaNews.class);
//	
//		List<DetailedCinemaNews> news = new ArrayList<>();		
//		for(int i=0;i<cinemaNews.size();++i){
//			news.add(new DetailedCinemaNews(cinemaNews.get(i), newsMakers.get(i), cinemas.get(i)));
//		}
//		Collections.sort(news);
//        return news;
	}
	
	private String formNewsExtractingClauseByCityAndName(String city, String cinemaName, Integer page){
		String clause="";
		if(city!=null){
			clause+=" cinema.city='"+city+"'";
		}
		if(city!=null&&cinemaName!=null){
			clause+=" AND";
		}
		if(cinemaName!=null){
			clause+=" cinema.name='"+cinemaName+"'";
		}
		
		if("".equals(clause)){
			clause+=" cinema_news.id>0";
		}
		clause+=" ORDER BY cinema_news.added_date_time DESC ";
		
		
		if(page!=null){
			clause+="LIMIT "+((page-1)*NEWS_PER_PAGE)+","+(NEWS_PER_PAGE+1);
		}else{
			clause+="LIMIT 2";
		}
		return clause;
	}
	
	private String formNewsExtractingClause(Integer userId, Integer cinemaId, Integer page){
		String clause="";
		if(userId!=null){
			clause+="cinema_news.user_id="+userId;
		}
		if(userId!=null&&cinemaId!=null){
			clause+=" AND ";
		}
		if(cinemaId!=null){
			clause+="cinema_news.cinema_id="+cinemaId;
		}
		
		if("".equals(clause)){
			clause+="cinema_news.id>0";
		}
		clause+=" ORDER BY cinema_news.added_date_time DESC ";
		
		
		if(page!=null){
			clause+=" LIMIT "+((page-1)*NEWS_PER_PAGE)+","+(NEWS_PER_PAGE+1);
		}else{
			clause+=" LIMIT 2";
		}
		return clause;
	}
}