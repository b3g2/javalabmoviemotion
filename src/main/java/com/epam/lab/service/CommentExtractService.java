package com.epam.lab.service;

import java.util.ArrayList;
import java.util.List;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dao.GeneralDAOService.JoinResultMap;
import com.epam.lab.dto.DetailedComment;
import com.epam.lab.model.Comment;
import com.epam.lab.model.User;

public class CommentExtractService {
	private static GeneralDAOService generalDAOService;
	static{
		generalDAOService = GeneralDAOService.getInstance();
	}

	public List<DetailedComment> getFilmComments(Integer filmId) {
        JoinResultMap result = generalDAOService.getJoin("film_id="+filmId, User.class, "user.id=comment.user_id", Comment.class);
        List<Comment> comments = result.get(Comment.class);
        List<User> commentators = result.get(User.class);
        List<DetailedComment> detailedComments = new ArrayList<>();
        for(int i=0;i<comments.size();++i){
        	detailedComments.add(new DetailedComment(comments.get(i), commentators.get(i)));
        }
        return detailedComments;
	}

}
