package com.epam.lab.service;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dto.FilmEx;
import com.epam.lab.dto.SimpleFilm;
import com.epam.lab.model.*;
import com.epam.lab.util.ServerUtil;
import com.epam.lab.util.StringConverter;
import com.google.common.base.Strings;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DataService {

    private GeneralDAOService daoService = GeneralDAOService.getInstance();

    public List<String> getCinemasCities(String search) {
        return getCinemasCities(null, search);
    }

    public List<String> getCinemasCities(String cinemaName, String search) {
        List<Cinema> cinemas;
        if (Strings.isNullOrEmpty(cinemaName)) {
            cinemas = daoService.getAll(Cinema.class);
        } else {
            cinemas = daoService.getList("lower(name) = \"" + cinemaName.toLowerCase() + "\"", Cinema.class);
        }
        List<String> cities = new ArrayList<>(cinemas.size());
        boolean getAll = Strings.isNullOrEmpty(search);
        for (Cinema cinema : cinemas) {
            if ((getAll || cinema.getCity().toLowerCase().startsWith(search.toLowerCase()))
                    && !cities.contains(cinema.getCity())) {
                cities.add(cinema.getCity());
            }
        }
        return cities;
    }

    public List<String> getCinemasNames(String city, String search) {
        List<Cinema> cinemas;
        if (Strings.isNullOrEmpty(city)) {
            cinemas = daoService.getAll(Cinema.class);
        } else {
            cinemas = daoService.getList("lower(city) = \"" + city + "\"", Cinema.class);
        }
        boolean getAll = Strings.isNullOrEmpty(search);
        List<String> names = new ArrayList<>();
        for (Cinema cinema : cinemas) {
            if ((getAll || cinema.getCinemaName().toLowerCase().startsWith(search.toLowerCase()))
                    && !names.contains(cinema.getCinemaName())) {
                names.add(cinema.getCinemaName());
            }
        }
        return names;
    }

    public Cinema getCinema(String name) {
        return getCinema(null, name);
    }

    public Cinema getCinema(String city, String name) {
        Cinema cinema = null;
        if (!Strings.isNullOrEmpty(name)) {
            String cityCondition = "";
            if (!Strings.isNullOrEmpty(city)) {
                cityCondition = " AND lower(city) = \"" + city + "\"";
            }
            cinema = daoService.get("lower(name) = \"" + name.toLowerCase() + "\"" + cityCondition, Cinema.class);
        }
        return cinema;
    }

    public boolean addCinema(Cinema cinema) {
        boolean result = false;
        List<Cinema> sameCinemas = daoService.getList("lower(name) = \"" + cinema.getCinemaName().toLowerCase() +
                "\" AND lower(city) = \"" + cinema.getCity().toLowerCase() + "\"", Cinema.class);
        if (sameCinemas.isEmpty()) {
            daoService.add(cinema);
            result = true;
        }
        return result;
    }

    public boolean addFilmCinema(FilmCinema filmCinema, int updaterId) {
        boolean result = false;
        FilmCinema sameRecord = daoService.get("film_id = \"" + filmCinema.getFilmId() +
                "\" AND cinema_id = \"" + filmCinema.getCinemaId() + "\"", FilmCinema.class);
        if (sameRecord == null) {
            daoService.add(filmCinema);
            result = true;
            updateStatistic(updaterId, "added-showing");
        }
        return result;
    }

    public boolean updateCinema(int id, Cinema updatedCinema) {
        boolean result = false;
        Cinema oldCinema = daoService.get("id = \"" + id + "\"", Cinema.class);
        if (oldCinema != null) {
            List<Cinema> sameCinemas = daoService.getList("lower(name) = \"" + updatedCinema.getCinemaName().toLowerCase() +
                    "\" AND lower(city) = \"" + updatedCinema.getCity().toLowerCase() +
                    "\" AND id != \"" + id + "\"", Cinema.class);
            if (sameCinemas.isEmpty()) {
                daoService.update(oldCinema, updatedCinema);
                result = true;
            }
        }
        return result;
    }

    public List<FilmEx> getFilms(GetParams p, boolean getGenres, boolean getCountries) {
        return getFilms(p, getGenres, getCountries, null);
    }

    public List<Film> getFilmsForDataList(GetParams p) {
        List<Film> films;
        if (p.all) {
            films = daoService.getAll(Film.class);
        } else if (!Strings.isNullOrEmpty(p.search)) {
            films = daoService.getList("(title LIKE \'" + p.search + "%\' OR title_uk LIKE \'" + p.search + "%\')",
                    p.from, p.amount, Film.class);
        } else {
            films = daoService.getList("TRUE", p.from, p.amount, Film.class);
        }
        return films;
    }

    public List<FilmEx> getFilms(GetParams p, boolean getGenres, boolean getCountries, Boolean verified) {
        List<FilmEx> films;
        String vc = "";
        if (Boolean.TRUE.equals(verified)) {
            vc = " AND verified = \"true\"";
        } else if (Boolean.FALSE.equals(verified)) {
            vc = " AND verified = \"false\"";
        }
        if (p.all) {
            films = daoService.getList("TRUE" + verified, FilmEx.class);
        } else if (!Strings.isNullOrEmpty(p.search)) {
            films = daoService.getList("(title LIKE \'%" + p.search + "%\' OR title_uk LIKE \'%" + p.search + "%\')"
                    + vc, p.from, p.amount, FilmEx.class);
        } else {
            films = daoService.getList("TRUE" + vc, p.from, p.amount, FilmEx.class);
        }
        if (getGenres) {
            for (FilmEx film : films) {
                List<FilmGenre> filmGenres = daoService.getList("film_id = \"" + film.getId() + "\"", FilmGenre.class);
                if (!filmGenres.isEmpty()) {
                    List<Genre> genres = getGenres(filmGenres);
                    film.setGenres(listToString(genres, ", ", o -> ((Genre) o).getName()));
                    film.setGenresUk(listToString(genres, ", ", o -> ((Genre) o).getNameUK()));
                }
            }
        }
        if (getCountries) {
            for (FilmEx film : films) {
                List<FilmCountry> filmCountries = daoService.getList("film_id = \"" + film.getId() + "\"", FilmCountry.class);
                if (!filmCountries.isEmpty()) {
                    List<Country> countries = getCountries(filmCountries);
                    film.setCountries(listToString(countries, ", ", o -> ((Country) o).getName()));
                    film.setCountriesUk(listToString(countries, ", ", o -> ((Country) o).getNameUK()));
                }
            }
        }
        return films;
    }

    private List<Genre> getGenres(List<FilmGenre> filmGenres) {
        String condition = "";
        for (FilmGenre filmGenre : filmGenres) {
            if (!condition.isEmpty()) condition += " OR ";
            condition += "id = \"" + filmGenre.getGenreId() + "\"";
        }
        return daoService.getList(condition, Genre.class);
    }

    private List<Country> getCountries(List<FilmCountry> filmCountries) {
        String condition = "";
        for (FilmCountry filmCountry : filmCountries) {
            if (!condition.isEmpty()) condition += " OR ";
            condition += "id = \"" + filmCountry.getCountryId() + "\"";
        }
        return daoService.getList(condition, Country.class);
    }

    private String listToString(List list, String separator, StringConverter converter) {
        String s = "";
        for (Object o : list) {
            if (!s.isEmpty()) s += separator;
            s += converter.convert(o);
        }
        return s;
    }

    public List<User> getUsers(String role, GetParams p) {
        List<User> result;
        List<User> all;
        if (role == null) {
            all = daoService.getAll(User.class);
        } else {
            all = daoService.getList("role = \"" + role + "\"", User.class);
        }
        searchUsers(all, p.search);
        if (p.all) {
            result = all;
        } else {
            result = new ArrayList<>(p.amount);
            int to = p.from + p.amount;
            for (int i = p.from; i < all.size() && i < to; i++) {
                result.add(all.get(i));
            }
        }
        return result;
    }

    public int updateUser(int id, String field, String value) {
        return daoService.update("id = \"" + id + "\"", field + " = \"" + value + "\"", User.class);
    }

    public int add(Object o) {
        return daoService.add(o);
    }

    public int update(int id, Class clazz, Object updatedObject) {
        Object oldObject = daoService.get("id = \"" + id + "\"", clazz);
        if (oldObject != null) {
            return daoService.update(oldObject, updatedObject);
        }
        return 0;
    }

    public boolean banUser(int userId, String value, int updaterId) {
        if (updaterId == userId) return false;
        User updater = daoService.get("id = \"" + updaterId + "\"", User.class);
        User target = daoService.get("id = \"" + userId + "\"", User.class);
        if (target.getAccessLevel() == null || updater.getAccessLevel() < target.getAccessLevel()) {
            daoService.update("id = \"" + userId + "\"", "ban = \"" + value + "\"", User.class);
            //update statistic
            updateStatistic(updaterId, "ban-" + value);
            return true;
        }
        return false;
    }

    public boolean changeUserRole(int userId, String value, int updaterId) {
        if (updaterId == userId) return false;
        User updater = daoService.get("id = \"" + updaterId + "\"", User.class);
        User target = daoService.get("id = \"" + userId + "\"", User.class);
        if (target.getAccessLevel() == null || updater.getAccessLevel() < target.getAccessLevel()) {
            daoService.update("id = \"" + userId + "\"", "role = \"" + value + "\"", User.class);
            Integer accessLevel = null;
            if (value.equals("administrator")) {
                accessLevel = updater.getAccessLevel() + 1;
            }
            daoService.update("id = \"" + userId + "\"", "access_level = \"" + accessLevel + "\"", User.class);
            //update statistic
            updateStatistic(updaterId, value);
            return true;
        }
        return false;
    }

    public List<User> searchUsers(List<User> users, String search) {
        if (Strings.isNullOrEmpty(search)) return users;
        String[] parts = search.toLowerCase().split(" ");
        for (int i = 0; i < parts.length; i++) {
            parts[i] = parts[i].trim();
        }
        for (Iterator<User> iterator = users.iterator(); iterator.hasNext(); ) {
            User user = iterator.next();
            boolean found = true;
            for (String s : parts) {
                if (!user.getFirstName().toLowerCase().contains(s)
                        && !user.getLastName().toLowerCase().contains(s)
                        && !user.getEmail().toLowerCase().contains(s)) {
                    found = false;
                }
            }
            if (!found) {
                iterator.remove();
            }
        }
        return users;
    }

    public Statistic getUserStatistic(int userId) {
        Statistic statistic = daoService.get("user_id = \"" + userId + "\"", Statistic.class);
        if (statistic == null) {
            statistic = new Statistic(userId);
            daoService.add(statistic);
        }
        return statistic;
    }

    private void updateStatistic(int updaterId, String key) {
        Statistic statistic = daoService.get("user_id = \"" + updaterId + "\"", Statistic.class);
        if (statistic == null) {
            statistic = new Statistic(updaterId);
            daoService.add(statistic);
            statistic = daoService.get(statistic);
        }
        Statistic oldStatistic = ServerUtil.clone(statistic);
        switch (key) {
            case "ban-true":
                statistic.incBanCount();
                break;
            case "ban-false":
                statistic.incUnbanCount();
                break;
            case "moderator":
                statistic.incAddedModerators();
                break;
            case "administrator":
                statistic.incAddedAdmins();
                break;
            case "user":
                statistic.incRemovedPowers();
                break;
            case "edited-item":
                statistic.incEditedItems();
                break;
            case "added-premier":
                statistic.incAddedPremieres();
                break;
            case "edited-showing":
                statistic.incEditedShowing();
                break;
            case "added-showing":
                statistic.incAddedShowing();
                break;
        }
        daoService.update(oldStatistic, statistic);
    }

    public Boolean subscribe(String email) {
        Mailing mailing = daoService.get("email = \"" + email + "\"", Mailing.class);
        if (mailing == null) {
            daoService.add(new Mailing(email));
        }
        return mailing == null;
    }

    public List<SimpleFilm> getFilmsIds() {
        return daoService.getAll(SimpleFilm.class);
    }

    public static class GetParams {
        private boolean all;
        private int from;
        private int amount;
        private String search;

        public void setAll(boolean all) {
            this.all = all;
        }

        public void setFrom(int from) {
            this.from = from;
        }

        public void setAmount(int amount) {
            this.amount = amount;
        }

        public void setSearch(String search) {
            this.search = search;
        }

        public GetParams() {
        }

        public GetParams(String search, int from, int amount) {
            this.from = from;
            this.amount = amount;
            this.search = search;
        }

        public GetParams(String search, boolean all) {
            this.search = search;
            this.all = all;
        }
    }
}
