package com.epam.lab.service;

import com.epam.lab.model.Film;
import com.epam.lab.model.User;
import com.epam.lab.util.ServerUtil;

import java.time.LocalDate;

public class EmailService {

    private String getFilmIsReleasingMsg(String personName, String poster, String movieName, String filmLink, String lang) {
        String fullMsg = null;
        if ("uk".equals(lang)) {
            fullMsg = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tbody> <tr>"
                    + " <td align=\"center\" valign=\"top\" bgcolor=\"#f6f3e4\" style=\"background-color: #FFFFFF;\">"
                    + " <br> <br> <table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-bottom: 30px; padding: 0px 10px 20px; border: 2px solid #233D63;\">"
                    + " <tbody style=\"margin-bottom: 200px;\"><tr> <td align=\"center\" valign=\"top\" style=\"padding-left:13px;padding-right:13px;background-color:#ffffff\">"
                    + " <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-bottom: 0px;\">"
                    + " <tbody> <tr> <td> <table width=\"74\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tbody> <tr>"
                    + " <td height=\"55\" align=\"center\" valign=\"middle\" bgcolor=\"#233D63\" style=\"font-family:Arial,Helvetica,sans-serif;color: #FFFFFF;\">"
                    + " <div style=\"font-size:14px\"><b> Лютий </b></div><div style=\"font-size:22px\"><b>" + (LocalDate.now().getDayOfMonth() + 1) + "</b></div></td> </tr> </tbody> </table> </td> </tr> "
                    + " <tr> <td align=\"center\" valign=\"middle\" style=\"padding-top:15px;\"><img src=\"cid:logo\" width=\"380\" height=\"70\" style=\"display:block;\">"
                    + " </td> </tr> <tr> <td align=\"center\" valign=\"middle\" style=\"padding-top:7px\">"
                    + " <table width=\"240\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tbody> <tr> "
                    + " <td height=\"31\" align=\"center\" valign=\"middle\" bgcolor=\"#233D63\" style=\"font-family:Georgia,\'Times New Roman\',Times,serif;font-size:19px;color: #FFFFFF;\">"
                    + "JavaLab12 Team \'B3G2\'</td> </tr> </tbody> </table> </td> </tr> "
                    + " <tr> <td align=\"left\" valign=\"middle\" style=\"font-family:Georgia,\'Times New Roman\',Times,serif;color:#000000;font-size:15px\">"
                    + " <i>Привіт, " + personName + "</i><br><br> Ми раді повідомити, що завтра виходить в прокат фільм <i>" + movieName
                    + " </i> Отримати детальну інформацію про цей фільм Ви можете тут:  <a href=" + filmLink + ">" + filmLink + "</a> </td></tr>"
                    + " <tr> <td align=\"center\" valign=\"middle\" style=\"padding-bottom:5px; padding-top:5px;\">"
                    + " <img src=\"cid:image\" width=\"573\" height=\"28\"></td> </tr> <tr> "
                    + " <td align=\"center\" valign=\"middle\" style=\"font-family:Georgia,\'Times New Roman\',Times,serif;color:#000000;font-size:24px;padding-bottom:5px\">"
                    + " <i>" + movieName
                    + "</i> </td></tr> <tr> <td align=\"center\" valign=\"middle\" style=\"padding-top:5px\">"
                    + " <img src=" + poster + " style=\"max-height: 300px\"> </td> </tr>"
                    + " <tr><td style=\"color: #233D63; font-size: 16px;text-align: -webkit-center;font-weight: 800;\"><br> MovieMotion - keep track of your movies! </td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>";
        } else {
            fullMsg = "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tbody> <tr>"
                    + " <td align=\"center\" valign=\"top\" bgcolor=\"#f6f3e4\" style=\"background-color: #FFFFFF;\">"
                    + " <br> <br> <table width=\"600\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-bottom: 30px; padding: 0px 10px 20px; border: 2px solid #233D63;\">"
                    + " <tbody style=\"margin-bottom: 200px;\"><tr> <td align=\"center\" valign=\"top\" style=\"padding-left:13px;padding-right:13px;background-color:#ffffff\">"
                    + " <table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\" style=\"margin-bottom: 0px;\">"
                    + " <tbody> <tr> <td> <table width=\"74\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tbody> <tr>"
                    + " <td height=\"55\" align=\"center\" valign=\"middle\" bgcolor=\"#233D63\" style=\"font-family:Arial,Helvetica,sans-serif;color: #FFFFFF;\">"
                    + " <div style=\"font-size:14px\"><b> February </b></div><div style=\"font-size:22px\"><b>" + (LocalDate.now().getDayOfMonth() + 1) + "</b></div></td> </tr> </tbody> </table> </td> </tr> "
                    + " <tr> <td align=\"center\" valign=\"middle\" style=\"padding-top:15px;\"><img src=\"cid:logo\" width=\"380\" height=\"70\" style=\"display:block;\">"
                    + " </td> </tr> <tr> <td align=\"center\" valign=\"middle\" style=\"padding-top:7px\">"
                    + " <table width=\"240\" border=\"0\" cellspacing=\"0\" cellpadding=\"0\"> <tbody> <tr> "
                    + " <td height=\"31\" align=\"center\" valign=\"middle\" bgcolor=\"#233D63\" style=\"font-family:Georgia,\'Times New Roman\',Times,serif;font-size:19px;color: #FFFFFF;\">"
                    + "JavaLab12 Team \'B3G2\'</td> </tr> </tbody> </table> </td> </tr> "
                    + " <tr> <td align=\"left\" valign=\"middle\" style=\"font-family:Georgia,\'Times New Roman\',Times,serif;color:#000000;font-size:15px\">"
                    + " <i>Hi, " + personName + "</i><br><br> We are glad to tell you that tomorrow <strong>" + movieName
                    + " </strong> will be released." + " Get detailed information about movie here: <a href=" + filmLink + ">" + filmLink + "</a> </td></tr>"
                    + " <tr> <td align=\"center\" valign=\"middle\" style=\"padding-bottom:5px; padding-top:5px;\">"
                    + " <img src=\"cid:image\" width=\"573\" height=\"28\"></td> </tr> <tr> "
                    + " <td align=\"center\" valign=\"middle\" style=\"font-family:Georgia,\'Times New Roman\',Times,serif;color:#000000;font-size:24px;padding-bottom:5px\">"
                    + " <i>" + movieName
                    + "</i> </td></tr> <tr> <td align=\"center\" valign=\"middle\" style=\"padding-top:5px\">"
                    + " <img src=" + poster + " style=\"max-height: 300px\"> </td> </tr>"
                    + " <tr><td style=\"color: #233D63; font-size: 16px;text-align: -webkit-center;font-weight: 800;\"><br> MovieMotion - keep track of your movies! </td></tr></tbody></table></td></tr></tbody></table></td></tr></tbody></table>";
        }
        return fullMsg;
    }

    public void filmIsReleasingNotify(User currentUser, Film film, String additionalMessage) {
        String nameClear = removeWhiteSpaces(currentUser.getFirstName());
        String msgClear = (additionalMessage == null ? null : removeWhiteSpaces(additionalMessage));
        String email = currentUser.getEmail();
        String lang = currentUser.getLanguage();
        String title;
        if (("uk".equals(lang)) && (film.getTitleUk() != null) && (removeWhiteSpaces(film.getTitleUk()) != "")) {
            title = film.getTitleUk();
        } else {
            title = film.getTitle();
        }
        String filmTitleClear = removeWhiteSpaces(title);
        String filmLink = "localhost:8080/moviemotion/movie_details?id=" + film.getId();
        ServerUtil.sendHTMLEmail(ServerUtil.MAIN_MAIL, email, "MovieMotion | " + filmTitleClear + " | RELEASE ",
                getFilmIsReleasingMsg(nameClear, film.getPoster(), filmTitleClear, filmLink, lang));
    }

    public void contactUs(String name, String email, String msg) {
        String nameClear = removeWhiteSpaces(name);
        String msgClear = removeWhiteSpaces(msg);
        ServerUtil.sendEmail(ServerUtil.CONTACT_MAIL, ServerUtil.MAIN_MAIL, "CONTACT US | " + nameClear,
                getContactUsMsg(nameClear, email, msgClear));
    }

    private String getContactUsMsg(String name, String email, String msg) {
        return "Name: " + name + "\nEmail: " + email + "\n\n" + msg;
    }

    private String removeWhiteSpaces(String s) {
        return s.trim().replaceAll("\\n+", "\n").replaceAll("(\\n +)", "\n").replaceAll("\\t+", " ").replaceAll(" +",
                " ");
    }

    public void mailRestorePasswordMail(String lang, String fname, String email, String password) {
        String subject;
        String msg;
        if ("uk".equals(lang)) {
            subject = "Movie Motion | Відновлення паролю";
            msg = "Привіт " + fname + ",\n\n" + "Ваш тимчасовий паролю: "
                    + password + "\nПароль дійсний протягом одного дня!";
        } else {
            subject = "Movie Motion | Restore password";
            msg = "Hello " + fname + ",\n\n" + "Your temporary password: "
                    + password + "\nPassword is valid during one day!";
        }
        ServerUtil.sendEmail(ServerUtil.MAIN_MAIL, email, subject, msg);
    }

    public void mailSignUp(String lang, String fname, String email) {
        String subject;
        String msg;
        if ("uk".equals(lang)) {
            subject = "Movie Motion | Реєстрація";
            msg = "Привіт " + fname + ",\n\n" + "Ви успішно зареєструвалися на нашому сайті!";
        } else {
            subject = "Movie Motion | Sign Up";
            msg = "Hello " + fname + ",\n\n" + "You have successfully registered on our site!";
        }
        ServerUtil.sendEmail(ServerUtil.MAIN_MAIL, email, subject, msg);
    }
}
