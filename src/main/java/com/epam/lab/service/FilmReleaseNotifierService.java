package com.epam.lab.service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmUser;
import com.epam.lab.model.User;

public class FilmReleaseNotifierService {
	private GeneralDAOService generalDAOService;
	private EmailService emailService;

	public FilmReleaseNotifierService() {
		generalDAOService = GeneralDAOService.getInstance();
		emailService = new EmailService();
	}

	public void sendNotificationsByDate(LocalDate date, String additionalMsg) {
		String stringDate = date.format(DateTimeFormatter.ISO_LOCAL_DATE);
		List<Film> films = generalDAOService.getList("released='" + stringDate + "'", Film.class);

		for (Film film : films) {
			List<FilmUser> filmUsers = generalDAOService.getList("film_id=" + film.getId() + " AND status='SCHEDULED'",
					FilmUser.class);
			for (FilmUser filmUser : filmUsers) {
				User currentUser = generalDAOService.get("id=" + filmUser.getUserId(), User.class);
				if (currentUser.getMailing() == true) {
					emailService.filmIsReleasingNotify(currentUser, film, additionalMsg);
				}
			}
		}
	}
}
