package com.epam.lab.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.TreeMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.epam.lab.connection.DataSource;
import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dao.GeneralDAOService.JoinResultMap;
import com.epam.lab.dto.DetailedWatcher;
import com.epam.lab.model.FilmGenre;
import com.epam.lab.model.FilmUser;
import com.epam.lab.model.Friend;
import com.epam.lab.model.Genre;
import com.epam.lab.model.User;
import com.epam.lab.util.PaginationUtils;

public class FriendsInformationService {
	private GeneralDAOService generalDAOService;
	private static FriendsInformationService instance;

	private FriendsInformationService() {
		if (generalDAOService == null) {
			generalDAOService = GeneralDAOService.getInstance();
		}
	}

	public static FriendsInformationService getInstance() {
		if (instance == null) {
			instance = new FriendsInformationService();
		}
		return instance;
	}

	public boolean validateNameSearchString(String nameToSearch) {
		Pattern p = Pattern.compile("^[\\p{L} .'-]+$");
		Matcher m = p.matcher(nameToSearch);
		return m.matches();
	}

	private String[] getNameTokens(String toSearch) {
		String[] toReturn = toSearch.split("(\\s)+");
		return toReturn;
	}

	private String formClauseWithOutLimit(int userId, String toSearch, String type) {
		String clause = null;
		if("subscribing".equals(type)){
			clause="follower_id=" + userId;
		} else if("subscribers".equals(type)){
			clause="followee_id=" + userId;
		}

		if (toSearch != null) {
			String[] nameTokens = getNameTokens(toSearch);
			if (nameTokens != null && nameTokens.length > 0) {
				if (nameTokens.length == 1) {
					clause += " AND (lname LIKE \'" + toSearch + "%\' OR fname LIKE \'" + toSearch + "%\')";
				} else if (nameTokens.length > 1) {
					clause += " AND ((lname = \'" + nameTokens[0] + "\'" + " AND fname = \'" + nameTokens[1]
							+ "\') OR (fname = \'" + nameTokens[0] + "\'" + " AND lname = \'" + nameTokens[1] + "\'))";
				}
			}
		}

		return clause;
	}

	private String formClauseWithLimit(int userId, String toSearch, Integer pageNumber, String type) {
		String clause = formClauseWithOutLimit(userId, toSearch, type);

		if (pageNumber != null && pageNumber >= 0) {
			clause += " LIMIT " + PaginationUtils.getOffset(pageNumber, PaginationUtils.DEFAULT_USER_COUNT) + ", "
					+ PaginationUtils.DEFAULT_USER_COUNT;
		}
		return clause;
	}
	
	public Integer getCountOfSubscriptionAll(int userId, String toSearch, String type) {
		String clause = formClauseWithOutLimit(userId, toSearch, type);
		String join = ("subscribing".equals(type)?"user.id=friend.followee_id":"user.id=friend.follower_id");
		String query="SELECT count(user.id) FROM friend INNER JOIN user ON "+join+" WHERE "+clause;
		Integer counter=null;
        Connection connection;
		try {
			connection = DataSource.getConnection();
			Statement stmt = connection.createStatement();
	        ResultSet rs = stmt.executeQuery(query);
	        if(rs.next()) {
	        	counter=rs.getInt(1);
	        }
		} catch (SQLException e) {
			e.printStackTrace();
		}        
		return counter;
	}
	
	public List<DetailedWatcher> getFriendsSubscribingInformation(int userId, String toSearch, Integer pageNumber,String locale) {
		String clause1 = formClauseWithLimit(userId, toSearch, pageNumber,"subscribing");
		
		JoinResultMap joinResultMap = generalDAOService.getJoin(clause1, Friend.class, "user.id=friend.followee_id",
				User.class);
		
		List<User> friends = joinResultMap.get(User.class);
		Set<User> users = new LinkedHashSet<>(friends);
		
		return getDetailedWatchers(users,locale);
	}

	public Map<DetailedWatcher, Boolean> getFriendsSubscribersInformation(int userId, String toSearch,
			Integer pageNumber,String locale) {
		String clause1 = formClauseWithLimit(userId, toSearch, pageNumber,"subscribers");
		JoinResultMap joinResultMap = generalDAOService.getJoin(clause1, Friend.class, "user.id=friend.follower_id",
				User.class);
		List<User> subscribers = joinResultMap.get(User.class);
		List<DetailedWatcher> detailedWatchers = getDetailedWatchers(new LinkedHashSet<User>(subscribers),locale);
		
		Map<DetailedWatcher, Boolean> detailedSubscribersWithIfMeSubscribed = new LinkedHashMap<>();
		for (DetailedWatcher detailedWatcher : detailedWatchers) {
			String clause = "follower_id=" + userId + " AND followee_id=" + detailedWatcher.getUser().getId();
			boolean ifMeSubscribed = (generalDAOService.get(clause, Friend.class) == null);
			detailedSubscribersWithIfMeSubscribed.put(detailedWatcher, ifMeSubscribed);
		}

		return sortByComparator(detailedSubscribersWithIfMeSubscribed, false);
	}

	private List<DetailedWatcher> getDetailedWatchers(Set<User> users, String locale) {
		List<DetailedWatcher> detailedWatchers = new ArrayList<>();
		List<FilmUser> watchedFilms;
		for (User user : users) {
			watchedFilms = generalDAOService.getList("user_id=" + user.getId() + " AND status=\'WATCHED\'",
					FilmUser.class);
			Map<Integer, Integer> genresAmount = new TreeMap<>();
			for (FilmUser watchedFilm : watchedFilms) {
				List<FilmGenre> genres = generalDAOService.getList("film_id=" + watchedFilm.getFilmId(),
						FilmGenre.class);
				for (FilmGenre filmGenre : genres) {
					Integer prevAmount = genresAmount.get(filmGenre.getGenreId());
					genresAmount.put(filmGenre.getGenreId(), prevAmount == null ? 1 : prevAmount + 1);
				}
			}

			int maxAmount = 0, maxNumber = -1;
			for (Map.Entry<Integer, Integer> genreByUser : genresAmount.entrySet()) {
				Integer key = genreByUser.getKey();
				Integer value = genreByUser.getValue();
				if (value > maxAmount) {
					maxAmount = value;
					maxNumber = key;
				}
			}

			String genreName=null;
			if(maxNumber!=-1){
				Genre currentGenre = generalDAOService.get("id=" + maxNumber, Genre.class);
				if("uk".equals(locale)){
					genreName=currentGenre.getNameUK();
				}
				else{
					genreName=currentGenre.getName();
				}
			}
			detailedWatchers.add(new DetailedWatcher(user, watchedFilms.size(), genreName));
		}
		return detailedWatchers;
	}

	private Map<DetailedWatcher, Boolean> sortByComparator(Map<DetailedWatcher, Boolean> unsortMap,
			final boolean order) {
		List<Entry<DetailedWatcher, Boolean>> list = new LinkedList<Entry<DetailedWatcher, Boolean>>(
				unsortMap.entrySet());

		Collections.sort(list, new Comparator<Entry<DetailedWatcher, Boolean>>() {
			public int compare(Entry<DetailedWatcher, Boolean> o1, Entry<DetailedWatcher, Boolean> o2) {
				if (order) {
					return o1.getValue().compareTo(o2.getValue());
				} else {
					return o2.getValue().compareTo(o1.getValue());

				}
			}
		});

		// Maintaining insertion order with the help of LinkedList
		Map<DetailedWatcher, Boolean> sortedMap = new LinkedHashMap<DetailedWatcher, Boolean>();
		for (Entry<DetailedWatcher, Boolean> entry : list) {
			sortedMap.put(entry.getKey(), entry.getValue());
		}

		return sortedMap;
	}
}
