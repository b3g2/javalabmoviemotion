package com.epam.lab.service;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dao.GeneralDAOService.JoinResultMap;
import com.epam.lab.dto.DetailedComment;
import com.epam.lab.dto.FilmWithStatistic;
import com.epam.lab.model.Comment;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmUser;
import com.epam.lab.model.User;

public class HomeService {

	private GeneralDAOService daoService = GeneralDAOService.getInstance();
	private Random random;
	private Set<Integer> indexSet;
	private Integer favouriteCount;

	public List<Film> getMoviesForTime(String interval, String count) {
		return daoService.getList("released between date_sub(now(),INTERVAL " + interval
				+ " DAY) and now() ORDER BY rating_custom desc LIMIT " + count + "", Film.class);
	}

	public List<Film> getMoviesForTime(String interval, Integer count) {

		random = new Random();
		indexSet = new HashSet<>();
		List<Film> randomTopMovies = new ArrayList<>();

		List<Film> filmsForTime = daoService
				.getList("released between now() and date_sub(now(),INTERVAL -" + interval + " DAY)", Film.class);

		if (filmsForTime.size() < count) {
			randomTopMovies = filmsForTime;

		} else {
			while (indexSet.size() < count) {
				indexSet.add(random.nextInt(filmsForTime.size()));
			}

			for (Integer index : indexSet) {
				randomTopMovies.add(filmsForTime.get(index));
			}
		}
		return randomTopMovies;
	}

	public List<Film> getMoviesByRating(String count) {

		return daoService.getList("id>0 AND rating_custom!='null' ORDER BY rating_custom desc LIMIT " + count + "",
				Film.class);
	}

	public List<FilmWithStatistic> getFavouriteMovies(Integer count) {

		List<FilmWithStatistic> favouriteMovies = new ArrayList<>();

		JoinResultMap topFavouriteMovies = daoService.getJoin("film_user.favourite = true", "film_user.film_id",
				"COUNT(film_user.id) desc", Film.class, "film.id = film_user.film_id", FilmUser.class);

		if (topFavouriteMovies.listSize(Film.class) < count) {
			count = topFavouriteMovies.listSize(Film.class);
		}

		for (int i = 0; i < count; i++) {
			favouriteCount = daoService
					.getList("favourite = true AND film_id = " + topFavouriteMovies.get(Film.class).get(i).getId() + "",
							FilmUser.class)
					.size();
			favouriteMovies
					.add(new FilmWithStatistic(topFavouriteMovies.get(Film.class).get(i), null, favouriteCount, null));
		}

		return favouriteMovies;
	}

	public List<FilmWithStatistic> getSceduledNewMovies(Integer count) {

		List<FilmWithStatistic> randomSceduledNewMovies = new ArrayList<>();
		Integer sheduledCount;
		random = new Random();
		indexSet = new HashSet<>();

		JoinResultMap releasedMovies = daoService.getJoin(
				"film.released > CURDATE() AND film_user.status = 'SCHEDULED'", "film.id", null, Film.class,
				"film.id = film_user.film_id", FilmUser.class);

		if (releasedMovies.listSize(Film.class) < count) {
			count = releasedMovies.listSize(Film.class);
			for (int i = 0; i < releasedMovies.listSize(Film.class); i++) {
				indexSet.add(i);
			}

		} else {
			while (indexSet.size() < count) {
				indexSet.add(random.nextInt(releasedMovies.listSize(Film.class)));
			}
		}

		for (Integer index : indexSet) {
			favouriteCount = daoService
					.getList("favourite = true AND film_id = " + releasedMovies.get(Film.class).get(index).getId() + "",
							FilmUser.class)
					.size();
			sheduledCount = daoService.getList(
					"status = 'SCHEDULED' AND film_id = " + releasedMovies.get(Film.class).get(index).getId() + "",
					FilmUser.class).size();
			randomSceduledNewMovies.add(new FilmWithStatistic(releasedMovies.get(Film.class).get(index), sheduledCount,
					favouriteCount, null));
		}

		return randomSceduledNewMovies;
	}

	public List<FilmWithStatistic> getMostCommentedMovies(Integer count) {

		List<FilmWithStatistic> mostCommentedMovies = new ArrayList<>();
		Integer commentCount;

		JoinResultMap commentedMovies = daoService.getJoin(null, "film.id", null, Film.class,
				"film.id = comment.film_id", Comment.class);

		if (commentedMovies.listSize(Film.class) < count) {
			count = commentedMovies.listSize(Film.class);
		}

		for (int i = 0; i < commentedMovies.listSize(Film.class); i++) {
			commentCount = daoService
					.getList("film_id = " + commentedMovies.get(Film.class).get(i).getId() + "", Comment.class).size();
			mostCommentedMovies
					.add(new FilmWithStatistic(commentedMovies.get(Film.class).get(i), null, null, commentCount));
		}

		mostCommentedMovies.stream()
				.sorted((object1, object2) -> object1.getCommentCount().compareTo(object2.getCommentCount()));

		return mostCommentedMovies.subList(commentedMovies.listSize(Film.class) - count,
				commentedMovies.listSize(Film.class));
	}

	public List<DetailedComment> getLastComments() {

		List<DetailedComment> lastComments = new ArrayList<>();
		JoinResultMap commentedMovies = daoService.getJoin(null, null, "comment.id desc LIMIT 5", Comment.class,
				"user.id = comment.user_id", User.class, "film.id = comment.film_id", Film.class);

		for (int i = 0; i < commentedMovies.listSize(Film.class); i++) {
			lastComments.add(new DetailedComment(commentedMovies.get(Comment.class).get(i),
					commentedMovies.get(User.class).get(i), commentedMovies.get(Film.class).get(i)));
		}
		return lastComments;
	}

}
