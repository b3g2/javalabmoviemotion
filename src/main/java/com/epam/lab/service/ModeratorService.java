package com.epam.lab.service;

import java.util.List;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.model.Film;
import com.epam.lab.model.Statistic;
import com.epam.lab.model.User;

public class ModeratorService {

	public void updateMovie(String titleUkParam, String descriptionUk, String id) {
		titleUkParam = titleUkParam.replaceAll("\"", "&quot;");
		titleUkParam = titleUkParam.replaceAll("\'", "&prime;");

		descriptionUk = descriptionUk.replaceAll("\"", "&quot;");
		descriptionUk = descriptionUk.replaceAll("\'", "&prime;");

		GeneralDAOService.getInstance().update(" id = '" + id + "' ",
				" plot_uk = '" + descriptionUk + "' " + " , title_uk = '" + titleUkParam + "' " + " , verified = b'1' ",
				Film.class);
	}

	public List<Film> getCheckedStatistics() {
		return GeneralDAOService.getInstance().getList("verified = b'1'", Film.class);
	}

	public List<Film> getUncheckedStatistics() {
		return GeneralDAOService.getInstance().getList("verified = b'0'", Film.class);
	}

	public Statistic getCheckedStatisticsByCurrent(User user) {
		Statistic statistic;
		statistic = GeneralDAOService.getInstance().get(" user_id = " + user.getId(), Statistic.class);
		if (statistic == null) {
			statistic = new Statistic(user.getId());
			GeneralDAOService.getInstance().add(statistic);
		}
		return statistic;
	}

	public void increaseVerifyCounter(User user) {
		GeneralDAOService.getInstance().update(" user_id = " + user.getId(), " edited_items = edited_items + 1 ",
				Statistic.class);
	}

}
