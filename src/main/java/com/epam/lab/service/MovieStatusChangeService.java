package com.epam.lab.service;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.model.FilmUser;
import com.epam.lab.util.ServerUtil;

public class MovieStatusChangeService {
	private GeneralDAOService generalDAOService;
	private static MovieStatusChangeService instance;

	private MovieStatusChangeService() {
		if (generalDAOService == null) {
			generalDAOService = GeneralDAOService.getInstance();
		}
	}

	public static MovieStatusChangeService getInstance() {
		if (instance == null) {
			instance = new MovieStatusChangeService();
		}
		return instance;
	}

	public int changeMovieStatus(String action, Integer userId, Integer filmId) {
		String clause = "user_id=" + userId + " AND film_id=" + filmId;
		FilmUser filmUserInitial = generalDAOService.get(clause, FilmUser.class);
		boolean status = (filmUserInitial != null);
		// try to ADD
		if (!status) {
			filmUserInitial = new FilmUser(filmId, userId);
			status = (generalDAOService.add(filmUserInitial) > 0);
		}
		if (status) {
			FilmUser filmUserUpdated = ServerUtil.clone(filmUserInitial);
			if (action.equals("LIKED")) {
				if (!action.equals(filmUserInitial.getLikeValue())) {
					filmUserUpdated.setLikeValue("LIKED");
				} else {
					filmUserUpdated.setLikeValue("NOTMARKED");
				}
			} else if (action.equals("DISLIKED")) {
				if (!action.equals(filmUserInitial.getLikeValue())) {
					filmUserUpdated.setLikeValue("DISLIKED");
				} else {
					filmUserUpdated.setLikeValue("NOTMARKED");
				}
			} else if (action.equals("SCHEDULED") || action.equals("WATCHED")) {
				if(!action.equals(filmUserInitial.getStatus())){
					filmUserUpdated.setStatus(action);
				}
				else{
					filmUserUpdated.setStatus("UNWATCHED");
				}
			} else if (action.equals("FAVOURITE")) {
				if (!filmUserInitial.getFavourite()) {
					filmUserUpdated.setFavourite(true);
				} else {
					filmUserUpdated.setFavourite(false);
				}
			}
			return generalDAOService.update(filmUserInitial, filmUserUpdated);
		} else {
			return 0;
		}
	}

}