package com.epam.lab.service;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dao.SpecificDAO;
import com.epam.lab.dto.FilmWithCurrentUser;
import com.epam.lab.model.*;
import com.epam.lab.util.PaginationUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;

import java.util.*;

public class MoviesSearchService {
	private static final String TITLE_COLUMN = " title LIKE ";
	private static final String TITLE_UK_COLUMN = " title_uk LIKE ";
//	private static final String ACTORS_COLUMN = " actors LIKE ";
//	private static final String WRITER_COLUMN = " writer LIKE ";
//	private static final String DIRECTOR_COLUMN = " director LIKE ";

	public List<Genre> getAllGenres() {
		return GeneralDAOService.getInstance().getAll(Genre.class);
	}

	public List<String> getAllYears() {
		return new SpecificDAO().getAllYears();
	}

	public List<Country> getAllCountries() {
		return GeneralDAOService.getInstance().getAll(Country.class);
	}

	public List<Film> doSearch(Map<String, Set<String>> params, Integer page, String sortBy, Boolean sortFlag) {
		String query = generateQuery(params, PaginationUtils.getOffset(page, PaginationUtils.DEFAULT_FILM_COUNT),
				PaginationUtils.DEFAULT_FILM_COUNT, sortBy, sortFlag);
		SpecificDAO specificDAO = new SpecificDAO();
		return specificDAO.getFilmsWithOptions("SELECT * FROM film " + query);

	}

	private String generateQuery(Map<String, Set<String>> params, Integer offset, Integer count, String sortBy,
			Boolean sortFlag) {
		StringBuilder stringBuilder = new StringBuilder();
		Iterator<String> paramsIterator = params.keySet().iterator();
		while (paramsIterator.hasNext()) {
			String key = paramsIterator.next();
			Set<String> set = params.get(key);
			if (!Iterables.isEmpty(set) && key.equals("genres")) {
				stringBuilder.append(" JOIN film_genre ON film.id = film_genre.film_id ");
			}
			if (!Iterables.isEmpty(set) && key.equals("countries")) {
				stringBuilder.append(" JOIN film_country ON film.id = film_country.film_id ");
			}
		}
		paramsIterator = params.keySet().iterator();
		if (!params.keySet().isEmpty()) {
			stringBuilder.append(" WHERE ");
		}
		while (paramsIterator.hasNext()) {
			String key = paramsIterator.next();
			Set<String> set = params.get(key);
			if (!Iterables.isEmpty(set) && key.equals("genres")) {
				String condition = generateCondition(set, "film_genre.genre_id");
				stringBuilder.append(condition);
			}
			if (!Iterables.isEmpty(set) && key.equals("years")) {
				String condition = generateCondition(set, "film.year");
				stringBuilder.append(condition);
			}
			if (!Iterables.isEmpty(set) && key.equals("countries")) {
				String condition = generateCondition(set, "film_country.country_id");
				stringBuilder.append(condition);
			}
			if (!Iterables.isEmpty(set) && key.equals("toSearch")) {
				String toSearch = Iterables.getOnlyElement(set);
				stringBuilder.append(" ( ");
				appendLikeCondition(toSearch, stringBuilder, TITLE_COLUMN);
				stringBuilder.append(" OR ");
				appendLikeCondition(toSearch, stringBuilder, TITLE_UK_COLUMN);
				stringBuilder.append(" ) ");
			}
			if (paramsIterator.hasNext()) {
				stringBuilder.append(" AND ");
			}

		}
		if (!Strings.isNullOrEmpty(sortBy)) {
			stringBuilder.append(" ORDER BY " + sortBy);
			if (Boolean.FALSE.equals(sortFlag)) {
				stringBuilder.append(" DESC ");
			} else {
				stringBuilder.append(" ASC ");
			}
		}
		stringBuilder.append(" LIMIT " + offset + ", " + count);
		return stringBuilder.toString();
	}

	private String generateCondition(Set<String> set, String columnName) {
		StringBuilder stringBuilder = new StringBuilder();
		stringBuilder.append(columnName);
		stringBuilder.append(" IN (");
		Iterator<String> iterator = set.iterator();
		while (iterator.hasNext()) {
			String string = iterator.next();
			stringBuilder.append(string);
			if (iterator.hasNext()) {
				stringBuilder.append(", ");
			}
		}
		stringBuilder.append(")");
		return stringBuilder.toString();
	}

	private void appendLikeCondition(String toSearch, StringBuilder builder, String condition) {
		builder.append(condition);
		builder.append(" '%");
		builder.append(toSearch.trim());
		builder.append("%' ");
	}

	public List<Film> searchFilmOnlyByName(String toSearch, Integer page, Boolean verified) {
		String query = generateQueryParam(toSearch, PaginationUtils.getOffset(page, PaginationUtils.DEFAULT_FILM_COUNT),
				PaginationUtils.DEFAULT_FILM_COUNT, verified);
		return GeneralDAOService.getInstance().getList(query, Film.class);
	}

	private String generateQueryParam(String toSearch, Integer offset, Integer count, Boolean verified) {
		StringBuilder builder = new StringBuilder();
		builder.append(" ( ");
		appendLikeCondition(toSearch, builder, TITLE_COLUMN);
		builder.append(" OR ");
		appendLikeCondition(toSearch, builder, TITLE_UK_COLUMN);
		builder.append(" ) ");
		if (verified != null) {
			if (verified) {
				builder.append(" AND verified = b'1' ");
			} else {
				builder.append(" AND verified = b'0' ");
			}
		}
		builder.append(" LIMIT " + offset + ", " + count);
		return builder.toString();
	}

	public List<FilmWithCurrentUser> getUserFilmsInfo(List<Film> filmsWithOptions, User user) {
		List<FilmWithCurrentUser> filmsWithCurrentUsers = new ArrayList<>(filmsWithOptions.size());
		GeneralDAOService daoService = GeneralDAOService.getInstance();
		for (Film film : filmsWithOptions) {
			FilmUser filmUser = null;
			if (user != null) {
				filmUser = daoService.get("film_id = \"" + film.getId() + "\" AND user_id = \"" + user.getId() + "\"",
						FilmUser.class);
			}
			filmsWithCurrentUsers.add(new FilmWithCurrentUser(film, filmUser));
		}
		return filmsWithCurrentUsers;
	}

//	public List<FilmGenre> getFilmGenreRelations() {
//		return GeneralDAOService.getInstance().getAll(FilmGenre.class);
//	}

//	public List<FilmCountry> getFilmCountryRelations() {
//		return GeneralDAOService.getInstance().getAll(FilmCountry.class);
//	}

}
