package com.epam.lab.service;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.epam.lab.connection.DataSource;
import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dao.GeneralDAOService.JoinResultMap;
import com.epam.lab.dto.UserWithStatistic;
import com.epam.lab.model.Comment;
import com.epam.lab.model.FilmUser;
import com.epam.lab.model.User;

public class RegisteredUsersService {

	private GeneralDAOService daoService = GeneralDAOService.getInstance();

	public List<User> getAllUsers() {
		long seed = System.nanoTime();
		List<User> users = daoService.getAll(User.class);

		Collections.shuffle(users, new Random(seed));
		return users;
	}

	public List<User> getTopWatched() {

		return daoService.getList("id !=0 ORDER BY duration desc LIMIT 10", User.class);
	}

	public List<UserWithStatistic> getTopActive() {

		Integer likeCount;
		Integer dislikeCount;
		List<UserWithStatistic> activeUsers = new ArrayList<>();

		JoinResultMap users = daoService.getJoin("film_user.like_value != 'NOTMARKED'", "film_user.user_id", "COUNT(film_user.like_value) desc LIMIT 10",
				User.class, "film_user.user_id = user.id", FilmUser.class);

		for (int i = 0; i < users.listSize(User.class); i++) {
			likeCount = daoService
					.getList("user_id = " + users.get(User.class).get(i).getId() + " AND like_value = 'LIKED'", FilmUser.class)
					.size();
			dislikeCount = daoService
					.getList("user_id = " + users.get(User.class).get(i).getId() + " AND like_value ='DISLIKED'", FilmUser.class)
					.size();
			activeUsers.add(
					new UserWithStatistic(users.get(User.class).get(i), null, likeCount, dislikeCount, null, null));
		}

		return activeUsers;
	}

	public List<UserWithStatistic> getTopCommented() {

		Integer comentCount;
		List<UserWithStatistic> comentedUsers = new ArrayList<>();

		JoinResultMap users = daoService.getJoin(null, "comment.user_id", "COUNT(comment.user_id) desc LIMIT 10",
				User.class, "comment.user_id = user.id", Comment.class);

		for (int i = 0; i < users.listSize(User.class); i++) {
			comentCount = daoService.getList("user_id = " + users.get(User.class).get(i).getId() + "", Comment.class)
					.size();

			comentedUsers.add(new UserWithStatistic(users.get(User.class).get(i), null, null, null,
					null, comentCount));
		}

		return comentedUsers;
	}
	
	public Integer getAllUsersCount(){
		Connection connection = null;
		Integer count = 0;
		
		try {
			connection = DataSource.getConnection();	
			Statement stmt = connection.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT COUNT(id) as count FROM user");
			while (rs.next()) {
				count = rs.getInt("count");
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			DataSource.returnConnection(connection);
		}
		
		return count;
	}
}
