package com.epam.lab.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmGenre;
import com.epam.lab.model.Genre;

public class RelatedFilmSearchService {
	private static GeneralDAOService generalDAOService;

	static {
		generalDAOService = GeneralDAOService.getInstance();
	}

	public List<Film> getRelatedFilms(Film currentMovie, List<Genre> genres, int amount, int year, int maxYearDifference) {
		final int genresAmount=genres.size();
		List<Film> similarFilms = null;
		List<Film> filmsToChooseFrom = null;
		if(genres!=null&&genres.size()!=0){
		filmsToChooseFrom = generalDAOService.getList(
				"year BETWEEN " + (year - maxYearDifference) + " AND " + (year + maxYearDifference), Film.class);
		filmsToChooseFrom.remove(currentMovie);
		
		Map<Film, Integer> markedFilms = new LinkedHashMap<>();

		for (Film film : filmsToChooseFrom) {
			List<FilmGenre> currentGenres = generalDAOService.getList("film_id=" + film.getId(), FilmGenre.class);
			int points = 0;
			List<Integer> genresId = new ArrayList<>();
			for (FilmGenre currentGenre : currentGenres) {
				genresId.add(currentGenre.getGenreId());
			}
			
			int index=0;
			for (Genre genre : genres) {				
				if (genresId.contains(genre.getId())) {
					points+=genresAmount-index;
				}
				++index;
			}
			markedFilms.put(film, points);
		}
		markedFilms=sortByComparator(markedFilms, false);
		similarFilms = new ArrayList<Film>();
		int counter=0;
		for(Entry<Film, Integer> entry : markedFilms.entrySet()) {
			Film currentFilm = entry.getKey();
			if(counter>=amount)break;
			similarFilms.add(currentFilm);
			++counter;
		}	
		}
		return similarFilms;
	}
	
	 private Map<Film, Integer> sortByComparator(Map<Film, Integer> markedFilms, final boolean order)
	    {
	        List<Entry<Film, Integer>> list = new LinkedList<Entry<Film, Integer>>(markedFilms.entrySet());

	        Collections.sort(list, new Comparator<Entry<Film, Integer>>()
	        {
	            public int compare(Entry<Film, Integer> o1,
	                    Entry<Film, Integer> o2)
	            {
	                if (order)
	                {
	                    return o1.getValue().compareTo(o2.getValue());
	                }
	                else
	                {
	                    return o2.getValue().compareTo(o1.getValue());

	                }
	            }
	        });

	        // Maintaining insertion order with the help of LinkedList
	        Map<Film, Integer> sortedMap = new LinkedHashMap<Film, Integer>();
	        for (Entry<Film, Integer> entry : list)
	        {
	            sortedMap.put(entry.getKey(), entry.getValue());
	        }

	        return sortedMap;
	    }
}
