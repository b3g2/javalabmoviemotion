package com.epam.lab.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dto.DetailedFilm;
import com.epam.lab.model.Country;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmCountry;
import com.epam.lab.model.FilmGenre;
import com.epam.lab.model.FilmUser;
import com.epam.lab.model.Genre;

public class SingleMovieService {
	private GeneralDAOService generalDAOService;
	private static SingleMovieService instance;

	private SingleMovieService() {
		if (generalDAOService == null) {
			generalDAOService = GeneralDAOService.getInstance();
		}
	}

	public static SingleMovieService getInstance() {
		if (instance == null) {
			instance = new SingleMovieService();
		}
		return instance;
	}

	public DetailedFilm getRecommendationByUserId(Integer userId) {
		List<FilmUser> favouritesId = generalDAOService.getList("user_id=" + userId + " AND favourite=true",
				FilmUser.class);
		int amount = favouritesId.size();
		int number;
		DetailedFilm favoriteFilm = null;
		if (amount > 0) {
			number = new Random().nextInt(amount);
			favoriteFilm = getDetailedFilmById(favouritesId.get(number).getFilmId());			
		}
		return favoriteFilm;
	}
	
	public DetailedFilm getDetailedFilmById(Integer filmId) {
		Film film = null;
		DetailedFilm detailedFilm = null;
		film = generalDAOService.get("id=" + filmId, Film.class);
		
		List<FilmGenre> filmGenres = generalDAOService.getList("film_id=" + film.getId(), FilmGenre.class);
		List<Genre> genres = new ArrayList<>();
		for (FilmGenre filmGenre : filmGenres) {
			genres.add(generalDAOService.get("id=" + filmGenre.getGenreId(), Genre.class));
		}
		
		List<FilmCountry> filmCountries = generalDAOService.getList("film_id=" + film.getId(), FilmCountry.class);
		List<Country> countries = new ArrayList<>();
		for (FilmCountry filmCountry : filmCountries) {
			countries.add(generalDAOService.get("id=" + filmCountry.getCountryId(), Country.class));
		}		
		
		detailedFilm = new DetailedFilm(film, genres, countries);

		return detailedFilm;
	}

}
