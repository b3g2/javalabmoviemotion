package com.epam.lab.service;

import java.util.ArrayList;
import java.util.List;
import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dao.GeneralDAOService.JoinResultMap;
import com.epam.lab.dto.UserWithStatistic;
import com.epam.lab.model.Comment;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmUser;
import com.epam.lab.model.Friend;
import com.epam.lab.model.User;

public class UserInfoService {

	private GeneralDAOService daoService = GeneralDAOService.getInstance();

	public UserWithStatistic getUserById(String id) {

		User user = daoService.get("id = " + id, User.class);
		Integer likeCount;
		Integer dislikeCount;
		Integer comentCount;

		likeCount = daoService.getList("user_id = " + user.getId() + " AND like_value = 'LIKED'", FilmUser.class)
				.size();
		dislikeCount = daoService.getList("user_id = " + user.getId() + " AND like_value ='DISLIKED'", FilmUser.class)
				.size();
		comentCount = daoService.getList("user_id = " + user.getId() + "", Comment.class).size();

		return new UserWithStatistic(user, null, likeCount, dislikeCount, null, comentCount);
	}

	public List<Film> getFavouriteFilms(String id) {

		List<Film> filmsFavourite = new ArrayList<>();
		List<FilmUser> filmsFavouriteForUser = new ArrayList<>(
				daoService.getList("user_id='" + id + "' AND favourite=true", FilmUser.class));

		for (FilmUser filmUser : filmsFavouriteForUser) {
			filmsFavourite.add(daoService.get("id='" + filmUser.getFilmId() + "'", Film.class));
		}
		return filmsFavourite;
	}

	public List<Film> getWatchedFilms(String id) {

		List<Film> filmsWatched = new ArrayList<>();
		List<FilmUser> filmsWathedForUser = new ArrayList<>(
				daoService.getList("user_id='" + id + "' AND status='WATCHED'", FilmUser.class));

		for (FilmUser filmUser : filmsWathedForUser) {
			filmsWatched.add(daoService.get("id='" + filmUser.getFilmId() + "'", Film.class));
		}
		return filmsWatched;
	}

	public List<Film> getScheduledFilms(String id) {

		List<Film> filmsScheduled = new ArrayList<>();
		List<FilmUser> filmsSceduledForUser = new ArrayList<>(
				daoService.getList("user_id='" + id + "' AND status='SCHEDULED'", FilmUser.class));

		for (FilmUser filmUser : filmsSceduledForUser) {
			filmsScheduled.add(daoService.get("id='" + filmUser.getFilmId() + "'", Film.class));
		}
		return filmsScheduled;
	}

	public List<Film> getLastWatchedFilms(String id) {

		List<Film> lastWatchedFilms = new ArrayList<>();

		JoinResultMap watchedFilmsForUser = daoService.getJoin("user_id = " + id + " AND status = 'WATCHED'", null,
				"film_user.id LIMIT 3", Film.class, "film.id = film_user.film_id", FilmUser.class);

		for (int i = 0; i < watchedFilmsForUser.listSize(Film.class); i++) {
			lastWatchedFilms.add(watchedFilmsForUser.get(Film.class).get(i));
		}
		return lastWatchedFilms;
	}

	public boolean isFriend(Integer followerId, Integer followeeId) {
		Friend friend = daoService.get("follower_id=" + followerId + " AND followee_id=" + followeeId + "",
				Friend.class);
		if (friend == null) {
			return false;
		} else {
			return true;
		}
	}
}
