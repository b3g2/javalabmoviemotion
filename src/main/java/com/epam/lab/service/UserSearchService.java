package com.epam.lab.service;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import com.epam.lab.dao.SpecificDAO;
import com.epam.lab.model.User;
import com.epam.lab.util.PaginationUtils;

public class UserSearchService {
	private static final String F_NAME_COLUMN = " fname LIKE ";
	private static final String L_NAME_COLUMN = " lname LIKE ";

	public List<User> doSearch(String toSearch, Integer page, Set<String> genres) {
		String query = generateQueryParam(toSearch, PaginationUtils.getOffset(page, PaginationUtils.DEFAULT_USER_COUNT),
				PaginationUtils.DEFAULT_USER_COUNT, genres);
		SpecificDAO specificDAO = new SpecificDAO();
		return specificDAO.getUsersWithOptions("SELECT DISTINCT user.* FROM user " + query);
	}

	private String generateQueryParam(String toSearch, Integer offset, Integer count, Set<String> genres) {
		StringBuilder builder = new StringBuilder();
		if (genres != null && !genres.isEmpty()) {
			builder.append(" JOIN film_user ON user.id = film_user.user_id ");
			builder.append(" JOIN film_genre ON film_user.film_id = film_genre.film_id ");
		}
		builder.append(" WHERE ");
		if (genres != null && !genres.isEmpty()) {
			builder.append(" film_user.favourite = b'1' ");
			builder.append(" AND ");

			builder.append(" film_genre.genre_id  IN (");
			Iterator<String> iterator = genres.iterator();
			while (iterator.hasNext()) {
				String string = iterator.next();
				builder.append(string);
				if (iterator.hasNext()) {
					builder.append(", ");
				}
			}
			builder.append(") ");
			builder.append(" AND ");
		}
		builder.append(" (");
		appendCondition(toSearch, builder, F_NAME_COLUMN);
		builder.append(" OR ");
		appendCondition(toSearch, builder, L_NAME_COLUMN);
		builder.append(") ");
		builder.append(" LIMIT " + offset + ", " + count);
		return builder.toString();
	}

	private void appendCondition(String toSearch, StringBuilder builder, String condition) {
		builder.append(condition);
		builder.append(" '%");
		builder.append(toSearch.trim());
		builder.append("%' ");
	}

}
