package com.epam.lab.service;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dao.GeneralDAOService.JoinResultMap;
import com.epam.lab.i18n.Localization;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmGenre;
import com.epam.lab.model.Genre;
import com.epam.lab.model.User;
import com.epam.lab.util.ServerUtil;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class UserService {

    private GeneralDAOService daoService = GeneralDAOService.getInstance();

    public void restorePassword(User user) {
        String password = ServerUtil.getRandomPassword().substring(8);
        String hashedPassword = ServerUtil.hashPassword(password);
        User updatedUser = ServerUtil.clone(user);
        updatedUser.setTempPassword(hashedPassword);
        updatedUser.setTempPasswordExpiry(LocalDateTime.now().plusDays(1).format(ServerUtil.DATE_TIME_PATTERN));
        daoService.update(user, updatedUser);
        new EmailService().mailRestorePasswordMail(user.getLanguage(), user.getFirstName(),
                user.getEmail(), password);
    }

    public void changePassword(User user, String newPassword) {
        User updatedUser = ServerUtil.clone(user);
        updatedUser.setPassword(ServerUtil.hashPassword(newPassword));
        daoService.update(user, updatedUser);
    }

    public boolean checkOldPassword(User user, String oldPassword) {
        if (oldPassword == null) return false;
        String hashedPassword = ServerUtil.hashPassword(oldPassword);
        return hashedPassword.equals(user.getPassword());
    }

    public boolean checkTempPassword(User user) {
        LocalDateTime.parse(user.getTempPasswordExpiry(), ServerUtil.DATE_TIME_PATTERN_EX);
        boolean valid = !Duration
                .between(LocalDateTime.now(),
                        LocalDateTime.parse(user.getTempPasswordExpiry(), ServerUtil.DATE_TIME_PATTERN_EX))
                .isNegative();
        User updatedUser = ServerUtil.clone(user);
        updatedUser.setTempPassword(null);
        updatedUser.setTempPasswordExpiry(null);
        daoService.update(user, updatedUser);
        return valid;
    }

    public void signIn(HttpServletRequest request, HttpServletResponse response, User user) {
        request.getSession().setAttribute("user", user);
        User updatedUser = ServerUtil.clone(user);
        updatedUser.setSessionId(ServerUtil.getRandomID());
        ServerUtil.setCookie("userID", String.valueOf(updatedUser.getId()), response);
        ServerUtil.setCookie("sessionID", String.valueOf(updatedUser.getSessionId()), response);
        daoService.update(user, updatedUser);
    }

    public void signUp(User user) {
        user.setRole("user");
        user.setEmail(user.getEmail().toLowerCase());
        if (user.getPassword() != null) {
            user.setPassword(ServerUtil.hashPassword(user.getPassword()));
        }
        daoService.add(user);
        new EmailService().mailSignUp(user.getLanguage(), user.getFirstName(), user.getEmail());
    }

    public void logOut(HttpServletRequest request, HttpServletResponse response) {
        User user = (User) request.getSession().getAttribute("user");
        daoService.update("id = \"" + user.getId() + "\"", "session_id = null", User.class);
        request.getSession().removeAttribute("user");
        ServerUtil.removeCookie("userID", request, response);
        ServerUtil.removeCookie("sessionID", request, response);
    }

    public void updatePhoto(User user, String photoURL) {
        String oldPhotoURL = user.getPhotoURL();
        if ((oldPhotoURL == null && photoURL != null) || (oldPhotoURL != null && photoURL == null)
                || (oldPhotoURL != null && !oldPhotoURL.equals(photoURL))) {
            User updatedUser = ServerUtil.clone(user);
            updatedUser.setPhotoURL(photoURL);
            daoService.update(user, updatedUser);
        }
    }

    public User getUser(String email) {
        return daoService.get("email = \"" + email.toLowerCase() + "\"", User.class);
    }

    @SuppressWarnings("unchecked")
    public JSONArray getGenreStatistic(List<Film> filmsWatched, HttpServletRequest request) {

        HttpSession session = request.getSession();
        StringBuilder filmId = new StringBuilder();
        JSONArray genreStatistic = new JSONArray();
        JSONObject genreSingle;
        List<String> colors = new ArrayList<>();
        Integer count;

        colors.add("#5CB85C");
        colors.add("#DA6034");
        colors.add("#D9534F");
        colors.add("#F0AD4E");
        colors.add("#003B64");
        colors.add("#007DD3");
        colors.add("#A52A2A");
        colors.add("#4B0082");
        colors.add("#8B008B");
        colors.add("#8B4513");
        colors.add("#BC8F8F");
        colors.add("#778899");
        colors.add("#00FFFF");
        colors.add("#B8860B");

        for (Film film : filmsWatched) {
            filmId.append(film.getId() + ", ");
        }
        filmId.replace(filmId.length() - 2, filmId.length(), "");

        JoinResultMap genre = daoService.getJoin("film_genre.film_id in (" + filmId.toString() + ")", "genre.name",
                null, Genre.class, "genre.id = film_genre.genre_id", FilmGenre.class);

        for (int i = 0; i < genre.listSize(Genre.class); i++) {

            JoinResultMap genredCount = daoService.getJoin(
                    "film_genre.genre_id = " + genre.get(Genre.class).get(i).getId() + " AND film_genre.film_id in ("
                            + filmId.toString() + ")",
                    null, null, Genre.class, "genre.id = film_genre.genre_id", FilmGenre.class);

            count = genredCount.get(Genre.class).size();
            genreSingle = new JSONObject();
            genreSingle.put("value", count);
            genreSingle.put("color", colors.get(i));
            genreSingle.put("highlight", "#d8d8d8");
            Localization locale = (Localization) session.getAttribute("locale");
            String language = locale.getLanguage();
            if (language.equals("en")) {
                genreSingle.put("label", genre.get(Genre.class).get(i).getName());
            } else {
                genreSingle.put("label", genre.get(Genre.class).get(i).getNameUK());
            }
            genreStatistic.add(genreSingle);
        }

        return genreStatistic;
    }
}
