package com.epam.lab.service;

import java.util.ArrayList;
import java.util.List;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dao.GeneralDAOService.JoinResultMap;
import com.epam.lab.dto.DetailedShowing;
import com.epam.lab.model.Cinema;
import com.epam.lab.model.FilmCinema;
import com.google.common.base.Strings;

public class WatchingLocationFindService {
	private static GeneralDAOService generalDAOService;

	static {
		generalDAOService = GeneralDAOService.getInstance();
	}

	public List<DetailedShowing> getWatchingLocations(Integer filmId) {
		JoinResultMap joinResultMap = generalDAOService.getJoin("film_id=" + filmId, FilmCinema.class, "film_cinema.cinema_id=cinema.id", Cinema.class);
		List<FilmCinema> filmCinemas = joinResultMap.get(FilmCinema.class);
		List<Cinema> cinemas = joinResultMap.get(Cinema.class);
		List<DetailedShowing> detailedShowings = new ArrayList<>();
		
		for(int i=0;i<filmCinemas.size();++i){
			DetailedShowing currentShowing = new DetailedShowing(filmCinemas.get(i),cinemas.get(i));
			detailedShowings.add(currentShowing);
		}		
		return detailedShowings;
	}
	
	public Cinema getCinemaByCityAndName(String city, String name){
		boolean byCity = !(Strings.isNullOrEmpty(city));
		boolean byName = !(Strings.isNullOrEmpty(name));
		String clause="";
		if(byCity){
		clause = "(city='"+city+"')";
		}
		if(byCity&&byName){
			clause+=" AND ";
		}
		if(byName){
			clause += "(name='"+name+"')";
		}		
		List<Cinema> cinemas = generalDAOService.getList(clause, Cinema.class);
		
		Cinema toReturn = (cinemas!=null&&cinemas.size()==1)?cinemas.get(0):null;
		return toReturn;
	}
}
