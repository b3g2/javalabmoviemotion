package com.epam.lab.transformer;

import com.epam.lab.annotation.ColumnName;
import com.epam.lab.annotation.TableName;
import com.epam.lab.dao.GeneralDAOService.JoinResultMap;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public final class GeneralTransformer {
    private static GeneralTransformer instance = null;

    private GeneralTransformer() {
    }

    public static GeneralTransformer getInstance() {
        if (instance == null) {
            instance = new GeneralTransformer();
        }
        return instance;
    }

    /**
     * @return <b>list of objects</b> of records in ResultSet or <b>empty list</b> if ResultSet is empty
     */
    public List<Object> transformToList(ResultSet rs, Class clazz, boolean addColumnPrefix) {
        final List<Object> resultList = new ArrayList<>();
        final String prefix = getTableName(clazz) + "_";
        try {
            //get empty constructor
            Constructor constructor = clazz.getDeclaredConstructor();
            //get all columns (fields annotated ColumnName)
            List<Field> fields = getColumns(clazz.getDeclaredFields());
            //get number of fields (columns)
            final int numberOfFields = fields.size();
            //fields setters
            List<Method> setters = new ArrayList<>(numberOfFields);
            //fields types
            List<Class> types = new ArrayList<>(numberOfFields);
            //columns names
            List<String> columns = new ArrayList<>(numberOfFields);
            //init (get columns names, field types and field setters)
            for (Field field : fields) {
                //get field column name
                columns.add(getColumnName(field));
                //get field type
                Class fieldType = field.getType();
                //get field setter
                setters.add(clazz.getDeclaredMethod(getSetterName(field.getName()), fieldType));
                types.add(fieldType);
            }
            //get indexes of columns in ResultSet
            int[] indexes = new int[columns.size()];
            for (int i = 0; i < indexes.length; i++) {
                if (addColumnPrefix) {
                    indexes[i] = rs.findColumn(prefix + columns.get(i));
                } else {
                    indexes[i] = rs.findColumn(columns.get(i));
                }
            }
            //create elements from ResultSet
            while (rs.next()) {
                //create new element
                Object el = constructor.newInstance();
                //read record from ResultSet and set values to created element
                readRecord(rs, indexes, setters, types, el);
                resultList.add(el);
            }
        } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | SQLException | InvocationTargetException e) {
            e.printStackTrace();
        }
        return resultList;
    }

    public List<Object> transformToList(ResultSet rs, Class clazz) {
        return transformToList(rs, clazz, false);
    }

    /**
     * @return <b>object</b> of first record in ResultSet or <b>null</b> if ResultSet is empty
     */

    public Object transformToObject(ResultSet rs, Class clazz) {
        List<Object> resultList = transformToList(rs, clazz);
        if (resultList.isEmpty()) {
            return null;
        } else {
            return resultList.get(0);
        }
    }

    public JoinResultMap transformToJoinedArray(ResultSet rs, Class[] joinedClasses) throws SQLException {
        JoinResultMap resultMap = new JoinResultMap(joinedClasses.length);
        for (Class joinedClass : joinedClasses) {
            rs.beforeFirst();
            resultMap.put(joinedClass, transformToList(rs, joinedClass, true));
        }
        return resultMap;
    }

    private String getSetterName(String fieldName) {
        return "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
    }

    /**
     * return list of fields annotated ColumnName
     */
    public List<Field> getColumns(Field[] fields) {
        List<Field> list = new ArrayList<>();
        for (Field field : fields) {
            if (field.isAnnotationPresent(ColumnName.class)) {
                list.add(field);
            }
        }
        return list;
    }

    public String getTableName(Class clazz) {
        return ((TableName) clazz.getDeclaredAnnotation(TableName.class)).value();
    }

    /**
     * @important: don't check for ColumnName annotation present
     * <p>
     * return value of ColumnName annotation
     */
    public String getColumnName(Field field) {
        return field.getDeclaredAnnotation(ColumnName.class).value();
    }

    /**
     * read one record from ResultSet and set into object
     */
    private <T> void readRecord(ResultSet rs, int[] indexes, List<Method> setters, List<Class> types, T object) throws SQLException, InvocationTargetException, IllegalAccessException {
        for (int i = 0; i < indexes.length; i++) {
            setters.get(i).invoke(object, getValueFromResultSet(rs, types.get(i), indexes[i]));
        }
    }

    /**
     * convert column value to specific type of object field
     */
    private Object getValueFromResultSet(ResultSet rs, Class type, int index) throws SQLException {
        Object result = null;
        if (type == String.class) {
            result = rs.getString(index);
        } else if (type == Integer.class) {
            result = rs.getObject(index);
        } else if (type == Double.class) {
            result = rs.getObject(index);
        } else if (type == LocalDate.class) {
            result = rs.getDate(index).toLocalDate();
        } else if (type == LocalTime.class) {
            result = rs.getTime(index).toLocalTime();
        } else if (type == LocalDateTime.class) {
            result = rs.getTimestamp(index).toLocalDateTime();
        } else if (type == Boolean.class) {
            result = rs.getBoolean(index);
        }
        //...................................................................requires completion!!!
        return result;
    }
}
