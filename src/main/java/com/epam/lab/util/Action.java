package com.epam.lab.util;

import java.io.IOException;

@FunctionalInterface
public interface Action {
    Object perform() throws IOException;
}
