package com.epam.lab.util;

import java.io.IOException;

@FunctionalInterface
public interface ActionVoid {
    void perform() throws IOException;
}
