package com.epam.lab.util;

import java.time.LocalDateTime;
import com.epam.lab.service.FilmReleaseNotifierService;

public class FilmReleaseNotifierRunnable implements Runnable {

	FilmReleaseNotifierService notifierService = new FilmReleaseNotifierService();
	private static final Integer TIMEOUT = 60*60*1000;

	@Override
	public void run() {
		while (true) {
			try {
				LocalDateTime localDateTime = LocalDateTime.now();
				 if (localDateTime.getHour() >= 14) {
					notifierService.sendNotificationsByDate(localDateTime.toLocalDate().plusDays(1), null);
				}
				Thread.sleep(TIMEOUT);// check and sleep for 1 hour
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}
}
