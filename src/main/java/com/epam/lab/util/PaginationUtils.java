package com.epam.lab.util;

public class PaginationUtils {
	public static final int DEFAULT_PAGE = 0;
	public static final int DEFAULT_FILM_COUNT = 3 * 3;
	public static final int DEFAULT_USER_COUNT = 3 * 4;
	
	public static Integer getOffset(Integer page, int count) {
		return page * count;
	}
}
