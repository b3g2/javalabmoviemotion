package com.epam.lab.util;

import com.epam.lab.exception.CloneObjectException;
import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.mail.DefaultAuthenticator;
import org.apache.commons.mail.Email;
import org.apache.commons.mail.EmailException;
import org.apache.commons.mail.HtmlEmail;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Properties;
import java.util.Random;

public class ServerUtil {
	public static String deployRealPath;
	public final static String SMTP_HOST = "smtp.gmail.com";
	public final static String MAIN_MAIL = "moviemotion.epam@gmail.com";
	public final static String PASSWORD = "moviemotionproject";
	public final static String CONTACT_MAIL = "contact.moviemotion.epam@gmail.com";
	public final static int SMTP_PORT = 465;

	public final static DateTimeFormatter DATE_TIME_PATTERN_EX = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss[.n]");
	public final static DateTimeFormatter DATE_TIME_PATTERN = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
	public final static DateTimeFormatter DATE_PATTERN = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	private static final int MONTH = 60 * 60 * 24 * 31;
	private final static String SALT = "movie_motion";

	public static void setCookie(String name, String value, HttpServletResponse response) {
		Cookie cookie = new Cookie(name, value);
		cookie.setMaxAge(MONTH); // store cookie for 1 month
		response.addCookie(cookie);
	}

	public static String getCookie(String name, HttpServletRequest request) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (name.equals(cookie.getName())) {
					return cookie.getValue();
				}
			}
		}
		return null;
	}

	public static void removeCookie(String name, HttpServletRequest request, HttpServletResponse response) {
		Cookie[] cookies = request.getCookies();
		if (cookies != null) {
			for (Cookie cookie : cookies) {
				if (name.equals(cookie.getName())) {
					cookie.setValue("");
					cookie.setMaxAge(0);
					response.addCookie(cookie);
					return;
				}
			}
		}
	}

	@SuppressWarnings({ "unchecked" })
	public static <T extends Serializable> T clone(T object) {
		try {
			ByteArrayOutputStream byteArrayOut = new ByteArrayOutputStream();
			ObjectOutputStream out = new ObjectOutputStream(byteArrayOut);
			out.writeObject(object);
			ByteArrayInputStream byteArrayIn = new ByteArrayInputStream(byteArrayOut.toByteArray());
			ObjectInputStream in = new ObjectInputStream(byteArrayIn);
			return (T) in.readObject();
		} catch (Exception e) {
			throw new CloneObjectException(e);
		}
	}

	public static String hashPassword(String password) {
		return DigestUtils.md5Hex(password + SALT);
	}

	public static void sendEmail(String from, String to, String subject, String msg) {
		new Thread(() -> {
			try {
				Email email = new HtmlEmail();
				email.setHostName(SMTP_HOST);
				email.setSmtpPort(SMTP_PORT);
				email.setAuthenticator(new DefaultAuthenticator(from, PASSWORD));
				email.setSSLOnConnect(true);
				email.setFrom(from);
				email.setSubject(subject);
				email.setMsg(msg);
				email.setCharset("utf-8");
				email.addTo(to);
				email.setStartTLSEnabled(true);
				email.send();
			} catch (EmailException e) {
				e.printStackTrace();
			}
		}).start();
	}
	
	public static void sendHTMLEmail(String from, String to, String subject, String msg) {
		new Thread(() -> {
			try {
				Properties props = new Properties();
				props.put("mail.smtp.auth", "true");
				props.put("mail.smtp.starttls.enable", "true");
				props.put("mail.smtp.host", SMTP_HOST);
				props.put("mail.smtp.port", "587");
				props.put("mail.user", from);
				props.put("mail.password", PASSWORD);

				Session session = Session.getInstance(props, new javax.mail.Authenticator() {
					protected PasswordAuthentication getPasswordAuthentication() {
						return new PasswordAuthentication(from, PASSWORD);
					}
				});
				Message message = new MimeMessage(session);

				message.setFrom(new InternetAddress(from));
				InternetAddress[] toAddresses = { new InternetAddress(to) };
				message.setRecipients(Message.RecipientType.TO, toAddresses);

				message.setSentDate(new Date());
				message.setSubject(subject);

				MimeBodyPart messageBodyPart = new MimeBodyPart();
				messageBodyPart.setContent(msg.toString(), "text/html; charset=utf-8");
				
				Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(messageBodyPart);

				MimeBodyPart imagePart = new MimeBodyPart();
				imagePart.setHeader("Content-ID", "<image>");
				imagePart.setDisposition(MimeBodyPart.INLINE);
				imagePart.attachFile(
						ServerUtil.deployRealPath + "/images/divider.gif");
				multipart.addBodyPart(imagePart);

				MimeBodyPart imagePart2 = new MimeBodyPart();
				imagePart2.setHeader("Content-ID", "<logo>");
				imagePart2.setDisposition(MimeBodyPart.INLINE);
				imagePart2.attachFile(
						ServerUtil.deployRealPath + "/images/mm_logo.png");
				multipart.addBodyPart(imagePart2);
				message.setContent(multipart);

				Transport.send(message);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}).start();
	}

	public static String getRandomPassword() {
		return Long.toHexString(Double.doubleToLongBits(Math.random()));
	}

	public static int getRandomID() {
		return new Random().nextInt(); 
	}
}
