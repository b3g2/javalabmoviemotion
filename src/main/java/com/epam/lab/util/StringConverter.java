package com.epam.lab.util;

@FunctionalInterface
public interface StringConverter {
    String convert(Object o);
}
