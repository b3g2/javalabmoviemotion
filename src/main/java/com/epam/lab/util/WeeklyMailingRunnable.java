package com.epam.lab.util;

import com.epam.lab.autoupdater.updater.FilmsUpdater;

import java.io.IOException;
import java.time.LocalDateTime;

public class WeeklyMailingRunnable implements Runnable {
    private final static int HOUR = 1000 * 60 * 60;

    @Override
    public void run() {
        FilmsUpdater filmsUpdater = new FilmsUpdater();
        while (true) {
            try {
                LocalDateTime now = LocalDateTime.now();
                if (now.getHour() == 9 && now.getDayOfWeek().getValue() == 1) {
                    new Thread(() -> {
                        try {
                            filmsUpdater.updateAll();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }).start();
                }
                Thread.sleep(HOUR);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
