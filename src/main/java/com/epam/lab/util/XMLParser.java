package com.epam.lab.util;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class XMLParser {

    private String xml;
    private String tag;

    public XMLParser(String xml) {
        this.xml = xml;
    }

    public XMLParser(String xml, String tag) {
        this.xml = xml;
        this.tag = tag;
    }

    public XMLParser find(String tag) {
        XMLParser result = null;
        Pattern patternOpen = Pattern.compile("<" + tag + "[^><]*>");
        Matcher matcherOpen = patternOpen.matcher(xml);
        if (matcherOpen.find()) {
            String tagXml = xml.substring(matcherOpen.start());
            //find close tag
            Pattern patternClose = Pattern.compile("[^><]*</" + tag + ">");
            Matcher matcherClose = patternClose.matcher(tagXml);
            if (matcherClose.find()) {
                result = new XMLParser(tagXml.substring(0, matcherClose.end()), tag);
            } else {
                result = new XMLParser(tagXml.substring(0, matcherOpen.end()), tag);
            }
        }
        return result;
    }

    public String text() {
        if (tag == null) return null;
        String result = null;
        Pattern patternOpen = Pattern.compile("<" + tag + "[^><]*>");
        Matcher matcherOpen = patternOpen.matcher(xml);
        if (matcherOpen.find()) {
            //find close tag
            Pattern patternClose = Pattern.compile("</" + tag + ">");
            Matcher matcherClose = patternClose.matcher(xml);
            if (matcherClose.find()) {
                result = xml.substring(matcherOpen.end(), matcherClose.start());
            }
        }
        return result;
    }

    public String attr(String attr) {
        if (tag == null) return null;
        String result = null;
        Pattern patternAttr = Pattern.compile(attr + "(=(\'|\")[^><\'\"]*('|\"))");
        Matcher matcherAttr = patternAttr.matcher(xml);
        if (matcherAttr.find()) {
            if (matcherAttr.start() + attr.length() + 1 < matcherAttr.end()) {
                result = xml.substring(matcherAttr.start() + attr.length() + 3, matcherAttr.end());
            } else {
                result = "";
            }
        }
        return result;
    }
}
