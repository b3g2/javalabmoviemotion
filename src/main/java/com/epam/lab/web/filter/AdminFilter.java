package com.epam.lab.web.filter;

import com.epam.lab.model.User;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class AdminFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpSession httpSession = ((HttpServletRequest) request).getSession();
        User user = (User) httpSession.getAttribute("user");
        if (user != null && user.getRole().equals("administrator")) {
            chain.doFilter(request, response);
        } else {
            ((HttpServletResponse)response).sendRedirect("home");
        }
    }

    @Override
    public void destroy() {
    }
}
