package com.epam.lab.web.filter;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.lab.model.User;

public class ModeratorFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		HttpSession httpSession = ((HttpServletRequest) request).getSession(true);
		User user = (User) httpSession.getAttribute("user");
		if (user != null && user.getRole().equals("moderator")) {
			chain.doFilter(request, response);

		} else {
			((HttpServletResponse) response).sendRedirect("home");
		}
	}

	@Override
	public void destroy() {		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {	
	}
}
