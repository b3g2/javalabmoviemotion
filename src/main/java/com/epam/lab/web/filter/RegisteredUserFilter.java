package com.epam.lab.web.filter;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import com.epam.lab.model.User;

public class RegisteredUserFilter implements Filter {

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {

	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {

		HttpSession httpSession = ((HttpServletRequest) request).getSession(true);
		User user = (User) httpSession.getAttribute("user");

		if (user != null) {
			chain.doFilter(request, response);
		} else {
			request.setAttribute("open_login_modal", true);
			request.getRequestDispatcher("WEB-INF/jsp/index.jsp").forward(request, response);
		}
	}

	@Override
	public void destroy() {
	}
}
