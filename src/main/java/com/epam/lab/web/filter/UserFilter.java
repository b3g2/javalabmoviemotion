package com.epam.lab.web.filter;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.i18n.Localization;
import com.epam.lab.model.User;
import com.epam.lab.util.ServerUtil;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class UserFilter implements Filter {
	public void destroy() {
	}

	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain)
			throws ServletException, IOException {
		checkLocalization((HttpServletRequest) req, (HttpServletResponse) resp);
		checkUser((HttpServletRequest) req);
		chain.doFilter(req, resp);
	}

	public void init(FilterConfig config) throws ServletException {

	}

	private void checkLocalization(HttpServletRequest req, HttpServletResponse resp) {
		final String resourceBundleBaseName = "content";
		final String cookiesLanguageAttrName = "language";
		final String sessionL10nAttrName = "locale";

		HttpSession session = req.getSession();

		String newLang = req.getParameter("lang");
		Localization locale;
		User user = (User) session.getAttribute("user");
		if (newLang != null) {
			// if user changed lang
			locale = new Localization(resourceBundleBaseName, newLang);
			session.setAttribute(sessionL10nAttrName, locale);
			updateUserLang(user, newLang, session);
			ServerUtil.setCookie(cookiesLanguageAttrName, newLang, resp);
		} else {
			// check is locale in session
			locale = (Localization) session.getAttribute(sessionL10nAttrName);
			if (locale == null) {
				String lang;
				if (user != null) {
					lang = user.getLanguage();
				} else {
					// if user not authorized
					// try to get language from cookies
					lang = ServerUtil.getCookie(cookiesLanguageAttrName, req);
					if (lang == null) {
						// if isn't language in cookies
						// get language from system settings
						lang = req.getLocale().getLanguage();
					}
				}
				// save locale to session
				locale = new Localization(resourceBundleBaseName, lang);
				session.setAttribute(sessionL10nAttrName, locale);
				// save language to cookies
				ServerUtil.setCookie(cookiesLanguageAttrName, lang, resp);
			}
		}
	}

	private void updateUserLang(User user, String lang, HttpSession session) {
		if (user != null) {
			User updatedUser = ServerUtil.clone(user);
			updatedUser.setLanguage(lang);
			GeneralDAOService.getInstance().update(user, updatedUser);
			session.setAttribute("user", updatedUser);
		}
	}

	private void checkUser(HttpServletRequest req) {
		if (req.getSession().getAttribute("user") == null) {
			String userID = ServerUtil.getCookie("userID", req);
			String sessionID = ServerUtil.getCookie("sessionID", req);
			if (userID != null && sessionID != null) {
				User user = GeneralDAOService.getInstance().get("id = \"" + userID + "\"", User.class);
				if (user != null && user.getSessionId() != null
						&& sessionID.equals(String.valueOf(user.getSessionId()))) {
					req.getSession().setAttribute("user", user);
				}
			}
		}
		User user = (User) req.getSession().getAttribute("user");
		if (user != null) {
			user = GeneralDAOService.getInstance().get("id = \"" + user.getId() + "\"", User.class);
			req.setAttribute("open_ban_modal", user.getBan());
		}
	}

}
