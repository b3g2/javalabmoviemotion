package com.epam.lab.web.listener;

import com.epam.lab.util.FilmAutoUpdaterRunnable;
import com.epam.lab.util.FilmReleaseNotifierRunnable;
import com.epam.lab.util.ServerUtil;
import com.epam.lab.util.WeeklyMailingRunnable;
import org.apache.log4j.PropertyConfigurator;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.File;

public class ContextListener implements ServletContextListener {

    /*
     * Initialize log4j when the application is being started
     */
    @Override
    public void contextInitialized(ServletContextEvent sce) {

        ServletContext context = sce.getServletContext();
        String log4jConfigFile = context.getInitParameter("log4j-config-location");
        String fullPath = context.getRealPath("") + File.separator + log4jConfigFile;
        ServerUtil.deployRealPath = context.getRealPath("");

        PropertyConfigurator.configure(fullPath);

        Thread weeklyMailing = new Thread(new WeeklyMailingRunnable());
        weeklyMailing.setDaemon(true);
        weeklyMailing.start();


        Thread emailFilmNotifier = new Thread(new FilmReleaseNotifierRunnable());
        emailFilmNotifier.setDaemon(true);
        emailFilmNotifier.start();

        Thread filmAutoUpdater = new Thread(new FilmAutoUpdaterRunnable());
        filmAutoUpdater.setDaemon(true);
        filmAutoUpdater.start(); // TODO: run auto-updater
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
    }
}
