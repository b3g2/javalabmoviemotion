package com.epam.lab.web.servlet.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.lab.model.User;
import com.epam.lab.service.CinemaNewsService;
import com.google.gson.Gson;

public class AddCinemaNewsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AddCinemaNewsServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer userId = ((User) request.getSession().getAttribute("user")).getId();
		Integer cinemaId = Integer.valueOf(request.getParameter("cinemaId"));
		String newsText = request.getParameter("newsText");
		String newsHeader = request.getParameter("newsHeader");	
		String newsPictureURL = request.getParameter("newsPicture");
		if("".equals(newsHeader)){
			newsHeader=null;
		}
		if("".equals(newsPictureURL)){
			newsPictureURL=null;
		}
		Integer addedAmount = new CinemaNewsService().addNews(userId, cinemaId, newsText, newsHeader, newsPictureURL);	
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(addedAmount));
	}

}
