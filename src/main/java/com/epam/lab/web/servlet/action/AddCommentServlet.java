package com.epam.lab.web.servlet.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.model.Comment;

public class AddCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public AddCommentServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer userId=Integer.valueOf(request.getParameter("userId"));
		Integer filmId=Integer.valueOf(request.getParameter("filmId"));
		String content=request.getParameter("content");
		JSONObject jsonObject = new JSONObject();
		Comment commentToAdd = new Comment(userId,filmId,content);
		GeneralDAOService generalDAOService = GeneralDAOService.getInstance();
		jsonObject.put("successfully",generalDAOService.add(commentToAdd)==1);
		
		String addedDateTime = commentToAdd.getAddedDateTime();
		String clause = "film_id="+filmId+" AND user_id="+userId+" AND added_date_time='"+addedDateTime+"'";
		Comment addedComment = generalDAOService.get(clause, Comment.class);
		
		int id=(addedComment!=null?addedComment.getId():null);
		
		jsonObject.put("commentId",id);
		jsonObject.put("dateTime", addedDateTime);
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonObject.toString());
	}

}
