package com.epam.lab.web.servlet.action;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dto.ExtendedCinema;
import com.epam.lab.dto.ExtendedFilm;
import com.epam.lab.model.Cinema;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmCinema;
import com.epam.lab.model.Statistic;
import com.epam.lab.model.User;

public class AddShowingServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	private List<ExtendedFilm> simple(User user) {
		GeneralDAOService daoService = GeneralDAOService.getInstance();
		List<Cinema> cinemaList = new ArrayList<>(daoService.getAll(Cinema.class));
		List<FilmCinema> filmCinema = new ArrayList<>(daoService.getAll(FilmCinema.class));
		List<ExtendedFilm> extendedFilm = new ArrayList<>();
		Cinema cinema = new Cinema();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String date1 = dateFormat.format(date);
		for (int i = 0; i < filmCinema.size(); ++i) {
			if ((cinema.getId() == filmCinema.get(i).getCinemaId())
					&& (date1.compareTo(filmCinema.get(i).getDateFinish()) <= 0)
					&& (filmCinema.get(i).getState().equals("RES"))) {
				ExtendedFilm exFilm = new ExtendedFilm();
				exFilm.setFilm(daoService.get("id='" + filmCinema.get(i).getFilmId() + "'", Film.class));
				exFilm.setDateOfStart(filmCinema.get(i).getDateStart());
				exFilm.setDateOfFinish(filmCinema.get(i).getDateFinish());
				extendedFilm.add(exFilm);
			}
		}
		return extendedFilm;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		GeneralDAOService daoService = GeneralDAOService.getInstance();
		String title = request.getParameter("title");
		String dateStart = request.getParameter("start");
		String dateFinish = request.getParameter("finish");
		String minPrice1 = request.getParameter("minprice");
		String maxPrice1 = request.getParameter("maxprice");
		String cId1 = request.getParameter("bookId");
		Integer cId = Integer.parseInt(cId1);
		double minPrice = Integer.parseInt(minPrice1);
		double maxPrice = Integer.parseInt(maxPrice1);
		Integer filmId = 0;
		List<Film> filmList = new ArrayList<>(daoService.getAll(Film.class));
		Cinema cinema = new Cinema();
		List<ExtendedFilm> extendedFilm = new ArrayList<>();
		int t = 0;
		for (int i = 0; i < filmList.size(); ++i) {
			if (title.equals(filmList.get(i).getTitle())) {
				++t;
				filmId = filmList.get(i).getId();
			}
		}
		List<FilmCinema> list = new ArrayList<>(daoService.getAll(FilmCinema.class));
		int sum = 0;
		for (int i = 0; i < list.size(); ++i) {
			if (cId.equals(list.get(i).getCinemaId())) {
				if (filmId.equals(list.get(i).getFilmId())) {
					++sum;
				}
			}
		}
		FilmCinema movie = new FilmCinema();
		movie.setCinemaId(cId);
		movie.setFilmId(filmId);
		movie.setDateStart(dateStart);
		movie.setDateFinish(dateFinish);
		movie.setMinPrice(minPrice);
		movie.setMaxPrice(maxPrice);
		movie.setState("RES");
		movie.setEdited("NO");
		cinema = daoService.get("id=" + cId, Cinema.class);
		if (t > 0) {
			if (minPrice < maxPrice) {
				if (sum == 0) {
					int n = daoService.add(movie);
					Statistic stat = new Statistic();
					stat = daoService.get("user_id=" + user.getId(), Statistic.class);
					Statistic newstat = new Statistic();
					int k = 0;
					k = stat.getAddedShowing() + 1;
					newstat.setEditedItems(stat.getEditedItems());
					newstat.setAddedPremieres(stat.getAddedPremieres());
					newstat.setBanCount(stat.getBanCount());
					newstat.setUnbanCount(stat.getUnbanCount());
					newstat.setAddedModerators(stat.getAddedModerators());
					newstat.setAddedAdmins(stat.getAddedAdmins());
					newstat.setRemovedPowers(stat.getRemovedPowers());
					newstat.setEditedShowing(stat.getEditedShowing());
					newstat.setAddedShowing(k);
					daoService.update(stat, newstat);
					request.setAttribute("addFilm", "yes");
					request.setAttribute("titFilm", title);
					request.setAttribute("cinFilm", cinema.getCinemaName());
				}
				else
				{
					request.setAttribute("error3", "true");
					request.setAttribute("titFilm", title);
					request.setAttribute("cinFilm", cinema.getCinemaName());
				}
			} else {
				request.setAttribute("error2", "true");
			}
		}
		List<Cinema> cinemaList = new ArrayList<>(daoService.getAll(Cinema.class));
		List<FilmCinema> filmCinema = new ArrayList<>(daoService.getAll(FilmCinema.class));
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String date1 = dateFormat.format(date);
		for (int j = 0; j < filmCinema.size(); ++j) {
			if ((cinema.getId() == filmCinema.get(j).getCinemaId())
					&& (date1.compareTo(filmCinema.get(j).getDateFinish()) <= 0)
					&& (filmCinema.get(j).getState().equals("RES"))) {
				ExtendedFilm exFilm = new ExtendedFilm();
				exFilm.setFilm(daoService.get("id='" + filmCinema.get(j).getFilmId() + "'", Film.class));
				exFilm.setDateOfStart(filmCinema.get(j).getDateStart());
				exFilm.setDateOfFinish(filmCinema.get(j).getDateFinish());
				extendedFilm.add(exFilm);
			}
		}
		if (extendedFilm.size() == 0) {
			request.setAttribute("error", "true");
		}
		if (t == 0) {
			request.setAttribute("error1", "true");
			extendedFilm = simple(user);
		}
		List<ExtendedCinema> exList = new ArrayList<ExtendedCinema>();

		for (int i = 0; i < cinemaList.size(); ++i) {
			ExtendedCinema ex = new ExtendedCinema();
			ex.setCinema(cinemaList.get(i));
			int total = 0;
			int total1 = 0;
			for (int j = 0; j < filmCinema.size(); ++j) {
				if (filmCinema.get(j).getCinemaId().equals(cinemaList.get(i).getId())
						&& (date1.compareTo(filmCinema.get(j).getDateFinish()) <= 0)
						&& (filmCinema.get(j).getState().equals("RES"))) {
					++total;
				}
				if (filmCinema.get(j).getCinemaId().equals(cinemaList.get(i).getId())
						&& (date1.compareTo(filmCinema.get(j).getDateFinish()) <= 0) && (filmCinema.get(j).getState().equals("DEL"))) {
					++total1;
				}
			}
			ex.setDelete(total1);
			ex.setTotal(total);
			exList.add(ex);
		}
		request.setAttribute("cinema", exList);
		Statistic stat = new Statistic();
		stat = daoService.get("user_id=" + user.getId(), Statistic.class);
		int my_edited = stat.getEditedShowing();
		int my_added = stat.getAddedShowing();
		List<Statistic> statList = new ArrayList<Statistic>();
		statList = daoService.getAll(Statistic.class);
		int all_edited = 0;
		int all_added = 0;
		for (int i = 0; i < statList.size(); ++i) {
			all_added = all_added + statList.get(i).getAddedShowing();
			all_edited = all_edited + statList.get(i).getEditedShowing();
		}
		request.setAttribute("my_edited", my_edited);
		request.setAttribute("my_added", my_added);
		request.setAttribute("all_edited", all_edited);
		request.setAttribute("all_added", all_added);
		request.setAttribute("moderator_content", "cinema-worker");
		request.getRequestDispatcher("WEB-INF/jsp/moderator.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
