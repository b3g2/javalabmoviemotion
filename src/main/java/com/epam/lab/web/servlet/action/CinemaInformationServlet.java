package com.epam.lab.web.servlet.action;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dto.ExtendedFilm;
import com.epam.lab.model.Cinema;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmCinema;

public class CinemaInformationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Integer id = Integer.parseInt(request.getParameter("id"));
		request.setAttribute("idc", id);
		GeneralDAOService daoService = GeneralDAOService.getInstance();
		Cinema cinema = daoService.get("id = " + id, Cinema.class);
		List<FilmCinema> films = daoService.getList("cinema_id = " + id, FilmCinema.class);
		List<ExtendedFilm> extendedFilm = new ArrayList<>();
		ExtendedFilm exFilm;

		request.setAttribute("cinema", cinema);

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		String date = dateFormat.format(new Date());

		for (FilmCinema film : films) {
			if (date.compareTo(film.getDateFinish()) <= 0) {
				exFilm = new ExtendedFilm(daoService.get("id='" + film.getFilmId() + "'", Film.class),
						film.getDateStart(), film.getDateFinish(), film.getMinPrice(), film.getMaxPrice(),
						film.getState());
				extendedFilm.add(exFilm);
			}
		}

		if (extendedFilm.size() == 0) {
			request.setAttribute("error", "true");
		} else {
			request.setAttribute("films", extendedFilm);
		}
		request.getRequestDispatcher("WEB-INF/jsp/information.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}
}