package com.epam.lab.web.servlet.action;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dto.ExtendedCinema;
import com.epam.lab.dto.ExtendedFilm;
import com.epam.lab.model.Cinema;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmCinema;
import com.epam.lab.model.Statistic;
import com.epam.lab.model.User;
import com.google.gson.Gson;

public class CinemaWorkerServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public CinemaWorkerServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	private List<ExtendedFilm> simple(User user) {
		GeneralDAOService daoService = GeneralDAOService.getInstance();
		List<Cinema> cinemaList = new ArrayList<>(daoService.getAll(Cinema.class));
		List<FilmCinema> filmCinema = new ArrayList<>(daoService.getAll(FilmCinema.class));
		List<ExtendedFilm> extendedFilm = new ArrayList<>();
		Cinema cinema = new Cinema();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String date1 = dateFormat.format(date);
		for (int i = 0; i < filmCinema.size(); ++i) {
			if ((cinema.getId() == filmCinema.get(i).getCinemaId())
					&& (date1.compareTo(filmCinema.get(i).getDateFinish()) <= 0)) {
				ExtendedFilm exFilm = new ExtendedFilm();
				exFilm.setFilm(daoService.get("id='" + filmCinema.get(i).getFilmId() + "'", Film.class));
				exFilm.setDateOfStart(filmCinema.get(i).getDateStart());
				exFilm.setDateOfFinish(filmCinema.get(i).getDateFinish());
				extendedFilm.add(exFilm);
			}
		}
		return extendedFilm;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		HttpSession session = request.getSession();
		GeneralDAOService daoService = GeneralDAOService.getInstance();
		User user = (User) session.getAttribute("user");
		Statistic stat = new Statistic();
		stat = daoService.get("user_id='" + user.getId() + "'", Statistic.class);
		int my_edited = stat.getEditedShowing();
		int my_added = stat.getAddedShowing();
		List<Statistic> statList = new ArrayList<Statistic>();
		statList = daoService.getAll(Statistic.class);
		int all_edited = 0;
		int all_added = 0;
		for (int i = 0; i < statList.size(); ++i)
		{
			all_added = all_added + statList.get(i).getAddedShowing();
			all_edited = all_edited + statList.get(i).getEditedShowing();
		}
		request.setAttribute("my_edited", my_edited);
		request.setAttribute("my_added", my_added);
		request.setAttribute("all_edited", all_edited);
		request.setAttribute("all_added", all_added);
		String title = request.getParameter("tit");
		
		List<ExtendedFilm> extendedFilm = simple(user);
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String date1 = dateFormat.format(date);
		List<FilmCinema> filmCinema = new ArrayList<>(daoService.getAll(FilmCinema.class));
		if (request.getParameter("filter_cinema") != null) {
			List<Film> filmList = new ArrayList<>(daoService.getAll(Film.class));
			for (int j = filmList.size() - 1; j >= 0; j--) {
				for (int i = 0; i < filmCinema.size(); ++i) {
					if (filmList.get(j).getId().equals(filmCinema.get(i).getFilmId())) {
						filmList.remove(filmList.get(j));
					}
				}
			}
			response.getWriter().write(new Gson().toJson(filmList));
		} else {
			if (request.getParameter("filter_movies") != null) {
				List<Cinema> filmList = new ArrayList<>(daoService.getAll(Cinema.class));

				response.getWriter().write(new Gson().toJson(filmList));

			} else {

				List<Cinema> cinema2 = new ArrayList<>(daoService.getAll(Cinema.class));
				List<Cinema> choose = new ArrayList<Cinema>();

				if (title != null) {
					for (int i = 0; i < cinema2.size(); ++i) {
						if (title.equals(cinema2.get(i).getCinemaName())) {
							choose.add(cinema2.get(i));
						}
					}
					List<Cinema> c1 = new ArrayList<Cinema>();
					for (int i = 0; i < choose.size() / 3; ++i) {
						c1.add(choose.get(i));
					}
					request.setAttribute("c1", c1);
					List<Cinema> c2 = new ArrayList<Cinema>();
					int k = choose.size() / 3 + 1;

					for (int j = choose.size() / 3 + 1; j < k + choose.size() / 3; ++j) {
						c2.add(choose.get(j));
					}
					request.setAttribute("c2", c2);
					List<Cinema> c3 = new ArrayList<Cinema>();
					int n = choose.size() / 3 + 1 + choose.size() / 3 + 1;
					for (int j = n; j < choose.size(); ++j) {
						c3.add(choose.get(j));
					}
					request.setAttribute("c3", c3);
					List<ExtendedCinema> exList = new ArrayList<ExtendedCinema>();

					for (int i = 0; i < choose.size(); ++i) {
						ExtendedCinema ex = new ExtendedCinema();
						ex.setCinema(choose.get(i));
						int total = 0;
						int total1 = 0;
						for (int j = 0; j < filmCinema.size(); ++j) {
							if (filmCinema.get(j).getCinemaId().equals(choose.get(i).getId())
									&& (date1.compareTo(filmCinema.get(j).getDateFinish()) <= 0) && (filmCinema.get(j).getState().equals("RES"))) {
								++total;
							}
							if (filmCinema.get(j).getCinemaId().equals(choose.get(i).getId())
									&& (date1.compareTo(filmCinema.get(j).getDateFinish()) <= 0) && (filmCinema.get(j).getState().equals("DEL"))) {
								++total1;
							}
						}
						ex.setDelete(total1);
						ex.setTotal(total);
						exList.add(ex);
					}
					request.setAttribute("cinema", exList);
				} else {

					List<ExtendedCinema> exList = new ArrayList<ExtendedCinema>();

					for (int i = 0; i < cinema2.size(); ++i) {
						ExtendedCinema ex = new ExtendedCinema();
						ex.setCinema(cinema2.get(i));
						int total = 0;
						int total1 = 0;
						for (int j = 0; j < filmCinema.size(); ++j) {
							if (filmCinema.get(j).getCinemaId().equals(cinema2.get(i).getId())
									&& (date1.compareTo(filmCinema.get(j).getDateFinish()) <= 0) && (filmCinema.get(j).getState().equals("RES"))) {
								++total;
							}
							if (filmCinema.get(j).getCinemaId().equals(cinema2.get(i).getId())
									&& (date1.compareTo(filmCinema.get(j).getDateFinish()) <= 0) && (filmCinema.get(j).getState().equals("DEL"))) {
								++total1;
							}
						}
						ex.setTotal(total);
						ex.setDelete(total1);
						exList.add(ex);
					}
					request.setAttribute("cinema", exList);
					List<String> city = new ArrayList<String>();
					for (int i = 0; i < cinema2.size(); ++i) {
						city.add(cinema2.get(i).getCity());
					}
					HashSet<String> uniqueSet = new HashSet<String>();
					uniqueSet.addAll(city);
					request.setAttribute("city2", uniqueSet);
					List<Cinema> c1 = new ArrayList<Cinema>();
					for (int i = 0; i < cinema2.size() / 3; ++i) {
						c1.add(cinema2.get(i));
					}
					request.setAttribute("c1", c1);
					List<Cinema> c2 = new ArrayList<Cinema>();
					int k = cinema2.size() / 3 + 1;
					for (int j = cinema2.size() / 3 + 1; j < k + cinema2.size() / 3; ++j) {
						c2.add(cinema2.get(j));
					}
					request.setAttribute("c2", c2);
					List<Cinema> c3 = new ArrayList<Cinema>();
					int n = cinema2.size() / 3 + 1 + cinema2.size() / 3 + 1;
					for (int j = n; j < cinema2.size(); ++j) {
						c3.add(cinema2.get(j));
					}
					request.setAttribute("c3", c3);
					request.setAttribute("cinema1", cinema2);

					/*
					 * DateFormat dateFormat = new
					 * SimpleDateFormat("yyyy-MM-dd"); Date date = new Date();
					 * String date1 = dateFormat.format(date); for (int i = 0; i
					 * < filmCinema.size(); ++i) { if ((cinema.getId() ==
					 * filmCinema.get(i).getCinemaId()) &&
					 * (date1.compareTo(filmCinema.get(i).getDateFinish()) <=
					 * 0)) { ExtendedFilm exFilm = new ExtendedFilm();
					 * exFilm.setFilm(daoService.get("id='" +
					 * filmCinema.get(i).getFilmId() + "'", Film.class));
					 * exFilm.setDateOfStart(filmCinema.get(i).getDateStart());
					 * exFilm.setDateOfFinish(filmCinema.get(i).getDateFinish())
					 * ; extendedFilm.add(exFilm); } }
					 */
					if (extendedFilm.size() == 0) {
						request.setAttribute("error", "true");
					}

					request.setAttribute("films", extendedFilm);

				}
				request.setAttribute("moderator_content", "cinema-worker");
				request.getRequestDispatcher("WEB-INF/jsp/moderator.jsp").forward(request, response);
			}
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
