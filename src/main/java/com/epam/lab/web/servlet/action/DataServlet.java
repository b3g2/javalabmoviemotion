package com.epam.lab.web.servlet.action;

import com.epam.lab.exception.AjaxParameterException;
import com.epam.lab.factory.ServiceFactory;
import com.epam.lab.model.Cinema;
import com.epam.lab.model.FilmCinema;
import com.epam.lab.model.User;
import com.epam.lab.service.DataService;
import com.epam.lab.util.Action;
import com.epam.lab.util.ActionVoid;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class DataServlet extends HttpServlet {

    private Gson gson = new Gson();
    private DataService dataService = ServiceFactory.getDataService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        Map<String, ActionVoid> actionMap = new HashMap<>(5);
        actionMap.put(null, () -> {
            throw new AjaxParameterException();
        }); //ignore
        actionMap.put("get", () -> get(request, response));
        actionMap.put("info", () -> info(request, response));
        actionMap.put("update", () -> update(request, response));
        actionMap.put("add", () -> add(request, response));

        String action = request.getParameter("action");
        actionMap.get(action).perform();
    }

    private void get(HttpServletRequest request, HttpServletResponse response) throws IOException {
        DataService.GetParams params = new DataService.GetParams();
        Map<String, Action> targetMap = new HashMap<>(6);
        String search = request.getParameter("search");
        String param = request.getParameter("param");

        targetMap.put(null, () -> {
            throw new AjaxParameterException();
        }); //ignore
        targetMap.put("all-users", () -> dataService.getUsers(null, params));
        targetMap.put("users", () -> dataService.getUsers("user", params));
        targetMap.put("administrators", () -> dataService.getUsers("administrator", params));
        targetMap.put("moderators", () -> dataService.getUsers("moderator", params));
        targetMap.put("films", () -> dataService.getFilms(params, true, false));
        targetMap.put("filmsForDataList", () -> dataService.getFilmsForDataList(params));
        targetMap.put("cinemas-cities", () -> dataService.getCinemasCities(toLowerCaseSafe(param), toLowerCaseSafe(search)));
        targetMap.put("cinemas-names", () -> dataService.getCinemasNames(toLowerCaseSafe(param), toLowerCaseSafe(search)));
        targetMap.put("cinema", () -> dataService.getCinema(toLowerCaseSafe(param), toLowerCaseSafe(search)));
        targetMap.put("user-statistic", () -> dataService.getUserStatistic(parseIntSafe(param)));

        if (search != null) {
            String from = request.getParameter("from");
            String amount = request.getParameter("amount");
            if (from == null) {
                from = "0";
            }
            if (amount == null) {
                params.setAll(true);
            } else {
                params.setFrom(Integer.parseInt(from));
                params.setAmount(Integer.parseInt(amount));
            }
            params.setSearch(search);
        }

        String target = request.getParameter("target");
        response.getWriter().write(gson.toJson(targetMap.get(target).perform()));
    }

    private void info(HttpServletRequest request, HttpServletResponse response) throws IOException {
        DataService.GetParams params = new DataService.GetParams();
        Map<String, Action> targetMap = new HashMap<>(5);

        targetMap.put(null, () -> {
            throw new AjaxParameterException();
        }); //ignore
        targetMap.put("all-users-amount", () -> dataService.getUsers(null, params).size());
        targetMap.put("users-amount", () -> dataService.getUsers("user", params).size());
        targetMap.put("administrators-amount", () -> dataService.getUsers("administrator", params).size());
        targetMap.put("moderators-amount", () -> dataService.getUsers("moderator", params).size());

        String search = request.getParameter("search");
        params.setAll(true);
        params.setSearch(search);

        String target = request.getParameter("target");
        response.getWriter().write(gson.toJson(targetMap.get(target).perform()));
    }

    private void update(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String, Action> targetMap = new HashMap<>(4);
        int oldId = parseIntSafe(request.getParameter("oldID"));
        int updaterId = parseIntSafe(request.getParameter("updaterId"));
        String data = request.getParameter("data");

        targetMap.put(null, () -> {
            throw new AjaxParameterException();
        }); //ignore
        targetMap.put("user-ban", () -> dataService.banUser(oldId, data, updaterId));
        targetMap.put("user-role", () -> dataService.changeUserRole(oldId, data, updaterId));
        targetMap.put("cinema", () -> dataService.updateCinema(oldId, gson.fromJson(data, Cinema.class)));
        String target = request.getParameter("target");
        response.getWriter().write(gson.toJson(targetMap.get(target).perform()));
    }

    private void add(HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String, Action> targetMap = new HashMap<>(4);
        String data = request.getParameter("data");
        User user = (User) request.getSession().getAttribute("user");

        targetMap.put(null, () -> {
            throw new AjaxParameterException();
        }); //ignore
        targetMap.put("cinema", () -> dataService.addCinema(gson.fromJson(data, Cinema.class)));
        targetMap.put("filmCinema", () -> dataService.addFilmCinema(gson.fromJson(data, FilmCinema.class), user.getId()));
        targetMap.put("subscribe", () -> dataService.subscribe(data));

        String target = request.getParameter("target");
        response.getWriter().write(gson.toJson(targetMap.get(target).perform()));
    }

    private int parseIntSafe(String s) {
        if (s != null) {
            return Integer.parseInt(s);
        } else {
            return -1;
        }
    }

    private String toLowerCaseSafe(String s) {
        if (s != null) {
            return s.toLowerCase();
        } else {
            return null;
        }
    }
}
