package com.epam.lab.web.servlet.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.model.Comment;

public class DeleteCommentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public DeleteCommentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer commentId = Integer.valueOf(request.getParameter("commentId"));
		JSONObject jsonObject = new JSONObject();
		Boolean deleted = (GeneralDAOService.getInstance().delete("id="+commentId, Comment.class)>0);
		jsonObject.put("successfully",deleted);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonObject.toString());
	}
}
