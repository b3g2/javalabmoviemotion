package com.epam.lab.web.servlet.action;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dto.ExtendedFilm;
import com.epam.lab.model.Cinema;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmCinema;
import com.epam.lab.model.User;

public class EditServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");
		GeneralDAOService daoService = GeneralDAOService.getInstance();
		List<FilmCinema> filmCinema = new ArrayList<>(daoService.getAll(FilmCinema.class));
		String del1 = request.getParameter("delete");
		String res1 = request.getParameter("restore");
		String id1 = request.getParameter("extfilm");
		Integer id = Integer.parseInt(request.getParameter("cinemaid"));
		request.setAttribute("idc", id);
		Cinema cinema = daoService.get("id =" + id, Cinema.class);

		List<ExtendedFilm> extendedFilm = new ArrayList<>();

		request.setAttribute("name", cinema.getCinemaName());
		request.setAttribute("city", cinema.getCity());
		request.setAttribute("address", cinema.getAddress());
		request.setAttribute("cinema", cinema);

		if (del1 != null) {
			Integer del = Integer.parseInt(del1);
			FilmCinema newfilm = new FilmCinema();
			for (int i = 0; i < filmCinema.size(); ++i) {
				if (del.equals(filmCinema.get(i).getFilmId())) {
					if (id.equals(filmCinema.get(i).getCinemaId())) {
						newfilm.setCinemaId(filmCinema.get(i).getCinemaId());
						newfilm.setFilmId(filmCinema.get(i).getFilmId());
						newfilm.setDateStart(filmCinema.get(i).getDateStart());
						newfilm.setDateFinish(filmCinema.get(i).getDateFinish());
						newfilm.setMinPrice(filmCinema.get(i).getMinPrice());
						newfilm.setMaxPrice(filmCinema.get(i).getMaxPrice());
						newfilm.setState("DEL");
						newfilm.setEdited(filmCinema.get(i).getEdited());
						int n = daoService.update(filmCinema.get(i), newfilm);
						filmCinema.get(i).setState("DEL");
						break;
					}
				}
			}

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String date1 = dateFormat.format(date);
			for (int i = 0; i < filmCinema.size(); ++i) {
				if ((id.equals(filmCinema.get(i).getCinemaId()))
						&& (date1.compareTo(filmCinema.get(i).getDateFinish()) <= 0)) {
					ExtendedFilm exFilm = new ExtendedFilm();
					exFilm.setFilm(daoService.get("id='" + filmCinema.get(i).getFilmId() + "'", Film.class));
					exFilm.setDateOfStart(filmCinema.get(i).getDateStart());
					exFilm.setDateOfFinish(filmCinema.get(i).getDateFinish());
					exFilm.setMinPrice(filmCinema.get(i).getMinPrice().intValue());
					exFilm.setMaxPrice(filmCinema.get(i).getMaxPrice().intValue());
					exFilm.setState(filmCinema.get(i).getState());
					extendedFilm.add(exFilm);

				}
			}
			if (extendedFilm.size() == 0) {
				request.setAttribute("error", "true");
			}
			request.setAttribute("films", extendedFilm);

			request.getRequestDispatcher("WEB-INF/jsp/information.jsp").forward(request, response);
		}
		if (res1 != null) {
			Integer res = Integer.parseInt(res1);
			FilmCinema newfilm = new FilmCinema();
			for (int i = 0; i < filmCinema.size(); ++i) {
				if (res.equals(filmCinema.get(i).getFilmId())) {
					if (id.equals(filmCinema.get(i).getCinemaId())) {
						newfilm.setCinemaId(filmCinema.get(i).getCinemaId());
						newfilm.setFilmId(filmCinema.get(i).getFilmId());
						newfilm.setDateStart(filmCinema.get(i).getDateStart());
						newfilm.setDateFinish(filmCinema.get(i).getDateFinish());
						newfilm.setMinPrice(filmCinema.get(i).getMinPrice());
						newfilm.setMaxPrice(filmCinema.get(i).getMaxPrice());
						newfilm.setState("RES");
						newfilm.setEdited(filmCinema.get(i).getEdited());
						int n = daoService.update(filmCinema.get(i), newfilm);
						filmCinema.get(i).setState("RES");
						break;
					}
				}
			}

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			Date date = new Date();
			String date1 = dateFormat.format(date);
			for (int i = 0; i < filmCinema.size(); ++i) {
				if ((id.equals(filmCinema.get(i).getCinemaId()))
						&& (date1.compareTo(filmCinema.get(i).getDateFinish()) <= 0)) {
					ExtendedFilm exFilm = new ExtendedFilm();
					exFilm.setFilm(daoService.get("id='" + filmCinema.get(i).getFilmId() + "'", Film.class));
					exFilm.setDateOfStart(filmCinema.get(i).getDateStart());
					exFilm.setDateOfFinish(filmCinema.get(i).getDateFinish());
					exFilm.setMaxPrice(filmCinema.get(i).getMaxPrice().intValue());
					exFilm.setMinPrice(filmCinema.get(i).getMinPrice().intValue());
					exFilm.setState(filmCinema.get(i).getState());
					extendedFilm.add(exFilm);

				}
			}
			if (extendedFilm.size() == 0) {
				request.setAttribute("error", "true");
			}
			request.setAttribute("films", extendedFilm);

			request.getRequestDispatcher("WEB-INF/jsp/information.jsp").forward(request, response);
		}
		if (del1 == null && res1 == null) {
			for (int i = 0; i < filmCinema.size(); ++i) {
				if (id.equals(filmCinema.get(i).getFilmId())) {
					request.setAttribute("start", filmCinema.get(i).getDateStart());
					request.setAttribute("finish", filmCinema.get(i).getDateFinish());
					request.setAttribute("min", filmCinema.get(i).getMinPrice().intValue());
					request.setAttribute("max", filmCinema.get(i).getMaxPrice().intValue());
					request.setAttribute("state", filmCinema.get(i).getState());
					break;
				}
			}
			List<Film> film = new ArrayList<>(daoService.getAll(Film.class));
			for (int i = 0; i < film.size(); ++i) {
				if (id.equals(film.get(i).getId())) {
					request.setAttribute("poster", film.get(i).getPoster());
					request.setAttribute("title", film.get(i).getTitle());
				}
			}
			request.getRequestDispatcher("WEB-INF/jsp/edit.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
