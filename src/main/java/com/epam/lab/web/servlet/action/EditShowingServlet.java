package com.epam.lab.web.servlet.action;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dto.ExtendedCinema;
import com.epam.lab.dto.ExtendedFilm;
import com.epam.lab.model.Cinema;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmCinema;
import com.epam.lab.model.Statistic;
import com.epam.lab.model.User;

public class EditShowingServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	private List<ExtendedFilm> simple(User user) {
		GeneralDAOService daoService = GeneralDAOService.getInstance();
		List<Cinema> cinemaList = new ArrayList<>(daoService.getAll(Cinema.class));
		List<FilmCinema> filmCinema = new ArrayList<>(daoService.getAll(FilmCinema.class));
		List<ExtendedFilm> extendedFilm = new ArrayList<>();
		Cinema cinema = new Cinema();
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String date1 = dateFormat.format(date);
		for (int i = 0; i < filmCinema.size(); ++i) {
			if ((cinema.getId() == filmCinema.get(i).getCinemaId())
					&& (date1.compareTo(filmCinema.get(i).getDateFinish()) <= 0)) {
				ExtendedFilm exFilm = new ExtendedFilm();
				exFilm.setFilm(daoService.get("id='" + filmCinema.get(i).getFilmId() + "'", Film.class));
				exFilm.setDateOfStart(filmCinema.get(i).getDateStart());
				exFilm.setDateOfFinish(filmCinema.get(i).getDateFinish());
				extendedFilm.add(exFilm);
			}
		}
		return extendedFilm;
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HttpSession session = request.getSession();
		User user = (User) session.getAttribute("user");

		GeneralDAOService daoService = GeneralDAOService.getInstance();
		String str = request.getParameter("bookId");
		String[] elements = str.split(",");
		String cid1 = elements[0];
		String film1 = elements[1];
		Integer cid = Integer.parseInt(cid1);
		Integer film = Integer.parseInt(film1);
		List<FilmCinema> list = new ArrayList<FilmCinema>();
		list = daoService.getAll(FilmCinema.class);

		for (int i = 0; i < list.size(); ++i) {
			if (film.equals(list.get(i).getFilmId()) && (cid.equals(list.get(i).getCinemaId()))) {
				String title = request.getParameter("titleF");
				String dateStart = request.getParameter("dateSF");
				String dateFinish = request.getParameter("dateFF");
				String minPrice1 = request.getParameter("priceMin");
				String maxPrice1 = request.getParameter("priceMax");
				double minPrice = Double.parseDouble(minPrice1);
				double maxPrice = Double.parseDouble(maxPrice1);
				FilmCinema filmUpdate = new FilmCinema();
				filmUpdate.setFilmId(list.get(i).getFilmId());
				filmUpdate.setCinemaId(list.get(i).getCinemaId());
				filmUpdate.setDateStart(dateStart);
				filmUpdate.setDateFinish(dateFinish);
				filmUpdate.setMinPrice(minPrice);
				filmUpdate.setMaxPrice(maxPrice);
				filmUpdate.setState(list.get(i).getState());
				int k = 0;
				Statistic stat = new Statistic();
				stat = daoService.get("user_id=" + user.getId(), Statistic.class);
				String str3 = list.get(i).getEdited();
				if (str3.equals("yes")) {
					k = stat.getEditedShowing();
				} else {
					k = stat.getEditedShowing() + 1;
				}
				filmUpdate.setEdited("yes");
				Cinema cinema = new Cinema();
				cinema = daoService.get("id=" + list.get(i).getCinemaId(), Cinema.class);
				if (minPrice < maxPrice) {
					int n = daoService.update(list.get(i), filmUpdate);
					if (n > 0) {

						Statistic newstat = new Statistic();

						newstat.setEditedItems(stat.getEditedItems());
						newstat.setAddedPremieres(stat.getAddedPremieres());
						newstat.setBanCount(stat.getBanCount());
						newstat.setUnbanCount(stat.getUnbanCount());
						newstat.setAddedModerators(stat.getAddedModerators());
						newstat.setAddedAdmins(stat.getAddedAdmins());
						newstat.setRemovedPowers(stat.getRemovedPowers());
						newstat.setEditedShowing(k);
						newstat.setAddedShowing(stat.getAddedShowing());
						daoService.update(stat, newstat);
						request.setAttribute("editFilm", "yes");
						request.setAttribute("titFilm", title);
						request.setAttribute("cinFilm", cinema.getCinemaName());
					}
				} else {
					request.setAttribute("error2", "true");
				}
			}
		}

		Cinema cinema = new Cinema();
		List<Cinema> cinemaList = new ArrayList<>(daoService.getAll(Cinema.class));
		List<FilmCinema> filmCinema1 = new ArrayList<>(daoService.getAll(FilmCinema.class));
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		Date date = new Date();
		String date1 = dateFormat.format(date);
		List<ExtendedFilm> extendedFilm = new ArrayList<>();
		for (int j = 0; j < filmCinema1.size(); ++j) {
			if ((cinema.getId() == filmCinema1.get(j).getCinemaId())
					&& (date1.compareTo(filmCinema1.get(j).getDateFinish()) <= 0)) {
				ExtendedFilm exFilm = new ExtendedFilm();
				exFilm.setFilm(daoService.get("id='" + filmCinema1.get(j).getFilmId() + "'", Film.class));
				exFilm.setDateOfStart(filmCinema1.get(j).getDateStart());
				exFilm.setDateOfFinish(filmCinema1.get(j).getDateFinish());
				exFilm.setState(filmCinema1.get(j).getState());
				extendedFilm.add(exFilm);
			}
		}
		/*
		 * if (extendedFilm.size() == 0) { request.setAttribute("error",
		 * "true"); }
		 */
		List<ExtendedCinema> exList = new ArrayList<ExtendedCinema>();

		for (int i = 0; i < cinemaList.size(); ++i) {
			ExtendedCinema ex = new ExtendedCinema();
			ex.setCinema(cinemaList.get(i));
			int total = 0;
			for (int j = 0; j < filmCinema1.size(); ++j) {
				if (filmCinema1.get(j).getCinemaId().equals(cinemaList.get(i).getId())
						&& (date1.compareTo(filmCinema1.get(j).getDateFinish()) <= 0)
						&& ((filmCinema1.get(j).getState().equals("RES")))) {
					++total;
				}
			}
			ex.setTotal(total);
			exList.add(ex);
		}
		request.setAttribute("cinema", exList);
		Statistic stat = new Statistic();
		stat = daoService.get("user_id=" + user.getId(), Statistic.class);
		int my_edited = stat.getEditedShowing();
		int my_added = stat.getAddedShowing();
		List<Statistic> statList = new ArrayList<Statistic>();
		statList = daoService.getAll(Statistic.class);
		int all_edited = 0;
		int all_added = 0;
		for (int i = 0; i < statList.size(); ++i) {
			all_added = all_added + statList.get(i).getAddedShowing();
			all_edited = all_edited + statList.get(i).getEditedShowing();
		}
		request.setAttribute("my_edited", my_edited);
		request.setAttribute("my_added", my_added);
		request.setAttribute("all_edited", all_edited);
		request.setAttribute("all_added", all_added);

		Cinema cinema1 = daoService.get("id = " + cid, Cinema.class);
		List<FilmCinema> films = daoService.getList("cinema_id = " + cid, FilmCinema.class);
		List<ExtendedFilm> extendedFilm1 = new ArrayList<>();
		ExtendedFilm exFilm;

		request.setAttribute("cinema", cinema1);

		DateFormat dateFormat1 = new SimpleDateFormat("yyyy-MM-dd");
		String date2 = dateFormat1.format(new Date());

		for (FilmCinema film2 : films) {
			if ((date2.compareTo(film2.getDateFinish()) <= 0)) {
				exFilm = new ExtendedFilm(daoService.get("id='" + film2.getFilmId() + "'", Film.class),
						film2.getDateStart(), film2.getDateFinish(), film2.getMinPrice().intValue(),
						film2.getMaxPrice().intValue(), film2.getState());
				extendedFilm1.add(exFilm);
			}
		}

		if (extendedFilm1.size() == 0) {
			request.setAttribute("error", "true");
		} else {
			request.setAttribute("films", extendedFilm1);
		}
		request.setAttribute("idc", cid);
		request.getRequestDispatcher("WEB-INF/jsp/information.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
