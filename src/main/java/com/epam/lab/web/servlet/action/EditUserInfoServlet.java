package com.epam.lab.web.servlet.action;

import java.io.File;
import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;

import org.apache.commons.io.FilenameUtils;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.model.User;

@MultipartConfig
public class EditUserInfoServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private static final String USERS_RELATIVE_DIRECTORY = "photos";

	public EditUserInfoServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		Part filePart = request.getPart("photo");
		String filePath = "";
		String firstName = request.getParameter("f_name");
		String lastName = request.getParameter("l_name");
		String email = request.getParameter("email");
		boolean mailing = (null != request.getParameter("mailing"));
		User user = (User) request.getSession().getAttribute("user");

		if (filePart.getSize() < 4194304 && filePart.getSize() > 0) {
			String uploadPath = getServletContext().getRealPath("");

			File uploadDir = new File(uploadPath);
			if (!uploadDir.exists()) {
				uploadDir.mkdir();
			}

			String fileNameWithExt = user.getEmail().replace("@", "at") + "."
					+ FilenameUtils.getExtension(filePart.getSubmittedFileName());
			String toSave = USERS_RELATIVE_DIRECTORY + "/" + fileNameWithExt;
			filePath = uploadPath + toSave;
			filePart.write(filePath);
			GeneralDAOService.getInstance().update("id=" + user.getId(), "fname = '" + firstName + "', lname = '"
					+ lastName + "', email = '" + email + "', photo_url='" + toSave + "'", User.class);
			user.setPhotoURL(toSave);
		} else {
			GeneralDAOService.getInstance().update("id=" + user.getId(),
					"fname = '" + firstName + "', lname = '" + lastName + "', email = '" + email + "', mailing = " + mailing + "", User.class);
		}
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setMailing(mailing);

		response.sendRedirect("profile");
	}
}
