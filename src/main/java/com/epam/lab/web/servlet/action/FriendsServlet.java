package com.epam.lab.web.servlet.action;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.lab.dto.DetailedWatcher;
import com.epam.lab.i18n.Localization;
import com.epam.lab.model.User;
import com.epam.lab.service.FriendsInformationService;
import com.epam.lab.util.PaginationUtils;
import com.google.common.base.Strings;

public class FriendsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public FriendsServlet() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String subscriptionType = request.getParameter("subscriptionType");
		String nameForSearch = null;
		if (subscriptionType == null) {
		} else {
			nameForSearch = request.getParameter("friendName");
			request.setAttribute("search", subscriptionType);
		}

		String lineForSubscribing = ("subscribing".equals(subscriptionType) ? nameForSearch : null);
		String lineForSubsribers = ("subscribers".equals(subscriptionType) ? nameForSearch : null);
		request.setAttribute("sthWasSearched", lineForSubscribing != null || lineForSubsribers != null);

		FriendsInformationService friendsInformationService = FriendsInformationService.getInstance();

		if (lineForSubscribing != null
				&& friendsInformationService.validateNameSearchString(lineForSubscribing) == false) {
			request.setAttribute("subscribingWarning", "NOT VALID PARAMETER FOR SEARCH");
		} else {
			String pageParamSubscribing = request.getParameter("pageSubscribing");
			Integer pageSubscribing = !Strings.isNullOrEmpty(pageParamSubscribing)
					? Integer.valueOf(pageParamSubscribing.trim()) : PaginationUtils.DEFAULT_PAGE;					
			populateSearch(request, pageSubscribing, "subscribing", lineForSubscribing);
			request.setAttribute("pageSubscribing", pageSubscribing);
		}
		if (lineForSubsribers != null
				&& friendsInformationService.validateNameSearchString(lineForSubsribers) == false) {
			request.setAttribute("subscribersWarning", "NOT VALID PARAMETER FOR SEARCH");
		} else {
			String pageParamSubscribers = request.getParameter("pageSubscribers");
			if(!Strings.isNullOrEmpty(pageParamSubscribers)){
				request.setAttribute("switchToSubscribers", "yes");
			}
			Integer pageSubscribers = !Strings.isNullOrEmpty(pageParamSubscribers)
					? Integer.valueOf(pageParamSubscribers.trim()) : PaginationUtils.DEFAULT_PAGE;
			populateSearch(request, pageSubscribers, "subscribers", lineForSubsribers);
			request.setAttribute("pageSubscribers", pageSubscribers);
		}
		request.setAttribute("profile_content", "friends-content");
		request.setAttribute("title", "friends.title");
		request.getRequestDispatcher("WEB-INF/jsp/profile.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

	private void populateSearch(HttpServletRequest request, Integer page, String type, String lineForSearch) {
		String locale=((Localization) request.getSession().getAttribute("locale")).getLanguage();
		FriendsInformationService friendsInformationService = FriendsInformationService.getInstance();
		User currentUser = (User) request.getSession().getAttribute("user");
		boolean isSearch = (lineForSearch != null);
		if (type.equalsIgnoreCase("subscribing")) {
			List<DetailedWatcher> friendsSubscribing = friendsInformationService
					.getFriendsSubscribingInformation(currentUser.getId(), lineForSearch, page, locale);
			Integer amount = friendsInformationService.getCountOfSubscriptionAll(currentUser.getId(), lineForSearch, "subscribing");
			 if (friendsSubscribing==null||friendsSubscribing.isEmpty()) {
			 request.setAttribute("subscribingWarning", "NO SUBSCRIBINGS FOUND!");
			 } else {
				request.setAttribute("friendsSubscribingInfo", friendsSubscribing);
				request.setAttribute("friendsSubscribingAmount", amount);
			boolean nextPage = false;
			if (friendsSubscribing.size() == PaginationUtils.DEFAULT_USER_COUNT) {
				List<DetailedWatcher> nextFriendsSubscribing = friendsInformationService.getFriendsSubscribingInformation(currentUser.getId(), lineForSearch,page+1,locale);
				nextPage = nextFriendsSubscribing.size() > 0;
			}
			request.setAttribute("nextPageSubscribing", nextPage);
		}
		}
		else if (type.equalsIgnoreCase("subscribers")) {
			Map<DetailedWatcher, Boolean> friendsSubscribers = friendsInformationService.getFriendsSubscribersInformation(currentUser.getId(), lineForSearch,page,locale);
			Integer amount = friendsInformationService.getCountOfSubscriptionAll(currentUser.getId(), lineForSearch, "subscribers");
			
			if (friendsSubscribers.isEmpty()) {
			 request.setAttribute("subscribersWarning", "NO SUBSCRIBERS FOUND!");
			} else{
				request.setAttribute("friendsSubscribersInfo", friendsSubscribers);
				request.setAttribute("friendsSubscribersAmount", amount);
			boolean nextPage = false;
			if (friendsSubscribers.size() == PaginationUtils.DEFAULT_USER_COUNT) {
				Map<DetailedWatcher, Boolean> nextFriendsSubscribers = friendsInformationService.getFriendsSubscribersInformation(currentUser.getId(), lineForSearch, page+1,locale);
				nextPage = nextFriendsSubscribers.size() > 0;
			}
			request.setAttribute("nextPageSubscribers", nextPage);
		}
	}
}
}
