package com.epam.lab.web.servlet.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.epam.lab.dto.DetailedCinemaNews;
import com.epam.lab.service.CinemaNewsService;
import com.google.common.base.Strings;
import com.google.gson.Gson;

public class GetCinemaNewsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GetCinemaNewsServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String pageParam = request.getParameter("page");
		Integer page = ((pageParam!=null)?Integer.valueOf(request.getParameter("page")):1);

		String cinemaCity = request.getParameter("cinemaCity");
		String cinemaName = request.getParameter("cinemaName");
		if(cinemaCity!=null&&cinemaCity.isEmpty()){
			cinemaCity = null;			
		}
		if(cinemaName!=null&&cinemaName.isEmpty()){
			cinemaName = null;
		}
		
		List<DetailedCinemaNews> cinemaNews = new CinemaNewsService().getNewsByCityAndCinemaName(cinemaCity, cinemaName, page);
		boolean hasMore = false;

		if(cinemaNews.size()>CinemaNewsService.NEWS_PER_PAGE){
			cinemaNews=cinemaNews.subList(0, CinemaNewsService.NEWS_PER_PAGE);
			hasMore=true;

		}
		if ((page == 1)&&(cinemaCity==null)&&(cinemaName==null)) {
			if(hasMore)	{request.setAttribute("hasNextItems", true);}
			request.setAttribute("news", cinemaNews);
			request.setAttribute("profile_content", "news-content");
			request.setAttribute("title", "events.journal.title");
			request.getRequestDispatcher("WEB-INF/jsp/profile.jsp").forward(request, response);
		} else {
			response.setContentType("application/json");
			response.setCharacterEncoding("UTF-8");
			response.getWriter().write(new Gson().toJson(new NewsInfo(cinemaNews,hasMore)));
		}
	}
	
	private class NewsInfo{
		
		public NewsInfo(List<DetailedCinemaNews>  newsList, Boolean hasNextItems) {
			super();
			this.newsList = newsList;
			this.hasNextItems = hasNextItems;
		}
		private List<DetailedCinemaNews>  newsList;
		private Boolean hasNextItems;
		@SuppressWarnings("unused")
		public List<DetailedCinemaNews> getNewsList() {
			return newsList;
		}
		@SuppressWarnings("unused")
		public void setNewsList(List<DetailedCinemaNews>  newsList) {
			this.newsList = newsList;
		}
		@SuppressWarnings("unused")
		public Boolean getHasNextItems() {
			return hasNextItems;
		}
		@SuppressWarnings("unused")
		public void setHasNextItems(Boolean hasNextItems) {
			this.hasNextItems = hasNextItems;
		}		
	}
}