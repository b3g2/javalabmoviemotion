package com.epam.lab.web.servlet.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.epam.lab.model.Cinema;
import com.epam.lab.service.WatchingLocationFindService;

public class GetCinemaServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public GetCinemaServlet() {
        super();
    }

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String city = request.getParameter("city");
		String name = request.getParameter("name");
		Cinema gotCinema = new WatchingLocationFindService().getCinemaByCityAndName(city, name);
		
		JSONObject jsonObject = new JSONObject();		
		boolean successfully = (gotCinema!=null);
		if(successfully){
			jsonObject.put("cinemaId", gotCinema.getId());
		}
		jsonObject.put("successfully", successfully);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonObject.toString());
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
