package com.epam.lab.web.servlet.action;

import com.epam.lab.factory.ServiceFactory;
import com.epam.lab.model.User;
import com.epam.lab.service.UserService;
import com.epam.lab.util.ServerUtil;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class LoginServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private Gson gson = new Gson();
    private UserService userService = ServiceFactory.getUserService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String data = request.getParameter("data");
        if (action == null || data == null) return;
        switch (action) {
            case "signIn":
                signIn(gson.fromJson(data, Credential.class), request, response);
                break;
            case "signInWithSocial":
                signInWithSocial(gson.fromJson(data, Credential.class), request, response);
                break;
            case "restorePassword":
                restorePassword(gson.fromJson(data, String.class));
                break;
            case "checkEmail":
                checkEmail(gson.fromJson(data, String.class), response);
                break;
            case "changePassword":
                changePassword(gson.fromJson(data, Credential.class), request, response);
                break;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO: 1/17/16
    }

    private void signIn(Credential credential, HttpServletRequest request, HttpServletResponse response) throws IOException {
        Map<String, Boolean> resp = new HashMap<>(2);
        resp.put("valid", false);
        resp.put("restore", false);
        User user = userService.getUser(credential.getEmail());
        if (user != null) {
            String password = ServerUtil.hashPassword(credential.getPassword());
            if (password.equals(user.getPassword())) {
                resp.put("valid", true);
                userService.signIn(request, response, user);
            } else if (password.equals(user.getTempPassword()) && userService.checkTempPassword(user)) {
                resp.put("valid", true);
                resp.put("restore", true);
                userService.signIn(request, response, user);
            }
        }
        response.getWriter().write(gson.toJson(resp));
    }

    private void signInWithSocial(Credential credential, HttpServletRequest request, HttpServletResponse response) {

        if (credential.getEmail() == null) {
            credential.setEmail("marianfediv@gmail.com");
        }

        User user = userService.getUser(credential.getEmail());

        if (user == null) {
            user = new User(credential.getFirstName(), credential.getLastName(), credential.getEmail());
            userService.signUp(user);
            user = userService.getUser(user.getEmail());
        }

        userService.updatePhoto(user, credential.getPhotoURL());
        userService.signIn(request, response, user);
    }

    private void restorePassword(String email) {
        User user = userService.getUser(email);
        if (user != null) {
            userService.restorePassword(user);
        }
    }

    private void checkEmail(String email, HttpServletResponse response) throws IOException {
        User user = userService.getUser(email);
        response.getWriter().write(gson.toJson(user != null));
    }

    private void changePassword(Credential credential, HttpServletRequest request, HttpServletResponse response) throws IOException {
        boolean result = true;
        User user = (User) request.getSession().getAttribute("user");
        user = userService.getUser(user.getEmail());
        if (user != null && (credential.oldPassword == null || userService.checkOldPassword(user, credential.oldPassword))
                && credential.password != null && credential.password.equals(credential.confirmPassword)) {
            userService.changePassword(user, credential.getPassword());
        } else {
            result = false;
        }
        response.getWriter().write(gson.toJson(result));
    }

    public static class Credential {
        private String email;
        private String oldPassword;
        private String password;
        private String confirmPassword;
        private String firstName;
        private String lastName;
        private String photoURL;

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getOldPassword() {
            return oldPassword;
        }

        public void setOldPassword(String oldPassword) {
            this.oldPassword = oldPassword;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getConfirmPassword() {
            return confirmPassword;
        }

        public void setConfirmPassword(String confirmPassword) {
            this.confirmPassword = confirmPassword;
        }

        public String getFirstName() {
            return firstName;
        }

        public void setFirstName(String firstName) {
            this.firstName = firstName;
        }

        public String getLastName() {
            return lastName;
        }

        public void setLastName(String lastName) {
            this.lastName = lastName;
        }

        public String getPhotoURL() {
            return photoURL;
        }

        public void setPhotoURL(String photoURL) {
            this.photoURL = photoURL;
        }
    }
}
