package com.epam.lab.web.servlet.action;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmUser;
import com.epam.lab.service.MovieStatusChangeService;

public class MovieStatusChangeServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public MovieStatusChangeServlet() {
		super();
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("status");
		Integer userId = Integer.valueOf(request.getParameter("userId"));
		Integer filmId = Integer.valueOf(request.getParameter("filmId"));
		MovieStatusChangeService service = MovieStatusChangeService.getInstance();
		JSONObject jsonObject = new JSONObject();
		boolean actionResult = service.changeMovieStatus(action, userId, filmId) > 0;
		jsonObject.put("successfully", actionResult);
		GeneralDAOService daoService = GeneralDAOService.getInstance();
		FilmUser userWatching = daoService.get("user_id=" + userId + " AND film_id=" + filmId, FilmUser.class);
		Film film = daoService.get("id=" + filmId, Film.class);
		jsonObject.put("status", userWatching.getStatus());
		jsonObject.put("likeValue", userWatching.getLikeValue());
		jsonObject.put("favourite", userWatching.getFavourite());
		jsonObject.put("reviews", film.getReviews());
		jsonObject.put("likes", film.getLikes());
		jsonObject.put("dislikes", film.getDislikes());
		jsonObject.put("customRating", film.getRatingCustom());
		jsonObject.put("imdbRating", film.getRatingImdb());
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonObject.toString());
	}
}
