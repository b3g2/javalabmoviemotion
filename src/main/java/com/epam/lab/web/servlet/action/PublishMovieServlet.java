package com.epam.lab.web.servlet.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.lab.model.User;
import com.epam.lab.service.ModeratorService;
import com.mchange.v1.lang.BooleanUtils;

public class PublishMovieServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		ModeratorService moderatorService = new ModeratorService();
		String titleUkparam = request.getParameter("titleUk");
		String plotUkParam = request.getParameter("plotUk");
		String filmIdParam = request.getParameter("filmId");
		moderatorService.updateMovie(titleUkparam, plotUkParam, filmIdParam);

		User user = (User) request.getSession().getAttribute("user");
		String verifiedParam = request.getParameter("verified");
		if (verifiedParam == null || !BooleanUtils.parseBoolean(verifiedParam)) {
			moderatorService.increaseVerifyCounter(user);
		}
	}

}
