package com.epam.lab.web.servlet.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.simple.JSONObject;

import com.epam.lab.dto.DetailedFilm;
import com.epam.lab.model.Genre;
import com.epam.lab.service.SingleMovieService;

public class RecommendationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doPost(request, response);
	}

	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String idParameter = request.getParameter("id");
		String language = request.getParameter("language");
		SingleMovieService service = SingleMovieService.getInstance();
		DetailedFilm recommendedFilm = service.getRecommendationByUserId(Integer.valueOf(idParameter));
		JSONObject jsonObject = new JSONObject();
		
		if (recommendedFilm != null) {
			List<Genre> genres = recommendedFilm.getGenres();
			for (int i = 0; i < genres.size(); ++i) {
				String currentGenre;
				if(("uk".equals(language))&&(genres.get(i).getNameUK()!=null)){
					currentGenre = genres.get(i).getNameUK();
				}else{
					currentGenre = genres.get(i).getName();
				}				
				jsonObject.put("genre" + i, currentGenre);
			}

			String title;
			if(("uk".equals(language))&&(recommendedFilm.getFilm().getTitleUk()!=null)){
				title = recommendedFilm.getFilm().getTitleUk();
			}else{
				title = recommendedFilm.getFilm().getTitle();
			}
			String description;
			if(("uk".equals(language))&&(recommendedFilm.getFilm().getPlotUk()!=null)){
				description = recommendedFilm.getFilm().getPlotUk();
			}else{
				description = recommendedFilm.getFilm().getPlot();
			}
			jsonObject.put("name", title);
			jsonObject.put("posterURL", recommendedFilm.getFilm().getPoster());
			jsonObject.put("duration", recommendedFilm.getFilm().getRuntime());
			jsonObject.put("description", description);
			jsonObject.put("actors", recommendedFilm.getFilm().getActors());
			jsonObject.put("released", recommendedFilm.getFilm().getReleased());
			jsonObject.put("ratingIMDB", recommendedFilm.getFilm().getRatingImdb());
			jsonObject.put("ratingMovieMotion", recommendedFilm.getFilm().getRatingCustom());
			jsonObject.put("genresAmount", genres.size());
			jsonObject.put("statusMessage", "success");
		} else {
			jsonObject.put("statusMessage", "failure");
		}
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(jsonObject.toString());
	}
}
