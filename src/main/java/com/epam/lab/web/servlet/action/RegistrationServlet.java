package com.epam.lab.web.servlet.action;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.factory.ServiceFactory;
import com.epam.lab.model.User;
import com.epam.lab.service.UserService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RegistrationServlet extends HttpServlet {

    private Gson gson = new Gson();
    private UserService userService = ServiceFactory.getUserService();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String action = request.getParameter("action");
        String data = request.getParameter("data");
        if (action == null || data == null) return;
        if (action.equals("checkEmail")) {
            checkEmail(gson.fromJson(data, String.class), response);
        } else if (action.equals("signUp")) {
            signUp(gson.fromJson(data, User.class));
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO: 1/17/16
    }

    private void checkEmail(String email, HttpServletResponse response) throws IOException {
        User user = GeneralDAOService.getInstance().get("email = \"" + email.toLowerCase() + "\"", User.class);
        response.getWriter().write(gson.toJson(user == null));
    }

    private void signUp(User user) throws IOException {
        if (GeneralDAOService.getInstance().get("email = \"" + user.getEmail().toLowerCase() + "\"", User.class) == null) {
            userService.signUp(user);
        }
    }
}
