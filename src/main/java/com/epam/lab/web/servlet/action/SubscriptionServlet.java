package com.epam.lab.web.servlet.action;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.model.Friend;
import com.epam.lab.model.User;

public class SubscriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public SubscriptionServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doPost(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String action = request.getParameter("subscriptionAction");
		Integer followeeId = Integer.valueOf(request.getParameter("userId"));
		GeneralDAOService service = GeneralDAOService.getInstance();
		User follower = (User) request.getSession().getAttribute("user");
		if ("subscribe".equals(action)) {
			service.add(new Friend(follower.getId(), followeeId));
		} else {
			if ("unsubscribe".equals(action)) {
				service.delete("follower_id=" + follower.getId() + " AND followee_id=" + followeeId,
						Friend.class);
			}
		}		
		request.getRequestDispatcher("WEB-INF/jsp/index.jsp").forward(request, response);
	}

}
