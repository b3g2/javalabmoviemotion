package com.epam.lab.web.servlet.action;

import com.epam.lab.util.XMLParser;
import com.google.gson.Gson;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TrailerServlet extends HttpServlet {

    private static final long serialVersionUID = -1412369103752947415L;

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String imdbId = request.getParameter("imdbId");
        String addictTrailerId = getAddictTrailerId(imdbId);
        String trailerPlayer = null;
        String trailerSrc = null;
        boolean trailerExist = false;
        if (addictTrailerId != null) {
            trailerSrc = getAddictTrailerSrc(addictTrailerId);
            trailerPlayer = "addict";
        } else {
            // TODO: alternative trailer source
        }
        if (trailerSrc != null) {
            trailerExist = true;
        }
        Object[] trailer = {trailerExist, trailerPlayer, trailerSrc};
        response.getWriter().write(new Gson().toJson(trailer));
    }

    private String getTrailerLink(String id) throws IOException {
        //send request to TrailerAddict api
        URL url = new URL("http://api.traileraddict.com/?imdb=" + id + "&count=1&width=480");
        HttpURLConnection httpcon = (HttpURLConnection) url.openConnection();
        httpcon.addRequestProperty("User-Agent", "Mozilla/4.76");
        InputStream is = httpcon.getInputStream();
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        StringBuilder xmlBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            xmlBuilder.append(line);
        }
        if (!xmlBuilder.toString().contains(id)) return null;
        //parse resp xml
        XMLParser xml = new XMLParser(xmlBuilder.toString());
        return xml.find("link").text();
    }

    private String getAddictTrailerId(String imdbId) throws IOException {
        String link = getTrailerLink(imdbId.substring(2));
        if (link == null) return null;
        //find film name in link
        Pattern pattern = Pattern.compile("/[^/]*$");
        Matcher matcher = pattern.matcher(link);
        matcher.find();
        String url = (link.substring(0, matcher.start() + 1) + "trailer").replace("www", "simpleapi");
        //send request to TrailerAddict simpleapi
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        HttpResponse response = client.execute(request);
        BufferedReader reader = new BufferedReader(new InputStreamReader(response.getEntity().getContent()));
        StringBuilder xmlBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            xmlBuilder.append(line);
        }
        //parse resp xml
        XMLParser xml = new XMLParser(xmlBuilder.toString());
        return xml.find("trailer_id").text();
    }

    private String getAddictTrailerSrc(String addictTrailerId) {
        return addictTrailerId != null ? "http://v.traileraddict.com/" + addictTrailerId : null;
    }

}
