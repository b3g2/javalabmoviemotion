package com.epam.lab.web.servlet.action;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.lab.dto.DetailedShowing;
import com.epam.lab.service.WatchingLocationFindService;
import com.google.gson.Gson;

public class WatchingLocationFindServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    public WatchingLocationFindServlet() {
        super();
    }

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		Integer filmId = Integer.valueOf(request.getParameter("filmId"));
		List<DetailedShowing> detailedShowing = new WatchingLocationFindService().getWatchingLocations(filmId);
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.getWriter().write(new Gson().toJson(detailedShowing));
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		doGet(request, response);
	}

}
