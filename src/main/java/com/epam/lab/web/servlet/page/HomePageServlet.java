package com.epam.lab.web.servlet.page;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.lab.dto.DetailedComment;
import com.epam.lab.dto.FilmWithStatistic;
import com.epam.lab.model.Film;
import com.epam.lab.service.HomeService;

public class HomePageServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		HomeService homePageService = new HomeService();

		List<Film> top_rating_mm = homePageService.getMoviesForTime("30", "16"); 
		List<Film> released_for_week = homePageService.getMoviesForTime("7", 3);
		List<Film> top_movies_list = homePageService.getMoviesByRating("15");
		List<FilmWithStatistic> top_favourite = homePageService.getFavouriteMovies(3);	
		List<FilmWithStatistic> top_released = homePageService.getSceduledNewMovies(6);
		List<FilmWithStatistic> top_commented = homePageService.getMostCommentedMovies(3);
		List<DetailedComment> last_comments = homePageService.getLastComments();

 		request.setAttribute("top_rating_mm", top_rating_mm);
		request.setAttribute("released_for_week", released_for_week);
		request.setAttribute("top_movies_list", top_movies_list);
		request.setAttribute("top_favourite", top_favourite);
		request.setAttribute("top_released", top_released);
		request.setAttribute("top_commented", top_commented);
		request.setAttribute("last_comments", last_comments);

		request.getRequestDispatcher("WEB-INF/jsp/index.jsp").forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
