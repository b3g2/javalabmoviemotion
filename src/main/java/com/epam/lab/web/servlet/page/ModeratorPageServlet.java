package com.epam.lab.web.servlet.page;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dto.FilmEx;
import com.epam.lab.factory.ServiceFactory;
import com.epam.lab.model.*;
import com.epam.lab.service.DataService;
import com.epam.lab.service.ModeratorService;
import com.epam.lab.service.MoviesSearchService;
import com.epam.lab.util.PaginationUtils;
import com.google.common.base.Strings;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class ModeratorPageServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String pageParam = request.getParameter("page");
        Integer page = !Strings.isNullOrEmpty(pageParam) ? Integer.valueOf(pageParam.trim())
                : PaginationUtils.DEFAULT_PAGE;

        ModeratorService moderatorService = new ModeratorService();
        User user = (User) request.getSession().getAttribute("user");
        if (user.getRole() != null) {
            Statistic statistic = moderatorService.getCheckedStatisticsByCurrent(user);
            Integer checkedByCurrentCount = statistic.getEditedItems();
            request.setAttribute("checkedByCurrentCount", checkedByCurrentCount);
        }
        List<Film> checkedStatistics = moderatorService.getCheckedStatistics();
        List<Film> uncheckedStatistics = moderatorService.getUncheckedStatistics();
        Integer checkedCount = checkedStatistics.size();
        Integer uncheckedCount = uncheckedStatistics.size();
        request.setAttribute("checkedCount", checkedCount);
        request.setAttribute("uncheckedCount", uncheckedCount);

        populateSearch(request, page);

        request.setAttribute("page", page);
        request.setAttribute("moderator_content", "moderator-requests");

        GeneralDAOService daoService = GeneralDAOService.getInstance();
        Statistic stat;
        stat = daoService.get("user_id='" + user.getId() + "'", Statistic.class);
        int my_edited = stat.getEditedShowing();
        int my_added = stat.getAddedShowing();
        List<Statistic> statList;
        statList = daoService.getAll(Statistic.class);
        int all_edited = 0;
        int all_added = 0;
        for (int i = 0; i < statList.size(); ++i) {
            all_added = all_added + statList.get(i).getAddedShowing();
            all_edited = all_edited + statList.get(i).getEditedShowing();
        }
        request.setAttribute("my_edited", my_edited);
        request.setAttribute("my_added", my_added);
        request.setAttribute("all_edited", all_edited);
        request.setAttribute("all_added", all_added);

        request.getRequestDispatcher("WEB-INF/jsp/moderator.jsp").forward(request, response);
    }

    private void populateSearch(HttpServletRequest request, Integer page) {
        String toSearch = request.getParameter("filmSearch");
        String verifiedParam = request.getParameter("verified");
        if (verifiedParam != null) {
            request.setAttribute("verified", verifiedParam);
        }
        Boolean verified = verifiedParam == null ? null : "checked".equals(verifiedParam);

        if (toSearch != null && !toSearch.isEmpty()) {
            request.setAttribute("filmSearch", toSearch);
        } else {
            toSearch = "";
        }

        MoviesSearchService moviesSearchService = new MoviesSearchService();
        DataService dataService = ServiceFactory.getDataService();
        DataService.GetParams params = new DataService.GetParams(toSearch,
                PaginationUtils.getOffset(page, PaginationUtils.DEFAULT_FILM_COUNT), PaginationUtils.DEFAULT_FILM_COUNT);
        List<FilmEx> foundFilms = dataService.getFilms(params, true, true, verified);

        if (foundFilms != null && !foundFilms.isEmpty()) {
            request.setAttribute("films", foundFilms);

            boolean nextPage = false;
            boolean prevPage = false;

            List<Film> nextPageFilms = moviesSearchService.searchFilmOnlyByName(toSearch, page + 1, verified);
            nextPage = nextPageFilms != null && nextPageFilms.size() > 0;
            if (page > 0) {
                List<Film> prevPageFilms = moviesSearchService.searchFilmOnlyByName(toSearch, page - 1, verified);
                prevPage = prevPageFilms != null && prevPageFilms.size() > 0;
            }
            request.setAttribute("nextPage", nextPage);
            request.setAttribute("prevPage", prevPage);
        }
    }

//    private void populateFields(HttpServletRequest request) {
//        MoviesSearchService moviesSearchService = new MoviesSearchService();
//
//        List<Genre> genres = moviesSearchService.getAllGenres();
//        List<FilmGenre> filmGenreRelations = moviesSearchService.getFilmGenreRelations();
//
//		List<Country> countries = moviesSearchService.getAllCountries();
//		List<FilmCountry> filmCountryRelations = moviesSearchService.getFilmCountryRelations();
//
//        request.setAttribute("genres", genres);
//        request.setAttribute("filmGenreRelations", filmGenreRelations);
//
//		request.setAttribute("countries", countries);
//		request.setAttribute("filmCountryRelations", filmCountryRelations);
//    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request, response);
    }

}
