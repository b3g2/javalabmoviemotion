package com.epam.lab.web.servlet.page;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.lab.dto.DetailedComment;
import com.epam.lab.dto.DetailedFilm;
import com.epam.lab.i18n.Localization;
import com.epam.lab.model.Country;
import com.epam.lab.model.Film;
import com.epam.lab.model.Genre;
import com.epam.lab.service.CommentExtractService;
import com.epam.lab.service.RelatedFilmSearchService;
import com.epam.lab.service.SingleMovieService;
import com.google.common.base.Strings;

public class MovieDetailedServlet extends HttpServlet {
	
	private static final long serialVersionUID = 1L;

	public MovieDetailedServlet() {
		super();
	}

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		Integer filmId = Integer.valueOf(request.getParameter("id"));
		DetailedFilm detailedFilm = SingleMovieService.getInstance().getDetailedFilmById(filmId);
		String lang = ((Localization)request.getSession().getAttribute("locale")).getLanguage();
		
		String genres = "";
		List<Genre> genresList = detailedFilm.getGenres();
		for (int i = 0; i < genresList.size(); ++i) {
			if("uk".equals(lang)&&(!Strings.isNullOrEmpty(genresList.get(i).getNameUK()))){
				genres += genresList.get(i).getNameUK();
			}
			else{
			genres += genresList.get(i).getName();
			}
			if (i != genresList.size() - 1)
				genres += ", ";
		}
		String countries = "";
		List<Country> countriesList = detailedFilm.getCountries();
		for (int i = 0; i < countriesList.size(); ++i) {
			if("uk".equals(lang)&&(!Strings.isNullOrEmpty(countriesList.get(i).getNameUK()))){
				countries += countriesList.get(i).getNameUK();
			}
			else{
			countries += countriesList.get(i).getName();
			}
			if (i != countriesList.size() - 1)
				countries += ", ";
		}
		// getting comments separately
		List<DetailedComment> comments = new CommentExtractService().getFilmComments(detailedFilm.getFilm().getId());
		for (DetailedComment comment : comments) {
			String addedDateTime = comment.getComment().getAddedDateTime();
			if (addedDateTime.endsWith(".0")) {
				comment.getComment().setAddedDateTime(addedDateTime.substring(0, addedDateTime.length() - 2));
			}
		}
		Collections.sort(comments);

		// getting related films
		final int forLastYears=4;
		final int amountToGet=4;
		List<Film> relatedFilms = new RelatedFilmSearchService().getRelatedFilms(detailedFilm.getFilm(),detailedFilm.getGenres(),amountToGet,detailedFilm.getFilm().getYear(),forLastYears);

		request.setAttribute("filmBase", detailedFilm.getFilm());
		request.setAttribute("filmCountry", countries);
		request.setAttribute("filmGenre", genres);
		request.setAttribute("filmCommentsInfo", comments);
		request.setAttribute("filmRelatedFilms", relatedFilms);

		request.getRequestDispatcher("WEB-INF/jsp/single-movie.jsp").forward(request, response);
	}


	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}

}
