package com.epam.lab.web.servlet.page;

import java.io.IOException;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.lab.model.Country;
import com.epam.lab.model.Film;
import com.epam.lab.model.Genre;
import com.epam.lab.model.User;
import com.epam.lab.service.MoviesSearchService;
import com.epam.lab.util.PaginationUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;

public class MoviesPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		String pageParam = request.getParameter("page");
		Integer page = !Strings.isNullOrEmpty(pageParam) ? Integer.valueOf(pageParam.trim())
				: PaginationUtils.DEFAULT_PAGE;
		populateAdvancedSearch(request, page);
		request.setAttribute("page", page);
		request.getRequestDispatcher("WEB-INF/jsp/movies.jsp").forward(request, response);
	}

	private void populateAdvancedSearch(HttpServletRequest request, Integer page) {
		Map<String, Set<String>> map = new HashMap<>();
		String[] genresInput = request.getParameterValues("genreInput");
		String[] yearsInput = request.getParameterValues("yearInput");
		String[] countriesInput = request.getParameterValues("countryInput");

		String sortParam = request.getParameter("sort");
		String sortFlagParam = request.getParameter("sortFlag");

		String toSearch = request.getParameter("toSearch");
		Set<String> set = null;
		if (genresInput != null) {
			set = new HashSet<>();
			Collections.addAll(set, genresInput);
			map.put("genres", set);
			request.setAttribute("genreInput", genresInput);
		}
		if (yearsInput != null) {
			set = new HashSet<>();
			Collections.addAll(set, yearsInput);
			map.put("years", set);
			request.setAttribute("yearInput", yearsInput);
		}
		if (countriesInput != null) {
			set = new HashSet<>();
			Collections.addAll(set, countriesInput);
			map.put("countries", set);
			request.setAttribute("countryInput", countriesInput);
		}
		if (toSearch != null && !toSearch.isEmpty()) {
			set = new HashSet<>();
			Collections.addAll(set, toSearch);
			map.put("toSearch", set);
			request.setAttribute("toSearch", toSearch);
		}

		MoviesSearchService advancedSearchService = new MoviesSearchService();
		List<Genre> genres = advancedSearchService.getAllGenres();
		List<String> years = advancedSearchService.getAllYears();
		List<Country> countries = advancedSearchService.getAllCountries();

		Boolean sortFlag = false;
		if ("ASC".equals(sortFlagParam)) {
			sortFlag = true;
		}

		List<Film> filmsWithOptions = advancedSearchService.doSearch(map, page, sortParam, sortFlag);

		if (filmsWithOptions != null && !Iterables.isEmpty(filmsWithOptions)) {

			User user = (User) request.getSession().getAttribute("user");
			request.setAttribute("films", advancedSearchService.getUserFilmsInfo(filmsWithOptions, user));
		}

		boolean nextPage = false;
		boolean prevPage = false;

		List<Film> nextPageFilms = advancedSearchService.doSearch(map, page + 1, sortParam, sortFlag);
		if (nextPageFilms != null) {
			nextPage = nextPageFilms.size() > 0;
		}
		if (page > 0) {
			List<Film> prevPageFilms = advancedSearchService.doSearch(map, page - 1, sortParam, sortFlag);
			if (prevPageFilms != null) {
				prevPage = prevPageFilms.size() > 0;
			}
		}

		if (sortFlagParam != null) {
			request.setAttribute("sortFlag", sortFlagParam);
		}
		if (sortParam != null) {
			request.setAttribute("sort", sortParam);
		}

		request.setAttribute("nextPage", nextPage);
		request.setAttribute("prevPage", prevPage);

		request.setAttribute("genres", genres);
		request.setAttribute("years", years);
		request.setAttribute("countries", countries);

	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}

}
