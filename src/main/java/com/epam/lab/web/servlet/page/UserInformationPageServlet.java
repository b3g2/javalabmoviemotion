package com.epam.lab.web.servlet.page;

import java.io.IOException;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dto.UserWithStatistic;
import com.epam.lab.model.Film;
import com.epam.lab.model.Friend;
import com.epam.lab.model.User;
import com.epam.lab.service.UserInfoService;

public class UserInformationPageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		UserInfoService userInfoService = new UserInfoService();
		HttpSession session = request.getSession();
		User loginUser = (User) session.getAttribute("user");

		if (request.getParameter("subscriptionAction") != null) {
			boolean isFriend = false;
			String action = request.getParameter("subscriptionAction");
			Integer followeeId = Integer.valueOf(request.getParameter("userId"));
			GeneralDAOService service = GeneralDAOService.getInstance();
			User follower = (User) request.getSession().getAttribute("user");
			if ("subscribe".equals(action)) {
				service.add(new Friend(follower.getId(), followeeId));
				isFriend = true;
			} else if ("unsubscribe".equals(action)) {
				isFriend = false;
				service.delete("follower_id=" + follower.getId() + " AND followee_id=" + followeeId, Friend.class);
			}
			request.setAttribute("isFriend", isFriend);
		}

		if (request.getParameter("id") != null) {

			String id = request.getParameter("id");

			if (id.equals(loginUser.getId().toString())) {
				response.sendRedirect("profile");
				
			} else {
				UserWithStatistic user = userInfoService.getUserById(id);
				List<Film> filmsFavourite = userInfoService.getFavouriteFilms(id);
				List<Film> filmsWatched = userInfoService.getWatchedFilms(id);
				List<Film> filmsSceduled = userInfoService.getScheduledFilms(id);
				List<Film> lastWatchedFilms = userInfoService.getLastWatchedFilms(id);
				boolean isFriend = userInfoService.isFriend(loginUser.getId(), user.getUser().getId());

				request.setAttribute("films", filmsWatched.size());
				request.setAttribute("filmsFavourite", filmsFavourite);
				request.setAttribute("filmsSceduled", filmsSceduled);
				request.setAttribute("filmsWatched", filmsWatched);
				request.setAttribute("lastWatchedFilms", lastWatchedFilms);
				request.setAttribute("userInfo", user);
				request.setAttribute("url", "user?id=" + id);
				request.setAttribute("isFriend", isFriend);

				request.getRequestDispatcher("WEB-INF/jsp/user-information.jsp").forward(request, response);
			}
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}
}
