package com.epam.lab.web.servlet.page;

import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dto.DetailedCinemaNews;
import com.epam.lab.model.Cinema;
import com.epam.lab.service.CinemaNewsService;
import com.google.gson.Gson;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

public class UserProfileCinemaPageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		GeneralDAOService daoService = GeneralDAOService.getInstance();
		if (request.getParameter("filter_movies") != null) {
			List<Cinema> filmList = new ArrayList<>(daoService.getAll(Cinema.class));
			response.getWriter().write(new Gson().toJson(filmList));

		} else {
			String title = request.getParameter("cine");
			List<Cinema> cinema = new ArrayList<>(daoService.getAll(Cinema.class));
           
			if (title != null)
			{
				if (title.equals("")){
					request.setAttribute("all", true);
				} else {
					request.removeAttribute("all");
				}
				
				List<Cinema> choose = new ArrayList<Cinema>();
				for (int i = 0; i < cinema.size(); ++i) {
					if (title.equals(cinema.get(i).getCinemaName())) {
						choose.add(cinema.get(i));
					}
				}
				
				List<Cinema> c1 = new ArrayList<Cinema>();
				if (choose.size() < 4)
				{
					request.setAttribute("c1", choose);
				}
				else
				{
				for (int i = 0; i < choose.size() / 3; ++i) {
					c1.add(choose.get(i));
				}
				request.setAttribute("c1", c1);
				List<Cinema> c2 = new ArrayList<Cinema>();
				int k = choose.size() / 3 + 1;

				for (int j = choose.size() / 3 + 1; j < k + choose.size() / 3; ++j) {
					c2.add(choose.get(j));
				}
				request.setAttribute("c2", c2);
				List<Cinema> c3 = new ArrayList<Cinema>();
				int n = choose.size() / 3 + 1 + choose.size() / 3 + 1;
				for (int j = n; j < choose.size(); ++j) {
					c3.add(choose.get(j));
				}
				request.setAttribute("c3", c3);
				}
				request.setAttribute("cinema", choose);
				request.setAttribute("show", "true1");
				if (choose.size() < 1)
				{
					request.setAttribute("found","no");
					request.setAttribute("cinema", cinema);
				}
			}
			else
			{
				List<Cinema> choose = new ArrayList<Cinema>();
				List<Cinema> c1 = new ArrayList<Cinema>();
				for (int i = 0; i < cinema.size() / 3; ++i) {
					c1.add(cinema.get(i));
				}
				request.setAttribute("c1", c1);
				List<Cinema> c2 = new ArrayList<Cinema>();
				int k = cinema.size() / 3 + 1;

				for (int j = cinema.size() / 3 + 1; j < k + cinema.size() / 3; ++j) {
					c2.add(cinema.get(j));
				}
				request.setAttribute("c2", c2);
				List<Cinema> c3 = new ArrayList<Cinema>();
				int n = cinema.size() / 3 + 1 + cinema.size() / 3 + 1;
				for (int j = n; j < cinema.size(); ++j) {
					c3.add(cinema.get(j));
				}
				request.setAttribute("c3", c3);
				request.setAttribute("cinema", cinema);
			}
			
            String cityName = request.getParameter("cityButton");
	        if (cityName != null)
	        {
	        	List<Cinema> cinemaCity = new ArrayList<Cinema>();
	        	cinemaCity = daoService.getList("city='" + cityName + "'", Cinema.class);
	        	request.setAttribute("cinema", cinemaCity);
	        	request.setAttribute("selectCity", cityName);
	        	
	        }
			List<String> city = new ArrayList<String>();
			for (int i = 0; i < cinema.size(); ++i) {
				city.add(cinema.get(i).getCity());
			}
			HashSet<String> uniqueSet = new HashSet<String>();
			uniqueSet.addAll(city);
			request.setAttribute("city", uniqueSet);
			List<DetailedCinemaNews> detailedCinemaNews = new CinemaNewsService().getNews(null, null, null);
			
			request.setAttribute("news", detailedCinemaNews);
			request.setAttribute("profile_content", "cinema-content");
		    request.setAttribute("title", "cinema.header");
		    request.getRequestDispatcher("WEB-INF/jsp/profile.jsp").forward(request, response);
		}
		
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		doGet(request, response);
	}
}
