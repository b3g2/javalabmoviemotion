package com.epam.lab.web.servlet.page;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.simple.JSONArray;
import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.dao.GeneralDAOService.JoinResultMap;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmUser;
import com.epam.lab.model.Friend;
import com.epam.lab.model.User;
import com.epam.lab.service.UserService;
import com.google.gson.Gson;

public class UserProfilePageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private HttpSession session;
	private User user;
	private List<Film> wantWatchMovies = new ArrayList<>();
	private List<Film> filmsForDay = new ArrayList<>();
	private List<Film> filmsWatched = new ArrayList<>();
	private UserService userService = new UserService();
	private String dayPremiere;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		if (request.getParameter("refresh") != null) {
			request.setAttribute("moviesForDay", filmsForDay);
			request.setAttribute("dayReleased", dayPremiere);

		} else {
			session = request.getSession();
			wantWatchMovies.clear();
			filmsWatched.clear();
			user = (User) session.getAttribute("user");
			dayPremiere = new SimpleDateFormat("yyyy-MM-dd").format(new Date());

			GeneralDAOService daoService = GeneralDAOService.getInstance();
			Integer duration = daoService.get("id=" + user.getId(), User.class).getDuration();

			JoinResultMap friendsDuration = daoService.getJoin("follower_id =" + user.getId(), null,
					"user.duration desc", User.class, "user.id = friend.followee_id", Friend.class);
			Integer place = 1;

			if (friendsDuration.listSize(User.class) > 0) {
				for (int i = 0; i < friendsDuration.listSize(User.class); i++) {
					if (duration > friendsDuration.get(User.class).get(i).getDuration()) {
						break;
					} else {
						place = i + 2;
					}
				}
				request.setAttribute("place", place);
			}
			request.setAttribute("mins", duration);
			request.setAttribute("hours", duration / 60);
			request.setAttribute("days", (duration / 60) / 24);

			List<FilmUser> filmsWathedForUser = new ArrayList<>(
					daoService.getList("user_id='" + user.getId() + "' AND status='WATCHED'", FilmUser.class));
			List<FilmUser> filmsFavouriteForUser = new ArrayList<>(
					daoService.getList("user_id='" + user.getId() + "' AND favourite=true", FilmUser.class));
			List<FilmUser> filmsSceduledForUser = new ArrayList<>(
					daoService.getList("user_id='" + user.getId() + "' AND status='SCHEDULED'", FilmUser.class));

			List<Film> filmsFavourite = new ArrayList<>();
			List<Film> filmsSceduled = new ArrayList<>();

			for (FilmUser filmUser : filmsFavouriteForUser) {
				filmsFavourite.add(daoService.get("id='" + filmUser.getFilmId() + "'", Film.class));
			}

			for (FilmUser filmUser : filmsWathedForUser) {
				filmsWatched.add(daoService.get("id='" + filmUser.getFilmId() + "'", Film.class));
			}

			for (FilmUser filmUser : filmsSceduledForUser) {
				filmsSceduled.add(daoService.get("id='" + filmUser.getFilmId() + "'", Film.class));
			}

			for (FilmUser filmUser : filmsSceduledForUser) {
				Film film = daoService.get("id='" + filmUser.getFilmId() + "' AND released > CURDATE()", Film.class);
				if (film != null) {
					wantWatchMovies.add(film);
				}
			}

			if (filmsWatched.isEmpty()) {
				request.setAttribute("withoutFilmsWatched", true);
			}
			
			if (wantWatchMovies.isEmpty()) {
				request.setAttribute("withoutWantWatchMovies", true);
			}

			request.setAttribute("dayReleased", dayPremiere);
			request.setAttribute("films", filmsWathedForUser.size());
			request.setAttribute("filmsFavourite", filmsFavourite);
			request.setAttribute("filmsSceduled", filmsSceduled);
			request.setAttribute("filmsWatched", filmsWatched);
			request.setAttribute("wantWatchMovies", wantWatchMovies);
		}

		if (request.getParameter("all_movies") != null) {
			request.setAttribute("moviesForDay", filmsForDay);
			response.getWriter().write(new Gson().toJson(filmsForDay));

		} else {
			request.setAttribute("profile_content", "profile-content");
			request.setAttribute("title", "profile.header");
			request.getRequestDispatcher("WEB-INF/jsp/profile.jsp").forward(request, response);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");

		if (request.getParameter("calendar_visible") != null) {
			StringBuilder result = new StringBuilder();

			result.append("{");
			Map<String, Integer> filmMap = new HashMap<>();

			for (Film film : wantWatchMovies) {
				String key = film.getReleased();
				if (key != null) {
					if (filmMap.containsKey(key)) {
						filmMap.put(film.getReleased(), filmMap.get(key) + 1);
					} else {
						filmMap.put(film.getReleased(), 1);
					}
				}
			}

			filmMap.forEach((k, v) -> result.append("\"" + k + "\": {\"number\": " + v + "},"));
			result.replace(result.length() - 1, result.length(), "}");

			response.getWriter().write(new Gson().toJson(result));
		}

		if (request.getParameter("pie_chart") != null) {
			if (!filmsWatched.isEmpty()) {
				JSONArray statisticGenre = userService.getGenreStatistic(filmsWatched, request);
				response.getWriter().write(new Gson().toJson(statisticGenre.toString()));
				request.removeAttribute("withoutFilmsWatched");
			}
		}

		if (request.getParameter("day_click") != null) {

			String clickDay = request.getParameter("day_click");
			dayPremiere = clickDay;
			filmsForDay.clear();
			for (Film film : wantWatchMovies) {
				if (film.getReleased() != null) {
					if (film.getReleased().equals(clickDay)) {
						filmsForDay.add(film);
					}
				}
			}
			dayPremiere = clickDay;
			response.getWriter().write(new Gson().toJson(filmsForDay));
		}
	}
}
