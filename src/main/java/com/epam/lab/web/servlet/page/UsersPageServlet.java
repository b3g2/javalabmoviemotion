package com.epam.lab.web.servlet.page;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.epam.lab.dto.UserWithStatistic;
import com.epam.lab.model.Genre;
import com.epam.lab.model.User;
import com.epam.lab.service.MoviesSearchService;
import com.epam.lab.service.RegisteredUsersService;
import com.epam.lab.service.UserSearchService;
import com.epam.lab.util.PaginationUtils;
import com.google.common.base.Strings;
import com.google.common.collect.Iterables;

public class UsersPageServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;
	private RegisteredUsersService usersService = new RegisteredUsersService();

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		List<UserWithStatistic> topUsers = new ArrayList<>();
		String topText = "users.users";

		String pageParam = request.getParameter("page");
		Integer page = !Strings.isNullOrEmpty(pageParam) ? Integer.valueOf(pageParam.trim())
				: PaginationUtils.DEFAULT_PAGE;

		if (request.getParameter("top_activ") != null) {
			topUsers = usersService.getTopActive();
			request.setAttribute("activeUsers", true);
			request.removeAttribute("users");
			topText = "users.topactive";

		} else if (request.getParameter("top_watched") != null) {
			List<User> watchedUsers = usersService.getTopWatched();

			for (User user : watchedUsers) {
				topUsers.add(new UserWithStatistic(user));
			}
			request.setAttribute("watchedUsers", true);
			request.removeAttribute("users");
			topText = "users.topshows";

		} else if (request.getParameter("top_comented") != null) {
			topUsers = usersService.getTopCommented();
			request.setAttribute("comentedUsers", true);
			request.removeAttribute("users");
			topText = "users.topcomments";

		} else {
			populateSearch(request, page);
			request.setAttribute("page", page);
		}
		
		populateGenres(request);
		
		request.setAttribute("topUsers", topUsers);
		request.setAttribute("topText", topText);
		request.setAttribute("profile_content", "users-content");
		request.setAttribute("title", "users.title");
		request.getRequestDispatcher("WEB-INF/jsp/profile.jsp").forward(request, response);
	}

	protected void populateSearch(HttpServletRequest request, Integer page) {
		String toSearch = request.getParameter("userSearch");
		String[] genreParams = request.getParameterValues("genreInput");
		Set<String> genres = null;
		if (genreParams != null) {
			genres = new HashSet<>();
			Collections.addAll(genres, genreParams);
			request.setAttribute("genreInput", genreParams);
		}

		if (toSearch != null && !toSearch.isEmpty()) {
			request.setAttribute("userSearch", toSearch);
		} else {
			toSearch = "";
		}

		UserSearchService userSearchService = new UserSearchService();
		List<User> foundUsers = userSearchService.doSearch(toSearch, page, genres);

		if (foundUsers != null && !Iterables.isEmpty(foundUsers)) {
			request.setAttribute("users", foundUsers);
			request.removeAttribute("topUsers");
		}

			boolean nextPage = false;
			boolean prevPage = false;
			List<User> nextPageFilms = userSearchService.doSearch(toSearch, page + 1, genres);
			nextPage = nextPageFilms == null ? false : nextPageFilms.size() > 0;
			if (page > 0) {
				List<User> prevPageFilms = userSearchService.doSearch(toSearch, page - 1, genres);
				prevPage = prevPageFilms == null ? false : prevPageFilms.size() > 0;
			}
			
			request.setAttribute("nextPage", nextPage);
			request.setAttribute("prevPage", prevPage);
		if (foundUsers.size() == 0 ) {
			request.setAttribute("users_count", usersService.getAllUsersCount());
		} else {
			request.setAttribute("users_count", foundUsers.size());
		}
	}

	protected void populateGenres(HttpServletRequest request){
		MoviesSearchService moviesSearchService = new MoviesSearchService();
		List<Genre> allGenres = moviesSearchService.getAllGenres();
		request.setAttribute("genres", allGenres);
	}
	
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		doGet(request, response);
	}
}