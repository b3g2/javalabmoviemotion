package com.epam.lab.web.tag;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.epam.lab.i18n.Localization;
import com.epam.lab.model.User;
import com.google.common.base.Strings;

public class AllUsersTag extends SimpleTagSupport {

	private List<User> users;
	private static StringBuilder result;
	private String locale;
	private Localization localization;

	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public void setLocale(String locale) {
		this.locale = locale;
	}

	@Override
	public void doTag() throws JspException, IOException {

		JspWriter out = getJspContext().getOut();
		localization = new Localization("content", locale);
		result = new StringBuilder();
		Integer day;
		Integer hour;
		Integer min;
		Integer duration;

		if (users != null) {

			for (User user : users) {
				result.append("<div id=\"row\" class=\"nbs-flexisel-item\"><div class=\"thumbnail\"><img src=\"");

				if (!Strings.isNullOrEmpty(user.getPhotoURL())) {
					result.append(user.getPhotoURL());
				} else {
					result.append("images/avatarStandard.png");
				}
				
				result.append("\" alt=\"photo\"	class=\"img-responsive zoom-img img-circle\">");
				result.append("<div class=\"caption date-city\"><h4>");
				result.append(user.getFirstName());
				result.append("</h4><h4>");
				result.append(user.getLastName());
				result.append("</h4><p>");
				duration = user.getDuration();
				day = (duration / 60) / 24;
				hour = duration / 60 - day * 24;
				min = duration - (day * 24 + hour) * 60;
				result.append(day);
				result.append("&nbsp;");
				result.append(localization.get("users.d"));
				result.append("&nbsp;&nbsp;");
				result.append(hour);
				result.append("&nbsp;");
				result.append(localization.get("users.h"));
				result.append("&nbsp;&nbsp;");
				result.append(min);
				result.append("&nbsp;");
				result.append(localization.get("users.m"));
				result.append("</p><div class=\"tab_desc top btn-flat\" style=\"padding: 7px 10px !important;\"><a href=\"user?id=");
				result.append(user.getId());
				result.append("\">");
				result.append(localization.get("users.open"));
				result.append("</a></div></div></div></div>");
			}

			result.append("</tbody></table>");
			out.println(result);
		}
	}
}
