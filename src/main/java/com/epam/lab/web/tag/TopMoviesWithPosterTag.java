package com.epam.lab.web.tag;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import com.epam.lab.dto.FilmWithStatistic;
import com.epam.lab.i18n.Localization;
import com.epam.lab.model.Film;

public class TopMoviesWithPosterTag extends SimpleTagSupport {

	private List<Film> movies;
	private List<FilmWithStatistic> moviesWithStatistic;
	private Map<Integer, Integer> filmCountFavourite;
	private Map<Integer, Integer> filmCountComment;
	private Map<Integer, Integer> filmCountSceduled;
	private Integer tab;
	private String locale;
	private static StringBuilder result;
	private Localization localization;

	public void setMovies(List<Film> films) {
		this.movies = films;
	}

	public void setMoviesWithStatistic(List<FilmWithStatistic> moviesWithStatistic) {
		this.moviesWithStatistic = moviesWithStatistic;
	}

	public void setTab(Integer tab) {
		this.tab = tab;
	}

	public void setLocale(String locale) {
		this.locale = locale;
	}

	@Override
	public void doTag() throws JspException, IOException {

		JspWriter out = getJspContext().getOut();
		localization = new Localization("content", locale);
		result = new StringBuilder();
		filmCountFavourite = new HashMap<>();
		filmCountComment = new HashMap<>();
		filmCountSceduled = new HashMap<>();

		if (moviesWithStatistic != null) {
			movies = new ArrayList<>();
			for (FilmWithStatistic filmWithStatistic : moviesWithStatistic) {
				movies.add(filmWithStatistic.getFilm());
				filmCountFavourite.put(filmWithStatistic.getFilm().getId(), filmWithStatistic.getFavouriteCount());
				filmCountComment.put(filmWithStatistic.getFilm().getId(), filmWithStatistic.getCommentCount());
				filmCountSceduled.put(filmWithStatistic.getFilm().getId(), filmWithStatistic.getSheduledCount());
			}
		}

		if (movies != null) {

			for (Film film : movies) {
				result.append("<li><div class=\"view view-first\"><a href=\"movie_details?id=");
				result.append(film.getId());
				result.append("\"><img src=\"");

				if (film.getPoster().equals("N/A")) {
					result.append("images/defaultMoviePoster.jpg");
				} else {
					result.append(film.getPoster());
				}

				result.append("\"class=\"img-responsive\" alt=\"poster\"/></a><div class=\"info1\"></div>");
				result.append(
						"<div class=\"mask\"></div><div class=\"tab_desc\"><a class=\"btn href btn-flat overflow-hidden\" style=\"width: 214px\" href=\"movie_details?id=");
				result.append(film.getId());
				result.append("\">");
				
				if (locale.equals("uk")) {
					result.append(film.getTitleUk());
				} else {
					result.append(film.getTitle());
				}
				result.append("</a><div class=\"percentage-w-t-s\">");

				if (tab.equals(1) || tab.equals(4)) {
					String[] parts = film.getReleased().split("-");
					result.append("<h5>");
					result.append(parts[0]);
					result.append("</h5><p>/</p><h5>");
					result.append(parts[1]);
					result.append("</h5><p>/</p><h5>");
					result.append(parts[2]);
					result.append("</h5>");
				} else if (tab.equals(2)) {
					result.append("<p>");
					result.append(localization.get("index.userfavor"));
					result.append("</p><h5>");
					result.append(filmCountFavourite.get(film.getId()));
					result.append("</h5><p>");
					result.append(localization.get("index.users"));
					result.append("</p>");
				} else if (tab.equals(3)) {
					result.append("<h5>");
					result.append(filmCountComment.get(film.getId()));
					result.append("</h5><p>");
					result.append(localization.get("index.comment"));
					result.append("</p>");
				}
				if (tab.equals(4)) {
					result.append("<p style=\"margin-left: 40px;\">");
					result.append(localization.get("index.expect"));
					result.append("</p><h5>");
					result.append(filmCountSceduled.get(film.getId()));
					result.append("</h5>");
				}
				result.append("<div class=\"clearfix\"></div></div></div></div></li>");
			}
			out.println(result);
		}
	}
}
