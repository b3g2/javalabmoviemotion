
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!doctype html>
<html>
<head>
<title>404</title>
<jsp:include page="parts/imports.jsp" />
</head>
<body id="body-image">
	<div class="container">
		<div class="main-content">
			<jsp:include page="parts/header.jsp" />
			<div class="col-xs-12" id="content-long">
				<div class="error-page">
					<img alt="404" src="images/404.png" width="400px">
					<p>${locale.get("404.message")}</p>
					<a href="home">${locale.get("404.home")}</a>
				</div>
			</div>
		</div>
		<jsp:include page="parts/footer.jsp" />
	</div>
</body>
</html>