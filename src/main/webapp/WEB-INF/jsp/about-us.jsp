
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!doctype html>
<html>
<head>
<title>${locale.get("about-us.header")}</title>
<jsp:include page="/WEB-INF/jsp/parts/imports.jsp" />
<link href="css/custom-style/users-style.css" rel="stylesheet"
	media="all">
</head>

<body id="body-image">
	<div class="container">
		<div class="main-content">
			<jsp:include page="/WEB-INF/jsp/parts/header.jsp" />
			<div class="col-xs-12" id="content-long">

				<div id="tab1" class="">
					<div class="about-grids" style="margin: 1em 0 2em 0;">
						<div class="about-bottom-grids">
							<div class="col-sm-6 about-left">
								<div class="about-left-grids">
									<div class="col-md-2 about-left-img">
										<i class="fa fa-pencil-square-o"></i>
									</div>
									<div class="col-md-10 about-left-info">
										<a href="#" style="margin-left: -20px;">${locale.get("about-us.header")}</a>
										<p>${locale.get("about-us.about-us")}</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="col-sm-6 about-right">
								<div class="about-left-grids">
									<div class="col-md-2 about-left-img">
										<i class="fa fa-sitemap"></i>
									</div>
									<div class="col-md-10 about-left-info">
										<a href="#" style="margin-left: -20px;">${locale.get("about-us.about-oursite")}</a>
										<p>${locale.get("about-us.about-site")}</p>
									</div>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>

					<h4 class="title"
						style="text-align: center; width: 100%; font-size: x-large;">${locale.get("about-us.team")}</h4>
					<div class="row">

						<div id="row" class="nbs-flexisel-item"
							style="width: 210px !important; margin-right: 10px; margin-left: 230px;">
							<div class="thumbnail">
								<img src="images/team/2.jpg" alt="photo"
									class="img-responsive zoom-img img-circle"
									style="width: 170px; height: 170px;">
								<div class="caption date-city">
									<h4>${locale.get("about-us.halyna")}</h4>
									<h4>${locale.get("about-us.bihun")}</h4>
									<ul class="list-inline"
										style="margin-left: 5px; margin-top: 10px;">
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-github"></i></a></li>
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
						</div>

						<div id="row" class="nbs-flexisel-item"
							style="width: 210px !important; margin-right: 10px; margin-left: 10px;">
							<div class="thumbnail">
								<img src="images/team/1.jpg" alt="photo"
									class="img-responsive zoom-img img-circle"
									style="width: 170px; height: 170px;">
								<div class="caption date-city">
									<h4>${locale.get("about-us.marian")}</h4>
									<h4>${locale.get("about-us.fediv")}</h4>
									<ul class="list-inline"
										style="margin-left: 5px; margin-top: 10px;">
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-github"></i></a></li>
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
						</div>

						<div id="row" class="nbs-flexisel-item"
							style="width: 210px !important; margin-right: 10px; margin-left: 10px;">
							<div class="thumbnail">
								<img src="images/team/4.jpg" alt="photo"
									class="img-responsive zoom-img img-circle"
									style="width: 170px; height: 170px;">
								<div class="caption date-city">
									<h4>${locale.get("about-us.vika")}</h4>
									<h4>${locale.get("about-us.ml")}</h4>
									<ul class="list-inline"
										style="margin-left: 5px; margin-top: 10px;">
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-github"></i></a></li>
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="row">
						<div id="row" class="nbs-flexisel-item"
							style="width: 210px !important; margin-right: 10px; margin-left: 350px;">
							<div class="thumbnail">
								<img src="images/team/3.jpg" alt="photo"
									class="img-responsive zoom-img img-circle"
									style="width: 170px; height: 170px;">
								<div class="caption date-city">
									<h4>${locale.get("about-us.panas")}</h4>
									<h4>${locale.get("about-us.sydor")}</h4>
									<ul class="list-inline"
										style="margin-left: 5px; margin-top: 10px;">
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-github"></i></a></li>
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
						</div>


						<div id="row" class="nbs-flexisel-item"
							style="width: 210px !important; margin-right: 10px; margin-left: 10px;">
							<div class="thumbnail">
								<img src="images/team/5.jpg" alt="photo"
									class="img-responsive zoom-img img-circle"
									style="width: 170px; height: 170px;">
								<div class="caption date-city">
									<h4>${locale.get("about-us.oleh")}</h4>
									<h4>${locale.get("about-us.kosar")}</h4>
									<ul class="list-inline"
										style="margin-left: 5px; margin-top: 10px;">
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-facebook"></i></a></li>
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-github"></i></a></li>
										<li><a href="#"><i
												style="color: #003B64; font-size: 20px;"
												class="fa fa-google-plus"></i></a></li>
									</ul>
								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>
			</div>
		</div>
		<jsp:include page="/WEB-INF/jsp/parts/footer.jsp" />
	</div>
</body>
</html>
