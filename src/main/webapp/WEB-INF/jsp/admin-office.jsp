<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--suppress ALL --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!doctype html>
<html>
<head>
    <title>${locale.get("admin.office.header")}</title>
    <jsp:include page="parts/imports.jsp"/>
</head>
<body id="body-image">
<div class="container">
    <div class="main-content">
        <%--header--%>
        <jsp:include page="parts/header.jsp"/>
        <div class="container-fluid" id="users-top">
            <div class="col-xs-1" id="left-side-bar">
                <%--left side bar--%>
                <nav>
                    <div class="menu-item">
                        <a href="admin_office?page=users" class="item-icon"><i class="fa fa-users"></i></a>
                        <a href="admin_office?page=users"
                           class="item-label"><span>${locale.get("admin.office.users")}</span></a>
                    </div>
                    <div class="menu-item open-modify-cinema-modal">
                        <a href="#" class="item-icon"><b><i class="fa fa-search"></i>c</b></a>
                        <a href="#" class="item-label"><span>${locale.get("admin.office.cinema")}</span></a>
                    </div>
                </nav>
            </div>
            <div class="col-xs-9" id="center-content" style="padding-left: 28px">
                <%--modals--%>
                <jsp:include page="parts/admin-office/modify-cinema-modal.jsp"/>
                <%--content--%>
                <c:if test="${page.equals('users')}">
                    <jsp:include page="parts/admin-office/users.jsp"/>
                </c:if>
            </div>
            <div class="col-sm-2 pull-right hidden-xs" id="right-side-bar">
                <%--right side bar--%>
                <c:if test="${page.equals('users')}">
                    <jsp:include page="parts/admin-office/right-sidebar.jsp"/>
                </c:if>
            </div>
        </div>
    </div>
    <jsp:include page="parts/footer.jsp"/>
</div>
</body>
</html>