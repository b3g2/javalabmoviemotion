
<%@ page contentType="text/html;charset=UTF-8" language="java"%>
<!doctype html>
<html>
<head>
<title>${locale.get("contact.header")}</title>
<jsp:include page="/WEB-INF/jsp/parts/imports.jsp" />
<link href='//fonts.googleapis.com/css?family=Oxygen:400,700,300'
	rel='stylesheet' type='text/css'>
<link
	href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800'
	rel='stylesheet' type='text/css'>
</head>
<body id="body-image">
	<div class="container">
		<div class="main-content">
			<jsp:include page="/WEB-INF/jsp/parts/header.jsp" />
			<div class="col-xs-12" id="content-long">
				<div class="contact-form">
					<div class="col-sm-10 col-sm-offset-1 contact-grid">
						<h3 class="title text-center">${locale.get("contact.header")}</h3>
						<form action="contact_us" target="_top" method="post"
							id="contact-from">
							<div style="display: flex;">
								<div class="col-xs-4">
									<div class="row">
										<h5 class="title">${locale.get("contact.name")}</h5>
										<div class="marker">
											<input class="form-control input-flat" type="text"
												name="name">
										</div>
									</div>
									<div class="row">
										<h5 class="title">${locale.get("contact.email")}</h5>
										<div class="marker">
											<input class="form-control input-flat" type="email"
												name="email">
										</div>
									</div>
								</div>
								<div class="col-xs-8">
									<h5 class="title" style="text-align: right;">${locale.get("contact.message")}</h5>
									<div class="marker">
										<textarea style="width: 103%; min-height: 100px;" class="form-control input-flat" cols="50" rows="4"
											name="msg" style="min-height: 0"></textarea>
									</div>
								</div>
							</div>
							<input type="submit"
								class="btn btn-primary btn-flat col-xs-2 col-xs-offset-10"
								value='${locale.get("contact.send")}'>
						</form>
					</div>
				</div>
				<div class="col-sm-10 col-sm-offset-1" style="padding-left: 0">
					<h3 class="title text-center">${locale.get("contact.location")}</h3>
					<div class="c-map">
						<iframe
							src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2572.9809141434325!2d23.99797901519316!3d49.842816138841634!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x473add9b13e1c4a3%3A0x35d59b22c44e0c80!2z0LLRg9C7LiDQntC70LXQvdC4INCh0YLQtdC_0LDQvdGW0LLQvdC4LCA0NSwg0JvRjNCy0ZbQsiwg0JvRjNCy0ZbQstGB0YzQutCwINC-0LHQu9Cw0YHRgtGM!5e0!3m2!1sru!2sua!4v1454613135198"
							width="600" height="450" frameborder="0" style="border: 0"
							allowfullscreen></iframe>
					</div>
				</div>
			</div>
			<jsp:include page="/WEB-INF/jsp/parts/footer.jsp" />
		</div>
	</div>
</body>
</html>
<script>
	var form = $('#contact-from').initForm();
	form.validate({
				rules : {
					name : {
						required : true,
						lValidate : {
							type : 'name'
						},
					},
					email : {
						required : true,
						lValidate : {
							type : 'email'
						},
					},
					msg : {
						required : true,
						lValidate : {
							type : 'msg'
						},
					}
				},
				messages : {
					name : {
						lValidate : '${locale.get("cinema.modal.valid.name")}',
					},
					email : {
						lValidate : '${locale.get("modal.restore.valid")}',
					},
					msg : {
						lValidate : '${locale.get("contact.valid.message")}',
					},
				}
			});
</script>