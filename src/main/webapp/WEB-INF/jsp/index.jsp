<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="tag" uri="/WEB-INF/custom.tld"%>

<!DOCTYPE html>
<html>
<head>
<title>${locale.get("parts.header.home")}</title>
<jsp:include page="parts/imports.jsp" />
<!-- Custom Theme files -->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<!-- start menu -->
<script type="text/javascript" src="scripts/jquery.leanModal.min.js"></script>
<script src="scripts/easyResponsiveTabs.js" type="text/javascript"></script>

<!---- start-smoth-scrolling---->
<script type="text/javascript" src="scripts/move-top.js"></script>
<script type="text/javascript" src="scripts/easing.js"></script>

<!---- start-smoth-scrolling---->
<link href="css/slider.css" rel="stylesheet" type="text/css" media="all" />
<link href="css/custom-style/mf-style.css" rel="stylesheet" media="all">
<link href="css/custom-style/index-style.css" rel="stylesheet"
	media="all">
<script type="text/javascript" src="scripts/jquery.nivo.slider.js"></script>
<script type="text/javascript" src="scripts/custom-js/index.js"></script>
<script defer src="scripts/jquery.flexslider.js"></script>
<script type="text/javascript" src="scripts/jquery.flexisel.js"></script>
</head>

<body id="body-image">
	<div class="container">
		<div class="main-content">
			<jsp:include page="parts/header.jsp" />

			<div class="main-banner">
				<div class="banner col-sm-12"
					style="padding-left: 0px; padding-right: 0px;">
					<section class="slider">
						<div class="slider">
							<div class="slider-wrapper theme-default">
								<div id="slider" class="nivoSlider">
									<img src="images/pic1.jpg" data-thumb="images/1.jpg" alt="" />
									<img src="images/pic2.jpg" data-thumb="images/2.jpg" alt="" />
									<img src="images/pic3.jpg" data-thumb="images/3.jpg" alt="" />
									<img src="images/pic4.jpg" data-thumb="images/4.jpg" alt="" />
								</div>
							</div>
						</div>
					</section>
				</div>
				<div class="clearfix"></div>
			</div>

			<h4 class="index-title title">${locale.get("index.title.top")}</h4>
			<div class="review-slider">
				<ul id="flexiselDemo1">
					<c:if test="${not empty top_rating_mm}">
						<c:forEach var="film" items="${top_rating_mm}">
							<li>
								<div class="poster-wrapper" style="margin: 0;">
									<div class="poster-view poster-view-tenth"
										style="width: 187px; height: 260px;">
										<c:if test="${film.poster ne 'N/A'}">
											<img src="${film.poster}" alt="poster">
										</c:if>
										<c:if test="${film.poster eq 'N/A'}">
											<img src="images/defaultMoviePoster.jpg" alt="poster">
										</c:if>
										<div class="mask">
											<c:if test="${locale.language eq 'uk'}">
												<h2 style="font-size: 13px; margin: 20px 10px 0 10px;">${film.titleUk}</h2>
											</c:if>
											<c:if test="${locale.language eq 'en'}">
												<h2 style="font-size: 13px; margin: 20px 10px 0 10px;">${film.title}</h2>
											</c:if>
											<p>
												<i class="fa fa-calendar">&nbsp;</i>
												<c:if test="${not empty film.plot}">${film.released}</c:if>
												<c:if test="${empty film.plot}">N/A</c:if>
											</p>
											<c:if
												test="${locale.language eq 'en' && not empty film.plot}">
												<p class="plot" style="font-size: 11px; line-height: 15px;">${film.plot}</p>
											</c:if>
											<c:if
												test="${locale.language eq 'uk' && not empty film.plotUk}">
												<p class="plot" style="font-size: 11px; line-height: 15px;">${film.plotUk}</p>
											</c:if>
											<a href="movie_details?id=${film.id}" class="info btn-flat">${locale.get("movies.details")}</a>
										</div>
									</div>
									<c:if test="${locale.language eq 'uk'}">
										<a href="movie_details?id=${film.id}"
											style="max-width: 187px;"
											class="btn href btn-primary btn-flat overflow-hidden">${film.titleUk}</a>
									</c:if>
									<c:if test="${locale.language eq 'en'}">
										<a href="movie_details?id=${film.id}"
											style="max-width: 187px;"
											class="btn href btn-primary btn-flat overflow-hidden">${film.title}</a>
									</c:if>
								</div>
							</li>
						</c:forEach>
					</c:if>
				</ul>
			</div>
			<div class="footer-top-grid">
				<div class="list-of-movies col-md-8">
					<div class="tabs">
						<div class="sap_tabs">
							<div id="horizontalTab"
								style="display: block; width: 100%; margin: 0px;">
								<ul class="resp-tabs-list">
									<li class="resp-tab-item" aria-controls="tab_item-2" role="tab"><span>${locale.get("index.release")}</span></li>
									<li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><span>${locale.get("index.favourite")}</span></li>
									<li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><span>${locale.get("index.commented")}</span></li>

								</ul>
								<div class="clearfix"></div>
								<div class="resp-tabs-container">
									<div class="tab-1 resp-tab-content"
										aria-labelledby="tab_item-0">
										<ul class="tab_img">
											<tag:top-three-movies movies="${released_for_week}" tab="1"
												locale="${locale.language}" />
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="tab-1 resp-tab-content"
										aria-labelledby="tab_item-1">
										<ul class="tab_img">
											<tag:top-three-movies moviesWithStatistic="${top_favourite}"
												tab="2" locale="${locale.language}" />
										</ul>
										<div class="clearfix"></div>
									</div>
									<div class="tab-1 resp-tab-content"
										aria-labelledby="tab_item-2">
										<ul class="tab_img">
											<tag:top-three-movies moviesWithStatistic="${top_commented}"
												tab="3" locale="${locale.language}" />
										</ul>
										<div class="clearfix"></div>
									</div>

								</div>
							</div>
						</div>
					</div>
					<div class="clearfix"></div>
					<div class="featured">
						<h4 class="index-title title" style="color: #003b64;">${locale.get("index.expected")}</h4>
						<ul>
							<tag:top-three-movies moviesWithStatistic="${top_released}"
								tab="4" locale="${locale.language}" />
						</ul>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="right-side-bar">
					<div class="top-movies-in-india">
						<h4 class="title" style="color: #003b64;">${locale.get("index.top")}</h4>
						<c:if test="${not empty top_movies_list}">
							<c:forEach var="film" items="${top_movies_list}">
								<ul class="mov_list">
									<li><i class="fa fa-star"></i></li>
									<a href="movie_details?id=${film.id}">
										<li>${film.ratingCustom * 10}&#09;%</li>&#09;&#09; <c:if
											test="${locale.language eq 'uk'}">
											<li style="font-family: sans-serif;">${film.titleUk}</li>
										</c:if> <c:if test="${locale.language eq 'en'}">
											<li style="font-family: sans-serif;">${film.title}</li>
										</c:if>
									</a>
								</ul>
							</c:forEach>
						</c:if>
					</div>
					<div class="our-blog">
						<h4 class="title">${locale.get("index.comments")}</h4>

						<c:forEach var="comment" items="${last_comments}">
							<div class="post-article">
								<a href="movie_details?id=${comment.film.id}" class="post-title">
									<c:if test="${locale.language eq 'uk'}">
										${comment.film.titleUk}
									</c:if> <c:if test="${locale.language eq 'en'}">
										${comment.film.title}
									</c:if>
								</a> <i>${locale.get("index.posted")} ${comment.postedTime}</i>
								<p>${comment.comment.content}<br> <a
										href="user?id=${comment.commentator.id}">${comment.commentator.firstName}&nbsp;${comment.commentator.lastName}</a>
								</p>
							</div>
						</c:forEach>

					</div>
				</div>
				<div class="clearfix"></div>
			</div>
			<jsp:include page="parts/footer.jsp" />
		</div>
	</div>
</body>
</html>