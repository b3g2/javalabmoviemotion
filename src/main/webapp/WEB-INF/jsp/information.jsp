<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>${cinema.cinemaName}</title>
<jsp:include page="parts/imports.jsp" />
<script src="scripts/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<script type="text/javascript">
	function MinimumNValidate() {
		var min = document.getElementById("minprice").value;
		var max = document.getElementById("maxprice").value;
		if (min > max) {
			swal("Minimum value must be lesser than maximum value.",'','error');
		}
	}

	function MaximumNValidate() {
		var min = document.getElementById("minprice").value;
		var max = document.getElementById("maxprice").value;
		if (max < min) {
            swal("Maximum value must be greater than minimum value.",'','error');
		}
	}

	function del() {
		var el = document.getElementById(arguments[0]);
		el.style.opacity = "0.4";
		var x = document.getElementsByClassName("del");
		for (i = 0; i < x.length; i++) {
			if (x[i].id == arguments[0]) {
				x[i].style.display = "none";
			}
		}
		var y = document.getElementsByClassName("res");
		for (i = 0; i < y.length; i++) {
			if (y[i].id == arguments[0]) {
				y[i].style.display = "block";
			}
		}
		var e = document.getElementsByClassName("edit");
		for (i = 0; i < e.length; i++) {
			if (e[i].id == arguments[0]) {
				e[i].style.display = "none";
			}
		}
	}

	function res() {
		var el = document.getElementById(arguments[0]);
		el.style.opacity = "1";
		var x = document.getElementsByClassName("del");
		for (i = 0; i < x.length; i++) {
			if (x[i].id == arguments[0]) {
				x[i].style.display = "block";
			}
		}
		var y = document.getElementsByClassName("res");
		for (i = 0; i < y.length; i++) {
			if (y[i].id == arguments[0]) {
				y[i].style.display = "none";
			}
		}
		var e = document.getElementsByClassName("edit");
		for (i = 0; i < e.length; i++) {
			if (e[i].id == arguments[0]) {
				e[i].style.display = "block";
			}
		}
	}
</script>
<script>
	$(document).on("click", ".open-editDialog", function() {
		var myBookId = $(this).data('id');
		var res = myBookId.split(",");
		var ma = document.getElementsByClassName(res[1] + "title");
		$(".modal-body #bookId").val(myBookId);
		$(".modal-body #titleF").val(ma[0].value);
		var dtS = document.getElementsByClassName(res[1] + "dateSMovie");
		var dtF = document.getElementsByClassName(res[1] + "dateFMovie");
		var minP = document.getElementsByClassName(res[1] + "minMovie");
		var maxP = document.getElementsByClassName(res[1] + "maxMovie");
		$(".modal-body #dateSF").val(dtS[0].value);
		$(".modal-body #dateFF").val(dtF[0].value);
		$(".modal-body #priceMin").val(minP[0].value);
		$(".modal-body #priceMax").val(maxP[0].value);
	});
</script>

</head>
<body id="body-image">
	<c:if test="${editFilm eq 'yes'}">
		<script>
			sweetAlert({
				title : "${locale.get('information.alert')}",
				type : "success"
			});
		</script>
	</c:if>
	<div class="container">
		<jsp:include page="modals/edit-film-modal.jsp" />
		<div class="main-content">
			<jsp:include page="parts/header.jsp" />
			<div class="container-fluid">
				<div class="col-xs-1" id="left-side-bar">
					<%--left side bar--%>
					<c:if test="${user.role eq 'moderator'}">
						<nav>
							<div class="menu-item">
								<a href="moderator" class="item-icon"><i class="fa fa-film"></i></a>
								<a href="moderator" class="item-label"><span>${locale.get("moderator.requests")}</span></a>
							</div>
							<div class="menu-item">
								<a href="worker" class="item-icon"><i
									class="fa fa-television"></i></a> <a href="worker"
									class="item-label"><span>${locale.get("profile.cinema")}</span></a>
							</div>
							<div class="menu-item open-add-news-modal">
								<a href="#" class="item-icon"><i class="fa fa-gift"></i></a> <a
									href="#" class="item-label"><span>${locale.get("moderator.news")}</span></a>
							</div>
						</nav>
					</c:if>
					<c:if test="${user.role ne 'moderator'}">
						<nav>
							<div class="menu-item">
								<a href="profile" class="item-icon"><i class="fa fa-user"></i></a>
								<a href="profile" class="item-label"><span>${locale.get("profile.prof")}</span></a>
							</div>
							<div class="menu-item">
								<a href="friends" class="item-icon"><i
									class="fa fa-hand-o-up"></i></a> <a href="friends"
									class="item-label"><span>${locale.get("profile.subscription")}</span></a>
							</div>
							<div class="menu-item">
								<a href="cinema" class="item-icon"><i
									class="fa fa-television"></i></a> <a href="cinema"
									class="item-label"><span>${locale.get("profile.cinema")}</span></a>
							</div>
							<div class="menu-item">
								<a href="users" class="item-icon"><i class="fa fa-users"></i></a>
								<a href="users" class="item-label"><span>${locale.get("profile.users")}</span></a>
							</div>
						</nav>
					</c:if>
				</div>
				<div class="col-xs-11" id="center-content" style="width: 90%;">

					<div style="padding-left: 1em;">
						<div>
							<h2 class="title" style="width:100%; text-align:center; padding: 10px;"><i class="fa fa-film"></i>&nbsp;${cinema.cinemaName}&nbsp;<i class="fa fa-film"></i></h2>							
						</div>
						<div class="row">
						<div class="col-xs-6">
						<h3 style="margin-left:45px;" class="title">${locale.get("cinema.showings")}</h3>
						</div>
						<div class="col-xs-6" style="">
						<div style="margin-right:30px;">
						<div style="width:100%; text-align:right;">${cinema.city}, ${cinema.address}
						</div>
						<div style="width:100%; text-align:right;">
						<c:if test="${not empty cinema.site}">
						<a class="title" target="_blank" href="${cinema.site}">${cinema.site}</a>
						</c:if>
						</div>						
						</div>
						</div>
						</div>
						<div style="clear: both;">
							<c:choose>
								<c:when test="${not empty error}">
									<p style="padding-top: 4em;">
										${locale.get("information.sorry")} <br>
										${locale.get("information.site")} <br>
										${locale.get("information.info")}
									</p>
								</c:when>
								<c:when test="${empty error}">

									<div style="padding-left: 1em;">
										<div class="films-content">
											<c:forEach var="film" items="${films}">
												<c:if test="${film.state eq 'RES'}">
													<div class="poster-wrapper" id='${film.film.id }'
														style="margin: 0 25px 20px 25px;">
														<div class="poster-view poster-view-tenth">
															<c:if test="${film.film.poster ne 'N/A'}">
																<img src="${film.film.poster}" alt="poster">
															</c:if>
															<c:if test="${film.film.poster eq 'N/A'}">
																<img src="images/defaultMoviePoster.jpg" alt="poster">
															</c:if>
															<div class="mask">
																<c:if test="${locale.language eq 'uk'}">
																	<h2>${film.film.titleUk}</h2>
																</c:if>
																<c:if test="${locale.language eq 'en'}">
																	<h2>
																<c:if test="${locale.language eq 'uk'}">
																	${film.film.titleUk}
																</c:if>
																<c:if test="${locale.language eq 'en'}">
																	${film.film.title}
																</c:if></h2>
																</c:if>
																<p>
																	<i class="fa fa-calendar">&nbsp;</i>${film.dateOfStart}
																</p>
																<p>
																	<i class="fa fa-calendar">&nbsp;</i>${film.dateOfFinish}
																</p>
																<p>${locale.get("information.price") } ${film.minPrice } - ${film.maxPrice }</p>
																<c:if test="${user.role ne 'moderator'}">
																	<a href="movie_details?id=${film.film.id}"
																		class="info btn-flat">${locale.get("movies.details")}</a>
																</c:if>
																<c:if test="${user.role eq 'moderator'}">

																	<div>
																		    <c:if test="${locale.language eq 'uk'}">
																				<input type="text" value='${film.film.titleUk}' id="titleFilm" style="display: none;"
																						class="${film.film.id}title" />
																			</c:if>
																			<c:if test="${locale.language eq 'en'}">
																				<input type="text" value='${film.film.title}' id="titleFilm" style="display: none;"
																						class="${film.film.id}title" />
																			</c:if>
																			 <input type="text"
																			value='${film.dateOfStart}' id="dateSFilm"
																			style="display: none;"
																			class="${film.film.id}dateSMovie" /> <input
																			type="text" value='${film.dateOfFinish}'
																			id="dateFFilm" style="display: none;"
																			class="${film.film.id}dateFMovie" /> <input
																			type="text" value='${film.minPrice}'
																			id="priceMinFilm" style="display: none;"
																			class="${film.film.id}minMovie" /> <input
																			type="text" value='${film.maxPrice}'
																			id="priceMaxFilm" style="display: none;"
																			class="${film.film.id}maxMovie" /> <a
																			style="width: 170px; margin-bottom: 10px;"
																			href="?cinemaid=${idc}&extfilm=${film.film.id}#myModal2"
																			value=${idc } data-target="#myModal2"
																			class="info btn-flat open-editDialog"
																			data-id='${idc},${film.film.id}' data-toggle="modal">${locale.get("information.edit")}</a>

																		<form action="edit">
																			<input type="text" value='${idc }' name="cinemaid"
																				style="display: none;" /> <a style="width: 170px;"
																				href="edit?cinemaid=${idc}&delete=${film.film.id}"
																				id='${film.film.id}' onclick="del(id)"
																				class="info btn-flat">${locale.get("information.delete")}</a>
																		</form>
																	</div>

																</c:if>
															</div>
														</div>
														<a href="movie_details?id=${film.film.id}"
															style="width: 270px"
															class="btn href btn-primary btn-flat overflow-hidden">
																<c:if test="${locale.language eq 'uk'}">
																	${film.film.titleUk}
																</c:if>
																<c:if test="${locale.language eq 'en'}">
																	${film.film.title}
																</c:if></a>
													</div>
												</c:if>
												<c:if test="${user.role eq 'moderator'}">
												<c:if test="${film.state eq 'DEL'}">
													<div class="poster-wrapper" id='${film.film.id }'
														style="opacity: 0.4;">
														<div class="poster-view poster-view-tenth">
															<c:if test="${film.film.poster ne 'N/A'}">
																<img src="${film.film.poster}" alt="poster">
															</c:if>
															<c:if test="${film.film.poster eq 'N/A'}">
																<img src="images/defaultMoviePoster.jpg" alt="poster">
															</c:if>
															<div class="mask">
																<h2><c:if test="${locale.language eq 'uk'}">
																	${film.film.titleUk}
																</c:if>
																<c:if test="${locale.language eq 'en'}">
																	${film.film.title}
																</c:if>
																</h2>
																<p>
																	<i class="fa fa-calendar">&nbsp;</i>${film.dateOfStart}
																</p>
																<p>
																	<i class="fa fa-calendar">&nbsp;</i>${film.dateOfFinish}
																</p>
																<p>${locale.get("information.price") } ${film.minPrice }-${film.maxPrice}</p>
																<c:if test="${user.role eq 'moderator'}">
																	<form action="edit">
																		<div>
																			<input type="text" value='${idc }' name="cinemaid"
																				style="display: none;" />
																			<button type="submit" name="extfilm"
																				style="display: none; width: 160px;"
																				value='${film.film.id}' class="edit btn btn-flat">${locale.get("information.edit")}</button>

																			<a
																				href="edit?cinemaid=${idc}&restore=${film.film.id}"
																				id='${film.film.id}' onclick="res(id)"
																				class="info btn-flat">${locale.get("information.restore")}</a>
																		</div>
																	</form>
																</c:if>
																<c:if test="${user.role eq 'user'}">
																	<form action="movie_details">
																		<button type="submit" name="id"
																			value='${film.film.id}' class="info btn-flat">${locale.get("information.details")}</button>
																	</form>
																</c:if>
															</div>
														</div>
														<a href="movie_details?id=${film.film.id}"
															style="width: 270px"
															class="btn href btn-primary btn-flat overflow-hidden">
															<c:if test="${locale.language eq 'uk'}">
																	${film.film.titleUk}
																</c:if>
																<c:if test="${locale.language eq 'en'}">
																	${film.film.title}
															</c:if>
															</a>
													</div>
												</c:if>
												</c:if>
											</c:forEach>
										</div>
									</div>
								</c:when>
							</c:choose>
						</div>
					</div>
				</div>
			</div>
		</div>
		<jsp:include page="parts/footer.jsp" />
	</div>
</body>
</html>