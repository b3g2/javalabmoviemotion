<%--@elvariable id="locale" type="com.epam.lab.i18n.Localization"--%>
<div class="modal fade in" id="add-film-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="glyphicon glyphicon-remove-sign"></i></button>
                <h3 class="modal-title">${locale.get("modal.add.film.header")}</h3>
            </div>
            <div class="modal-body" style="padding: 50px 40px 30px 40px;">
                <form autocomplete="off" class="form-horizontal" id="add-film-mf">
                    <input type="hidden" ajax-submit="true" name="cinemaId">
                    <input type="hidden" ajax-submit="true" name="filmId">
                    <div class="form-group">
                        <label for="add-film-mf-title" class="col-sm-3 control-label">
                            ${locale.get("modal.add.film.title")}</label>
                        <div class="col-sm-9 marker">
                            <input class="form-control input-flat" id="add-film-mf-title" name="filmTitle"
                                   placeholder="${locale.get("modal.add.film.enter")}" type="text"
                                   list="add-film-title-list">
                            <datalist id="add-film-title-list"></datalist>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-film-mf-date-start" class="col-sm-3 control-label">
                            ${locale.get("modal.add.film.start")}</label>
                        <div class="col-sm-9 marker">
                            <input class="form-control input-flat" id="add-film-mf-date-start" type="date"
                                   min="" data-date-split-input="true" name="dateStart"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-film-mf-date-finish" class="col-sm-3 control-label">
                            ${locale.get("modal.add.film.finish")}</label>
                        <div class="col-sm-9 marker">
                            <input class="form-control input-flat" id="add-film-mf-date-finish" type="date"
                                   min="" data-date-split-input="true" name="dateFinish"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="modify-cinema-mf-price" class="col-sm-3 control-label">
                            ${locale.get("modal.add.film.price")}</label>
                        <div class="col-sm-9"
                             style="display: flex; flex-flow: row nowrap; justify-content: space-between">
                            <div class="marker" style="width: 45%">
                                <input class="form-control input-flat" type="text" name="minPrice"
                                       placeholder='${locale.get("modal.add.film.min")}' id="modify-cinema-mf-price">
                            </div>
                            <div class="marker" style="width: 45%">
                                <input class="form-control input-flat" type="text" name="maxPrice"
                                       placeholder='${locale.get("modal.add.film.max")}'/>
                            </div>
                        </div>
                    </div>
                    <input type="button" class="btn btn-primary btn-flat hidden col-xs-3 col-xs-offset-9"
                           id="modify-cinema-mf-submit">
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary btn-sm"
                        id="add-film-mf-submit">${locale.get("modal.add.film.add")}</button>
                <button type="reset" class="btn btn-primary btn-sm"
                        data-dismiss="modal">${locale.get("edit.close")}</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        function dateToString(date) {
            return '' + date.getFullYear() + '-' + (date.getMonth() + 1 > 9 ? date.getMonth() + 1 : '0' + (date.getMonth() + 1)) + '-' + date.getDate();
        }

        function getCurrentDate(plusDay, plusMonth, plusYear, startDate) {
            var date;
            if (startDate) {
                date = new Date(startDate);
            } else {
                date = new Date();
            }
            date.setDate(date.getDate() + (plusDay ? plusDay : 0));
            date.setMonth(date.getMonth() + (plusMonth ? plusMonth : 0));
            date.setFullYear(date.getFullYear() + (plusYear ? plusYear : 0));
            return dateToString(date);
        }

        var modal = $('#add-film-modal').initModal();
        var form = $('#add-film-mf').initForm(true);
        $('#add-film-mf-date-start').attr('min', getCurrentDate());
        $('#add-film-mf-date-finish').attr('min', getCurrentDate(1)).focus(function () {
            $(this).attr('min', getCurrentDate(1, 0, 0, $('#add-film-mf-date-start').val()));
        });
        form.validate({
            rules: {
                filmTitle: {
                    required: true,
                    lValidate: {
                        type: 'title'
                    }
                },
                dateStart: {
                    required: true,
                },
                dateFinish: {
                    required: true,
                },
                minPrice: {
                    required: true,
                    more: 0
                },
                maxPrice: {
                    required: true,
                    moreThan: '#add-film-mf input[name="minPrice"]'
                }
            }
        });
        $('#add-film-mf-submit').click(function () {
            if (form.force().valid()) {
                var filmTitle = $('#add-film-mf-title').val();
                var filmOption = $('#add-film-title-list').find('option[value="' + filmTitle + '"]');
                if (filmOption.length) {
                    var filmId = filmOption.attr('film-id');
                    form.find('input[name="filmId"]').val(filmId);
                    form.ajaxSubmit('data', 'add', 'filmCinema', function (json) {
                        var success = JSON.parse(json);
                        if (success) {
                            swal({
                                title: '${locale.get("modal.add.film.swal.success.title")}',
                                text: '${locale.get("modal.add.film.swal.success.sub")}',
                                type: 'success'
                            }, function () {
                                modal.modal('hide');
                            });
                        } else {
                            swal({
                                title: '${locale.get("modal.add.film.swal.error.title")}',
                                text: '${locale.get("modal.add.film.swal.error.sub")}',
                                type: 'error'
                            }, function () {
                                modal.modal('hide');
                            });
                        }
                    });
                } else {
                    form.find('input[name="filmTitle"]').closest('.marker').addClass('has-error');
                    swal({
                        title: '${locale.get("modal.add.film.swal.title.title")}',
                        text: '${locale.get("modal.add.film.swal.title.sub")}',
                        timer: 2000,
                        showConfirmButton: false,
                        type: 'error'
                    });
                    form.focusInvalid();
                }
            }
        });
        $('#add-film-mf-title').on('keyup focus', function (e) {
            if (e.which >= 37 && e.which <= 40) return;
            var search = $(this).val();
            if (search.length > 2) {
                $.get('data', {action: 'get', target: 'filmsForDataList', search: search, amount: 16}, function (json) {
                    var films = JSON.parse(json);
                    $('#add-film-title-list').empty();
                    films.forEach(function (el, i) {
                        var title = (localeLanguage == 'uk' ? (el.titleUk ? el.titleUk : el.title) : el.title);
                        $('#add-film-title-list').append('<option film-id="' + el.id + '" value="' + title + '"></option>');
                    });
                });
            } else {
                $('#search-list').hideb();
            }
        });
        modal.on('hidden.bs.modal', function () {
            location.reload();
        });
    });
</script>
