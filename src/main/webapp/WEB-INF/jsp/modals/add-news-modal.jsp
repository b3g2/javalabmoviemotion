<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<script src="scripts/sweetalert.min.js"></script>
<c:choose>
<c:when test="${locale.language=='uk'}">
<script src="scripts/translation-js/add-cinema-news-uk.js"></script>
</c:when>
<c:otherwise>
<script src="scripts/translation-js/add-cinema-news.js"></script>
</c:otherwise>
</c:choose>
<script src="scripts/custom-js/add-cinema-news.js"></script>
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<div class="modal fade in" id="add-news-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog" style="margin-top:5px;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="glyphicon glyphicon-remove-sign"></i></button>
                <h3 class="modal-title">${locale.get("moderator.add.news.window.title")}</h3>
            </div>
            <div class="modal-body" style="padding-bottom: 5px; padding-top: 10px;">

                <form autocomplete="off" style="border-bottom: 1px solid #e5e5e5">
                    <div class="row">
                        <div class="form-group col-xs-6">
                            <label for="add-event-cinema-city">${locale.get("moderator.add.news.cinema.city")}*</label>
                            <input name="city" list="city-list" class="form-control input-flat" id="add-event-cinema-city"
                                   placeholder="Choose city" autofocus>
                            <datalist id="city-list">
                            </datalist>
                        </div>
                        <div id="add-event-cinema-name-container" class="form-group col-xs-6" style="opacity:0.45;">
                            <label for="add-event-cinema-name">${locale.get("moderator.add.news.cinema.name")}*</label>
                            <input name="name" list="cinema-list" class="form-control input-flat" id="add-event-cinema-name"
                                   placeholder="Choose cinema" disabled>
                            <datalist id="cinema-list">
                            </datalist>
                        </div>
                            <button type="button" style="display:none;" class="btn btn-info btn-flat btn-block"
                                    id="clear-filter">${locale.get("moderator.add.news.clear")}
                            </button>
                    </div>
                </form>
                <form autocomplete="off" class="form-horizontal" id="add-event-mf" style="margin-top: 20px">
                    <div id="newsInputs" style="opacity:0.45;" class="form-group">
                        <input type="hidden" class="oldID" id="add-event-mf-id">

                        <div class="form-group marker col-xs-12" style="margin-left:0;margin-right:0;">
                        <div style="text-align:center;">
                            <label for="add-event-mf-eventHeader">${locale.get("moderator.add.news.header")}</label>
                        </div>
                        <input name="eventHeader" class="form-control input-flat" id="add-event-mf-eventHeader" disabled>
                        </div>
                        
                        <div class="form-group marker col-xs-12" style="margin-left:0;margin-right:0;">
                        <div style="text-align:center;">
                            <label for="add-event-mf-eventPicture">${locale.get("moderator.add.news.pictureURL")}</label>
                        </div>
                        <input name="eventPicture" class="form-control input-flat" id="add-event-mf-eventPicture" disabled>
                        </div>                        

                        <div style="text-align:center;"><label for="add-event-mf-eventText" class="control-label">
                            ${locale.get("moderator.add.news.description")}*</label>
                        </div>
                        <div class="col-sm-12 marker">
                            <textarea rows="6" maxlength="500" class="form-control input-flat" id="add-event-mf-eventText" name="newsDescription"
                                   placeholder="" disabled></textarea>
                        </div>
                    </div>
                    <input type="button" style="visibility:hidden; padding-left: 10px !important; padding-right: 10px !important;" value="${locale.get('general.save')}" class="btn btn-primary btn-flat col-xs-2 col-lg-offset-10"  id="add-event-mf-submit">
                </form>
            </div>
            <div style="border-top-style:none;" class="modal-footer">
            </div>
        </div>
    </div>
</div>