<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<c:if test="${open_ban_modal}">
	<script>
        $(document).ready(function () {
        	var box  = bootbox.dialog({
        		  message: "<div style='display:flex;'><img style='width: 50%;  margin: 0 140px;' src='images/ban.png' width='250px' height='150px'></div><h4 style='text-align: center; margin-top: 20px;'>Для поновлення напишіть нам на сторінці 'Контакти'</h4>",
        		  title: "<h3>${locale.get('modal.ban.text')}</h3>",
        		  buttons: {
        		    success: {
        		      label: "Продовжити",
        		      className: "btn btn-primary btn-sm",
        		      callback: function() {
        		    	  window.location.href = 'logout';
        		      }
        		    }
        		  }
        		});

            box.find('.modal-title').css({'color': '#FFF'}); 
            box.find('.modal-body').css({'padding': '20px'});
        });
    </script>
</c:if>