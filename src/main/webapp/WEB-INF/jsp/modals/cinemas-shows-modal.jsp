<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="container">
<div style="display:none;" class="row location-template">
	<div class="col-sm-12">
		<div class="movie-date-selection">
			<ul>
				<li class="location"><a href="pic-a-movie.html"><i
						class="fa fa-map-marker"></i>${locale.get("modal.cinemas.shows.header")}</a></li>						
				<li class="date-limits"><a href="#">11:00 AM</a></li>
				<li class="price-limits"><a href="#">6:20 PM</a></li>
			</ul>
		</div>
	</div>
</div>
	<div class="header-top-right">
		<!-- Large modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" style="width: 580px;">
				<div class="modal-content">
					<div class="modal-header" id="recommendation-modal-header">
						<button type="button" class="close" style="color: white;"
							data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 id="listTitle" style="text-align: center;"
							class="list-group-item-heading active modal-title modalHeader">${locale.get("modal.cinemas.shows.cinema")}</h3>
					</div>
					<div class="modal-body" id="location-modal-body" style="overflow-x: hidden;">
					
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
</div>