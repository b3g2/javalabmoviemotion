<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>

<div class="modal fade" id="myModal2" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">
                    <i class="glyphicon glyphicon-remove-sign"></i>
                </button>
                <h3 class="modal-title">${locale.get("edit.showing")}</h3>
            </div>
            <div class="modal-body" style="padding: 50px 40px 30px 40px;">
                <form action="edit-showing" autocomplete="off" class="form-horizontal" id="edit-film-mf">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">${locale.get("edit.film.title")}</label>
                        <div class="col-sm-9">
                            <input type="text" name="titleF" class="form-control input-flat" id="titleF" readonly
                                   required/>
                        </div>
                    </div>
                    <div>
                        <input type="text" name="bookId" id="bookId"
                               style="display: none;" value=""/>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">${locale.get("edit.film.start")}</label>
                        <div class="col-sm-9">
                            <input class="form-control input-flat min-today" id="dateSF" type="date"
                                   placeholder="YYYY-MM-DD"
                                   data-date-split-input="true" name="dateSF" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">${locale.get("edit.film.finish")}</label>
                        <div class="col-sm-9">
                            <input class="form-control input-flat min-today" id="dateFF" type="date"
                                   placeholder="YYYY-MM-DD"
                                   data-date-split-input="true" name="dateFF" required/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-sm-3 control-label">${locale.get("modal.add.film.price")}</label>
                        <div class="col-sm-9"
                             style="display: flex; flex-flow: row nowrap; justify-content: space-between">
                            <div style="width: 45%">
                                <input type="text" name="priceMin" id="priceMin"
                                       class="form-control input-flat" onkeyup="MinimumNValidate()"
                                       oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                       required/>
                            </div>
                            <div style="width: 45%">
                                <input type="text" name="priceMax" id="priceMax"
                                       class="form-control input-flat" onkeyup="MaximumNValidate()"
                                       oninput="this.value = this.value.replace(/[^0-9.]/g, ''); this.value = this.value.replace(/(\..*)\./g, '$1');"
                                       required/>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="submit" class="btn btn-primary btn-sm" form="edit-film-mf"
                        id="login-mf-submit">${locale.get("edit.save")}</button>
                <button type="reset" class="btn btn-primary btn-sm" form="edit-film-mf"
                        data-dismiss="modal">${locale.get("edit.close")}</button>
            </div>
        </div>
    </div>
</div>
