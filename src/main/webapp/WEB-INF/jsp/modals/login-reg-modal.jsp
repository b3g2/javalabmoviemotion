<%--@elvariable id="locale" type="com.epam.lab.i18n.Localization"--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%--login + registration modal--%>
<div class="modal fade in" id="login-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="glyphicon glyphicon-remove-sign"></i></button>
                <h3 class="modal-title">${locale.get("modal.login.header")}</h3>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-8" style="border-right: 1px dotted #C2C2C2;padding-right: 30px;">
                        <!-- Nav tabs -->
                        <ul class="nav nav-tabs">
                            <li class="active"><a href="#login-tab" data-toggle="tab">${locale.get("modal.login.log")}</a></li>
                            <li class=""><a href="#registration-tab" data-toggle="tab">${locale.get("modal.login.reg")}</a></li>
                        </ul>
                        <!-- Tab panes -->
                        <div class="tab-content">
                            <div class="tab-pane active" id="login-tab">
                                <form role="form" class="form-horizontal" id="login-mf">
                                    <div class="form-group">
                                        <label for="login-mf-email" class="col-sm-3 control-label">
                                            ${locale.get("modal.login.mail")}</label>
                                        <div class="col-sm-9 marker">
                                            <input class="form-control input-flat" id="login-mf-email" name="email"
                                                   placeholder=${locale.get("modal.login.mail")} type="email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="login-mf-password" class="col-sm-3 control-label">
                                            ${locale.get("modal.login.password")}</label>
                                        <div class="col-sm-9 marker">
                                            <input class="form-control input-flat" id="login-mf-password"
                                                   name="password"
                                                   placeholder=${locale.get("modal.login.password")} type="password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-9">
                                            <a href="#" id="login-mf-restore-password">${locale.get("modal.login.forgot")}</a>
                                            <button type="button" class="btn btn-primary btn-sm" id="login-mf-submit">
                                                ${locale.get("modal.login.submit")}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                            <div class="tab-pane" id="registration-tab">
                                <form role="form" class="form-horizontal" id="reg-mf">
                                    <div class="form-group">
                                        <label for="reg-mf-name" class="col-sm-3 control-label">
                                            ${locale.get("modal.login.pib")}</label>
                                        <div class="col-sm-9">
                                            <div class="row">
                                                <div class="col-md-6 marker">
                                                    <input class="form-control input-flat" id="reg-mf-name"
                                                           placeholder=${locale.get("modal.login.fname")}
                                                           type="text"
                                                           name="firstName">
                                                </div>
                                                <div class="col-md-6 marker">
                                                    <input class="form-control input-flat" placeholder=${locale.get("modal.login.lname")}
                                                           type="text"
                                                           name="lastName">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg-mf-email" class="col-sm-3 control-label">
                                            ${locale.get("modal.login.mail")}</label>
                                        <div class="col-sm-9 marker">
                                            <input class="form-control input-flat" id="reg-mf-email" placeholder=${locale.get("modal.login.mail")}
                                                   type="email" name="email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg-mf-password" class="col-sm-3 control-label">
                                            ${locale.get("modal.login.password")}</label>
                                        <div class="col-sm-9 marker">
                                            <input class="form-control input-flat" id="reg-mf-password" name="password"
                                                   placeholder=${locale.get("modal.login.password")}
                                                   type="password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="reg-mf-confirm-password" class="col-sm-3 control-label">
                                            ${locale.get("modal.login.confirm")}</label>
                                        <div class="col-sm-9 marker">
                                            <input class="form-control input-flat" id="reg-mf-confirm-password"
                                                   name="confirmPassword" placeholder=${locale.get("modal.login.confirm")}
                                                   type="password">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-sm-3">
                                        </div>
                                        <div class="col-sm-9">
                                            <button type="button" class="btn btn-primary btn-sm" id="reg-mf-submit">
                                                ${locale.get("modal.login.save")} &amp; ${locale.get("modal.login.continue")}
                                            </button>
                                            <button type="button" class="btn btn-default btn-sm" id="reg-mf-cancel">
                                                ${locale.get("modal.login.cancel")}
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        <div id="OR" class="hidden-xs">${locale.get("modal.login.orr")}</div>
                    </div>
                    <div class="col-md-4">
                        <div class="row text-center sign-with">
                            <div class="col-md-12">
                                <h3 class="other-nw">
                                    ${locale.get("modal.login.signwith")}</h3>
                            </div>
                            <div class="col-md-12">
                                <div class="btn-group btn-group-justified">
                                    <a href="#" class="btn btn-flat hover-shadow" id="signInWithFacebook"
                                       style="background-color: #3b5998;color:#fff">
                                        <i class="fa fa-facebook"
                                           style="margin-left: -5px;margin-right: 5px"></i> Facebook</a>
                                    <a href="#" class="btn btn-flat hover-shadow" id="signInWithGoogle"
                                       style="background-color: #dc4e41;color:#fff">
                                        <i class="fa fa-google-plus"
                                           style="margin-left: -5px;margin-right: 5px"></i>Google</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<%--change password modal--%>
<style>
    #change-modal .modal-dialog {
        width: 360px;
    }

    #change-modal .modal-body {
        padding: 25px;
        margin: 0;
    }

    #change-modal .modal-footer {
        padding: 19px 25px 20px 25px;
        margin: 0;
    }
</style>
<div class="modal fade in" id="change-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="glyphicon glyphicon-remove-sign"></i></button>
                <h3 class="modal-title">${locale.get("modal.restore.enter")}</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="change-mf">
                    <div class="form-group">
                        <div class="col-sm-12 marker">
                            <input class="form-control input-flat" id="change-mf-password" name="password"
                                   placeholder=${locale.get("modal.login.password")} type="password">
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-sm-12 marker">
                            <input class="form-control input-flat" id="change-mf-confirm-password"
                                   name="confirmPassword" placeholder=${locale.get("modal.login.confirm")} type="password">
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="reset" class="btn btn-default btn-sm" id="change-mf-cancel">
                    ${locale.get("modal.login.cancel")}
                </button>
                <button type="button" class="btn btn-primary btn-sm" id="change-mf-submit">
                    ${locale.get("modal.login.submit")}
                </button>
            </div>
        </div>
    </div>
</div>
<%--resore password modal--%>
<div class="modal fade in" id="restore-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="glyphicon glyphicon-remove-sign"></i></button>
                <h3 class="modal-title">${locale.get("modal.restore.header")}</h3>
            </div>
            <div class="modal-body">
                <form class="form-horizontal" id="restore-mf">
                    <div class="group-1">
                        <div class="row">
                            <p>${locale.get("modal.restore.mail")}</p>
                        </div>
                        <div class="row marker">
                            <input class="form-control input-flat" id="restore-mf-email" name="email"
                                   placeholder="example@example.cm" type="email">
                        </div>
                    </div>
                    <div class="group-2 hidden fade in">
                        <p><b>${locale.get("modal.restore.soon")}</b></p>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <div class="group-1">
                    <button type="button" form="restore-mf" id="restore-mf-cancel"
                            class="btn btn-primary">${locale.get("modal.login.cancel")}
                    </button>
                    <button type="button" form="restore-mf" id="restore-mf-submit"
                            class="btn btn-primary">${locale.get("modal.login.submit")}
                    </button>
                </div>
                <div class="group-2">
                    <button type="button" form="restore-mf" id="restore-mf-OK"
                            class="btn btn-primary">OK
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        //open modal
        $('.open-login-modal').click(function (e) {
            e.preventDefault();
            $('#login-modal').modal({show: true});
        });
        var modal = $('#login-modal').initModal();
        //---login form-------------------------------------------------------------------------------------------------
        var loginForm = $('#login-mf').initForm();
        loginForm.validate({
            rules: {
                email: {
                    required: true,
                    lValidate: {
                        type: 'email'
                    }
                },
                password: {
                    required: true
                }
            },
            messages: {
                email: {
                    lValidate: '${locale.get("modal.restore.valid")}'
                }
            }
        });
        $('#login-mf-submit').click(function () {
            if (loginForm.force().valid()) {
                loginForm.ajaxSubmit('login', 'signIn', null, function (json) {
                    var resp = JSON.parse(json);
                    if (!resp.valid) {
                        loginForm.find('.marker').addClass('has-error').removeClass('has-success')
                                .end().find('.marker').last().find('.help-block').text('${locale.get("modal.restore.wrong")}').showb();
                    } else {
                        loginForm.find('.marker').addClass('has-success').removeClass('has-error')
                                .end().find('.marker').last().find('.help-block').text('').hideb();
                        modal.modal('hide');
                        loginForm.clearForm();
                        if (resp.restore) {
                            $('#change-modal').modal({show: true});
                        } else {
                            onlogin();
                        }
                    }
                });
            }
        });
        loginForm.find('input[name="password"]').keypress(function (e) {
            if (e.which == 13) {
                $('#login-mf-submit').click();
            }
        });
        $('#login-mf-restore-password').click(function (e) {
            e.preventDefault();
            $('#restore-modal').modal({show: true});
        });
        //---restore password modal--------------------------------------------------------------------------------------
        var restoreModal = $('#restore-modal').initModal();
        var restoreForm = $('#restore-mf').initForm();
        restoreForm.validate({
            rules: {
                email: {
                    required: true,
                    lValidate: {
                        type: 'email'
                    },
                    remote: {
                        url: 'login',
                        type: 'POST',
                        data: {
                            'action': "checkEmail",
                            'data': function () {
                                return $('#restore-mf').find('[name="email"]').val().toLowerCase();
                            },
                            delay: 3
                        }
                    }
                }
            },
            messages: {
                email: {
                    lValidate: '${locale.get("modal.restore.valid")}',
                    remote: '${locale.get("modal.restore.nosuch")}'
                }
            }
        });
        var restoreFormGroup1 = restoreModal.find('.group-1');
        var restoreFormGroup2 = restoreModal.find('.group-2');
        restoreModal.on('shown.bs.modal', function () {
            restoreFormGroup1.showb();
            restoreFormGroup2.hideb();
        });
        $('#restore-mf-cancel').click(function () {
            restoreModal.modal('hide');
        });
        $('#restore-mf-submit').click(function () {
            if (restoreForm.force().valid()) {
                var email = restoreForm.find('[name="email"]').val().toLowerCase();
                $.post('login', {'action': 'restorePassword', 'data': email});
                restoreFormGroup1.hideb();
                restoreFormGroup2.showb();
            }

        });
        $('#restore-mf-OK').click(function () {
            restoreModal.modal('hide');
        });
        //---change password modal--------------------------------------------------------------------------------------
        var changeModal = $('#change-modal').initModal();
        var changeForm = $('#change-mf').initForm(true);
        changeForm.validate({
            rules: {
                password: {
                    required: true,
                    lValidate: {
                        type: 'password'
                    }
                },
                confirmPassword: {
                    equalTo: '#change-mf input[name="password"]'
                }
            }
        });
        $('#change-mf-cancel').click(function () {
            changeModal.modal('hide');
        });
        $('#change-mf-submit').click(function () {
            if (changeForm.force().valid()) {
                changeForm.ajaxSubmit('login', 'changePassword', null, function (json) {
                    onlogin();
                });
            }
        });
        //---registration form------------------------------------------------------------------------------------------
        var regForm = $('#reg-mf').initForm();
        regForm.validate({
            rules: {
                firstName: {
                    required: true,
                    lValidate: {
                        type: 'name'
                    }
                },
                lastName: {
                    required: true,
                    lValidate: {
                        type: 'name'
                    }
                },
                email: {
                    required: true,
                    lValidate: {
                        type: 'email'
                    },
                    remote: {
                        url: 'registration',
                        type: 'POST',
                        data: {
                            'action': "checkEmail",
                            'data': function () {
                                return $('#reg-mf').find('[name="email"]').val().toLowerCase();
                            },
                            delay: 3
                        }
                    }
                },
                password: {
                    required: true,
                    lValidate: {
                        type: 'password'
                    }
                },
                confirmPassword: {
                    equalTo: '#reg-mf [name="password"]'
                }
            },
            messages: {
                firstName:{
                    lValidate: '${locale.get("reg.modal.valid.fname")}'
                },
                lastName:{
                    lValidate: '${locale.get("reg.modal.valid.lname")}'
                },
                email: {
                    lValidate: '${locale.get("reg.modal.valid.email")}',
                    remote: '${locale.get("reg.modal.valid.already")}'
                },
                password:{
                    lValidate: '${locale.get("reg.modal.valid.password")}'
                }
            }
        });
        $('#reg-mf-submit').click(function () {
            if (regForm.force().valid()) {
                modal.find('.nav.nav-tabs a[href="#login-tab"]').click();
                var email = regForm.find('[name="email"]').val();
                var password = regForm.find('[name="password"]').val();
                regForm.ajaxSubmit('registration', 'signUp', null, function () {
                    regForm.clearForm();
                    loginForm.find('[name="email"]').val(email);
                    loginForm.find('[name="password"]').val(password);
                    $('#login-mf-submit').click();
                });
            }
        });
        $('#reg-mf-cancel').click(function () {
            regForm.clearForm();
            modal.find('.nav.nav-tabs a[href="#login-tab"]').click();
        });
        startGoogleApi();
        $('#signInWithGoogle').click(function () {
            modal.modal('hide');
        });
        $('#signInWithFacebook').click(function () {
            fb_login();
            modal.modal('hide');
        });
    });
</script>
<c:if test="${not empty open_login_modal}">
    <script>
        $(document).ready(function () {
            $('#login-modal').modal({show: true});
        });
    </script>
</c:if>