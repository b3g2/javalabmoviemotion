<%--@elvariable id="locale" type="com.epam.lab.i18n.Localization"--%>
<%--@elvariable id="user" type="com.epam.lab.model.User"--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link href="css/check-box.css" rel="stylesheet" media="all">

<div class="modal fade in" id="profile-edit-modal" tabindex="-1"
     role="dialog" aria-hidden="false">
    <div class="modal-dialog modal-lg" style="width: 55%;">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"
                        aria-hidden="true">
                    <i class="glyphicon glyphicon-remove-sign"></i>
                </button>
                <h3 class="modal-title">${locale.get("modal.profileedit.header")}</h3>
            </div>
            <div class="modal-body" style="padding: 30px;">
                <div class="row">
                    <form id="profile-edit-mf" action="edit_profile" method="post"
                          enctype="multipart/form-data" class="form-horizontal">
                        <div class="row">
                            <div class="col-sm-8" style="border-right: 1px dotted #C2C2C2; padding-right: 30px;">
                                <div class="form-group">
                                    <label for="profile-edit-mf-fname" class="col-sm-3 control-label">
                                        ${locale.get("modal.profileedit.fname")}</label>
                                    <div class="col-sm-9 marker">
                                        <input class="form-control input-flat" id="profile-edit-mf-fname"
                                               name="f_name" value="${user.firstName}" type="text" tabindex="1">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="profile-edit-mf-lname" class="col-sm-3 control-label">
                                        ${locale.get("modal.profileedit.lname")}</label>
                                    <div class="col-sm-9 marker">
                                        <input class="form-control input-flat" id="profile-edit-mf-lname"
                                               name="l_name" value="${user.lastName}" type="text" tabindex="2">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="profile-edit-mf-email" class="col-sm-3 control-label">
                                        ${locale.get("modal.login.mail")}</label>
                                    <div class="col-sm-9 marker">
                                        <input class="form-control input-flat" id="profile-edit-mf-email"
                                               name="email" value="${user.email}" type="text" readonly tabindex="3">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-3 control-label"></div>
                                    <div class="col-sm-9 marker">
                                        <div class="checkbox checkbox-success">
                                            <c:if test="${user.mailing eq true}">
                                                <input id="checkbox3" name="mailing" type="checkbox" checked tabindex="4">
                                            </c:if>
                                            <c:if test="${user.mailing eq false}">
                                                <input id="checkbox3" type="checkbox" tabindex="4">
                                            </c:if>
                                            <label for="checkbox3"> ${locale.get("modal.profileedit.mailing")} </label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div align="center" id="photo-user-div" style="height: 201px">
                                    <c:if test="${not empty user.photoURL}">
                                        <img src="${user.photoURL}" alt="..." class="img-circle"
                                             id="photoUser" width="150px" height="150px"
                                             style="margin-bottom: 10px;">
                                    </c:if>
                                    <c:if test="${empty user.photoURL}">
                                        <img src="images/avatarStandard.png" alt="..." id="photoUser"
                                             class="img-circle" width="150px" height="150px"
                                             style="margin-bottom: 10px;">
                                    </c:if>
                                    <input type="hidden" name="MAX_FILE_SIZE" value="4194304">

                                    <input id="selectPhoto" type="file" accept="image/*"
                                           name="photo" class="form-control" onchange='openFile(event)'
                                           style="visibility: hidden; font-size: 13px; border-radius: 0px;">
                                    <output id="list"></output>

                                    <script>
                                        $('#photo-user-div').hover(
                                                function () {
                                                    $('#selectPhoto').css(
                                                            "visibility",
                                                            "visible");
                                                },
                                                function () {
                                                    $('#selectPhoto').css(
                                                            "visibility",
                                                            "hidden");
                                                });
                                    </script>
                                </div>
                                <div align="left" id="changepass-form-div" style="height: 201px"
                                     class="col-xs-12 hidden">
                                    <div class="form-group marker">
                                        <input type="password" class="form-control input-flat"
                                               name="oldPassword" placeholder="${locale.get('changepass.oldpass')}"
                                               form="changepass-form" tabindex="8">
                                    </div>
                                    <div class="form-group marker">
                                        <input type="password" class="form-control input-flat"
                                               name="password" placeholder="${locale.get('changepass.newpass')}"
                                               form="changepass-form" tabindex="9">
                                    </div>
                                    <div class="form-group marker">
                                        <input type="password" class="form-control input-flat"
                                               name="confirmPassword"
                                               placeholder="${locale.get('changepass.confirmpass')}"
                                               form="changepass-form" tabindex="10">
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="row">
                            <div class="col-sm-2"></div>
                            <div class="col-sm-4" style="display: inline-flex;">
                                <button type="submit" class="btn btn-primary btn-sm" tabindex="5"
                                        id="login-mf-submit">${locale.get("modal.profileedit.confirm")}</button>
                                <button type="button" class="btn btn-primary btn-sm" tabindex="6"
                                        data-dismiss="modal"
                                        style="margin-left: 20px;">${locale.get("modal.profileedit.close")}</button>
                            </div>
                            <div class="col-sm-2"></div>
                            <div class="col-sm-4">
                                <button type="button" class="hidden col-xs-4 btn btn-primary btn-flat"
                                        tabindex="12" id="profile-changepass-back"><i class="fa fa-arrow-left"></i>
                                </button>
                                <c:if test="${not empty user.password}">
                                    <button type="button" class="col-xs-12 btn btn-primary btn-flat"
                                            tabindex="7" id="profile-changepass">${locale.get('changepass.change')}
                                    </button>
                                </c:if>
                                <button type="button" class="hidden col-xs-7 col-xs-offset-1 btn btn-primary btn-flat"
                                        tabindex="11" id="profile-changepass-submit"><i class="fa fa-floppy-o"></i>
                                </button>
                            </div>
                        </div>
                    </form>
                    <form autocomplete="off" id="changepass-form">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        //open modal
        $('.profile-edit-modal').click(function (e) {
            e.preventDefault();
            $('#profile-edit-modal').modal({
                show: true
            });
        });
        //on open modal
        $('#profile-edit-modal').on('shown.bs.modal', function () {
            $('#profile-edit-mf-fname').val('${user.firstName}');
            $('#profile-edit-mf-lname').val('${user.lastName}');
            $('#profile-edit-mf-email').val('${user.email}');
        });
        //validation
        var modal = $('#profile-edit-modal').initModal();
        var editForm = $('#profile-edit-mf').initForm(true);
        editForm.validate({
            rules: {
                f_name: {
                    required: true,
                    lValidate: {
                        type: 'name'
                    }
                },
                l_name: {
                    required: true,
                    lValidate: {
                        type: 'name'
                    }
                }
            },
            messages: {
                f_name: {
                    lValidate: '${locale.get("reg.modal.valid.fname")}'
                },
                l_name: {
                    lValidate: '${locale.get("reg.modal.valid.lname")}'
                }
            }
        });
        //--------------------------------------------
        $('#profile-changepass').click(function () {
            $('#photo-user-div').hideb();
            $('#profile-changepass').hideb();
            $('#changepass-form-div').showb();
            $('#profile-changepass-back').showb();
            $('#profile-changepass-submit').showb();
        });
        $('#profile-changepass-back').click(function () {
            $('#photo-user-div').showb();
            $('#profile-changepass').showb();
            $('#changepass-form-div').hideb();
            $('#profile-changepass-back').hideb();
            $('#profile-changepass-submit').hideb();
        });
        var changeForm = $('#changepass-form');
        var changeFormDiv = $('#changepass-form-div').initForm(true);
        changeForm.validate({
            rules: {
                oldPassword: {
                    required: true,
                    lValidate: {
                        type: 'password'
                    }
                },
                password: {
                    required: true,
                    lValidate: {
                        type: 'password'
                    }
                },
                confirmPassword: {
                    equalTo: '#changepass-form-div input[name="password"]'
                }
            }
        });
        $('#profile-changepass-submit').click(function () {
            changeFormDiv.force();
            if (changeForm.valid()) {
                changeFormDiv.ajaxSubmit('login', 'changePassword', null, function (json) {
                    var valid = JSON.parse(json);
                    if (valid) {
                        changeFormDiv.clearForm();
                        swal({
                            title: '${locale.get("changepass.swal.success.title")}',
                            text: '${locale.get("changepass.swal.success.sub")}',
                            timer: 2000,
                            showConfirmButton: false,
                            type: 'success'
                        });
                        $('#profile-changepass-back').click();
                    } else {
                        changeFormDiv.find('input[name="oldPassword"]').closest('.marker').addClass('has-error');
                        swal({
                            title: '${locale.get("changepass.swal.error.title")}',
                            text: '${locale.get("changepass.swal.error.sub")}',
                            timer: 2000,
                            showConfirmButton: false,
                            type: 'error'
                        });
                        changeFormDiv.focusInvalid();
                    }
                });
            }
        });
    });
</script>

<script>
    var openFile = function (event) {
        var input = event.target;

        var reader = new FileReader();
        reader.onload = function () {
            var dataURL = reader.result;
            $('#photoUser').attr('src', dataURL);
        };
        reader.readAsDataURL(input.files[0]);
    };
</script>