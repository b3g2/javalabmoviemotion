<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<style>
.filmDescription{
	overflow: hidden;
    line-height: 20px;
    height: 120px;
    padding: 0;
    margin: 0;

    display: -webkit-box;
    -webkit-line-clamp: 6;
    -webkit-box-orient: vertical
    }
</style>
	<div class="header-top-right">
		<!-- Large modal -->
		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myLargeModalLabel" aria-hidden="true">
			<div class="modal-dialog modal-lg" style="width: 600px;">
				<div class="modal-content" style="border: none">
					<div class="modal-header" id="recommendation-modal-header"
						style="border: none">
						<button type="button" class="close" style="color: white;"
							data-dismiss="modal" aria-hidden="true">&times;</button>
						<h3 id="recommendationsHeader" class="modal-title"
							style="text-align: center;"></h3>
					</div>
					<div class="modal-body" id="recommendation-modal-body">
						<div id="without_recommend">
							<div style='display: flex;'>
								<img style='margin: 0 160px;'
									src='images/recommended.png' width='200px' height='200px'>
							</div>
							<h4 style='text-align: center; margin-top: 20px; margin-bottom: -20px;' class="title">
							${locale.get("recommendation.nothing")}</h4>
						</div>
						<div class="row"  id="with_recommend">
							<div class="col-sm-5">
								<img id="moviePoster" class="img-responsive img-rounded"
									src="images/defaultMoviePoster.jpg" width="300px"
									height="320px"></img>
							</div>
							<div class="col-sm-7">
								<h2 id="movieName" style="text-align: center;"
									class="list-group-item-heading active modalHeader"></h2>

								<div class="characteristicListModal"
									style="list-style-type: none;">
									<p class="characteristicModal">${locale.get("modal.reccomendation.genre")}&nbsp;<span
											class="characteristicContentModal" id="movieGenres"></span>
									</p>
									<p class="characteristicModal">${locale.get("modal.reccomendation.duration")}&nbsp;<span
											class="characteristicContentModal" id="movieDuration"></span>
									</p>
									<p style=""	class="characteristicModal filmDescription">${locale.get("modal.reccomendation.description")}&nbsp;
										<span class="characteristicContentModal" id="movieDescription">
										</span>
									</p>
									<p class="characteristicModal">${locale.get("modal.reccomendation.actors")}&nbsp;<span
											class="characteristicContentModal" id="movieActors"></span>
									</p>
									<p class="characteristicModal">${locale.get("modal.reccomendation.released")}&nbsp;<span
											class="characteristicContentModal" id="movieReleased"></span>
									</p>
									<p class="characteristicModal">${locale.get("modal.reccomendation.imdb")}&nbsp;<span
											class="characteristicContentModal" id="movieIMDBRating"></span>
									</p>
									<p class="characteristicModal">${locale.get("modal.reccomendation.mm")}&nbsp;<span
											class="characteristicContentModal"
											id="movieRatingMovieMotion"></span>
									</p>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
