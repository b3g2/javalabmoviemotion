<%--@elvariable id="filmBase" type="com.epam.lab.model.Film"--%>
<style>
    #player-wrapper{
        width: 640px;
        height: 360px;
        background-color: #000;
    }

    #player-modal .modal-dialog {
        width: 640px;
        border: none;
        /*centering*/
        position: absolute;
        margin: 0;
        left: 50%;
        top: 50%;
        transform: translate(-50%, -50%);
    }

    #player-modal .modal-content {
        background-color: #101010;
        border: none;
    }

    #player-modal .modal-body {
        padding: 0;
        border: none;
    }

    #player-modal button.close {
        position: absolute;
        color: #fff;
        right: 2px;
        top: 2px;
        box-shadow: none;
    }
</style>
<div id="player-html">
    <%--<video id="player-frame" width="640" height="360" src="" controls="" autoplay=""></video>--%>
</div>
<div id="player-addict" class="hidden">
    <iframe class="player-frame" width="640" height="360" src=""
            allowfullscreen="true" webkitallowfullscreen="true" mozallowfullscreen="true"
            scrolling="no" frameborder="0"></iframe>
</div>
<div class="modal fade in" id="player-modal" tabindex="-1"
     role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                    <i class="glyphicon glyphicon-remove-sign"></i>
                </button>
                <div id="player-wrapper">

                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        $('#open-trailer').click(function () {
            $('#player-modal').modal({show: true});
        });
        $('#player-modal').find('.modal-content').mouseleave(function () {
            //hide close button on mouse leave
            $(this).find('button.close').hide();
        }).mouseenter(function () {
            //show close button on mouse enter
            $(this).find('button.close').show();
        }).end().on('hidden.bs.modal', function () {
            //pause player on modal hide
            $('#player-wrapper').empty();
        }).on('shown.bs.modal', function () {
            $('#player-wrapper').empty().append($(playerId).html());
        });
        var playerId;
        $.get('trailer', {imdbId: "${filmBase.imdbId}"}, function (json) {
            var trailer = JSON.parse(json);
            if (trailer[0]) {
                if (trailer[1] == 'addict') {
                    $('.player-frame').attr('src', trailer[2]);
                    playerId = '#player-addict';
                }
            } else {
                $('#open-trailer').attr('disabled', 'disabled').text('${locale.get("single.movie.notrailer")}');
            }
        });
    });
</script>
