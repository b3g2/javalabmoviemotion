
<%--suppress ALL --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!doctype html>
<html>
<head>
<title>${locale.get("moderator.title")}</title>
<jsp:include page="parts/imports.jsp" />
<%--add here your custom imports--%>
<style>
#users-search-bar {
	display: none;
	padding: 2px;
	margin: 1px 0;
	border-top: 1px solid #fff
}

#users-search-bar input, #users-search-bar input:focus {
	border: none;
}
</style>
</head>
<body id="body-image">
	<div class="container">
		<div class="main-content">
			<%--header--%>
            <jsp:include page="modals/add-film-modal.jsp" />
			<jsp:include page="modals/add-news-modal.jsp" />
			<jsp:include page="parts/header.jsp" />
			<div class="container-fluid">
				<div class="col-xs-1" id="left-side-bar">
					<%--left side bar--%>
					<nav>
						<div class="menu-item">
							<a href="moderator" class="item-icon"><i class="fa fa-film"></i></a>
							<a href="moderator" class="item-label"><span>${locale.get("moderator.requests")}</span></a>
						</div>
						<div class="menu-item">
							<a href="worker" class="item-icon"><i
								class="fa fa-television"></i></a> <a href="worker"
								class="item-label"><span>${locale.get("profile.cinema")}</span></a>
						</div>
						<div class="menu-item open-add-news-modal">
							<a href="#" class="item-icon"><i
								class="fa fa-gift"></i></a> <a href="worker"
								class="item-label"><span>${locale.get("moderator.news")}</span></a>
						</div>
					</nav>
				</div>
				<div class="col-xs-9" id="center-content">
					<%--content--%>
					<jsp:include page="parts/moderator-office/${moderator_content}.jsp" />
				</div>
				<div class="col-sm-2 pull-right hidden-xs" id="right-side-bar">
					<%--right side bar--%>
					<c:choose>
						<c:when test="${moderator_content eq 'cinema-worker'}">
							<jsp:include
								page="parts/moderator-office/cinema-statistic-bar.jsp" />
						</c:when>
						<c:when test="${moderator_content ne 'cinema-worker'}">
							<jsp:include page="parts/moderator-office/statisticsbar.jsp" />
						</c:when>
					</c:choose>
				</div>
			</div>
		</div>
		<jsp:include page="parts/footer.jsp" />
	</div>
</body>
</html>