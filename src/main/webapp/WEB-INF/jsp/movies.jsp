<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<title>${locale.get("parts.header.movies")}</title>
<jsp:include page="parts/imports.jsp" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- Custom Theme files -->

<script type="text/javascript">
        function goToPage(elem, newPage) {
            var uri = URI(window.location.href);
            uri.removeSearch("page");
            uri.addSearch("page", newPage);
            $(elem).attr("href", uri.normalizePathname());
        }
    </script>
<script type="text/javascript">
        $(document).ready(function () {
            $('.advanced-search-sub-list > li *').change(function () {
                $('#search-form').submit();
            })
        });
    </script>
<script>
        function changeFilmStatus(status, userId, filmId) {
            var likeBtn = $('#like-' + filmId);
            var dislikeBtn = $('#dislike-' + filmId);
            if (status == "LIKED") {
                if (likeBtn.hasClass('active')) {
                    likeBtn.removeClass('active');
                } else {
                    likeBtn.addClass('active');
                }
                dislikeBtn.removeClass('active');
            }
            else if (status == "DISLIKED") {
                if (dislikeBtn.hasClass('active')) {
                    dislikeBtn.removeClass('active');
                } else {
                    dislikeBtn.addClass('active');
                }
                likeBtn.removeClass('active');
            }
            $.post("changeMovieStatus", {status: status, userId: userId, filmId: filmId});
        }
    </script>
<script type="text/javascript">
    function sort(inputSort, sortFlag){
         $("#movies-sort").val(inputSort);
         $("#movies-sort-flag").val(sortFlag);
    	 $('#search-form').submit();
    }
 </script>

</head>

<body>
	<div class="container">
		<div class="main-content">
			<jsp:include page="parts/header.jsp" />
			<div class="dropdown" style="margin-top: 1em;">
				<input type="hidden" id="movies-sort" name="sort" value="${sort}" form="search-form"/>
				<input type="hidden" id="movies-sort-flag" name="sortFlag" value="${sortFlag}" form="search-form"/>
				<button data-toggle="dropdown"
					class="btn btn-flat btn-primary dropdown-toggle">
					<i class="fa fa-exchange fa-rotate-90"></i>&nbsp;${locale.get("movies.sort")}&nbsp;<b
						class="caret"></b>
				</button>
				<ul class="dropdown-menu dropdown-menu-flat">
					<c:choose>
						<c:when test="${(sort eq 'year') && (sortFlag eq 'ASC')}">
							<li><a onclick="sort('year', 'DESC')"><i
									class="fa fa-sort-amount-asc">&nbsp;</i><i
									class="fa fa-calendar"></i>&nbsp;${locale.get("movies.date")}</a></li>
						</c:when>
						<c:otherwise>
							<li><a onclick="sort('year', 'ASC')"><i
									class=" fa fa-sort-amount-asc fa-flip-vertical">&nbsp;</i><i
									class="fa fa-calendar"></i>&nbsp;${locale.get("movies.date")}</a></li>
						</c:otherwise>
					</c:choose>

					<c:choose>
						<c:when test="${(sort eq 'rating_imdb') && (sortFlag eq 'ASC')}">
							<li><a onclick="sort('rating_imdb', 'DESC')"> <i
									class=" fa fa-sort-amount-asc">&nbsp;</i> <i
									class="fa fa-bar-chart"></i>&nbsp;${locale.get("movies.imdb")}
							</a></li>
						</c:when>
						<c:otherwise>
							<li><a onclick="sort('rating_imdb', 'ASC')"><i
									class=" fa fa-sort-amount-asc fa-flip-vertical">&nbsp;</i><i
									class="fa fa-bar-chart"></i>&nbsp;${locale.get("movies.imdb")}</a></li>
						</c:otherwise>
					</c:choose>

					<c:choose>
						<c:when test="${(sort eq 'rating_custom') && (sortFlag eq 'ASC')}">
							<li><a onclick="sort('rating_custom', 'DESC')"> <i
									class=" fa fa-sort-amount-asc">&nbsp;</i> <i
									class="fa fa-line-chart"></i>&nbsp;${locale.get("movies.mm")}
							</a></li>
						</c:when>
						<c:otherwise>
							<li><a onclick="sort('rating_custom', 'ASC')"><i
									class=" fa fa-sort-amount-asc fa-flip-vertical">&nbsp;</i><i
									class="fa fa-line-chart"></i>&nbsp;${locale.get("movies.mm")}</a></li>
						</c:otherwise>
					</c:choose>

				</ul>
			</div>
			<div class="now-showing-movies">
				<div class="col-xs-8 col-sm-9 col-md-10"
					style="border-right: 1px solid #eee;">
					<div class="films-content">
						<c:if test="${not empty films}">
							<c:forEach var="record" items="${films}">
								<div class="poster-wrapper">
									<div class="poster-view poster-view-tenth">
										<c:if test="${record.film.poster ne 'N/A'}">
											<img src="${record.film.poster}" alt="poster">
										</c:if>
										<c:if test="${record.film.poster eq 'N/A'}">
											<img src="images/defaultMoviePoster.jpg" alt="poster">
										</c:if>
										<div class="mask">
											<c:if test="${(locale.language eq 'en') || (empty record.film.titleUk)}">
												<h2>${record.film.title}</h2>
											</c:if>
											<c:if test="${(not empty record.film.titleUk) && (locale.language eq 'uk')}">
												<h2>${record.film.titleUk}</h2>
											</c:if>
											<p>
												<c:if test="${not empty record.film.released}">
													<i class="fa fa-calendar">&nbsp;${record.film.released}</i>
												</c:if>
												<%--<c:if test="${empty record.film.released}"></c:if>--%>
											</p>
											<c:if test="${record.film.plot ne 'null' && not empty record.film.plot}">
												<c:if test="${locale.language eq 'en'}">
													<p class="plot">${record.film.plot}</p>
												</c:if>
												<c:if test="${locale.language eq 'uk'}">
													<p class="plot">${record.film.plotUk}</p>
												</c:if>
											</c:if>
											<%--<c:if test="${empty record.film.plot || record.film.plot eq 'null'}"></c:if>--%>
											<a href="movie_details?id=${record.film.id}"
												class="info btn-flat">${locale.get("movies.details")}</a>
											<c:if test="${not empty user}">
												<div class="likes">
													<div class="like" id="like-${record.film.id}">
														<span class="fa-stack"> <i
															class="fa fa-circle fa-stack-2x"></i> <i
															class="fa fa-thumbs-o-up fa-stack-1x fa-inverse"></i>
														</span>
													</div>
													<div class="dislike" id="dislike-${record.film.id}">
														<span class="fa-stack fa-flip-horizontal"> <i
															class="fa fa-circle fa-stack-2x"></i> <i
															class="fa fa-thumbs-o-down fa-stack-1x fa-inverse"></i>
														</span>
													</div>
												</div>
											</c:if>
										</div>
									</div>
									<a href="movie_details?id=${record.film.id}"
										style="width: 270px"
										class="btn href btn-primary btn-flat overflow-hidden">
										<c:if test="${(locale.language eq 'en') || (empty record.film.titleUk)}">
											${record.film.title}
										</c:if>
										<c:if test="${(not empty record.film.titleUk) && (locale.language eq 'uk')}">
											${record.film.titleUk}
										</c:if></a>
								</div>
								<c:if test="${not empty user}">
									<script>
                                    if ('${record.filmUser.likeValue}' == 'LIKED') {
                                        $('#like-${record.film.id}').addClass('active');
                                    }
                                    $('#like-${record.film.id}').click(function () {
                                        changeFilmStatus('LIKED', '${user.id}', '${record.film.id}');
                                    });
                                    if ('${record.filmUser.likeValue}' == 'DISLIKED') {
                                        $('#dislike-${record.film.id}').addClass('active');
                                    }
                                    $('#dislike-${record.film.id}').click(function () {
                                        changeFilmStatus('DISLIKED', '${user.id}', '${record.film.id}');
                                    });
                                </script>
								</c:if>
							</c:forEach>
						</c:if>
						<c:if test="${empty films}">
							<div class="nothingFoundIllustartion row"
								style="text-align: center; margin-bottom: 25px;">
								<img alt="nothing found image" src="images/empty-search.png">
								<h4 class="text-center"
									style="color: #003b64; font-size: 22px; font-weight: 800;">${locale.get("movies.nothing")}</h4>
							</div>
						</c:if>
					</div>
					<div class="text-center row" style="margin-bottom: 25px;">
						<c:if test="${prevPage}">
							<a class="movies-pagination" onclick="goToPage(this,${page-1})">${locale.get("movies.previous")}</a>
						</c:if>
						&nbsp;
						<c:if test="${nextPage}">
							<a class="movies-pagination" onclick="goToPage(this,${page+1})">${locale.get("movies.next")}</a>
						</c:if>
					</div>
				</div>
				<div class="col-xs-4 col-sm-3 col-md-2">
					<jsp:include page="parts/searchbar.jsp" />
				</div>
			</div>
			<jsp:include page="parts/footer.jsp" />
		</div>
	</div>
</body>
</html>