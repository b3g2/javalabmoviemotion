<%--@elvariable id="locale" type="com.epam.lab.i18n.Localization"--%>
<div class="modal fade in" id="modify-cinema-modal" tabindex="-1" role="dialog" aria-hidden="false">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true"><i
                        class="glyphicon glyphicon-remove-sign"></i></button>
                <h3 class="modal-title">${locale.get("cinema.modal.header")}</h3>
            </div>
            <div class="modal-body" style="padding: 30px 40px 55px 40px;">
                <form autocomplete="off" style="border-bottom: 1px solid #e5e5e5">
                    <div class="row">
                        <div class="form-group col-xs-4">
                            <label for="modify-cinema-city">${locale.get("cinema.modal.city")}:</label>
                            <input list="city-list" class="form-control input-flat" id="modify-cinema-city"
                                   placeholder="${locale.get("cinema.modal.choosecity")}" autofocus>
                            <datalist id="city-list">
                            </datalist>
                        </div>
                        <div class="form-group col-xs-4">
                            <label for="modify-cinema-cinema">${locale.get("cinema.modal.cinema")}:</label>
                            <input list="cinema-list" class="form-control input-flat" id="modify-cinema-cinema"
                                   placeholder="${locale.get("cinema.modal.choosecinema")}">
                            <datalist id="cinema-list">
                            </datalist>
                        </div>
                        <div class="form-group col-xs-4">
                            <label>&nbsp;</label>
                            <button type="button" class="btn btn-info btn-flat btn-block"
                                    id="clear-filter">${locale.get("cinema.modal.clear")}
                            </button>
                        </div>
                    </div>
                </form>
                <form autocomplete="off" class="form-horizontal" id="modify-cinema-mf" style="margin-top: 20px">
                    <div class="form-group">
                        <input type="hidden" class="oldID" id="modify-cinema-mf-id">
                        <input type="hidden" id="modify-cinema-mf-def-name">
                        <input type="hidden" id="modify-cinema-mf-def-city">
                        <input type="hidden" id="modify-cinema-mf-def-address">
                        <input type="hidden" id="modify-cinema-mf-def-site">
                        <label for="modify-cinema-mf-name" class="col-sm-2 control-label">
                            ${locale.get("cinema.modal.name")}*</label>
                        <div class="col-sm-10 marker">
                            <input class="form-control input-flat" id="modify-cinema-mf-name" name="cinemaName"
                                   placeholder="${locale.get("cinema.modal.name")}" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="modify-cinema-mf-city" class="col-sm-2 control-label">
                            ${locale.get("cinema.modal.city")}*</label>
                        <div class="col-sm-10 marker">
                            <input class="form-control input-flat" id="modify-cinema-mf-city" name="city"
                                   placeholder="${locale.get("cinema.modal.city")}" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="modify-cinema-mf-address" class="col-sm-2 control-label">
                            ${locale.get("cinema.modal.address")}*</label>
                        <div class="col-sm-10 marker">
                            <input class="form-control input-flat" id="modify-cinema-mf-address" name="address"
                                   placeholder="${locale.get("cinema.modal.address")}" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="modify-cinema-mf-site" class="col-sm-2 control-label">
                            ${locale.get("cinema.modal.site")}</label>
                        <div class="col-sm-10 marker">
                            <input class="form-control input-flat" id="modify-cinema-mf-site" name="site"
                                   placeholder="www.example-site.com" type="text">
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="modify-cinema-mf-state" class="col-sm-2 control-label">
                            ${locale.get("cinema.modal.state")}</label>
                        <div class="col-sm-10" style="margin-bottom: 0">
                            <input type="checkbox" id="modify-cinema-mf-state" name="state" data-toggle="toggle"
                                   data-onstyle="success" data-offstyle="danger"
                                   data-width="100" data-height="34" checked-on="working" checked-off="unworking"
                                   data-on="${locale.get('main.on')}" data-off="${locale.get('main.off')}" checked>
                        </div>
                    </div>
                    <input type="button" class="btn btn-primary btn-flat hidden col-xs-3 col-xs-offset-9"
                           id="modify-cinema-mf-submit">
                </form>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        //open modal
        $('.open-modify-cinema-modal').click(function (e) {
            e.preventDefault();
            $('#modify-cinema-modal').modal({show: true});
        });
        var modal = $('#modify-cinema-modal').initModal();
        //---login form-------------------------------------------------------------------------------------------------
        var cinemaForm = $('#modify-cinema-mf').initForm();
        cinemaForm.validate({
            rules: {
                cinemaName: {
                    required: true,
                    lValidate: {
                        type: 'title'
                    }
                },
                city: {
                    required: true,
                    lValidate: {
                        type: 'city'
                    }
                },
                address: {
                    required: true,
                    lValidate: {
                        type: 'street'
                    }
                },
                site: {
                    required: false,
                    lValidate: {
                        type: 'url'
                    }
                }
            },
            messages: {
                cinemaName: {
                    lValidate: '${locale.get("cinema.modal.valid.name")}'
                },
                city: {
                    lValidate: '${locale.get("cinema.modal.valid.city")}'
                },
                address: {
                    lValidate: '${locale.get("cinema.modal.valid.address")}'
                },
                site: {
                    lValidate: '${locale.get("cinema.modal.valid.url")}'
                }
            }
        });
        modal.on('hidden.bs.modal', function () {
            $('#modify-cinema-mf-submit').val('').hideb();
            $('#modify-cinema-mf-state').bootstrapToggle('on');
        });
        $('#modify-cinema-mf-submit').click(function () {
            if (cinemaForm.force().valid()) {
                if ($(this).val() == '${locale.get("cinema.modal.add")}') {
                    cinemaForm.ajaxSubmit('data', 'add', 'cinema', function (json) {
                        var success = JSON.parse(json);
                        if (success) {
                            $(this).val('').hideb();
                            $('#clear-filter').click();
                            cinemaForm.clearForm();
                            $('#modify-cinema-cinema').trigger('blur');
                            swal('${locale.get("cinema.modal.swal.add.title")}', '${locale.get("cinema.modal.swal.add.sub")}', 'success');
                        } else {
                            swal('${locale.get("cinema.modal.swal.opps.title")}', '${locale.get("cinema.modal.swal.opps.sub")}', 'error');
                        }
                    });
                } else if ($(this).val() == '${locale.get("cinema.modal.save")}') {
                    cinemaForm.ajaxSubmit('data', 'update', 'cinema', function (json) {
                        var success = JSON.parse(json);
                        if (success) {
                            $(this).val('').hideb();
                            $('#clear-filter').click();
                            cinemaForm.clearForm();
                            $('#modify-cinema-cinema').trigger('blur');
                            swal('${locale.get("cinema.modal.swal.update.title")}', '${locale.get("cinema.modal.swal.update.sub")}', 'success');
                        } else {
                            swal('${locale.get("cinema.modal.swal.opps.title")}', '${locale.get("cinema.modal.swal.opps.sub")}', 'error');
                        }
                    });
                }
            }
        });
//        $('#modify-cinema-mf-cancel').click(function () {
//            modal.modal('hide');
//        });
        $('#clear-filter').click(function () {
            $('#modify-cinema-city').val('').trigger('blur');
            $('#modify-cinema-cinema').val('').trigger('blur');
            $('#modify-cinema-mf-state').bootstrapToggle('on');
            cinemaForm.clearForm();
        });
        $('#modify-cinema-city').on('keyup focus', function (e) {
            if (e.which >= 37 && e.which <= 40) return;
            $('#modify-cinema-mf-submit').val('').hideb();
            var search = $('#modify-cinema-city').val();
            if (search.length) {
                cinemaForm.clearForm();
            }
            var cinema = $('#modify-cinema-cinema').val();
            $.get('data', {action: 'get', target: 'cinemas-cities', search: search, param: cinema}, function (json) {
                var content = $('#city-list').empty();
                var cities = JSON.parse(json);
                cities.forEach(function (city) {
                    content.append('<option value="' + city + '"></option>');
                });
                if (cinema.length) {
                    $('#modify-cinema-cinema').trigger('blur');
                }
            });
        });
        $('#modify-cinema-cinema').on('keyup focus blur', function (e) {
            if (e.which >= 37 && e.which <= 40) return;
            $('#modify-cinema-mf-submit').val('').hideb();
            var city = $('#modify-cinema-city').val();
            var search = $('#modify-cinema-cinema').val();
            $.get('data', {action: 'get', target: 'cinemas-names', search: search, param: city}, function (json) {
                var content = $('#cinema-list').empty();
                var cities = JSON.parse(json);
                cities.forEach(function (cinema) {
                    content.append('<option value="' + cinema + '"></option>');
                });
            });
            var idInput = $('#modify-cinema-mf-id').val('');
            var nameInput = $('#modify-cinema-mf-name, #modify-cinema-mf-def-name').val('');
            var cityInput = $('#modify-cinema-mf-city, #modify-cinema-mf-def-city').val('');
            var addressInput = $('#modify-cinema-mf-address, #modify-cinema-mf-def-address').val('');
            var siteInput = $('#modify-cinema-mf-site, #modify-cinema-mf-def-site').val('');
            $.get('data', {action: 'get', target: 'cinema', search: search, param: city}, function (json) {
                var cinema = JSON.parse(json);
                if (cinema != null) {
                    idInput.val(cinema.id);
                    nameInput.val(cinema.cinemaName);
                    cityInput.val(cinema.city);
                    addressInput.val(cinema.address);
                    siteInput.val(cinema.site);
                    if (cinema.state.toLowerCase() == "working") {
                        $('#modify-cinema-mf-state').bootstrapToggle('on');
                    } else {
                        $('#modify-cinema-mf-state').bootstrapToggle('off');
                    }
                    stateInputChange = false;
                }
            });
        });
        var stateInputChange = false;
        var inputs = $('#modify-cinema-mf-name, #modify-cinema-mf-city, #modify-cinema-mf-address, #modify-cinema-mf-site, #modify-cinema-mf-state');
        inputs.on('modify blur', function () {
            if ($('#modify-cinema-mf-id').val().length && ($('#modify-cinema-mf-name').val() != $('#modify-cinema-mf-def-name').val()
                    || $('#modify-cinema-mf-city').val() != $('#modify-cinema-mf-def-city').val()
                    || $('#modify-cinema-mf-address').val() != $('#modify-cinema-mf-def-address').val()
                    || $('#modify-cinema-mf-site').val() != $('#modify-cinema-mf-def-site').val()
                    || stateInputChange)) {
                $('#modify-cinema-mf-submit').val('${locale.get("cinema.modal.save")}').showb();
            } else if (!$('#modify-cinema-mf-id').val().length && ($('#modify-cinema-mf-name').val().length
                    || $('#modify-cinema-mf-city').val().length || $('#modify-cinema-mf-address').val().length)) {
                $('#modify-cinema-mf-submit').val('${locale.get("cinema.modal.add")}').showb();
            } else {
                $('#modify-cinema-mf-submit').val('').hideb();
            }
        });
        $('#modify-cinema-mf-state').on('click', function () {
            stateInputChange = !stateInputChange;
            $(this).trigger('modify');
        })
    });
</script>