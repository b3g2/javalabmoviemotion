<%--@elvariable id="locale" type="com.epam.lab.i18n.localization"--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<h3 class="title text-center">...</h3>
<div>
    <div>
        <div class="panel" style="background-color: #003b64;color: #fff">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <b style="font-size: 32px">A</b>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div id="admins-count"></div>
                        <div>${locale.get("admin.stat.admins")}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel" style="background-color: #004e7d;color: #fff">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <b style="font-size: 32px">M</b>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div id="moderators-count"></div>
                        <div>${locale.get("admin.stat.moders")}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel" style="background-color: #005796;color: #fff">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <b style="font-size: 32px">R</b>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div id="removed-povers-count"></div>
                        <div>${locale.get("admin.stat.removed")}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel" style="background-color: #D9534F;color: #fff">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <b style="font-size: 32px"><i class="fa fa-ban"></i></b>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div id="ban-count"></div>
                        <div>${locale.get("admin.stat.banned")}</div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel" style="background-color: #5CB85C;color: #fff">
            <div class="panel-heading">
                <div class="row">
                    <div class="col-xs-3">
                        <b style="font-size: 32px"><i class="fa fa-circle-o"></i></b>
                    </div>
                    <div class="col-xs-9 text-right">
                        <div id="unban-count"></div>
                        <div>${locale.get("admin.stat.unbanned")}</div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        adminStatistic = {
            update: function () {
                $.get('data', {
                    action: 'get',
                    target: 'user-statistic',
                    param: '${user.id}'
                }, function (json) {
                    var statistic = JSON.parse(json);
                    $('#admins-count').text(statistic.addedAdmins);
                    $('#moderators-count').text(statistic.addedModerators);
                    $('#removed-povers-count').text(statistic.removedPowers);
                    $('#ban-count').text(statistic.banCount);
                    $('#unban-count').text(statistic.unbanCount);
                });
            }
        };
        adminStatistic.update();
    });
</script>