<%--@elvariable id="locale" type="com.epam.lab.i18n.Localization"--%>
<%--@elvariable id="user" type="com.epam.lab.model.User"--%>
<%--/*hidden block--%>
<div id="user-bar-container" class="hidden">
    <div class="user-bar-main">
        <div class="user-bar-role"></div>
        <div class="user-bar-img">
            <img class="img-responsive img-circle" src="" alt="photo">
        </div>
        <div class="user-bar-info">
            <a href="#">
                <h4></h4>
                <h5></h5>
            </a>
        </div>
    </div>
    <div class="user-bar-mask">
        <div class="user-bar-mask-ban">
            <button class="_ban btn btn-flat bg-success" name="ban" d-target="user-ban" d-value="true"
                    d-group="user-ban-group">${locale.get("parts.admin.users.ban")}</button>
            <button class="_unban btn btn-flat bg-danger" name="unban" d-target="user-ban" d-value="false"
                    d-group="user-ban-group">${locale.get("parts.admin.users.unban")}</button>
        </div>
        <div class="user-bar-mask-role">
            <button class="_make-user btn btn-flat user" name="user" d-target="user-role" d-value="user"
                    d-group="user-role-group">${locale.get("parts.admin.users.makeuser")}</button>
            <button class="_make-moderator btn btn-flat moderator" name="moder" d-target="user-role"
                    d-value="moderator"
                    d-group="user-role-group">${locale.get("parts.admin.users.makemod")}</button>
            <button class="_make-administrator btn btn-flat administrator" name="admin" d-target="user-role"
                    d-value="administrator"
                    d-group="user-role-group">${locale.get("parts.admin.users.makeadmin")}</button>
        </div>
    </div>
</div>
<h3 class="title">${locale.get("parts.admin.users.user")}</h3>
<div class="btn-group btn-group-justified" id="admin-users-control-pane">
    <div class="btn-group btn-group-justified" style="width: 16%; border-right: 1px solid #fff">
        <div class="btn-group">
            <button type="button" class="btn btn-info btn-flat" value="users" id="users-search-btn">
                <i class="fa fa-search"></i>
            </button>
        </div>
        <div class="btn-group">
            <button type="button" class="btn btn-info btn-flat" value="users" id="users-refresh-btn">
                <i class="fa fa-refresh"></i>
            </button>
        </div>
    </div>
    <div class="btn-group btn-group-justified" id="users-role" style="width: 84%">
        <div class="btn-group">
            <button type="button" class="btn btn-primary btn-flat active"
                    value="all-users">${locale.get("parts.admin.users.all")}</button>
        </div>
        <div class="btn-group">
            <button type="button" class="btn btn-primary btn-flat"
                    value="users">${locale.get("parts.admin.users.user")}</button>
        </div>
        <div class="btn-group">
            <button type="button" class="btn btn-primary btn-flat"
                    value="administrators">${locale.get("parts.admin.users.admin")}</button>
        </div>
        <div class="btn-group">
            <button type="button" class="btn btn-primary btn-flat"
                    value="moderators">${locale.get("parts.admin.users.moderator")}</button>
        </div>
    </div>
</div>
<div id="users-search-bar" class="bg-info">
    <input type="text" class="form-control input-flat" placeholder=${locale.get("parts.moderator.menu.search")}>
</div>
<div class="users-bar-grid" id="user-bar-grid">
</div>
<div style="text-align: center;" class="hidden" id="nothing-found">
    <img alt="#" src="images/empty-search.png">
    <h4 class="title">Nothing found!</h4>
</div>
<nav>
    <ul class="pager">
        <li class="previous disabled" onclick="$('#users-top').goTo()"><a href="#">${locale.get("parts.admin.users.previous")}</a></li>
        <li class="page-number">1</li>
        <li class="next" onclick="$('#users-top').goTo()"><a href="#">${locale.get("parts.admin.users.next")}</a></li>
    </ul>
</nav>
<script>
    $(document).ready(function () {
        resetPager = function () {
            var content = $('#user-bar-grid').empty();
            var target = $('#users-role').find('button.active').val();
            var search = $('#users-search-bar').find('input').val();
            $.get('data', {
                action: 'info', target: target + "-amount", search: search
            }, function (json) {
                var usersAmount = JSON.parse(json);
                var onPage = 8;
                var option = {
                    totalPages: Math.ceil(usersAmount / onPage),
                    onPage: onPage,
                    nothing: function () {
                        $('#nothing-found').showb();
                    },
                    get: function () {
                        var target = $('#users-role').find('button.active').val();
                        var search = $('#users-search-bar').find('input').val();
                        $.get('data', {
                            action: 'get',
                            target: target,
                            from: usersPager.nextIndex,
                            amount: usersPager.onPage,
                            search: search
                        }, function (json) {
                            content.empty();
                            var list = JSON.parse(json);
                            if (!list.length) {
                                $('#nothing-found').showb();
                                return;
                            } else {
                                $('#nothing-found').hideb();
                            }
                            $.each(list, function (i, el) {
                                content.append('<div id="user-bar" class="user-bar">' + $('#user-bar-container').html() + '</div>');
                                var userbar = $('#user-bar');
                                userbar.find('.user-bar-role').text((el.role == 'user' ? '${locale.get("role.user")}' : el.role == 'moderator' ? '${locale.get("role.moder")}' : '${locale.get("role.admin")}'))
                                userbar.find('.user-bar-role').addClass(el.role);
                                userbar.find('.user-bar-img img').attr('src', el.photoURL != null ? el.photoURL : 'images/avatarStandard.png')
                                userbar.find('.user-bar-info').find('h4').text(el.firstName + ' ' + el.lastName)
                                userbar.find('.user-bar-info').find('h5').text(el.email);
                                userbar.find('.user-bar-info').find('a').attr('href', 'user?id=' + el.id);
                                if ('${user.id}' == el.id || '${user.accessLevel}' >= el.accessLevel) {
                                    userbar.find('.user-bar-mask button').attr('disabled', 'disabled');
                                }
                                var role = el.role.toLowerCase();
                                var ban = el.ban ? 'ban' : 'unban';
                                userbar.find('._make-' + role).hideb();
                                userbar.find('._' + ban).hideb();
                                userbar.find('[d-group]').click(
                                        function () {
                                            if (!$(this).hasClass('disabled')) {
                                                var group = $(this).attr('d-group');
                                                var target = $(this).attr('d-target');
                                                var value = $(this).attr('d-value');
                                                var currEl = $(this);
                                                $.get('data', {
                                                    action: 'update',
                                                    target: target,
                                                    data: value,
                                                    oldID: el.id,
                                                    updaterId: '${user.id}'
                                                }, function () {
                                                    $('[d-group="' + group + '"]').showb();
                                                    currEl.hideb();
                                                    usersPager.update();
                                                    adminStatistic.update();
                                                });
                                            }
                                        });
                                userbar.removeAttr('id');
                            });
                        });
                    }
                };
                usersPager = new Pager($('.pager'), option);
                usersPager.reset();
            });
        };
        resetPager();
//users role button group
        $('#users-role').find('button').click(function (e) {
            e.preventDefault();
            $('#users-role').find('button').removeClass('active');
            $(this).addClass('active');
            resetPager();
        });
//search btn
        $('#users-search-btn').click(function (e) {
            e.preventDefault();
            $('#users-search-bar').find('input').val('')
                    .end().toggle();
            resetPager();
        });
//refresh btn
        $('#users-refresh-btn').click(function (e) {
            e.preventDefault();
            resetPager()
        });
//search bar
        $('#users-search-bar').find('input').keypress(function (e) {
            if (e.which == 13) {
                resetPager();
            }
        });
    });
</script>