<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
         pageEncoding="ISO-8859-1" %>
<div class="clearfix"></div>
<div class="footer">
    <div class="col-md-12" style="margin-top: 10px;">
        <div class="footer-right">
            <div class="follow-us">
                <h5 class="f-head">${locale.get("parts.footer.follow")}</h5>
                <ul class="social-icons">
                    <li><a href="#"> <i class="fa fa-facebook"></i>
                    </a></li>
                    <li><a href="#"> <i class="fa fa-twitter"></i>
                    </a></li>
                    <li><a target="_blank" href="https://plus.google.com/u/2/106584950653345808985"> <i class="fa fa-google-plus"></i>
                    </a></li>
                    <li><a href="#"> <i class="fa fa-linkedin"></i>
                    </a></li>
                </ul>
                <div class="clearfix"></div>
            </div>
            <div class="subscribe">
                <h5 class="f-head">${locale.get("parts.footer.subscribe")}</h5>
                <input type="text" placeholder="Email" id="footer-subscribe-input">
                <input type="submit" value="${locale.get('parts.footer.subs')}" id="footer-subscribe">
                <div class="clearfix"></div>
            </div>
            <div class="recent_24by7">
                <a href="contact_us"> <i class="fa fa-question"></i>24/7
                    ${locale.get("parts.footer.care")}
                </a>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="copy-rights text-center">
        <p>&copy; 2016 Movie Motion.
            ${locale.get("parts.footer.reserved")}</p>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {

        $().UItoTop({
            easingType: 'easeOutQuart'
        });

    });
</script>
<a href="#header-scroll" class="scroll" id="toTop"
   style="display: block;"> <span id="toTopHover" style="opacity: 1;">
</span>
</a>

<style>
    .recent_24by7 {
        text-align: center;
        margin-top: 15px;
    }

    .subscribe {
        width: 38.333%;
        margin-right: 0%;
    }

    .follow-us ul {
        margin-top: 1.6em;
    }

    .footer-right {
        margin: 0.2em 0;
    }

    .copy-rights {
        margin-top: 0em;
    }
</style>