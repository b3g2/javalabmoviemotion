<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%--@elvariable id="user" type="com.epam.lab.model.User"--%>
<c:set var="currentURL"
	value="${requestScope['javax.servlet.forward.request_uri']}?${requestScope['javax.servlet.forward.query_string'].replaceAll('&lang=[a-z]{2,2}','')}" />
<jsp:include page="../modals/login-reg-modal.jsp" />
<jsp:include page="../modals/profile-edit-modal.jsp" />
<div class="mm-header" id="header-scroll">
	<div class="hidden-xs col-sm-3 left-search">
		<span>${locale.get("parts.header.search")}</span>
	</div>
	<div class="col-xs-12 col-sm-6 center-search">
		<div class="mm-search-wrapper">
			<div class="mm-search" id="header-search">
				<form action="movies" id="search-form" autocomplete="off"></form>
				<i class="fa fa-search"></i> <input type="text" id="search-input"
					form="search-form" name="toSearch" value="${toSearch}" />
				<ul id="search-list" class="hidden">
				</ul>
			</div>
		</div>
	</div>
	<div class="hidden-xs col-sm-3 right-search"></div>
</div>
<div class="bootstrap_container">
	<nav class="navbar navbar-default" role="navigation" id="headermenu">
		<div class="navbar-header">
			<button type="button" data-toggle="collapse"
				data-target="#defaultmenu" class="navbar-toggle">
				<span class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a href="home" class="item-icon header-menu-home"
				style="font-size: 28px"> <i class="fa fa-home"></i></a>
		</div>
		<div id="defaultmenu" class="navbar-collapse collapse">
			<ul class="nav navbar-nav" id="header-menu">
				<li><a href="home">${locale.get("parts.header.home")}</a></li>
				<li><a href="movies">${locale.get("parts.header.movies")}</a></li>
				<li><a href="cinema">${locale.get("parts.header.cinemas")}</a></li>
				<li><a href="users">${locale.get("parts.header.users")}</a></li>
				<li><a href="about_us">${locale.get("parts.header.about")}</a></li>
				<li><a href="contact_us">${locale.get("parts.header.contact")}</a></li>
			</ul>
			<!-- end nav navbar-nav -->
			<ul class="nav navbar-nav navbar-right">
				<c:if test="${not empty user}">
					<li class="dropdown"><a href="#" data-toggle="dropdown"
						class="dropdown-toggle header-menu">${user.firstName}&nbsp;${user.lastName}<b
							class="caret"></b></a>
						<ul class="dropdown-menu dropdown-menu-flat">
							<li role="presentation"><a href="profile" role="menuitem"><span
									class="fa fa-user fa-and-text"> </span>${locale.get("parts.header.userprofile")}</a>
							</li>
							<c:if test="${user.role eq 'administrator'}">
								<li role="presentation"><a href="admin_office"
									role="menuitem"> <span
										class="fa fa-user-secret fa-and-text"></span>${locale.get("parts.header.adminoffice")}
								</a></li>
							</c:if>
							<c:if test="${user.role eq 'moderator'}">
								<li role="presentation"><a href="moderator" role="menuitem">
										<span class="fa fa-user-secret fa-and-text"></span>${locale.get("parts.header.moderatoroffice")}
								</a></li>
							</c:if>
							<li role="presentation"><a href="#"
								class="profile-edit-modal" role="menuitem"> <span
									class="fa fa-pencil-square-o fa-and-text"></span>${locale.get("parts.header.edit")}
							</a></li>
							<li role="presentation" class="divider"></li>
							<li role="presentation"><a role="menuitem" href="logout"><span
									class="fa fa-sign-out fa-and-text"></span>${locale.get("parts.header.log")}</a></li>
						</ul></li>
				</c:if>
				<c:if test="${empty user}">
					<li><a href="#" class="open-login-modal header-menu"> <i
							class="fa fa-sign-in"></i> ${locale.get("parts.header.sign")}
					</a></li>
				</c:if>
				<li class="dropdown"><a href="#" data-toggle="dropdown"
					class="dropdown-toggle header-menu"><i class="fa fa-globe"
						style="font-size: 19px;"></i><b class="caret"></b></a>
					<ul class="dropdown-menu dropdown-menu-flat">
						<li><a href="${currentURL}&lang=en">&nbsp;&nbsp;<img
								alt="en" src="images/en.png">&nbsp;&nbsp;
						</a></li>
						<li><a href="${currentURL}&lang=uk">&nbsp;&nbsp;<img
								alt="uk" src="images/ua.png">&nbsp;&nbsp;
						</a></li>
					</ul></li>
			</ul>
		</div>
	</nav>
</div>
