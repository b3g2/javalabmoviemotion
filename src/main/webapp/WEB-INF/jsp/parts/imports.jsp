<%--@elvariable id="locale" type="com.epam.lab.i18n.Localization"--%>
<link rel="icon" href="images/logo.png">
<%--css--%>
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/bootstrap-theme.min.css">
<link rel="stylesheet" href="css/bootstrap-toggle.min.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/sweetalert.css">
<link rel="stylesheet" href="css/menu.css">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/flexslider.css">
<link rel="stylesheet" href="css/custom-style.css">
<link rel="stylesheet" href="css/poster.css">
<%--js--%>
<script src="scripts/jquery.min.js"></script>
<script src="scripts/bootstrap.min.js"></script>
<script src="scripts/bootbox.min.js"></script>
<script src="scripts/sweetalert.min.js"></script>
<script src="scripts/bootstrap-toggle.min.js"></script>
<script src="scripts/jquery.validate.min.js"></script>
<script src="scripts/custom-lib.js"></script>
<script src="scripts/custom-validator.js"></script>
<script src="scripts/heder-menu-highlighter.js"></script>
<script src="scripts/search.js"></script>
<script>
    localeLanguage = '${locale.language}';
    function onlogin() {
        location.reload();
    }
</script>
<script src="scripts/translation-js/translation.js"></script>
<script src="scripts/footer-subscribe.js"></script>
<%--social api--%>
<script src="https://apis.google.com/js/platform.js"></script>
<script src="scripts/google.signuplogin.js"></script>
<script src="scripts/fb.signuplogin.js"></script>
<!--webfont-->
<link href='//fonts.googleapis.com/css?family=Oxygen:400,700,300' rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700,800' rel='stylesheet' type='text/css'>
<!---- start-smoth-scrolling---->
<script type="text/javascript" src="scripts/move-top.js"></script>
<script type="text/javascript" src="scripts/easing.js"></script>
<script type="text/javascript" src="scripts/URI.js"></script>
<script type="text/javascript">
    $(document).ready(function ($) {
        $(".scroll").click(function (event) {
            event.preventDefault();
            $('html,body').animate({
                scrollTop: $(this.hash).offset().top
            }, 1200);
        });
    });
</script>
<!---- start-smoth-scrolling---->
<link href="css/slider.css" rel="stylesheet" type="text/css" media="all"/>
<script type="text/javascript" src="scripts/jquery.nivo.slider.js"></script>
<script type="text/javascript">
    $(window).load(function () {
        $('#slider').nivoSlider();
    });
</script>
<%--jsp--%>
<jsp:include page="../modals/ban-modal.jsp"/>
<jsp:include page="../t-validator.jsp"/>