<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<div>
	<div class="panel custom" id="panel">
		<div id="statistic-panel">
			<h3 class="title text-center">...</h3>
			<div class="panel" style="background-color: #5CB85C;">
				<div class="panel-heading">
					<div class="row" style="color: white;">
						<div class="col-xs-3">
							<i class="fa fa-plus fa-3x" style="color: #FFFFFF;"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div>${my_added}</div>
							<div class="huge">${locale.get("parts.cinema.statisticsbar.addedbyme")}</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel" style="background-color: #F0AD4E;">
				<div class="panel-heading">
					<div class="row" style="color: white;">
						<div class="col-xs-3">
							<i class="fa fa-pencil fa-3x" style="color: #FFFFFF;"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div>${my_edited}</div>
							<div class="huge">${locale.get("parts.cinema.statisticsbar.editedbyme")}</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel" style="background-color: #007DD3;">
				<div class="panel-heading">
					<div class="row" style="color: white;">
						<div class="col-xs-3">
							<i class="fa fa-plus fa-3x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div>${all_added}</div>
							<div class="huge">${locale.get("parts.cinema.statisticsbar.alladded")}</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel" style="background-color: #007DD3;">
				<div class="panel-heading">
					<div class="row" style="color: white;">
						<div class="col-xs-3">
							<i class="fa fa-pencil fa-3x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div>${all_edited}</div>
							<div class="huge">${locale.get("parts.cinema.statisticsbar.alledited")}</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
