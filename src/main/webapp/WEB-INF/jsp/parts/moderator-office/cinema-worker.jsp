<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<link href="css/dataTables.bootstrap.min.css" rel="stylesheet">
<script src="scripts/jquery.dataTables.min.js"></script>
<script src="scripts/dataTables.bootstrap.min.js"></script>
<script src="scripts/jquery.bootpag.min.js"></script>

<script>
    $(document).ready(function () {
        $('.profile_cinema_table').DataTable();
        $('.open-add-film-modal').click(function () {
            $('#add-film-mf').find('input[name="cinemaId"]').val($(this).attr('cinema-id'));
            $('#add-film-modal').modal({
                show: true
            })
        });
    });
</script>

<style>
    .dataTables_info {
        display: none;
    }

    div.dataTables_wrapper div.dataTables_length label {
        visibility: hidden;
    }
</style>

<h3 class="title" style="padding: 0 30px;">${locale.get("worker.cinemas")}</h3>
<div style="padding: 0 30px;">
    <table class="profile_cinema_table table table-striped">
        <thead>
        <tr>
            <th>${locale.get("worker.name")}</th>
            <th>${locale.get("worker.city")}</th>
            <th>${locale.get("worker.address")}</th>
            <th style="text-align: right">${locale.get("worker.showings")}<br>
                <small>${locale.get("worker.active")}</small>
            </th>
            <th style="visibility: hidden;"></th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="c" items="${cinema}">
            <tr bgcolor="#fff">
                <td><h5>
                    <a href="cinema-info?id=${c.cinema.id}" class="info">
                        <b><i class="fa fa-home"></i>&nbsp;${c.cinema.cinemaName}</b>
                    </a>
                </h5>
                </td>
                <td><h5>${c.cinema.city}</h5></td>
                <td><h5>${c.cinema.address}</h5></td>
                <td style="text-align: right;">${c.total }/${c.delete }&nbsp;&nbsp;
                    <i class="fa fa-film"></i>
                </td>
                <td style="text-align: right;">
                    <a cinema-id='${c.cinema.id }' class="open-add-film-modal"><i class="fa fa-plus"></i></a>
                </td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>