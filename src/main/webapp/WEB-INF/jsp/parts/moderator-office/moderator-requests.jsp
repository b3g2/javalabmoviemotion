<%@ page contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"
	language="java"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%--@elvariable id="user" type="com.epam.lab.model.User"--%>
<%--/*hidden block--%>
<script type="text/javascript">
function goToPage(elem, newPage){
var uri = URI(window.location.href);
uri.removeSearch("page");
uri.addSearch("page", newPage);
$(elem).attr("href", uri.normalizePathname() );
};
</script>

<script type="text/javascript">
function publish(elem){
	$(elem).button("loading");
	var id = $(elem).val();
	var plotUk = $("#plot-uk-" + id).val();
	var titleUk = $("#title-uk-" + id).val();
	var verified = $("#verified-" + id).val();
	var params = {
			filmId : id,
			plotUk : plotUk,
			titleUk : titleUk,
			verified: verified
		};
		$.ajax({
			type : "POST",
			url : "publish_movie",
			data : params,
			success : function() {
				$("#film-requests").load(document.URL + " #film-list");
				$("#panel").load(document.URL + " #statistic-panel");
				$(elem).button("reset");
			}
		});
}
</script>
<script type="text/javascript">
function collapseFilm(id){
	$("#collapse-film-title-uk-" + id).collapse('toggle');
	$("#collapse-film-name-" + id).collapse('toggle');
	}
</script>
<script type="text/javascript">
$(document).ready(function(){
$('.film-description').tooltip();
});
</script>
<script type="text/javascript">
function showSearch(){
var search = $('#users-search-bar');
if(search.css('display') == 'block'){
search.css('display','none');
} else {
search.css('display','block');
}

};
</script>
<h3 class="title" style="padding: 0 30px">${locale.get("moderator.requests")}</h3>
<form action="moderator">
	<div class="btn-group btn-group-justified"
		style="padding: 0px 30px;">
		<div class="btn-group btn-group-justified" style="width: 16%;">
			<div class="btn-group">
				<button type="button" class="btn btn-info btn-flat" value="users"
					id="users-search-btn" onclick="showSearch()">
					<i class="fa fa-search"></i>
				</button>
			</div>
			<div class="btn-group">
				<a href="moderator"
					class="btn btn-info btn-flat color-tooltip film-description"
					data-toggle="tooltip" data-placement="top" title=""
					data-original-title="Clear&nbsp;filters!"> <i
					class="fa fa-eraser"></i>
				</a>
			</div>
		</div>

		<div class="btn-group btn-group-justified" id="users-role"
			style="width: 84%; margin-bottom: 10px;">
			<div class="btn-group">
				<button type="submit"
					class="btn btn-primary btn-flat <c:if test="${empty verified}">active</c:if>"
					value="all-requests">${locale.get("parts.moderator.menu.all")}</button>
			</div>
			<div class="btn-group">
				<button type="submit"
					class="btn btn-primary btn-flat <c:if test="${not empty verified && verified eq 'checked'}">active</c:if>"
					value="checked" name="verified">${locale.get("parts.moderator.menu.checked")}</button>
			</div>
			<div class="btn-group">
				<button type="submit"
					class="btn btn-primary btn-flat <c:if test="${not empty verified && verified eq 'unchecked'}">active</c:if>"
					value="unchecked" name="verified">${locale.get("parts.moderator.menu.unchecked")}</button>
			</div>

		</div>
	</div>
	<div id="users-search-bar" class="bg-info" style="margin: 0px 30px;"
		<c:if test="${not empty filmSearch}">style="display:block;"</c:if>>
		<input type="text" class="form-control input-flat"
			placeholder="Search" name="filmSearch" value="${filmSearch}" />
	</div>
</form>
<div class="film-requests" id="film-requests" style="margin-top: 25px;">
	<ul class="list-unstyled" id="film-list">
		<c:if test="${empty films}">
			<div class="nothingFoundIllustartion row"
				style="text-align: center; margin-bottom: 25px;">
				<img alt="nothing found image" src="images/empty-search.png">
				<h4 class="text-center"
					style="color: #003b64; font-size: 22px; font-weight: 800;">${locale.get("parts.moderator.menu.nothing")}</h4>
			</div>
		</c:if>
		<c:forEach items="${films}" var="film">
			<li>
				<div class="row" style="padding: 10px;">
					<div class="col-xs-3 col-xs-offset-1 center-block"
						style="margin-left: 26px;">
						<c:if test="${film.poster ne 'N/A'}">
							<img src="${film.poster}" class="img-responsive zoom-img"
								style="height: 230px; width: 164px;" />
						</c:if>
						<c:if test="${film.poster eq 'N/A'}">
							<img src="images/defaultMoviePoster.jpg"
								class="img-responsive zoom-img"
								style="height: 230px; width: 164px;" />
						</c:if>

					</div>
					<div class="film-info col-xs-8">
						<div class="row">
							<div class="col-xs-6" id="film-status-${film.id}">
								<a class="film-editor" data-toggle="collapse"
									onclick="collapseFilm(${film.id})" href="javascript:void(0);"
									aria-expanded="false" aria-controls="collapse-film-name"
									id="film-collapse-link-${film.id}">
									<h4>${film.title}&nbsp;<c:if
											test="${film.verified eq true}">
											<span class="glyphicon glyphicon-ok" style="color: green"></span>
										</c:if> <c:if test="${film.verified eq false}">
											<span class="glyphicon glyphicon-remove" style="color: red"></span>
										</c:if> <input type="hidden" value="${film.verified}"
										id="verified-${film.id}">
								</h4>
								</a> <br />
							</div>
							<div class="col-xs-6">
								<div id="collapse-film-title-uk-${film.id}" class="collapse">
									<input type="text"
										class="film-description form-control color-tooltip"
										placeholder="Fill ukrainian title" id="title-uk-${film.id}"
										value="${film.titleUk}" data-toggle="tooltip"
										data-placement="top" title=""
										data-original-title="Ukrainian title!" />
								</div>


							</div>
						</div>
						<div class="film-features">
							<h5>${locale.get("parts.moderator.content.genre")}<small>
                                <c:if test="${locale.language eq 'en'}">${film.genres}</c:if>
                                <c:if test="${locale.language eq 'uk'}">${film.genresUk}</c:if>
							</small>
							</h5>
                            <h5>${locale.get("parts.moderator.content.country")}<small>
                                <c:if test="${locale.language eq 'en'}">${film.countries}</c:if>
                                <c:if test="${locale.language eq 'uk'}">${film.countriesUk}</c:if>
                            </small>
                            </h5>
							<c:if test="${not empty film.year}">
								<h5>${locale.get("parts.moderator.content.year")}<small>${film.year}</small>
								</h5>
							</c:if>
							<c:if test="${empty film.year}">
								<h5>${locale.get("parts.moderator.content.year")}<small>${locale.get("parts.moderator.content.unknown")}</small>
								</h5>
							</c:if>

							<c:if test="${not empty film.released}">
								<h5>${locale.get("parts.moderator.content.release")}<small>${film.released}</small>
								</h5>
							</c:if>
							<c:if test="${empty film.released}">
								<h5>${locale.get("parts.moderator.content.release")}<small>${locale.get("parts.moderator.content.unknown")}</small>
								</h5>
							</c:if>

							<c:if test="${not empty film.runtime}">
								<h5>${locale.get("parts.moderator.content.duration")}<small>${film.runtime}
										&nbsp;${locale.get("parts.moderator.content.mins")}</small>
								</h5>
							</c:if>
							<c:if test="${empty film.runtime}">
								<h5>${locale.get("parts.moderator.content.duration")}<small>${locale.get("parts.moderator.content.unknown")}</small>
								</h5>
							</c:if>

							<c:if test="${not empty film.director}">
								<h5>${locale.get("parts.moderator.content.directors")}<small>${film.director}</small>
								</h5>
							</c:if>
							<c:if test="${empty film.director}">
								<h5>${locale.get("parts.moderator.content.directors")}<small>${locale.get("parts.moderator.content.unknown")}</small>
								</h5>
							</c:if>

							<c:if test="${not empty film.writer}">
								<h5>${locale.get("parts.moderator.content.writers")}<small>${film.writer}</small>
								</h5>
							</c:if>
							<c:if test="${empty film.writer}">
								<h5>${locale.get("parts.moderator.content.writers")}<small>${locale.get("parts.moderator.content.unknown")}</small>
								</h5>
							</c:if>
							<c:if test="${not empty film.actors}">
								<h5>${locale.get("parts.moderator.content.stars")}<small>${film.actors}</small>
								</h5>
							</c:if>
							<c:if test="${empty film.actors}">
								<h5>${locale.get("parts.moderator.content.stars")}<small>${locale.get("parts.moderator.content.unknown")}</small>
								</h5>
							</c:if>

							<c:if test="${not empty film.ratingImdb}">
								<h5>${locale.get("parts.moderator.content.imdb")}<small>${film.ratingImdb}</small>
								</h5>
							</c:if>
							<c:if test="${empty film.ratingImdb}">
								<h5>${locale.get("parts.moderator.content.imdb")}<small>${locale.get("parts.moderator.content.unknown")}</small>
								</h5>
							</c:if>

							<c:if test="${not empty film.ratingCustom}">
								<h5>${locale.get("parts.moderator.content.mm")}<small>${film.ratingCustom}</small>
								</h5>
							</c:if>
							<c:if test="${empty film.ratingCustom}">
								<h5>${locale.get("parts.moderator.content.mm")}<small>${locale.get("parts.moderator.content.unknown")}</small>
								</h5>
							</c:if>
						</div>
					</div>
				</div>
				<div class="collapse" id="collapse-film-name-${film.id}">
					<div class="row" style="margin-top: 25px; padding-left: 25px;">
						<div class="film-description color-tooltip pull-left col-md-6"
							data-toggle="tooltip" data-placement="top" title=""
							data-original-title="English description!">
							<textarea class="film-description form-control" rows="8"
								placeholder="English description is empty!" disabled>${film.plot}</textarea>
						</div>
						<div class="pull-right col-md-6">
							<textarea class="film-description form-control color-tooltip"
								rows="8" placeholder="Fill ukrainian description"
								id="plot-uk-${film.id}" name="plotUk" data-toggle="tooltip"
								data-placement="top" title=""
								data-original-title="Ukrainian description!">${film.plotUk}</textarea>
						</div>
					</div>
					<br />
					<button type="button" onclick="publish(this)"
						class="btn-publish pull-right" value="${film.id}" name="filmId"
						data-loading-text="Loading...">${locale.get("parts.moderator.menu.publish")}</button>
					<div class="moderator-bottom-collapse"></div>
				</div>
			</li>
		</c:forEach>
	</ul>
</div>

<div class="row">
	<div class="col-xs-12 text-center" style="border-right: 1px #eee;">
		<br />
		<c:if test="${prevPage}">
			<a class="moderator-pagination" onclick="goToPage(this,${page-1})">${locale.get("parts.moderator.content.previous")}</a>
		</c:if>
		<c:if test="${nextPage}">
			<a class="moderator-pagination" onclick="goToPage(this,${page+1})">${locale.get("parts.moderator.content.next")}</a>
		</c:if>
	</div>
</div>
