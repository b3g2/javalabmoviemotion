<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<h3 class="title text-center">...</h3>
<div>
	<div class="panel custom" id="panel">
		<div id="statistic-panel">
			<div class="panel" style="background-color: #5CB85C;">
				<div class="panel-heading">
					<div class="row" style="color: white;">
						<div class="col-xs-3">
							<i class="fa fa-pencil-square-o fa-3x" style="color: #FFFFFF;"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">${checkedByCurrentCount}</div>
							<div>${locale.get("parts.statisticsbar.checkedByMe")}</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel" style="background-color: #F0AD4E;">
				<div class="panel-heading">
					<div class="row" style="color: white;">
						<div class="col-xs-3">
							<i class="fa fa-check-square fa-3x" style="color: #FFFFFF;"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">${checkedCount}</div>
							<div>${locale.get("parts.statisticsbar.allChecked")}</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel" style="background-color: #D9534F;">
				<div class="panel-heading">
					<div class="row" style="color: white;">
						<div class="col-xs-3">
							<i class="fa fa-tasks fa-3x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">${uncheckedCount}</div>
							<div>${locale.get("parts.statisticsbar.allUnchecked")}</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
