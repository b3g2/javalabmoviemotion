
<link href="css/responsive-calendar.css" rel="stylesheet">
<script src="scripts/responsive-calendar.min.js"></script>

<div class="responsive-calendar">
	<div class="controls" style="margin-bottom: 15px;">
		<a style="float: inherit;" data-go="prev"><div
				class="btn btn-primary btn-flat">${locale.get("parts.calendar.previous")}</div></a>
		<h4>
			<span data-head-year></span> <span data-head-month></span>
		</h4>
		<a class="pull-right" data-go="next"><div
				class="btn btn-primary btn-flat">${locale.get("parts.calendar.next")}</div></a>
	</div>
	<div class="day-headers">
		<div class="day header">${locale.get("parts.calendar.mon")}</div>
		<div class="day header">${locale.get("parts.calendar.tue")}</div>
		<div class="day header">${locale.get("parts.calendar.wed")}</div>
		<div class="day header">${locale.get("parts.calendar.thu")}</div>
		<div class="day header">${locale.get("parts.calendar.fri")}</div>
		<div class="day header">${locale.get("parts.calendar.sat")}</div>
		<div class="day header">${locale.get("parts.calendar.sun")}</div>
	</div>
	<div class="days" data-group="days"></div>
</div>