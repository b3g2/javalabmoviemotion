<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<script src="http://maps.googleapis.com/maps/api/js"></script>
<script src="scripts/gmap3.js"></script>
<script src="scripts/custom-js/cinema-content.js"></script>
<link href="css/custom-style/cinema-content.css" rel="stylesheet"
	type="text/css" media="all" />
<script src="scripts/sweetalert.min.js"></script>
<link rel="stylesheet" type="text/css" href="css/sweetalert.css">
<style>
label.showMoreLabel:after {
	content: "${locale.get('general.showMore')}";
}

input:checked ~ label.showMoreLabel:after {
	content: "${locale.get('general.showLess')}";
}
</style>
<script>
$(document).ready(function() {
	$("#mysel option[value='${selectCity}']").prop('selected', true);
});

function filtFunc(id) {

	var text = "";
	var zn = document.getElementById("mysel").value;
	var found = document.getElementById(zn);
	$("#" + found.value).click();

	if (zn == "all") {
		$("#showAll").click();
	}
}

function plotmap(m, b, c, d, f) {
	$("#map").gmap3({
		getlatlng : {//address:addr,
		callback : function(results) {

	$("#map").gmap3({
		map : {
		options : {
		  zoom : 8,
		  maxZoom : 12
		}},
		marker : {
	   //latLng:results[0].geometry.location,
			values : m,
		options : {draggable : false,},
		events : {
		mouseover : function(marker, event,	context) {
		var map = $(this).gmap3("get");
		infowindow = $(this).gmap3({get : {	name : "infowindow"	}});
		
		var zminna = b;
		var zmName = c;
		var zmCity = d;
		var zmAddress = f;
		if (infowindow) {infowindow.open(map, marker);
			infowindow.setContent('<div onmouseout="hideInfo()"><button id="infoB" class="btn-transp" value=' + zminna
					          + ' onclick="windFunc()"><b style="font-size: 1.5em;">' + zmName
						      + '</b>,<br>' + zmAddress + ',' + zmCity + '</button></div>');
	    } else {
			$(this).gmap3({infowindow : {
				anchor : marker,
				options : {
				content : '<div onmouseout="hideInfo()"><button id="infoB" class="btn-transp" value=' + zminna
			 			+ ' onclick="windFunc()"><b style="font-size: 1.5em;">' + zmName
						+ '</b>,<br>' + zmAddress + ',' + zmCity + '</button></div>' }
					}
				   });
		       	  }},
        		 }},
				autofit : {}});
					 }
					}
				});
        }
		
		function hideInfo() {
	      infowindow.close();
        }
        
		function windFunc() {
			var inf = document.getElementById("infoB");
			imitate(inf.value);
		}
		
		function imitate(id) {
			document.getElementById("cl").value = arguments[0];
			$("#cl").click();
		}
</script>
<div class="col-xs-7">
	<div style="padding-top: 29px;">
		<button type="submit" value="all" id="call" style="display: none"></button>
		<div style="display: none;">
		<form action="cinema">
		  <c:forEach var="c" items="${city}">
	 		<button type="submit" value='${c}' name="cityButton" id='${c}'
	     		class="cities">${c}</button>
		  </c:forEach>
		</form>
		</div>
		<form action="cinema" id="alert-form">
			<h3 class="title text-center">${locale.get("cinema.findcinema")}</h3>
			<div
				style="padding-bottom: 10px; float: left; display: block;">
				<select onchange="filtFunc()" id="mysel"
					style="float: left; margin-right: 10px; padding-bottom: 6px; width: 150px; padding-top: 6px;">
					<option value="all">${locale.get("cinema.all")}</option>
					<c:forEach var="c" items="${city}">
						<option value='${c}'>${c}</option>
					</c:forEach>
				</select>
			</div>
			<div class="bg-info"
				style="width: 315px; float: left;">
				<input type="text" class="form-control input-flat" list="cinemaName"
					id="cise" name="cine" placeholder='${locale.get("cinema.search")}' />
				<datalist id="cinemaName">
				</datalist>
			</div>
			<div class="btn-group" style="float: left; padding-right: 15px;">
				<button type="submit" class="btn btn-primary btn-flat"
					style="height: 34px; width: 50px;" value="" id=""
					onclick="search()">
					<i class="fa fa-search"></i>
				</button>
			</div>
			<button type="submit" style="height: 34px; min-width: 90px;"
				class="btn btn-primary btn-flat">
				<h5>${locale.get("cinema.showall")}</h5>
			</button>
		</form>
		<form action="cinema">
			<c:if test="${found eq 'no' && empty all}">
				<script>
					sweetAlert({
						title : "${locale.get('cinema.empty2')}",
						type : "error"
					});
				</script>
			</c:if>
		</form>
		<div id="map" style="width: 630px; height: 500px; margin-top: 25px;"></div>

		<div style="float: left;">
			<div id="demo">
				<form action="cinema-info">
					<div id="addies" style="display: none;"></div>
					<div class="subscribe" style="width: 200px;">
						<c:choose>
							<c:when test="${not empty cinema}">
								<ul id="adds" class="" style="list-style-type: none;">
									<c:forEach var="c" items="${cinema}">
										<li class="lic ci" style="display: none;" value='${c.city }'>${c.address },${c.city },${c.country }</li>
										<li style="padding-bottom: 30px;">
											<button type="submit" value='${c.id }' id="cl" name="id"
												class="butAd" style="display: none;">${c.id }</button>
										</li>
										<li class="infoName" style="display: none"
											value='${c.cinemaName }'>${c.cinemaName }</li>
										<li class="infoCity" style="display: none" value='${c.city }'>${c.city }</li>
										<li class="infoAddress" style="display: none"
															value='${c.address }'>${c.address }</li>
									</c:forEach>
								</ul>
							</c:when>
						</c:choose>
					</div>
				</form>
			</div>
		</div>
		<input type="text"
			style="visibility: hidden; padding-top: 80px; float: left; width: 50px;" />

	</div>
</div>
<div class="col-sm-4 pull-right hidden-xs" id="right-side-bar">
	<div id="cinemasNews">
		<div id="cinemaNewsHeader">
			<h3 class="title text-center allNewsTitle">${locale.get("cinema.lastevent")}</h3>
		</div>
		<div id="cinemaNewsContent">

			<c:forEach var="pieceOfNews" items="${news}" varStatus="theCount">
				<c:if test="${pieceOfNews.cinema.state eq 'WORKING'}">
					<div class="row newsItem grid_3">
						<img style="max-height: 150px; min-height: 150px;" width="400;"
							<c:choose>
								<c:when test="${not empty pieceOfNews.mainInfo.pictureURL}">
									src="${pieceOfNews.mainInfo.pictureURL}"
								</c:when>
								<c:otherwise>
									src="images/defaultEvent.jpg"
								</c:otherwise>
							</c:choose>
							class="img-responsive" alt="">
						<div class="blog-poast-info">
							<ul>
								<li><i class="fa fa-user"></i><a
									href="user?id=${pieceOfNews.user.id}"><span>&#09;${ pieceOfNews.user.firstName}&nbsp;${pieceOfNews.user.lastName}&nbsp;</span></a></li>
								<li><i class="fa fa-calendar"></i><span>&#09;${ pieceOfNews.mainInfo.addedDateTime}</span></li>
							</ul>
						</div>
						<h4>
							<label style="margin-top: 5px; color: #007DD3;"> <c:choose>
									<c:when test="${not empty pieceOfNews.mainInfo.header}">
                                                            ${pieceOfNews.mainInfo.header}
                                    </c:when>
									<c:otherwise>
										<c:choose>
											<c:when test="${locale.language eq 'uk'}">
												Оголошення
											</c:when>
											<c:otherwise>
												Announcement
											</c:otherwise>
										</c:choose>
									</c:otherwise>
								</c:choose>
							</label>
						</h4>
						<input class="showMoreInput" id="ch${theCount.index}"
							type="checkbox">
						<p class="newsItemContent">${pieceOfNews.mainInfo.content}</p>
						<label style="display: none;" class="showMoreLabel"
							for="ch${theCount.index}"></label>
						<div class="locationPartTitle">
							${locale.get("cinema.cinema")}&nbsp;<a
								href="cinema-info?id=${pieceOfNews.cinema.id}"><span>${pieceOfNews.cinema.cinemaName}</span></a>
						</div>
						<div class="locationPartTitle">
							${locale.get("cinema.address")}&nbsp;<span>${pieceOfNews.cinema.city},
								${pieceOfNews.cinema.address}</span>
						</div>
					</div>
				</c:if>

			</c:forEach>
			<form action="events_history" method="post">
				<input type="hidden" name="page" value="1">
				<button type="submit"
					class="showMoreNewsBtn btn btn-primary btn-block btn-flat">
					${locale.get("cinema.moreevents")}</button>
			</form>
		</div>
	</div>
</div>