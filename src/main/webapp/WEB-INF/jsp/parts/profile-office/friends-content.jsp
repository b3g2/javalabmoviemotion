<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link rel="stylesheet" href="css/custom-friends.css">
<script src="scripts/custom-js/friends.js"></script>
<script src="scripts/sweetalert.min.js"></script>
<script>
$(document).ready(function() {
	$("#subscribing1")
    .appendTo("#subscribing");
	$("#subscribers1")
    .appendTo("#subscribers");
	if('${search}' == 'subscribing'){
		
		$("#showAllContainer").appendTo("#subscribing");
		$("#subscribingClicker").trigger("click");
		$("#subscribersShow").css("display","none");
		$("#subscribers").css("display","none");
		$("#profile-tabs").css("display","none");
	}
	
	if('${search}' == 'subscribers'){
		$("#showAllContainer").appendTo("#subscribers");
		$("#subscribersClicker").trigger("click");
		$("#subscribingShow").css("display","none");
		$("#subscribing").css("display","none");
		$("#profile-tabs").css("display","none");
	}
	if('${switchToSubscribers}'=='yes'){
		$("#subscribersClicker").trigger("click");
	}
	if(Boolean('${not empty subscribingWarning&&not empty subscribersWarning&&empty search}')){
		$('#noSubscriptionsSubscribing').appendTo("#subscribing");
		$('#noSubscriptionsSubscribers').appendTo("#subscribers");
	}
	$("#center-content").css("visibility","visible");
});


function goToPage(elem, newPage, type){
var uri = URI(window.location.href);
if(type=='subscribing'){
uri.removeSearch("pageSubscribing"); 
uri.addSearch("pageSubscribing", newPage);
}
if(type=='subscribers'){
	uri.removeSearch("pageSubscribers"); 
	uri.addSearch("pageSubscribers", newPage);
	}
$(elem).attr("href", uri.normalizePathname() );
alert(next);
}; 
</script>
<style>
.inputSearchContainer {
	padding-left: 0;
	padding-right: 0;
}

.resp-tab-item {
	background-color: #E7E7E7;
}

.transitionButton {
	width: 10%;
	display: inline-block;
}
</style>

<div class="container">
	<div class="col-xs-11" id="center-content"
		style="overflow-y: hidden; overflow-x: hidden; margin: 28px 20px; visibility: hidden; width: 100%; padding-left: 25px; padding-right: 25px;">
		<jsp:include page="../../modals/recommendation-modal.jsp" />
		<div id="subscriptions">
			<c:choose>
				<c:when
					test="${not empty subscribingWarning && not empty subscribersWarning}">
					<c:if test="${empty search || search eq 'subscribing'}">
						<div class="subscriptionHeader">
							<c:choose>
								<c:when test="${search eq 'subscribing'}">
									<div class="row" style="margin-top: 55px; margin-bottom: 0;">
										<h3 class="title">
											<span>${locale.get("friends.search")}</span>
										</h3>
									</div>
									<form method="get" action="friends" class="subscriptionForm">
										<div class="col-xs-11 inputSearchContainer">
											<input placeholder="Search" class="input-flat form-control"
												id="friendName" name="friendName" maxlength="65" type="text" />
											<input type="hidden" name="subscriptionType"
												value="subscribing"></input>
										</div>
										<div class="col-xs-1 inputSearchContainer">
											<button class="btn btn-default btn-flat btn-block search-btn"
												type="submit">
												<i class="fa fa-search"></i>
											</button>
										</div>
									</form>
									<div class="nothingFoundIllustartion row"
										style="text-align: center;">
										<img style="margin-top: 15px;" alt="nothing found image"
											src="images/empty-search.png">
										<h4 style="margin-left: 0px;" class="title">${locale.get("movies.nothing")}</h4>
										<c:if
											test="${subscribingWarning eq 'NOT VALID PARAMETER FOR SEARCH' || subscribersWarning eq 'NOT VALID PARAMETER FOR SEARCH'}">
											<h4 class="index-title">${locale.get("friends.invalid")}</h4>
										</c:if>
									</div>
								</c:when>
								<c:otherwise>
									<div id="noSubscriptionsSubscribing">
										<form method="get" action="friends" class="subscriptionForm">
											<div class="col-xs-11 inputSearchContainer">
												<input placeholder="Search" class="input-flat form-control"
													id="friendName" name="friendName" maxlength="65"
													type="text" /> <input type="hidden"
													name="subscriptionType" value="subscribing"></input>
											</div>
											<div class="col-xs-1 inputSearchContainer">
												<button
													class="btn btn-default btn-flat btn-block search-btn"
													type="submit">
													<i class="fa fa-search"></i>
												</button>
											</div>
										</form>
										<div class="nothingFoundIllustartion row"
											style="text-align: center; margin-bottom: 25px;">
											<img alt="nothing found image" src="images/empty-search.png">
											<h4 class="index-title">${locale.get("friends.nothing")}</h4>
										</div>
									</div>
								</c:otherwise>
							</c:choose>
						</div>
					</c:if>
					<c:if test="${empty search || search eq 'subscribers'}">
						<div class="subscriptionHeader">
							<c:choose>
								<c:when test="${search eq 'subscribers'}">
									<div class="row">
										<h3 class="title">
											<span>${locale.get("friends.search")}</span>
										</h3>
									</div>
									<form method="get" action="friends" class="subscriptionForm">
										<div class="col-xs-11 inputSearchContainer">
											<input placeholder="Search" class="input-flat form-control"
												id="friendName" name="friendName" maxlength="65" type="text" />
											<input type="hidden" name="subscriptionType"
												value="subscribers"></input>
										</div>
										<div class="col-xs-1 inputSearchContainer">
											<button class="btn btn-default btn-flat btn-block search-btn"
												type="submit">
												<i class="fa fa-search"></i>
											</button>
										</div>
									</form>
									<div class="nothingFoundIllustartion row"
										style="text-align: center; margin-bottom: 25px;">
										<img alt="nothing found image" src="images/empty-search.png">
										<h4 class="index-title">${locale.get("movies.nothing")}</h4>
										<c:if
											test="${subscribingWarning eq 'NOT VALID PARAMETER FOR SEARCH' || subscribersWarning eq 'NOT VALID PARAMETER FOR SEARCH'}">
											<h4 class="index-title">${locale.get("friends.invalid")}</h4>
										</c:if>
									</div>
								</c:when>
								<c:otherwise>
									<div id="noSubscriptionsSubscribers">
										<form method="get" action="friends" class="subscriptionForm">
											<div class="col-xs-11 inputSearchContainer">
												<input placeholder="Search" class="input-flat form-control"
													id="friendName" name="friendName" maxlength="65"
													type="text" /> <input type="hidden"
													name="subscriptionType" value="subscribers"></input>
											</div>
											<div class="col-xs-1 inputSearchContainer">
												<button
													class="btn btn-default btn-flat btn-block search-btn"
													type="submit">
													<i class="fa fa-search"></i>
												</button>
											</div>
										</form>
										<div id="noSubscriptionsSubscribers"
											class="nothingFoundIllustartion row"
											style="text-align: center; margin-bottom: 25px;">
											<img alt="nothing found image" src="images/empty-search.png">
											<h4 class="index-title">${locale.get("friends.nothing")}</h4>
										</div>
									</div>
								</c:otherwise>
							</c:choose>
						</div>
					</c:if>
				</c:when>

				<c:otherwise>
					<c:if test="${empty search || search eq 'subscribing'}">
						<c:choose>
							<c:when test="${not empty subscribingWarning}">
								<div id="subscribing1">
									<div class="subscriptionHeader">
										<div class="row"
											style="margin-top: 55px; margin-left: 0px; margin-right: 0px;">
											<h3 class="title">
												<c:choose>
													<c:when test="${search eq 'subscribing'}">
														<span>${locale.get("friends.search")}</span>
													</c:when>
													<c:otherwise>
														<span></span>
													</c:otherwise>
												</c:choose>
											</h3>
										</div>
										<form method="get" action="friends" class="subscriptionForm">
											<div class="col-xs-11 inputSearchContainer">
												<input placeholder="Search" class="input-flat form-control"
													id="friendName" name="friendName" maxlength="65"
													type="text" /> <input type="hidden"
													name="subscriptionType" value="subscribing"></input>
											</div>
											<div class="col-xs-1 inputSearchContainer">
												<button
													class="btn btn-default btn-flat btn-block search-btn"
													type="submit">
													<i class="fa fa-search"></i>
												</button>
											</div>
										</form>
									</div>
									<div class="nothingFoundIllustartion row"
										style="text-align: center; margin-bottom: 25px;">
										<img alt="nothing found image" src="images/empty-search.png">
										<h4 class="index-title">${locale.get("movies.nothing")}</h4>
										<c:if
											test="${subscribingWarning eq 'NOT VALID PARAMETER FOR SEARCH' || subscribersWarning eq 'NOT VALID PARAMETER FOR SEARCH'}">
											<h4 class="index-title">${locale.get("friends.invalid")}</h4>
										</c:if>
									</div>
								</div>
							</c:when>
							<c:otherwise>
								<div id="subscribing1">
									<div class="subscriptionHeader">
										<div class="row" style="margin-top: 55px; margin-bottom: 0px;">
											<div class="col-xs-6">
												<h3 class="title">
													<c:choose>
														<c:when test="${search eq 'subscribing'}">
															<span>${locale.get("friends.search")}</span>
														</c:when>
														<c:otherwise>
															<span></span>
														</c:otherwise>
													</c:choose>
												</h3>
											</div>
											<div style="margin-bottom: 3px;" class="totalSubscriptions col-xs-3 col-xs-offset-3">${locale.get("friends.total1")}
												&nbsp;${friendsSubscribingAmount}</div>
										</div>
										<form method="get" action="friends" class="subscriptionForm">
											<div class="col-xs-11 inputSearchContainer">
												<input
													placeholder=${locale.get("parts.moderator.menu.search")}
													class="input-flat form-control" id="friendName"
													name="friendName" maxlength="65" type="text" /> <input
													type="hidden" name="subscriptionType" value="subscribing"></input>
											</div>
											<div class="col-xs-1 inputSearchContainer">
												<button
													class="btn btn-default btn-flat btn-block search-btn"
													type="submit">
													<i class="fa fa-search"></i>
												</button>
											</div>
										</form>
									</div>
									<div class="clearfix" style="height: 20px"></div>
									<div class="subscriptionContent">
										<ul>
											<c:forEach items="${friendsSubscribingInfo}" var="friendInfo">
												<li style="width: 28%; margin: 1.5% 2.5%;"
													class="subscribingItem">
													<div class="allItemBig">
														<div class="dataBig">
															<a class="goToUserLink"
																href="user?id=${friendInfo.user.id}"
																style="display: block;">
																<div class="mainInfo col-xs-7">
																	<div class="smallFriendPhoto">
																		<c:choose>
																			<c:when
																				test="${friendInfo.user.photoURL eq null || friendInfo.user.photoURL eq ''}">
																				<img class="img-circle"
																					src="images/avatarStandard.png"
																					alt="You don't have a photo!" height="135"
																					width="125">
																			</c:when>
																			<c:otherwise>
																				<img class="img-circle"
																					src="${friendInfo.user.photoURL}"
																					alt="We can't display this photo" height="135"
																					width="125">
																			</c:otherwise>
																		</c:choose>
																	</div>
																	<div class="Aligner">
																		<div class="Aligner-item Aligner-item--top"></div>
																		<div class="friendName Aligner-item"
																			style="text-align: center;">
																			${friendInfo.user.firstName}<br>${friendInfo.user.lastName}
																		</div>
																		<div class="Aligner-item Aligner-item--bottom"></div>
																	</div>
																</div>

															</a>
															<div class="additionalInfo col-xs-5">
																<label class="heading">${locale.get("friends.watched")}</label>
																${locale.get("friends.films")}:<br>${friendInfo.watchedFilms}<br>
																${locale.get("friends.min")}:<br>${friendInfo.user.duration}
																<c:if test="${not empty friendInfo.genre}">
																	<label class="heading">${locale.get("friends.favourite")}</label>									 
																 ${friendInfo.genre}
																 </c:if>
															</div>
														</div>
														<div class="clearfix"></div>
														<button style="height: 35px; font-size: 14px;"
															onclick="getRecommendation(${friendInfo.user.id},'${locale.language}')"
															class="btn-recommendation btn btn-base btn-small btn-block">${locale.get("friends.get")}</button>
														<button onclick="unsubscribe(${friendInfo.user.id},'${locale.language}')"
															class="btn btn-small btn-base btn-block">${locale.get("friends.unsub")}</button>
													</div>
												</li>
											</c:forEach>
										</ul>
									</div>
									<div class="clearfix"></div>
									<div class="row paginationContainer">
										<div class="col-xs-12 text-center"
											style="border-right: 1px solid #eee; margin: 20px;">
											<div style="margin-left: auto; margin-right: auto;">
												<c:if test="${pageSubscribing-1 ge 0}">
													<a class="users-pagination btn-block transitionButton"
														onclick="goToPage(this,${pageSubscribing-1},'subscribing')">${locale.get("users.previous")}</a>
												</c:if>
												<c:if test="${nextPageSubscribing}">
													<a class="users-pagination btn-block transitionButton"
														onclick="goToPage(this,${pageSubscribing+1},'subscribing')">${locale.get("users.next")}</a>
												</c:if>
											</div>
										</div>
									</div>
								</div>
							</c:otherwise>
						</c:choose>
					</c:if>
					<c:if test="${empty search || search eq 'subscribers'}">
						<c:choose>
							<c:when test="${not empty subscribersWarning}">
								<div id="subscribers1">
									<div class="subscriptionHeader">
										<div class="row"
											style="margin-left: 0 !important; margin-right: 0 !important;">
											<h3 class="title" style="clear: both;">
												<c:choose>
													<c:when test="${search eq 'subscribers'}">
														<span>${locale.get("friends.result")}</span>
													</c:when>
													<c:otherwise>
														<span></span>
													</c:otherwise>
												</c:choose>
											</h3>
										</div>
										<form method="get" action="friends" class="subscriptionForm">
											<div class="col-xs-11 inputSearchContainer">
												<input placeholder="Search" class="input-flat form-control"
													id="friendName" name="friendName" maxlength="65"
													type="text" /> <input type="hidden"
													name="subscriptionType" value="subscribers"></input>
											</div>
											<div class="col-xs-1 inputSearchContainer">
												<button
													class="btn btn-default btn-flat btn-block search-btn"
													type="submit">
													<i class="fa fa-search"></i>
												</button>
											</div>
										</form>
									</div>
									<div class="nothingFoundIllustartion row"
										style="text-align: center; margin-bottom: 25px;">
										<img alt="nothing found image" src="images/empty-search.png">
										<h4 class="index-title">${locale.get("movies.nothing")}</h4>
										<c:if
											test="${subscribingWarning eq 'NOT VALID PARAMETER FOR SEARCH' || subscribersWarning eq 'NOT VALID PARAMETER FOR SEARCH'}">
											<h4 class="index-title">${locale.get("friends.invalid")}</h4>
										</c:if>
									</div>
								</div>
							</c:when>
							<c:otherwise>
								<div id="subscribers1">
									<div class="subscriptionHeader">
										<div class="row" style="margin-top: 55px; margin-bottom: 0px;">
											<div class="col-xs-6">
												<h3 class="title" style="clear: both;">
													<c:choose>
														<c:when test="${search eq 'subscribers'}">
															<span>${locale.get("friends.result")}</span>
														</c:when>
														<c:otherwise>
															<span></span>
														</c:otherwise>
													</c:choose>
												</h3>
											</div>
											<div class="totalSubscriptions col-xs-3 col-xs-offset-3">${locale.get("friends.total2")}&nbsp;${friendsSubscribersAmount}</div>
										</div>
										<form method="get" action="friends" class="subscriptionForm">
											<div class="col-xs-11 inputSearchContainer">
												<input placeholder="Search" class="form-control input-flat"
													id="friendName" name="friendName" maxlength="65"
													type="text" /> <input type="hidden"
													name="subscriptionType" value="subscribers"></input>
											</div>
											<div class="col-xs-1 inputSearchContainer">
												<button
													class="btn btn-default btn-flat btn-block search-btn"
													type="submit">
													<i class="fa fa-search"></i>
												</button>
											</div>
										</form>
									</div>
									<div class="clearfix" style="height: 20px"></div>
									<div class="subscriptionContent">
										<ul>
											<c:forEach items="${friendsSubscribersInfo}" var="friendInfo">
												<li style="width: 20%; margin: 1.5% 2.5%;"
													class="subscriberItem">
													<div class="allItemSmall">
														<div class="dataSmall">
															<a class="goToUserLink"
																href="user?id=${friendInfo.key.user.id}"
																style="display: block;">
																<div class="mainInfo col-xs-6">
																	<div class="smallFriendPhoto">
																		<c:choose>
																			<c:when
																				test="${empty friendInfo.key.user.photoURL || friendInfo.key.user.photoURL eq ''}">
																				<img class="img-circle"
																					src="images/avatarStandard.png"
																					alt="You don't have a photo!" height="77"
																					width="67">
																			</c:when>
																			<c:otherwise>
																				<img class="img-circle"
																					src="${friendInfo.key.user.photoURL}"
																					alt="We can't display this photo!" height="77"
																					width="67">
																			</c:otherwise>
																		</c:choose>
																	</div>
																	<div class="Aligner-small">
																		<div class="Aligner-item Aligner-item--top"></div>
																		<div class="friendName Aligner-item"
																			style="text-align: center; font-size: 0.9em;">
																			${friendInfo.key.user.firstName}<br>${friendInfo.key.user.lastName}
																		</div>
																		<div class="Aligner-item Aligner-item--bottom"></div>
																	</div>
																</div>
															</a>
															<div class="otherInfo col-xs-6">
																<label class="heading">${locale.get("friends.watched")}</label>
																${locale.get("friends.films")}:<br>${friendInfo.key.watchedFilms}<br>
																${locale.get("friends.mins")}:<br>${friendInfo.key.user.duration}
															</div>
															<div class="clearfix"></div>
														</div>
														<div class="subscribeIfPossible">
															<c:choose>
																<c:when test="${friendInfo.value eq true}">
																	<button onclick="subscribe(${friendInfo.key.user.id}, '${locale.language}')"
																		class="btn btn-base btn-subscribe btn-block"
																		style="display: block;">${locale.get("friends.sub")}</button>
																</c:when>
																<c:otherwise>
																	<div
																		style="display: block; padding: 4px 9px; disabled: true; background-color: #003b64; color: white;">
																		<i style="font-size: 2em;"
																			class="glyphicon glyphicon-ok-circle"></i>
																	</div>
																</c:otherwise>
															</c:choose>
														</div>
													</div>
												</li>
											</c:forEach>
										</ul>
									</div>
									<div class="clearfix"></div>
									<div class="row paginationContainer">
										<div class="col-xs-10 text-center"
											style="border-right: 1px solid #eee; margin: 20px;">
											<c:if test="${pageSubscribers-1 ge 0}">
												<a class="users-pagination"
													onclick="goToPage(this,${pageSubscribers-1},'subscribers')">${locale.get("users.previous")}</a>
											</c:if>
											<c:if test="${nextPageSubscribers}">
												<a class="users-pagination"
													onclick="goToPage(this,${pageSubscriberse+1},'subscribers')">${locale.get("users.next")}</a>
											</c:if>
										</div>
									</div>
								</div>
							</c:otherwise>
						</c:choose>
					</c:if>
				</c:otherwise>
			</c:choose>
		</div>

		<div id="showAllContainer" class="row">
			<c:if test="${search eq 'subscribing' || search eq 'subscribers'}">
				<c:if test="${sthWasSearched eq true}">
					<div id="showAllButtonContainer" class="col-xs-2 col-xs-offset-5">
						<form method="get" action="friends" class="subscriptionForm">
							<button type="submit" id="showAllBtn"
								style="margin: auto; display: block;"
								class="btn btn-block btn-primary">${locale.get("friends.show")}</button>
						</form>
					</div>
				</c:if>
			</c:if>
		</div>

		<div class="featured" id="featured"
			style="margin: 0; padding-top: 1px; margin-left: -20px; margin-right: 20px;">
			<ul style="margin-top: 0;" class="resp-tabs-list" id="profile-tabs">
				<li id="subscribingShow" class="resp-tab-item active"
					aria-controls="tab_item-1" role="tab"><a data-toggle="tab"
					id="subscribingClicker" href="#subscribing">${locale.get("friends.subscribing")}</a></li>
				<li id="subscribersShow" class="resp-tab-item"
					aria-controls="tab_item-0" role="tab"><a data-toggle="tab"
					id="subscribersClicker" href="#subscribers">${locale.get("friends.subscribers")}</a></li>
			</ul>
			<div class="tab-content">
				<div id="subscribing" class="tab-pane fade in active"></div>
				<div id="subscribers" class="tab-pane fade"></div>
			</div>
		</div>
	</div>
</div>