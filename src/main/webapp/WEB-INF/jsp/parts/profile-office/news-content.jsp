<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link rel="stylesheet" href="css/custom-style/cinema-news-style.css">
<script src="scripts/custom-js/cinema-news.js"></script>

<style>
label.showMoreLabel:after {
	content: "${locale.get('general.showMore')}";
}

input:checked ~ label.showMoreLabel:after {
	content: "${locale.get('general.showLess')}";
}
</style>

<div class="col-xs-11" style="padding-right: 0; padding-left: 40px;"
	id="right-side-bar">
	<div id="cinemasNews" style="visibility:hidden;">
		<div class="row">
			<div class="col-xs-10">
				<div class="col-xs-4 col-xs-offset-4" id="cinemaNewsHeader">
					<h3 style="margin-bottom: -20px;"
						class="title text-center allNewsTitle">${locale.get('events.journal.title')}</h3>
				</div>
				<div class="clearfix"></div>
				<div id="cinemaNewsContent" style="">
				<div class="row">
						<div id="colNews1" class="col-xs-4"></div>
						<div id="colNews2" class="col-xs-4"></div>
						<div id="colNews3" class="col-xs-4"></div>
				</div>
					<c:forEach var="pieceOfNews" items="${news}" varStatus="theCount">
						<div id="newsItem${theCount.count}" class="newsItem grid_3">
							<img style="max-height: 150px; min-height: 150px; width: 400px;"
								<c:choose>
					<c:when test="${not empty pieceOfNews.mainInfo.pictureURL}">
					src="${pieceOfNews.mainInfo.pictureURL}"
					</c:when>
					<c:otherwise>
					src="images/defaultEvent.jpg"
					</c:otherwise>
					</c:choose>
								class="img-responsive" alt="">

							<div class="blog-poast-info"
								style="background: #003B64; padding: 0.2em 1em;">
								<ul>
									<li><i class="fa fa-user"></i>&nbsp;<a
										href="user?id=${pieceOfNews.user.id}"><span>${pieceOfNews.user.firstName}&nbsp;${pieceOfNews.user.lastName}&nbsp;</span></a></li>
									<li><i class="fa fa-calendar">&nbsp;</i><span>${pieceOfNews.mainInfo.addedDateTime}</span></li>
								</ul>
							</div>
							<h4 style="height: 45px;">
								<label style="margin-top: 5px; color: #007DD3;"> <c:choose>
										<c:when test="${not empty pieceOfNews.mainInfo.header}">
                                                            ${pieceOfNews.mainInfo.header}
                                                        </c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${locale.language eq 'uk'}">
													Оголошення
												</c:when>
												<c:otherwise>
													Announcement
												</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose></label>
							</h4>
							<div class="mainPart">
								<input class="showMoreInput" id="ch${pieceOfNews.mainInfo.id}"
									type="checkbox">
								<p class="newsItemContent">${pieceOfNews.mainInfo.content}</p>
								<label style="visibility: hidden;" class="showMoreLabel"
									for="ch${pieceOfNews.mainInfo.id}"></label>
							</div>
							<div class="aboutCinema">
								<div class="locationPartTitle">
									${locale.get('cinema.cinema')}&nbsp;<a
										href="cinema-info?id=${pieceOfNews.cinema.id}"><span>${pieceOfNews.cinema.cinemaName}</span></a>
								</div>
								<div class="clearfix"></div>
								<div class="locationPartTitle">
									${locale.get('cinema.address')}&nbsp;<span>${pieceOfNews.cinema.city},&nbsp;${pieceOfNews.cinema.address}</span>
								</div>
							</div>
						</div>
					</c:forEach>
				</div>
				<div class="clearfix"></div>
				<div class="row">
					<input id="currentPageNumber" type="hidden" name="page" value="1">
					<input id="currentPageNumberWithCinema" type="hidden"
						name="pageWithCinema" value="0"> <input id="currentCity"
						type="hidden" name="currentCity"> <input
						id="currentCinemaName" type="hidden" name="currentCinemaName">
					<span class="col-xs-4 col-xs-offset-4"> <c:if
							test="${hasNextItems == true}">
							<button id="showMoreButton"
								onclick="showNextNews('${locale.language}')"
								class="btn btn-block btn-primary">${locale.get('general.cinema.news.showMore')}</button>
						</c:if>
					</span>
				</div>
			</div>
			<div class="col-xs-2">
				<div>
					<div>
						<form autocomplete="off" id="#get-news-mf" autocomplete="off"
							style="margin-top: 25px;">
							<div class="form-group">
								<div style="text-align: center;">
									<label class="heading" for="cinema-city-input">${locale.get("moderator.add.news.cinema.city")}</label>
								</div>
								<input type="text" name="city" list="city-list"
									class="form-control input-flat clearable"
									id="cinema-city-input" placeholder="Any city">
								<datalist id="city-list">
								</datalist>
							</div>
							<div id="cinema-name-input-container" class="form-group">
								<div style="text-align: center;">
									<label class="heading" for="cinema-name-input">${locale.get("moderator.add.news.cinema.name")}</label>
								</div>
								<input type="text" name="name" list="cinema-list"
									class="form-control input-flat clearable"
									id="cinema-name-input" placeholder="Any cinema">
								<datalist id="cinema-list">
								</datalist>
							</div>
							<div style="text-align: center;">
								<input type="button"
									onclick="showNextNews('${locale.language}',true)"
									value="${locale.get('events.show.news')}"
									class="btn btn-primary btn-flat btn-block"
									id="get-news-mf-submit" disabled>
							</div>
							<div style="text-align: center;">
								<a style="margin-top: 35px; text-transform: uppercase;"
									href="events_history" type="button"
									class="btn btn-block btn-flat">${locale.get('events.to.all')}</a>
							</div>
							<input type="hidden" id="chosenCity"> <input
								type="hidden" id="chosenName">
							<button type="button" style="display: none;"
								class="btn btn-info btn-flat btn-block" id="clear-filter">${locale.get("moderator.add.news.clear")}
							</button>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="templateContainer">
	<div style="display: none;"
		class="newsItem newNewsItem grid_3">
		<img style="max-height: 150px; min-height: 150px; width: 400px;"
			class="img-responsive" alt="">

		<div class="blog-poast-info"
			style="background: #003B64; padding: 0.2em 1em;"></div>
		<h4>
			<label style="margin-top: 5px; height: 40px; color: #007DD3;">
				<!-- header -->
			</label>
		</h4>
		<div class="mainPart"></div>
		<div class="aboutCinema"></div>
	</div>
</div>