<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag" uri="/WEB-INF/custom.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link href="css/dataTables.bootstrap.min.css" rel="stylesheet"
	media="all">
<script src="scripts/metro.js"></script>
<c:if test="${locale.language eq 'uk'}">
<script src="scripts/jquery.dataTables.min.js"></script>
</c:if>
<c:if test="${locale.language eq 'en'}">
<script src="scripts/jquery.dataTablesen.min.js"></script>
</c:if>
<script src="scripts/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/jquery.flexisel.js"></script>
<script src="scripts/easyResponsiveTabs.js" type="text/javascript"></script>
<script type="text/javascript" src="scripts/jquery.bootpag.min.js"></script>
<script src="scripts/custom-js/user-profile.js"></script>
<div class="col-xs-9" id="center-content"
	style="overflow-y: hidden; margin: 28px 20px;">
	<c:if
		test="${not empty filmsFavourite || not empty filmsWatched || not empty filmsSceduled}">
		<h4 id="user-profile" class="title">
			${locale.get("profile.follow")}
			<c:if test="${empty withoutWantWatchMovies}">
				<a id="show-calendar"> <i class="fa fa-calendar-plus-o" id="i1"></i><i
					class="fa fa-calendar-minus-o" id="i2" style="display: none"></i>
				</a>
			</c:if>
		</h4>
	</c:if>
	<div class="featured user-profile">
		<div id="toggleText" style="display: none">
			<jsp:include page="calendar-new-movies.jsp" />
		</div>
		<div class="featured" id="moviesForDayTag"
			style="display: none; margin: 0; min-height: 370px">

			<div id="scrollbar_area">
				<div>
					<h4 class="title">${locale.get("profile.released")}&#09;${dayReleased}</h4>
				</div>
				<div style="display: inline-flex;">
					<c:if test="${not empty moviesForDay}">
						<c:forEach var="film" items="${moviesForDay}">
							<li style="display: block; margin-right: 20px;">
								<div class="poster-wrapper" style="margin: 0;">
									<div class="poster-view poster-view-tenth"
										style="width: 230px; height: 310px;">
										<c:if test="${film.poster ne 'N/A'}">
											<img src="${film.poster}" alt="poster"
												style="max-width: 230px;">
										</c:if>
										<c:if test="${film.poster eq 'N/A'}">
											<img src="images/defaultMoviePoster.jpg" alt="poster"
												style="max-width: 230px;">
										</c:if>
										<div class="mask">
											<c:if test="${locale.language eq 'uk'}">
												<h2 style="font-size: 13px; margin: 20px 10px 0 10px;">${film.titleUk}</h2>
											</c:if>
											<c:if test="${locale.language eq 'en'}">
												<h2 style="font-size: 13px; margin: 20px 10px 0 10px;">${film.title}</h2>
											</c:if>
											<p>
												<i class="fa fa-calendar">&nbsp;</i>
												<c:if test="${not empty film.plot}">${film.released}</c:if>
												<c:if test="${empty film.plot}">N/A</c:if>
											</p>
											<c:if
												test="${locale.language eq 'en' && not empty film.plot}">
												<p class="plot" style="font-size: 11px; line-height: 15px;">${film.plot}</p>
											</c:if>
											<c:if
												test="${locale.language eq 'uk' && not empty film.plotUk}">
												<p class="plot" style="font-size: 11px; line-height: 15px;">${film.plotUk}</p>
											</c:if>
											<a href="movie_details?id=${film.id}" class="info btn-flat">${locale.get("movies.details")}</a>
										</div>
									</div>
									<c:if test="${locale.language eq 'uk'}">
										<a href="movie_details?id=${film.id}"
											style="max-width: 230px;"
											class="btn href btn-primary btn-flat overflow-hidden">${film.titleUk}</a>
									</c:if>
									<c:if test="${locale.language eq 'en'}">
										<a href="movie_details?id=${film.id}"
											style="max-width: 230px;"
											class="btn href btn-primary btn-flat overflow-hidden">${film.title}</a>
									</c:if>
								</div>
							</li>
						</c:forEach>
					</c:if>

					<c:if test="${empty moviesForDay}">
						<div style="width: 825px; text-align: -webkit-center;">
							<img style="width: 250px; margin-bottom: 10px;"
								src="images/film.png" class="img-responsive zoom-img poster"
								alt="" />
							<h4 class="title">${locale.get("profile.no")}</h4>
						</div>
					</c:if>
				</div>
			</div>
		</div>
	</div>

	<c:if
		test="${empty filmsFavourite && empty filmsWatched && empty filmsSceduled}">
		<div style="text-align: -webkit-center;">
			<img style="width: 200px; height: 200px; margin-bottom: 20px;"
				src="images/film.png" class="img-responsive zoom-img poster" alt="" />
			<h5 class="title">${locale.get("profile.witoutmovies")}</h5>
			<h5 class="title">
				${locale.get("profile.maywant2")} <a href="movies"
					style="color: #0D9AFC; font-style: italic;">
					${locale.get("profile.maywant2")} </a>
			</h5>
			<h5 class="title">
				${locale.get("profile.orf")} <a href="users"
					style="color: #0D9AFC; font-style: italic;">
					${locale.get("profile.findfriend")} </a>
				&#09;${locale.get("profile.friendrecom")}
			</h5>
		</div>
	</c:if>
	<c:if
		test="${not empty filmsFavourite || not empty filmsWatched || not empty filmsSceduled}">
		<div id="review-slider" class="review-slider user-profile">
			<ul id="flexiselDemo1">
				<c:if test="${not empty wantWatchMovies}">
					<c:forEach var="film" items="${wantWatchMovies}">
						<li>
							<div class="poster-wrapper" style="margin: 0;">
								<div class="poster-view poster-view-tenth"
									style="width: 187px; height: 260px;">
									<c:if test="${film.poster ne 'N/A'}">
										<img src="${film.poster}" alt="poster"
											style="max-width: 180px;">
									</c:if>
									<c:if test="${film.poster eq 'N/A'}">
										<img src="images/defaultMoviePoster.jpg" alt="poster"
											style="max-width: 180px;">
									</c:if>
									<div class="mask">
										<c:if test="${locale.language eq 'uk'}">
											<h2 style="font-size: 13px; margin: 20px 10px 0 10px;">${film.titleUk}</h2>
										</c:if>
										<c:if test="${locale.language eq 'en'}">
											<h2 style="font-size: 13px; margin: 20px 10px 0 10px;">${film.title}</h2>
										</c:if>
										<p>
											<i class="fa fa-calendar">&nbsp;</i>
											<c:if test="${not empty film.plot}">${film.released}</c:if>
											<c:if test="${empty film.plot}">N/A</c:if>
										</p>
										<c:if test="${locale.language eq 'en' && not empty film.plot}">
											<p class="plot" style="font-size: 11px; line-height: 15px;">${film.plot}</p>
										</c:if>
										<c:if
											test="${locale.language eq 'uk' && not empty film.plotUk}">
											<p class="plot" style="font-size: 11px; line-height: 15px;">${film.plotUk}</p>
										</c:if>
										<a href="movie_details?id=${film.id}" class="info btn-flat">${locale.get("movies.details")}</a>
									</div>
								</div>
								<c:if test="${locale.language eq 'uk'}">
									<a href="movie_details?id=${film.id}" style="max-width: 187px;"
										class="btn href btn-primary btn-flat overflow-hidden">${film.titleUk}</a>
								</c:if>
								<c:if test="${locale.language eq 'en'}">
									<a href="movie_details?id=${film.id}" style="max-width: 187px;"
										class="btn href btn-primary btn-flat overflow-hidden">${film.title}</a>
								</c:if>
							</div>
						</li>
					</c:forEach>
				</c:if>
			</ul>
		</div>

		<div class="featured" id="featured" style="margin: 0;">
			<ul class="resp-tabs-list" id="profile-tabs">
				<li class="resp-tab-item active" aria-controls="tab_item-2"
					role="tab"><a data-toggle="tab" href="#favourite">${locale.get("profile.favourite")}</a></li>
				<li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><a
					data-toggle="tab" href="#watched">${locale.get("profile.watched")}</a></li>
				<li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><a
					data-toggle="tab" href="#sceduled">${locale.get("profile.scheduled")}</a></li>
			</ul>
			<div class="tab-content">
				<div id="favourite" class="tab-pane fade in active">
					<table class="profile_movies_table table table-striped">
						<thead>
							<tr>
								<th style="width: 174px;">${locale.get("profile.title")}</th>
								<th style="width: 65px;">${locale.get("profile.premiere")}</th>
								<th style="width: 63px;">${locale.get("profile.duration")}</th>
								<th style="width: 127px; text-align: right;">
									${locale.get("profile.ratingimdb")}</th>
								<th style="width: 127px; text-align: right;">
									${locale.get("profile.ratingmm")}</th>
								<th style="width: 41px;">${locale.get("profile.views")}</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${not empty filmsFavourite}">
								<c:forEach var="film" items="${filmsFavourite}">
									<tr>
										<td><a href="movie_details?id=${film.id}"> <c:if
													test="${locale.language eq 'uk'}">
				                          ${film.titleUk}
				                    </c:if> <c:if test="${locale.language eq 'en'}">
										    ${film.title}
									</c:if>
										</a></td>
										<td style="text-align: right;"><c:if
												test="${empty film.released}">
									    N/A
									</c:if> <c:if test="${not empty film.released}">
									    ${film.released}
									</c:if></td>
										<td style="text-align: right;"><c:if
												test="${empty film.runtime}">
									    N/A
									</c:if> <c:if test="${not empty film.runtime}">
									    ${film.runtime}&#09;${locale.get("profile.min")}
									</c:if></td>
										<td style="text-align: right;"><c:if
												test="${empty film.ratingImdb}">
												<p>${locale.get("profile.unrating")}</p>
											</c:if> <c:if test="${not empty film.ratingImdb}">
				                       ${film.ratingImdb}&#09;
				                    <span class="rating" data-role="rating"
													data-value="${film.ratingImdb/2}" data-color-rate="true"
													data-step="0.1" data-static="true" data-stars=5>&nbsp;</span>
											</c:if></td>
										<td style="text-align: right;"><c:if
												test="${empty film.ratingCustom}">
												<p>${locale.get("profile.unrating")}</p>
											</c:if> <c:if test="${not empty film.ratingCustom}">
				                       ${film.ratingCustom}&#09;
				                       <span class="rating" data-role="rating"
													data-value="${film.ratingCustom/2}" data-color-rate="true"
													data-step="0.1" data-static="true" data-stars=5>&nbsp;</span>
											</c:if></td>
										<td style="text-align: right;">
											${film.reviews}&nbsp;&nbsp;<i class="fa fa-eye"></i>
										</td>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</div>
				<div id="watched" class="tab-pane fade">
					<table class="profile_movies_table table table-striped">
						<thead>
							<tr>
								<th style="width: 174px;">${locale.get("profile.title")}</th>
								<th style="width: 65px;">${locale.get("profile.premiere")}</th>
								<th style="width: 63px;">${locale.get("profile.duration")}</th>
								<th style="width: 127px; text-align: right;">
									${locale.get("profile.ratingimdb")}</th>
								<th style="width: 127px; text-align: right;">
									${locale.get("profile.ratingmm")}</th>
								<th style="width: 41px;">${locale.get("profile.views")}</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${not empty filmsWatched}">
								<c:forEach var="film" items="${filmsWatched}">
									<tr>
										<td><a href="movie_details?id=${film.id}"> <c:if
													test="${locale.language eq 'uk'}">
				                          ${film.titleUk}
				                    </c:if> <c:if test="${locale.language eq 'en'}">
										    ${film.title}
									</c:if>
										</a></td>
										<td style="text-align: right;"><c:if
												test="${empty film.released}">
									    N/A
									</c:if> <c:if test="${not empty film.released}">
									    ${film.released}
									</c:if></td>
										<td style="text-align: right;"><c:if
												test="${empty film.runtime}">
									    N/A
									</c:if> <c:if test="${not empty film.runtime}">
									    ${film.runtime}&#09;${locale.get("profile.min")}
									</c:if></td>
										<td style="text-align: right;"><c:if
												test="${empty film.ratingImdb}">
												<p>${locale.get("profile.unrating")}</p>
											</c:if> <c:if test="${not empty film.ratingImdb}">
				                       ${film.ratingImdb}&#09;
				                    <span class="rating" data-role="rating"
													data-value="${film.ratingImdb/2}" data-color-rate="true"
													data-step="0.1" data-static="true" data-stars=5>&nbsp;</span>
											</c:if></td>
										<td style="text-align: right;"><c:if
												test="${empty film.ratingCustom}">
												<p>${locale.get("profile.unrating")}</p>
											</c:if> <c:if test="${not empty film.ratingCustom}">
				                       ${film.ratingCustom}&#09;
				                       <span class="rating" data-role="rating"
													data-value="${film.ratingCustom/2}" data-color-rate="true"
													data-step="0.1" data-static="true" data-stars=5>&nbsp;</span>
											</c:if></td>
										<td style="text-align: right;">
											${film.reviews}&nbsp;&nbsp;<i class="fa fa-eye"></i>
										</td>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</div>
				<div id="sceduled" class="tab-pane fade">
					<table class="profile_movies_table table table-striped">
						<thead>
							<tr>
								<th style="width: 174px;">${locale.get("profile.title")}</th>
								<th style="width: 65px;">${locale.get("profile.premiere")}</th>
								<th style="width: 63px;">${locale.get("profile.duration")}</th>
								<th style="width: 127px; text-align: right;">
									${locale.get("profile.ratingimdb")}</th>
								<th style="width: 127px; text-align: right;">
									${locale.get("profile.ratingmm")}</th>
								<th style="width: 41px;">${locale.get("profile.views")}</th>
							</tr>
						</thead>
						<tbody>
							<c:if test="${not empty filmsSceduled}">
								<c:forEach var="film" items="${filmsSceduled}">
									<tr>
										<td><a href="movie_details?id=${film.id}"> <c:if
													test="${locale.language eq 'uk'}">
				                          ${film.titleUk}
				                    </c:if> <c:if test="${locale.language eq 'en'}">
										    ${film.title}
									</c:if>
										</a></td>
										<td style="text-align: right;"><c:if
												test="${empty film.released}">
									    N/A
									</c:if> <c:if test="${not empty film.released}">
									    ${film.released}
									</c:if></td>
										<td style="text-align: right;"><c:if
												test="${empty film.runtime}">
									    N/A
									</c:if> <c:if test="${not empty film.runtime}">
									    ${film.runtime}&#09;${locale.get("profile.min")}
									</c:if></td>
										<td style="text-align: right;"><c:if
												test="${empty film.ratingImdb}">
												<p>${locale.get("profile.unrating")}</p>
											</c:if> <c:if test="${not empty film.ratingImdb}">
				                       ${film.ratingImdb}&#09;
				                    <span class="rating" data-role="rating"
													data-value="${film.ratingImdb/2}" data-color-rate="true"
													data-step="0.1" data-static="true" data-stars=5>&nbsp;</span>
											</c:if></td>
										<td style="text-align: right;"><c:if
												test="${empty film.ratingCustom}">
												<p>${locale.get("profile.unrating")}</p>
											</c:if> <c:if test="${not empty film.ratingCustom}">
				                       ${film.ratingCustom}&#09;
				                       <span class="rating" data-role="rating"
													data-value="${film.ratingCustom/2}" data-color-rate="true"
													data-step="0.1" data-static="true" data-stars=5>&nbsp;</span>
											</c:if></td>
										<td style="text-align: right;">
											${film.reviews}&nbsp;&nbsp;<i class="fa fa-eye"></i>
										</td>
									</tr>
								</c:forEach>
							</c:if>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</c:if>
</div>
<div class="col-sm-2 pull-right hidden-xs" id="right-side-bar">
	<jsp:include page="profilebar.jsp" />
</div>