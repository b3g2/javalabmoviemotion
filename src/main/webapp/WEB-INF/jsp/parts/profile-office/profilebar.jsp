<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>

<script src="scripts/Chart.min.js"></script>
<div>
	<div align="center">
		<c:if test="${not empty user.photoURL}">
			<img src="${user.photoURL}" alt="..." class="img-circle team-img"
				width="150px" height="160px">
		</c:if>
		<c:if test="${empty user.photoURL}">
			<img src="images/avatarStandard.png" alt="..."
				class="img-circle team-img" width="150px" height="160px">
		</c:if>
	</div>

	<div class="panel custom">
		<div>
			<c:if test="${empty withoutFilmsWatched}">
				<h5 class="title" style="text-align: center;">${locale.get("parts.profilebar.genre")}</h5>
				<div id="canvas-holder" style="margin-bottom: 10px;">
					<canvas id="chart-area" width="149" height="149"></canvas>
				</div>
			</c:if>

			<div class="panel" style="background-color: #003B64;">
				<div class="panel-heading">
					<div class="row" style="color: white;">
						<c:if test="${empty place}">
							<div class="col-xs-12" style="text-align: center;">
								<div class="huge">${locale.get("parts.profilebar.emptyrating")}</div>
								<a href="users" style="color: rgb(1, 125, 211);">${locale.get("parts.profilebar.findfriends")}</a>
							</div>
						</c:if>
						<c:if test="${not empty place}">
							<div class="col-xs-3">
								<i class="fa fa-trophy fa-3x"></i>
							</div>
							<div class="col-xs-9 text-right">
								<div class="huge">${place}</div>
								<div>${locale.get("parts.profilebar.place")}</div>
							</div>
						</c:if>
					</div>
				</div>
			</div>

			<div class="panel" style="background-color: #DA6034;">
				<div class="panel-heading">
					<div class="row" style="color: white;">
						<div class="col-xs-3">
							<i class="fa fa-eye fa-3x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">${films}</div>
							<div>${locale.get("parts.profilebar.all")}</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel" style="background-color: #5CB85C;">
				<div class="panel-heading">
					<div class="row" style="color: white;">
						<div class="col-xs-3">
							<i class="fa fa-clock-o fa-3x" style="color: #FFFFFF;"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">${mins}</div>
							<div>${locale.get("parts.profilebar.mins")}</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel" style="background-color: #F0AD4E;">
				<div class="panel-heading">
					<div class="row" style="color: white;">
						<div class="col-xs-3">
							<i class="fa fa-clock-o fa-3x" style="color: #FFFFFF;"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">${hours}</div>
							<div>${locale.get("parts.profilebar.hours")}</div>
						</div>
					</div>
				</div>
			</div>

			<div class="panel" style="background-color: #D9534F;">
				<div class="panel-heading">
					<div class="row" style="color: white;">
						<div class="col-xs-3">
							<i class="fa fa-calendar fa-3x"></i>
						</div>
						<div class="col-xs-9 text-right">
							<div class="huge">${days}</div>
							<div>${locale.get("parts.profilebar.days")}</div>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>