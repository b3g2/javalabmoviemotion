<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag" uri="/WEB-INF/custom.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<link href="css/custom-style/users-style.css" rel="stylesheet"
	media="all">

<script type="text/javascript">
function goToPage(elem, newPage){
var uri = URI(window.location.href);
uri.removeSearch("page"); 
uri.addSearch("page", newPage);
$(elem).attr("href", uri.normalizePathname() );
}; 
</script>

<script type="text/javascript">
        $(document).ready(function () {
            $('.advanced-search-sub-list > li *').change(function () {
                $('#user-form').submit();
            })
        });
    </script>
<div>
	<div class="col-xs-9" id="center-content"
		style="padding: 0px 0px 1px 35px;">
		<div
			style="display: flex; width: 100%; align-items: baseline; margin-top: -30px;">
			<h4 id="user-profile" class="title">${locale.get(topText)}</h4>
			<c:if test="${not empty users}">
				<div id="row"
					style="width: 100%; margin-right: 20px; text-align: right;">
					<div id="total-users">${locale.get("users.total")}&nbsp;
						${users_count}</div>
				</div>
			</c:if>
		</div>
		<c:if test="${empty topUsers}">
			<form class="navbar-form" role="search" action="users" id="user-form">
				<div class="input-group">
					<input type="text" class="form-control input-flat"
						placeholder="${locale.get('parts.moderator.menu.search')}"
						name="userSearch" value="${userSearch}">
					<div class="input-group-btn">
						<button class="btn btn-default btn-flat" id="search-btn"
							type="submit">
							<i class="fa fa-search"></i>
						</button>
					</div>
				</div>
			</form>
		</c:if>

		<div id="row">
			<div id="users_area">
				<c:if test="${not empty users}">
					<tag:all-users users="${users}" locale="${locale.language}" />
				</c:if>
				<c:if test="${empty users && empty topUsers}">
					<div style="text-align: center;">
						<img alt="#" src="images/empty-search.png">
						<h4 class="index-title"
							style="color: #003b64; font-size: 22px; font-weight: 800;">${locale.get("movies.nothing")}</h4>
					</div>
				</c:if>
				<c:if test="${not empty topUsers}">
					<c:forEach var="user" items="${topUsers}">
						<div id="row" class="nbs-flexisel-item top">
							<div class="thumbnail top" style="margin-bottom: 10px;">
								<c:if test="${not empty user.user.photoURL}">
									<img src="${user.user.photoURL}" id="img_top" alt="..."
										class="img-responsive img-circle">
								</c:if>
								<c:if test="${empty user.user.photoURL}">
									<img src="images/avatarStandard.png" id="img_top" alt="..."
										class="img-responsive img-circle">
								</c:if>

								<div class="caption date-city top">
									<h4 class="title">${user.user.firstName}&#09;${user.user.lastName}</h4>

									<c:choose>
										<c:when test="${not empty activeUsers}">
											<div class="div-like">

												<div class="likes">
													<div class="like active">
														<span class="fa-stack" style="margin-top: -5px;"> <i
															class="fa fa-circle fa-stack-2x"></i> <i
															class="fa fa-thumbs-o-up fa-stack-1x fa-inverse"></i>
														</span>
													</div>
													<p style="font-size: 16px;" class="p_like">${user.likeCount}</p>
													<div class="dislike active">
														<span class="fa-stack fa-flip-horizontal"> <i
															class="fa fa-circle fa-stack-2x"></i> <i
															class="fa fa-thumbs-o-down fa-stack-1x fa-inverse"></i>
														</span>
													</div>
													<p style="font-size: 16px;" class="p_like">${user.dislikeCount}</p>
												</div>
											</div>
											<div class="clearfix"></div>
										</c:when>
										<c:when test="${not empty watchedUsers}">
											<div class="percentage-w-t-s top">
												<h5>${user.daysWatchedCount}</h5>
												<p>:</p>
												<h5>${user.hoursWatchedCount}</h5>
												<p>:</p>
												<h5>${user.minsWatchedCount}</h5>
											</div>
											<div class="top_content">
												<p>${locale.get("users.days")}</p>
												<p class="top_p">${locale.get("users.hours")}</p>
												<p>${locale.get("users.mins")}</p>
												<div class="clearfix"></div>
											</div>
										</c:when>
										<c:when test="${not empty comentedUsers}">
											<div class="percentage-w-t-s top">
												<h5 style="margin-left: 0px;">${user.commentCount}</h5>
												<p style="font-size: 1.15em; margin-top: 13px;">${locale.get("users.comments")}</p>
											</div>
											<div class="clearfix" style="margin-bottom: 10px;"></div>
										</c:when>
									</c:choose>

									<div class="tab_desc top btn-flat">
										<a href="user?id=${user.user.id}">${locale.get("users.open")}</a>
									</div>
								</div>
							</div>
						</div>
					</c:forEach>
				</c:if>

			</div>
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="col-sm-2 pull-right hidden-xs" id="right-side-bar"
		style="padding-bottom: 700px;">
		<div>

			<ul class="nav nav-pills nav-stacked" role="tablist">
				<li role="presentation"><a href="users" class="users-top"
					style="display: inline-flex; padding: 10px 5px;"><i
						class="fa fa-eraser user-star" style="color: #003B64;"></i>${locale.get("parts.searchbar.clear")}</a></li>
				<li role="presentation"><a href="users?top_watched"
					class="users-top" style="display: inline-flex; padding: 10px 5px;"><i
						class="fa fa-star fa-item-x user-star"></i>${locale.get("users.topshows")}</a></li>
				<li role="presentation"><a href="users?top_comented"
					class="users-top" style="display: inline-flex; padding: 10px 5px;"><i
						class="fa fa-star user-star"></i>${locale.get("users.topcomments")}</a></li>
				<li role="presentation"><a href="users?top_activ"
					class="users-top" style="display: inline-flex; padding: 10px 5px;"><i
						class="fa fa-star user-star"></i>${locale.get("users.topactive")}</a></li>

				<li role="presentation"><a class="users-top"
					data-toggle="collapse" href="#collapseGenre" aria-expanded="false"
					aria-controls="collapseGenre"
					style="display: inline-flex; padding: 10px 5px;"> <i
						class="fa fa-star user-star"></i>&nbsp;${locale.get("parts.profilebar.genres")}
				</a>
					<div
						class="collapse <c:if test="${not empty genreInput}"> in </c:if>"
						id="collapseGenre">
						<ul class="list-unstyled advanced-search-sub-list">
							<c:forEach var="genre" items="${genres}">
								<li>
									<div class="checkbox checkbox-primary"
										style="margin-top: 3px; margin-bottom: 3px;">
										<input form="user-form" type="checkbox" value="${genre.id}"
											name="genreInput" id="checkboxGenre${genre.id}"
											<c:forEach items="${genreInput}" var="input"><c:if test="${input eq genre.id}"> checked="checked" </c:if></c:forEach> />
										<label class="advance-search-sub-element"
											for="checkboxGenre${genre.id}"><c:if
												test="${locale.language eq 'en'}">${genre.name}</c:if> <c:if
												test="${locale.language eq 'uk'}">${genre.nameUK}</c:if> </label>
									</div>
								</li>
							</c:forEach>
						</ul>
						<br />
					</div></li>
			</ul>
		</div>
	</div>

	<div class="row">
		<div class="col-xs-10 text-center"
			style="border-right: 1px solid #eee; margin: 20px;">
			<c:if test="${prevPage}">
				<a class="users-pagination" onclick="goToPage(this,${page-1})">${locale.get("users.previous")}</a>
			</c:if>
			<c:if test="${nextPage}">
				<a class="users-pagination" onclick="goToPage(this,${page+1})">${locale.get("users.next")}</a>
			</c:if>
		</div>
	</div>
</div>
