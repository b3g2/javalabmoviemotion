<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<link href="css/check-box.css" rel="stylesheet" media="all">

<div style="margin-left: -15px;">
	<div class="advanced-search">
		<div class="clear-btn">
					<a href="movies" class="advance-search-sub"
						style="white-space: nowrap;"><h4><i class="fa fa-eraser"></i>&nbsp;${locale.get("parts.searchbar.clear")}</h4></a>
				</div>
		<a class="advance-search-sub" data-toggle="collapse"
			href="#collapseGenre" aria-expanded="false"
			aria-controls="collapseGenre">
			<h4><span class="glyphicon glyphicon-chevron-right"></span>&nbsp;${locale.get("parts.searchbar.genre")}
		</h4>
		</a>
		<div class="collapse <c:if test="${not empty genreInput}"> in </c:if>"
			id="collapseGenre">
			<ul class="list-unstyled advanced-search-sub-list">
				<c:forEach var="genre" items="${genres}">
					<li>
						<div class="checkbox checkbox-primary"
							style="margin-top: 3px; margin-bottom: 3px;">
							<input form="search-form" type="checkbox" value="${genre.id}"
								name="genreInput" id="checkboxGenre${genre.id}"
								<c:forEach items="${genreInput}" var="input"><c:if test="${input eq genre.id}"> checked="checked" </c:if></c:forEach> />
							<label class="advance-search-sub-element"
								for="checkboxGenre${genre.id}"><c:if
									test="${locale.language eq 'en'}">${genre.name}</c:if> <c:if
									test="${locale.language eq 'uk'}">${genre.nameUK}</c:if> </label>
						</div>
					</li>
				</c:forEach>
			</ul>
			<br />
		</div>
		<a class="advance-search-sub" data-toggle="collapse"
			href="#collapseYear" aria-expanded="false"
			aria-controls="collapseYear">
			<h4><span class="glyphicon glyphicon-chevron-right"></span>&nbsp;${locale.get("parts.searchbar.year")}
		</h4>
		</a>
		<div class="collapse <c:if test="${not empty yearInput}"> in </c:if>"
			id="collapseYear">
			<ul class="list-unstyled advanced-search-sub-list">
				<c:forEach var="year" items="${years}">
					<li>
						<div class="checkbox checkbox-primary"
							style="margin-top: 3px; margin-bottom: 3px;">
							<input form="search-form" type="checkbox" value="${year}"
								name="yearInput" id="checkboxYear${year}"
								<c:forEach items="${yearInput}" var="input"><c:if test="${input eq year}"> checked="checked" </c:if></c:forEach> />
							<label class="advance-search-sub-element"
								for="checkboxYear${year}">${year}</label>
						</div>
					</li>
				</c:forEach>
			</ul>
			<br />
		</div>
		<a class="advance-search-sub" data-toggle="collapse"
			href="#collapseCountry" aria-expanded="false"
			aria-controls="collapseCountry">
			<h4><span class="glyphicon glyphicon-chevron-right"></span>&nbsp;${locale.get("parts.searchbar.country")}
		</h4>
		</a>
		<div
			class="collapse <c:if test="${not empty countryInput}"> in </c:if>"
			id="collapseCountry">
			<ul class="list-unstyled advanced-search-sub-list">
				<c:forEach var="country" items="${countries}">
					<li>
						<div class="checkbox checkbox-primary"
							style="margin-top: 3px; margin-bottom: 3px; text-overflow: ellipsis; overflow: hidden;">
							<input form="search-form" type="checkbox" value="${country.id}"
								name="countryInput" id="checkboxCountry${country.id}"
								<c:forEach items="${countryInput}" var="input"><c:if test="${input eq country.id}"> checked="checked" </c:if></c:forEach> />
							<label class="advance-search-sub-element"
								for="checkboxCountry${country.id}"
								style="display: inline; white-space: nowrap;"
								title="<c:if
									test="${locale.language eq 'en'}">${country.name}</c:if> <c:if
									test="${locale.language eq 'uk'}">${country.nameUK}</c:if>"><c:if
									test="${locale.language eq 'en'}">${country.name}</c:if> <c:if
									test="${locale.language eq 'uk'}">${country.nameUK}</c:if></label>
						</div>
					</li>
				</c:forEach>
			</ul>
			<br />
		</div>
	</div>
</div>
