<%--suppress ALL --%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag" uri="/WEB-INF/custom.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!doctype html>
<html>
<head>
<title>${locale.get(title)}</title>
<jsp:include page="parts/imports.jsp" />
<link href="css/custom-style/mf-style.css" rel="stylesheet"
	media="all">
</head>
<body id="body-image">
	<div class="container">
		<div class="main-content">
			<%--header--%>
			<jsp:include page="parts/header.jsp" />
			<div class="container-fluid">
					<div class="col-xs-1" id="left-side-bar">
						<%--left side bar--%>
						<nav>
							<div class="menu-item">
								<a href="profile" class="item-icon"><i class="fa fa-user"></i></a>
								<a href="profile" class="item-label"><span>${locale.get("profile.prof")}</span></a>
							</div>
							<div class="menu-item">
								<a href="friends" class="item-icon"><i
									class="fa fa-hand-o-up"></i></a> <a href="friends"
									class="item-label"><span>${locale.get("profile.subscription")}</span></a>
							</div>
							<div class="menu-item">
								<a href="cinema" class="item-icon"><i
									class="fa fa-television"></i></a> <a href="cinema"
									class="item-label"><span>${locale.get("profile.cinema")}</span></a>
							</div>
							<div class="menu-item">
								<a href="users" class="item-icon"><i class="fa fa-users"></i></a>
								<a href="users" class="item-label"><span>${locale.get("profile.users")}</span></a>
							</div>
						</nav>
					</div>
				<jsp:include page="parts/profile-office/${profile_content}.jsp" />
			</div>
		</div>
		<jsp:include page="parts/footer.jsp" />
	</div>
</body>
</html>

