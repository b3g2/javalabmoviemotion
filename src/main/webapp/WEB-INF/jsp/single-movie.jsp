<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><c:choose>
		<c:when test="${locale.language=='uk'&&not empty filmBase.titleUk}">
			${filmBase.titleUk}:
		</c:when>
		<c:otherwise>
			${filmBase.title}:
		</c:otherwise>
	</c:choose>&nbsp;${locale.get("single.movie.title")}</title>
<jsp:include page="parts/imports.jsp" />
<link rel="stylesheet" href="css/custom-style/index-style.css">
<link rel="stylesheet" href="css/custom-single-movie.css">
<link rel="stylesheet" href="css/poster.css">
<link rel="stylesheet" href="css/custom-style/single-movie.css">
<script src="scripts/custom-js/friends.js"></script>
<script src="scripts/custom-js/single-movie.js"></script>
</head>

<body id="body-image">
	<jsp:include page="modals/cinemas-shows-modal.jsp" />
	<jsp:include page="modals/trailer-player-modal.jsp" />

	<div class="container">
		<div class="main-content">
			<%--header--%>
			<jsp:include page="parts/header.jsp" />
			<div class="col-xs-12" id="content-long">
				<div class="col-xs-4">
					<div class="row filmPoster">
						<c:choose>
							<c:when
								test="${empty filmBase.poster || filmBase.poster eq 'N/A'}">
								<img class="img-responsive" src="images/defaultMoviePoster.jpg"
									alt="We don't have this poster!">
							</c:when>
							<c:otherwise>
								<img class="img-responsive" src="${filmBase.poster}"
									alt="We can't display this poster">
							</c:otherwise>
						</c:choose>
					</div>
					<div class="watcherButtonsBlock"
						style="border: 1px solid #003B64; margin: 0 20px;">
						<div class="statisticsAndMarks row"
							style="background-color: #007DD3; color: #FFF; margin: 0">
							<div class="col-xs-2 statisticsItem">
								<span id="reviewsAmount">${filmBase.reviews}</span><br> <i
									class="fa fa-users"></i>
							</div>
							<div class="col-xs-2 statisticsItem" style="margin-left: -10px;">
								<span id="customRating"> <c:choose>
										<c:when test="${not empty filmBase.ratingCustom}">
								${filmBase.ratingCustom}/10
								</c:when>
										<c:otherwise> N/A </c:otherwise>
									</c:choose>
								</span> <br>MM
							</div>
							<div class="col-xs-2 statisticsItem" style="margin-left: 5px;">
								<span id="imdbRating"> <c:choose>
										<c:when test="${not empty filmBase.ratingImdb}">
								${filmBase.ratingImdb}/10
								</c:when>
										<c:otherwise>N/A</c:otherwise>
									</c:choose>
								</span> <br>IMDB
							</div>
							<div class="likes col-xs-6"
								style="font-size: 21px; padding-left: 10px;">
								<div class="like" id="likeBtn"
									onclick="changeFilmStatus('LIKED',${user.id},${filmBase.id});">
									<span class="fa-stack"> <i
										class="fa fa-circle fa-stack-2x"></i> <i
										class="fa fa-thumbs-o-up fa-stack-1x fa-inverse"></i>
									</span>
								</div>
								<div id="likes" class="markAmount">${filmBase.likes}</div>
								<div class="dislike" id="dislikeBtn"
									onclick="changeFilmStatus('DISLIKED',${user.id},${filmBase.id});">
									<span class="fa-stack fa-flip-horizontal"> <i
										class="fa fa-circle fa-stack-2x"></i> <i
										class="fa fa-thumbs-o-down fa-stack-1x fa-inverse"></i>
									</span>
								</div>
								<div id="dislikes" class="markAmount">${filmBase.dislikes}</div>
							</div>
						</div>

						<c:if test="${not empty user}">
							<div class="row"
								style="margin: 0; font-size: 12px; border-top: 1px solid #003B64;">
								<div id="horizontalTab" style="display: block; width: 100%;">
									<ul class="resp-tabs-list">
										<li class="resp-tab-item col-xs-6" aria-controls="tab_item-2"
											role="tab" id="scheduleBtn"
											style="border-right: 1px solid #003B64; text-transform: initial;"
											onclick="changeFilmStatus('SCHEDULED',${user.id},${filmBase.id});"
											class="btn btn-block notActive btn-relation"><span>${locale.get("single.movie.scheduled")}</span></li>
										<li class="resp-tab-item col-xs-6" aria-controls="tab_item-1"
											role="tab" id="markAsWatchedBtn"
											onclick="changeFilmStatus('WATCHED',${user.id},${filmBase.id});"
											class="btn btn-block btn-relation notActive"><span>${locale.get("single.movie.watched")}</span></li>
									</ul>
									<div class="clearfix"></div>
								</div>
							</div>
							<div class="row"
								style="margin: 0; font-size: 12px; border-top: 1px solid #003B64;">
								<div id="horizontalTab" style="display: block; width: 100%;">
									<ul class="resp-tabs-list">
										<li class="resp-tab-item col-xs-12 favourite"
											aria-controls="tab_item-0" role="tab" id="favouriteBtn"
											onclick="changeFilmStatus('FAVOURITE',${user.id},${filmBase.id});"
											class="btn btn-block btn-relation notActive"><span>${locale.get("single.movie.favourite")}</span></li>
									</ul>
									<div class="clearfix"></div>
								</div>
							</div>
						</c:if>
					</div>
				</div>

				<div class="col-xs-8 filmFeaturesList">
					<div class="filmDetails" style="height: 35.5em;">
						<div class="filmTitle index-title">
							<h3 class="title">
								<c:choose>
									<c:when
										test="${locale.language=='uk'&&not empty filmBase.titleUk}">
								${filmBase.titleUk}
								</c:when>
									<c:otherwise>
									${filmBase.title}
								</c:otherwise>
								</c:choose>
							</h3>
						</div>

						<div class="form-group filmFeature">
							<label class="col-sm-2 control-label">
								${locale.get("single.movie.genre")}:</label>
							<div class="col-sm-10 marker">
								<span id="genre" class="featureValue"> <c:choose>
										<c:when test="${not empty filmGenre}">
									${filmGenre}
								</c:when>
										<c:otherwise>
									${locale.get("single.movie.nodata")}
								</c:otherwise>
									</c:choose>
								</span>
							</div>
						</div>

						<div class="form-group filmFeature">
							<label class="col-sm-2 control-label control-label-big">
								${locale.get("single.movie.duration")}:</label>
							<div class="col-sm-10 marker">
								<span id="genre" class="featureValue"> <c:choose>
										<c:when test="${not empty filmBase.runtime}">
									${filmBase.runtime}&nbsp;${locale.get("single.movie.minutes")}
								</c:when>
										<c:otherwise>
									${locale.get("single.movie.nodata")}
								</c:otherwise>
									</c:choose>
								</span>
							</div>
						</div>

						<div class="form-group filmFeature">
							<label class="col-sm-2 control-label">${locale.get("single.movie.description")}:</label>
							<div class="col-sm-10 marker">
								<span id="genre" class="featureValue"> <c:choose>
										<c:when
											test="${locale.language=='uk'&&not empty filmBase.plotUk}">
								${filmBase.plotUk}
								</c:when>
										<c:otherwise>
											<c:choose>
												<c:when test="${not empty filmBase.plot}">
									${filmBase.plot}
								</c:when>
												<c:otherwise>
															${locale.get("single.movie.nodata")}								
								</c:otherwise>
											</c:choose>
										</c:otherwise>
									</c:choose>
								</span>
							</div>
						</div>

						<div class="form-group filmFeature">
							<label class="col-sm-2 control-label">
								${locale.get("single.movie.actors")}:</label>
							<div class="col-sm-10 marker">
								<span id="genre" class="featureValue"> <c:if
										test="${not empty filmBase.actors}">
									${filmBase.actors}
								</c:if> <c:if test="${empty filmBase.actors}">
															${locale.get("single.movie.nodata")}
								</c:if>
								</span>
							</div>
						</div>

						<div class="form-group filmFeature">
							<label class="col-sm-2 control-label">${locale.get("single.movie.director")}:</label>
							<div class="col-sm-10 marker">
								<span id="genre" class="featureValue"> <c:if
										test="${not empty filmBase.director}">
									${filmBase.director}
								</c:if> <c:if test="${empty filmBase.director}">
															${locale.get("single.movie.nodata")}
								</c:if>
								</span>
							</div>
						</div>

						<div class="form-group filmFeature">
							<label class="col-sm-2 control-label control-label-big">${locale.get("single.movie.written")}:</label>
							<div class="col-sm-10 marker">
								<span id="genre" class="featureValue"> <c:if
										test="${not empty filmBase.writer}">
									${filmBase.writer}
								</c:if> <c:if test="${empty filmBase.writer}">
															${locale.get("single.movie.nodata")}
								</c:if>
								</span>
							</div>
						</div>

						<div class="form-group filmFeature">
							<label class="col-sm-2 control-label">${locale.get("single.movie.country")}:</label>
							<div class="col-sm-10 marker">
								<span id="genre" class="featureValue"> <c:if
										test="${not empty filmCountry}">
									${filmCountry}
								</c:if> <c:if test="${empty filmCountry}">
															${locale.get("single.movie.nodata")}
								</c:if>
								</span>
							</div>
						</div>

						<div class="form-group filmFeature">
							<label class="col-sm-2 control-label">${locale.get("single.movie.year")}:</label>
							<div class="col-sm-10 marker">
								<span id="genre" class="featureValue"> <c:if
										test="${not empty filmBase.year}">
									${filmBase.year}
								</c:if> <c:if test="${empty filmBase.year}">
															${locale.get("single.movie.nodata")}
								</c:if>
								</span>
							</div>
						</div>

						<div class="form-group filmFeature">
							<label class="col-sm-2 control-label">${locale.get("single.movie.released")}:</label>
							<div class="col-sm-10 marker">
								<span id="genre" class="featureValue"> <c:if
										test="${not empty filmBase.released}">
									${filmBase.released}
								</c:if> <c:if test="${empty filmBase.released}">
															${locale.get("single.movie.nodata")}
								</c:if>
								</span>
							</div>
						</div>
					</div>

					<div class="trailerAndCinema row" style="margin: 0 10px;">
						<div class="col-xs-6">
							<button id="open-trailer" class="btn btn-block btn-primary"
								style="padding: 10px 20px;">${locale.get("single.movie.trailer")}</button>
						</div>
						<div class="col-xs-6">
							<input type="hidden" id="checkIfFirst" name="checkIfFirst">
							<button id="getCinemasButton" class="btn btn-block btn-primary"
								style="padding: 10px 20px;">${locale.get("single.movie.where")}</button>
						</div>
					</div>
				</div>
			</div>

			<c:if test="${not empty filmRelatedFilms}">
				<div class="related">
					<div class="row relatedHeader">
						<h3 class="sectionHeadline title">${locale.get("single.movie.related")}</h3>
					</div>
					<div class="row relatedFilms" style="text-align: center;">
						<c:forEach items="${filmRelatedFilms}" var="record">

							<div class="poster-wrapper">
								<div class="poster-view poster-view-tenth">
									<c:if test="${record.poster ne 'N/A'}">
										<img src="${record.poster}" alt="poster"
											style="max-width: 230px;">
									</c:if>
									<c:if test="${record.poster eq 'N/A'}">
										<img src="images/defaultMoviePoster.jpg" alt="poster"
											style="max-width: 230px;">
									</c:if>
									<div class="mask">
										<c:if test="${locale.language eq 'uk'}">
											<h2 style="font-size: 13px; margin: 20px 10px 0 10px;">${record.titleUk}</h2>
										</c:if>
										<c:if test="${locale.language eq 'en'}">
											<h2 style="font-size: 13px; margin: 20px 10px 0 10px;">${record.title}</h2>
										</c:if>
										<p>
											<i class="fa fa-calendar">&nbsp;</i>
											<c:if test="${record.plot!=null}">${record.released}</c:if>
											<c:if test="${record.plot==null}">N/A</c:if>
										</p>
										<c:if
											test="${locale.language eq 'en' && not empty record.plot}">
											<p class="plot" style="font-size: 11px; line-height: 15px;">${record.plot}</p>
										</c:if>
										<c:if
											test="${locale.language eq 'uk' && not empty record.plotUk}">
											<p class="plot" style="font-size: 11px; line-height: 15px;">${record.plotUk}</p>
										</c:if>
										<a href="movie_details?id=${record.id}" class="info btn-flat">${locale.get("movies.details")}</a>
									</div>
								</div>
								<c:if test="${locale.language eq 'uk'}">
									<a href="movie_details?id=${record.id}"
										style="max-width: 230px;"
										class="btn href btn-primary btn-flat overflow-hidden">${record.titleUk}</a>
								</c:if>
								<c:if test="${locale.language eq 'en'}">
									<a href="movie_details?id=${record.id}"
										style="max-width: 230px;"
										class="btn href btn-primary btn-flat overflow-hidden">${record.title}</a>
								</c:if>
							</div>
						</c:forEach>
					</div>
				</div>
			</c:if>

			<div class="comments">
				<div id="template">
					<div class="comment newComment" style="display: none;">
						<div class="client clientPhoto newCommentator"></div>
						<div style="width: 70%; float: left;"
							class="client-message commentMainInfo newCommentContent"></div>
						<div style="width: 10%; float: left;" class="deleteContainer">
							<button class="btn btn-flat"
								onclick='deleteComment("${commentInfo.comment.id}")'>${locale.get("single.movie.comment.delete")}</button>
						</div>
						<div class="clearfix"></div>
					</div>
				</div>
				<div class="row">
					<h3 class="sectionHeadline title">${locale.get("single.movie.comment.comments.title")}</h3>
					<c:if test="${empty user}">
						<span class="col-xs-6 col-xs-offset-3" style="text-align: center;">${locale.get("single.movie.comment.restriction")}</span>
					</c:if>
				</div>
				<div class="commentArea">
					<c:if test="${not empty user}">
						<div class="row">

							<div class="col-xs-offset-1 col-xs-10"
								style="display: inline-flex; align-items: center;">
								<c:choose>
									<c:when test="${empty user.photoURL}">
										<img class="img-responsive img-circle"
											src="images/avatarStandard.png" alt="no photo!" width="120px"
											height="120px" style="max-height: 140px;">
									</c:when>
									<c:otherwise>
										<img class="img-responsive img-circle" src="${user.photoURL}"
											alt="no photo" width="120px" height="140px"
											style="max-height: 120px;">
									</c:otherwise>
								</c:choose>
								<textarea id="inputCommentArea" class="form-control"
									maxlength="500" name="comments" rows="5"
									placeholder='${locale.get("single.movie.comment.invitation")}'></textarea>
								<button id="commentSubmitBtn"
									onclick='addComment("${user.id}","${filmBase.id}")'
									class="btn btn-block btn-primary btn-flat"
									style="width: 160px;">${locale.get("single.movie.comment.add")}</button>
							</div>
						</div>
						<div class="row" style="text-align: center;">
							<div class="col-xs-2 col-xs-offset-5"></div>
						</div>
					</c:if>
				</div>
				<div id="allComments" style="margin: 0px 100px;">
					<c:forEach items="${filmCommentsInfo}" var="commentInfo">
						<div id="comment${commentInfo.comment.id}" class="comment">
							<div class="client clientPhoto">
								<c:choose>
									<c:when test="${empty commentInfo.commentator.photoURL}">
										<img src="images/avatarStandard.png" alt="no photo!"
											height="100px;" width="100px;">
									</c:when>
									<c:otherwise>
										<img src="${commentInfo.commentator.photoURL}" alt="no photo"
											height="100px;" width="100px;">
									</c:otherwise>
								</c:choose>
							</div>
							<div style="width: 70%; float: left;"
								class="client-message commentMainInfo">
								<p>
									<a href="user?id=${commentInfo.commentator.id}">${commentInfo.commentator.firstName}&nbsp;${commentInfo.commentator.lastName}</a>
									&nbsp;${commentInfo.comment.addedDateTime}
								</p>
								<h6>${commentInfo.comment.content}</h6>
							</div>
							<div style="width: 10%; float: left;" class="deleteContainer">
								<c:if
									test="${(user.role eq 'administrator')||(user.role eq 'user' && user.id eq commentInfo.comment.userId)}">
									<button class="btn btn-flat"
										onclick='deleteComment("${commentInfo.comment.id}")'>${locale.get("single.movie.comment.delete")}</button>
								</c:if>
							</div>
							<div class="clearfix"></div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
		<jsp:include page="parts/footer.jsp" />
	</div>
	<script>
	$(document).ready(function() {
		initialView();
		var today = new Date();
		var releasedDate = new Date('${filmBase.released}');
		if (today < releasedDate) {
			$('#markAsWatchedBtn').css("pointer-events", "none");
		}

		var initialUserId = "${user.id}";
		if (initialUserId != "" && initialUserId != null) {
			var initialMovieId = ${filmBase.id};
			changeFilmStatus('', initialUserId, initialMovieId);
		}
		var filmIdValue = ${filmBase.id};
		$("#getCinemasButton").click({
			filmId : filmIdValue,
			lang : '${locale.language}'
		}, getThisFilmShowingCinemas);
		$("#checkIfFirst").val('yes');
		$("#getCinemasButton").trigger("click");
	});
	</script>
</body>

</html>