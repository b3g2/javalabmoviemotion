<%--@elvariable id="locale" type="com.epam.lab.i18n.Localization"--%>
<script>
    $.extend($.validator.messages, {
        email: '${locale.get("validator.email")}',
        required: '${locale.get("validator.required")}',
        remote: '${locale.get("validator.remote")}',
        lValidate: '${locale.get("validator.lValidate")}',
        selected: '${locale.get("validator.selected")}'
    });
</script>
