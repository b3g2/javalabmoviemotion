<%--suppress ALL --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head>
    <title>Template</title>
    <jsp:include page="../parts/imports.jsp"/>
    <%--add here your custom imports--%>
</head>
<body id="body-image">
<div class="container">
    <div class="main-content">
        <%--header--%>
        <jsp:include page="../parts/header.jsp"/>
        <div class="container-fluid">
            <div class="col-xs-1" id="left-side-bar">
                <%--left side bar--%>
                <nav>
                    <div class="menu-item">
                        <a href="#" class="item-icon"><i class="fa fa-user"></i></a>
                        <a href="#" class="item-label"><span>User</span></a>
                    </div>
                    <div class="menu-item">
                        <a href="#" class="item-icon"><i class="fa fa-calendar"></i></a>
                        <a href="#" class="item-label"><span>Calendar</span></a>
                    </div>
                    <div class="menu-item">
                        <a href="#" class="item-icon"><i class="fa fa-facebook"></i></a>
                        <a href="#" class="item-label"><span>Sign in with facebook</span></a>
                    </div>
                </nav>
            </div>
            <div class="col-xs-9" id="center-content">
                <%--content--%>
            </div>
            <div class="col-sm-2 pull-right hidden-xs" id="right-side-bar">
                <%--right side bar--%>
            </div>
        </div>
    </div>
    <jsp:include page="../parts/footer.jsp"/>
</div>
</body>
</html>