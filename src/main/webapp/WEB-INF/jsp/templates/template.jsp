<%--suppress ALL --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!doctype html>
<html>
<head>
    <title>Template</title>
    <jsp:include page="../parts/imports.jsp"/>
    <%--add here your custom imports--%>
</head>
<body id="body-image">
<div class="container">
    <div class="main-content">
        <%--header--%>
        <jsp:include page="../parts/header.jsp"/>
        <div class="col-xs-12" id="content-long">
            <%--content--%>
        </div>
    </div>
    <jsp:include page="../parts/footer.jsp"/>
</div>
</body>
</html>