<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="tag" uri="/WEB-INF/custom.tld"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>

<!doctype html>
<html>
<head>
<title>${userInfo.user.firstName}&#09;${userInfo.user.lastName}</title>
<jsp:include page="parts/imports.jsp" />
<link href="css/dataTables.bootstrap.min.css" rel="stylesheet"
	media="all">

<script src="scripts/metro.js"></script>
<c:if test="${locale.language eq 'uk'}">
<script src="scripts/jquery.dataTables.min.js"></script>
</c:if>
<c:if test="${locale.language eq 'en'}">
<script src="scripts/jquery.dataTablesen.min.js"></script>
</c:if>
<script src="scripts/dataTables.bootstrap.min.js"></script>
<script type="text/javascript" src="scripts/jquery.flexisel.js"></script>
<script src="scripts/easyResponsiveTabs.js" type="text/javascript"></script>
<link href="css/custom-style/mf-style.css" rel="stylesheet" media="all">
<link href="css/custom-style/user-info-style.css" rel="stylesheet"
	media="all">
<script type="text/javascript" src="scripts/jquery.bootpag.min.js"></script>
<script src="scripts/custom-js/user-info.js"></script>

</head>
<body id="body-image">
	<div class="container">
		<div class="main-content">
			<jsp:include page="parts/header.jsp" />

			<div class="container-fluid">
				<div class="col-xs-1" id="left-side-bar">
					<%--left side bar--%>
					<nav>
						<div class="menu-item">
							<a href="profile" class="item-icon"><i class="fa fa-user"></i></a>
							<a href="profile" class="item-label"><span>${locale.get("profile.prof")}</span></a>
						</div>
						<div class="menu-item">
							<a href="friends" class="item-icon"><i class="fa fa-hand-o-up"></i></a>
							<a href="friends" class="item-label"><span>${locale.get("profile.subscription")}</span></a>
						</div>
						<div class="menu-item">
							<a href="cinema" class="item-icon"><i class="fa fa-television"></i></a>
							<a href="cinema" class="item-label"><span>${locale.get("profile.cinema")}</span></a>
						</div>
						<div class="menu-item">
							<a href="users" class="item-icon"><i class="fa fa-users"></i></a>
							<a href="users" class="item-label"><span>${locale.get("profile.users")}</span></a>
						</div>
					</nav>
				</div>
				<div class="col-xs-8" id="center-content">
					<input type="hidden" id="url" value="${url}" />
					<div class="user">
						<div id="user-photo">
							<c:if test="${not empty userInfo.user.photoURL}">
								<img src="${userInfo.user.photoURL}" alt="..."
									class="img-circle user-photo">
							</c:if>
							<c:if test="${empty userInfo.user.photoURL}">
								<img src="images/avatarStandard.png" alt="..."
									class="img-circle user-photo">
							</c:if>
							<div class="tab_desc" id="friend-div" style="margin-top: 10px;">
								<c:if test="${isFriend eq false}">
									<button onclick="subscribe(${userInfo.user.id})"
										class="btn btn-base btn-subscribe btn-flat"
										style="display: block;">${locale.get("user.information.sub")}</button>
								</c:if>
								<c:if test="${isFriend eq true}">
									<button onclick="unsubscribe(${userInfo.user.id})"
										class="btn btn-base btn-unsubscribe btn-flat"
										style="display: block;">${locale.get("user.information.unsub")}</button>
								</c:if>
							</div>
						</div>
						<div>
							<h4 id="userName">${userInfo.user.firstName}&#09;${userInfo.user.lastName}
							<small style="display: block; margin-top: 5px;">${userInfo.user.email}</small></h4>
							<div class="panel custom">
								<div id="statistics">

									<div class="panel"
										style="background-color: #0D4F7D; width: 150px;">
										<div class="panel-heading">
											<div class="row" style="color: white;">
												<div class="col-xs-3">
													<i class="fa fa-eye fa-3x" style="color: #FFFFFF;"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge">${films}</div>
													<div>${locale.get("parts.profilebar.all")}</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel"
										style="background-color: #5CB85C; width: 150px; margin-right: 20px; margin-left: 20px;">
										<div class="panel-heading">
											<div class="row" style="color: white;">
												<div class="col-xs-3">
													<i class="fa fa-clock-o fa-3x" style="color: #FFFFFF;"></i>
												</div>
												<div class="col-xs-9 text-right">
													<div class="huge">${userInfo.user.duration}</div>
													<div>${locale.get("user.information.mins")}</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel"
										style="background-color: #F0AD4E; width: 150px; margin-right: 20px;">
										<div class="panel-heading">
											<div class="row" style="color: white;">
												<div class="col-xs-3">
													<i class="fa fa-clock-o fa-3x" style="color: #FFFFFF;"></i>
												</div>
												<div class="col-xs-9 text-right">
													<fmt:formatNumber var="hours"
														value="${userInfo.user.duration/60}" maxFractionDigits="2" />
													<div class="huge">${fn:substringBefore(hours, ',')}</div>
													<div>${locale.get("user.information.hours")}</div>
												</div>
											</div>
										</div>
									</div>

									<div class="panel"
										style="background-color: #D9534F; width: 150px;">
										<div class="panel-heading">
											<div class="row" style="color: white;">
												<div class="col-xs-3">
													<i class="fa fa-calendar fa-3x"></i>
												</div>
												<div class="col-xs-9 text-right">
													<fmt:formatNumber var="mins"
														value="${userInfo.user.duration/60/24}"
														maxFractionDigits="2" />
													<div class="huge">${fn:substringBefore(mins, ',')}</div>
													<div>${locale.get("user.information.days")}</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>

							<div class="div-like">

								<div class="likes">
									<div class="like active">
										<span class="fa-stack"> <i
											class="fa fa-circle fa-stack-2x"></i> <i
											class="fa fa-thumbs-o-up fa-stack-1x fa-inverse"></i>
										</span>
									</div>
									<p class="p-like">${userInfo.likeCount}</p>
									<div class="dislike active">
										<span class="fa-stack fa-flip-horizontal"> <i
											class="fa fa-circle fa-stack-2x"></i> <i
											class="fa fa-thumbs-o-down fa-stack-1x fa-inverse"></i>
										</span>
									</div>
									<p class="p-like">${userInfo.dislikeCount}</p>
								</div>


								<div class="percentage-w-t-s top"
									style="text-align: center; margin-left: 95px;">
									<h5>${userInfo.commentCount}</h5>
									<p class="p-like">${locale.get("user.information.comments")}</p>
								</div>
							</div>
						</div>

					</div>

					<div class="featured" id="featured" style="margin: 0;">
						<ul class="resp-tabs-list" id="profile-tabs">
							<li class="resp-tab-item active" aria-controls="tab_item-2"
								role="tab"><a data-toggle="tab" href="#favourite">${locale.get("user.information.favourite")}</a></li>
							<li class="resp-tab-item" aria-controls="tab_item-1" role="tab"><a
								data-toggle="tab" href="#watched">${locale.get("user.information.watched")}</a></li>
							<li class="resp-tab-item" aria-controls="tab_item-0" role="tab"><a
								data-toggle="tab" href="#sceduled">${locale.get("user.information.scheduled")}</a></li>
						</ul>
						<div class="tab-content">
							<div id="favourite" class="tab-pane fade in active">
								<table class="profile_movies_table table table-striped">
									<thead>
										<tr>
											<th style="width: 174px;">${locale.get("profile.title")}</th>
											<th style="width: 65px;">${locale.get("profile.premiere")}</th>
											<th style="width: 63px;">${locale.get("profile.duration")}</th>
											<th style="width: 127px; text-align: right;">
												${locale.get("profile.ratingimdb")}</th>
											<th style="width: 127px; text-align: right;">
												${locale.get("profile.ratingmm")}</th>
											<th style="width: 41px;">${locale.get("profile.views")}</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${not empty filmsFavourite}">
											<c:forEach var="film" items="${filmsFavourite}">
												<tr>
													<td><a href="movie_details?id=${film.id}"> <c:if
																test="${locale.language eq 'uk'}">
								                          ${film.titleUk}
								                    </c:if> <c:if test="${locale.language eq 'en'}">
														    ${film.title}
													</c:if>
													</a></td>
													<td style="text-align: right;"><c:if
															test="${empty film.released}">
													    N/A
													</c:if> <c:if test="${not empty film.released}">
													    ${film.released}
													</c:if></td>
													<td style="text-align: right;"><c:if
															test="${empty film.runtime}">
													    N/A
													</c:if> <c:if test="${not empty film.runtime}">
													    ${film.runtime}&#09;${locale.get("profile.min")}
													</c:if></td>
													<td style="text-align: right;"><c:if
															test="${empty film.ratingImdb}">
															<p>${locale.get("profile.unrating")}</p>
														</c:if> <c:if test="${not empty film.ratingImdb}">
								                       ${film.ratingImdb}&#09;
								                    <span class="rating" data-role="rating"
																data-value="${film.ratingImdb/2}" data-color-rate="true"
																data-step="0.1" data-static="true" data-stars=5>&nbsp;</span>
														</c:if></td>
													<td style="text-align: right;"><c:if
															test="${empty film.ratingCustom}">
															<p>${locale.get("profile.unrating")}</p>
														</c:if> <c:if test="${not empty film.ratingCustom}">
								                       ${film.ratingCustom}&#09;
								                       <span class="rating" data-role="rating"
																data-value="${film.ratingCustom/2}"
																data-color-rate="true" data-step="0.1"
																data-static="true" data-stars=5>&nbsp;</span>
														</c:if></td>
													<td style="text-align: right;">
														${film.reviews}&nbsp;&nbsp;<i class="fa fa-eye"></i>
													</td>
												</tr>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
							</div>
							<div id="watched" class="tab-pane fade">
								<table class="profile_movies_table table table-striped">
									<thead>
										<tr>
											<th style="width: 174px;">${locale.get("profile.title")}</th>
											<th style="width: 65px;">${locale.get("profile.premiere")}</th>
											<th style="width: 63px;">${locale.get("profile.duration")}</th>
											<th style="width: 127px; text-align: right;">
												${locale.get("profile.ratingimdb")}</th>
											<th style="width: 127px; text-align: right;">
												${locale.get("profile.ratingmm")}</th>
											<th style="width: 41px;">${locale.get("profile.views")}</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${not empty filmsWatched}">
											<c:forEach var="film" items="${filmsWatched}">
												<tr>
													<td><a href="movie_details?id=${film.id}"> <c:if
																test="${locale.language eq 'uk'}">
								                          ${film.titleUk}
								                    </c:if> <c:if test="${locale.language eq 'en'}">
														    ${film.title}
													</c:if>
													</a></td>
													<td style="text-align: right;"><c:if
															test="${empty film.released}">
													    N/A
													</c:if> <c:if test="${not empty film.released}">
													    ${film.released}
													</c:if></td>
													<td style="text-align: right;"><c:if
															test="${empty film.runtime}">
													    N/A
													</c:if> <c:if test="${not empty film.runtime}">
													    ${film.runtime}&#09;${locale.get("profile.min")}
													</c:if></td>
													<td style="text-align: right;"><c:if
															test="${empty film.ratingImdb}">
															<p>${locale.get("profile.unrating")}</p>
														</c:if> <c:if test="${not empty film.ratingImdb}">
								                       ${film.ratingImdb}&#09;
								                    <span class="rating" data-role="rating"
																data-value="${film.ratingImdb/2}" data-color-rate="true"
																data-step="0.1" data-static="true" data-stars=5>&nbsp;</span>
														</c:if></td>
													<td style="text-align: right;"><c:if
															test="${empty film.ratingCustom}">
															<p>${locale.get("profile.unrating")}</p>
														</c:if> <c:if test="${not empty film.ratingCustom}">
								                       ${film.ratingCustom}&#09;
								                       <span class="rating" data-role="rating"
																data-value="${film.ratingCustom/2}"
																data-color-rate="true" data-step="0.1"
																data-static="true" data-stars=5>&nbsp;</span>
														</c:if></td>
													<td style="text-align: right;">
														${film.reviews}&nbsp;&nbsp;<i class="fa fa-eye"></i>
													</td>
												</tr>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
							</div>
							<div id="sceduled" class="tab-pane fade">
								<table class="profile_movies_table table table-striped">
									<thead>
										<tr>
											<th style="width: 174px;">${locale.get("profile.title")}</th>
											<th style="width: 65px;">${locale.get("profile.premiere")}</th>
											<th style="width: 63px;">${locale.get("profile.duration")}</th>
											<th style="width: 127px; text-align: right;">
												${locale.get("profile.ratingimdb")}</th>
											<th style="width: 127px; text-align: right;">
												${locale.get("profile.ratingmm")}</th>
											<th style="width: 41px;">${locale.get("profile.views")}</th>
										</tr>
									</thead>
									<tbody>
										<c:if test="${not empty filmsSceduled}">
											<c:forEach var="film" items="${filmsSceduled}">
												<tr>
													<td><a href="movie_details?id=${film.id}"> <c:if
																test="${locale.language eq 'uk'}">
								                          ${film.titleUk}
								                    </c:if> <c:if test="${locale.language eq 'en'}">
														    ${film.title}
													</c:if>
													</a></td>
													<td style="text-align: right;"><c:if
															test="${empty film.released}">
													    N/A
													</c:if> <c:if test="${not empty film.released}">
													    ${film.released}
													</c:if></td>
													<td style="text-align: right;"><c:if
															test="${empty film.runtime}">
													    N/A
													</c:if> <c:if test="${not empty film.runtime}">
													    ${film.runtime}&#09;${locale.get("profile.min")}
													</c:if></td>
													<td style="text-align: right;"><c:if
															test="${empty film.ratingImdb}">
															<p>${locale.get("profile.unrating")}</p>
														</c:if> <c:if test="${not empty film.ratingImdb}">
								                       ${film.ratingImdb}&#09;
								                    <span class="rating" data-role="rating"
																data-value="${film.ratingImdb/2}" data-color-rate="true"
																data-step="0.1" data-static="true" data-stars=5>&nbsp;</span>
														</c:if></td>
													<td style="text-align: right;"><c:if
															test="${empty film.ratingCustom}">
															<p>${locale.get("profile.unrating")}</p>
														</c:if> <c:if test="${not empty film.ratingCustom}">
								                       ${film.ratingCustom}&#09;
								                       <span class="rating" data-role="rating"
																data-value="${film.ratingCustom/2}"
																data-color-rate="true" data-step="0.1"
																data-static="true" data-stars=5>&nbsp;</span>
														</c:if></td>
													<td style="text-align: right;">
														${film.reviews}&nbsp;&nbsp;<i class="fa fa-eye"></i>
													</td>
												</tr>
											</c:forEach>
										</c:if>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<div class="col-xs-3 pull-right hidden-xs" id="right-side-bar"
					style="margin-left: 16px;">
					<h4 style="margin-bottom: 5px; text-align: center;">
					  ${locale.get("user.information.last")}</h4>

					<c:forEach var="film" items="${lastWatchedFilms}">
						<div class="view">
							<a href="movie_details?id=${film.id}"> <c:if
									test="${film.poster ne 'N/A'}">
									<img src="${film.poster}"
										class="img-responsive zoom-img poster" alt="" />
								</c:if> <c:if test="${film.poster eq 'N/A'}">
									<img src="images/defaultMoviePoster.jpg"
										class="img-responsive zoom-img poster" alt="" />
								</c:if></a>

							<div class="info1"></div>
							<div class="mask"></div>
							<div class="tab_desc">
								<a href="movie_details?id=${film.id}"> <c:if
										test="${locale.language eq 'uk'}">
									${film.titleUk}
								</c:if> <c:if test="${locale.language eq 'en'}">
									${film.title}
								</c:if>
								</a>
							</div>
						</div>
					</c:forEach>
				</div>
			</div>
		</div>
		<jsp:include page="parts/footer.jsp" />
	</div>
</body>
</html>
