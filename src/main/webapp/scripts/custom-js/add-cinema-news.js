    $(document).ready(function () {
        //open modal
        $('.open-add-news-modal').click(function (e) {
            e.preventDefault();
            $('#add-news-modal').modal({show: true});
        });
        var modal = $('#add-news-modal').initModal();

        var cinemaForm = $('#add-event-mf').initForm();
        cinemaForm.validate({
            rules: {
                name: {
                    required: true,
                    lValidate: {
                        type: 'name'
                    }
                },
                city: {
                    required: true,
                    lValidate: {
                        type: 'name'
                    }
                },
                newsDescription: {
                    required: true,
                    lValidate: {
                        type: 'msg'
                    }
                },
                eventHeader: {
                    required: false,
                   lValidate: {
                      type: 'msg'
                    }
                },
                eventPicture: {
                    required: false,
                   lValidate: {
                       type: 'url'
                    }
                }

            },
            messages: {
                name: {
                    lValidate: 'Please enter a valid cinema name.'
                },
                city: {
                    lValidate: 'Please enter a valid city.'
                },
                newsDescription: {
                    lValidate: 'Please enter a valid news description.'
                },
                eventHeader: {
                    lValidate: 'Please enter a valid news header.'
                },
                eventPicture: {
                    lValidate: 'Please enter a valid news picture URL.'
                }
            }
        });
        //when modal window is totally hidden
        modal.on('hidden.bs.modal', function () {

        });
        $('#add-event-mf-submit').click(function () {
            if (cinemaForm.force().valid()) {
            		if($('#add-event-mf-eventHeader').val()!=''){
            			addEvent();
            		}else{
            			if($('#add-event-mf-eventHeader').val()==''){
            			    sweetAlert({
            			        title: newsAddVocabulary.surenessCheck, 
            			        text: newsAddVocabulary.surenessAboutAnonymousNews, 
            			        type: "warning",
            			        showCancelButton: true,
            			        closeOnConfirm: false,
            			        confirmButtonText: newsAddVocabulary.confirmationAddNews,
            			        confirmButtonColor: "#ec6c62"
            			      }, function() {
            			    	  addEvent();
            			      });
            			}
            		}
            }
        });
        
        $('#add-event-mf-cancel').click(function () {
            modal.modal('hide');
        });
        $('#clear-filter').click(function () {
            $('#add-event-mf-id').val('');
            $('#add-event-mf-eventText').val('');
        });
        $('#add-event-cinema-city').on('keypress change focus', function () {
            var search = $('#add-event-cinema-city').val();
            $.get('data', {action: 'get', target: 'cinemas-cities', search: search}, function (json) {
                var content = $('#city-list').empty();
                var cities = JSON.parse(json);
                if(search!=''&&search!=null&&cities.length==1){
                	$('#add-event-cinema-city').val(cities[0]);
                	$('#add-event-cinema-name-container').css('opacity','1');
                	$('#add-event-cinema-name').removeAttr('disabled');
                }else{
                	$('#add-event-cinema-name-container').css('opacity','0.4');
                	$('#add-event-cinema-name').attr('disabled','disabled');
                }
                cities.forEach(function (city) {
                    content.append('<option value="' + city + '"></option>');
                });
            });
        });
        $('#add-event-cinema-name').on('keypress change focus', function () {
            var city = $('#add-event-cinema-city').val();
            var search = $('#add-event-cinema-name').val();
            $.get('data', {action: 'get', target: 'cinemas-names', search: search, param: city}, function (json) {
                var content = $('#cinema-list').empty();
                var cities = JSON.parse(json);
                cities.forEach(function (cinema) {
                    content.append('<option value="' + cinema + '"></option>');
                });
            });
            var idInput = $('#add-event-mf-id').val('');
            $.get('get_cinema', 
            	{
            	city: $('#add-event-cinema-city').val(),
            	name:$('#add-event-cinema-name').val()
            	},
            	function (json) {
                if(json.successfully){
                    idInput.val(json.cinemaId);
                    $('#add-event-mf-eventHeader').attr('disabled', false);
                    $('#add-event-mf-eventPicture').attr('disabled', false);
                    $('#add-event-mf-eventText').attr('disabled', false); 
                    $('#newsInputs').css('opacity','1');
                    $('#add-event-mf-submit').css('visibility','visible');
                }else{
                    $('#add-event-mf-eventHeader').attr('disabled', true);
                    $('#add-event-mf-eventPicture').attr('disabled', true);
                    $('#add-event-mf-eventText').attr('disabled', true);
                    $('#newsInputs').css('opacity','0.45');
                    $('#add-event-mf-submit').css('visibility','hidden');
                }
            });
        });
        var inputs = $('#add-event-mf-id, #add-event-mf-event');
        inputs.on('change focus', function () {
            if ($('#add-event-mf-id').val().length && ($('#add-event-mf-eventText').val().length)) {
                $('#add-event-mf-submit').val('Save').showb();
            } else {
                
            }
        });
    });
    
    function addEvent(){
		var text = $('#add-event-mf-eventText').val();
    	var header = $('#add-event-mf-eventHeader').val();
		var picture = $('#add-event-mf-eventPicture').val();
	$.post('add_cinema_news',{
		cinemaId:$('#add-event-mf-id').val(),
		newsText:text,
		newsHeader:header,
		newsPicture:picture
	},function(result){
		sweetAlert(newsAddVocabulary.newsWasAddedSuccess,newsAddVocabulary.newsWasAdded,'success');
        $('#add-event-mf').clearForm();
        $('#add-news-modal').modal('hide');
	});
    }