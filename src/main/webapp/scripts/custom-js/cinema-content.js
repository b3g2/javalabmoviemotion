(function() {
	$(document).ready(function() {
		
	  $('#cise').keyup(function() {
		$.ajax({
			type : "GET",
			url : "cinema",
			data : {"filter_movies" : true},
			dataType : "json",
			success : function(data) {
			f = data;
			$sel = $('datalist');
			$sel.html('');
			var $this = $(this);
	
		$.each(data, function(k,v) {
			if (v.cinemaName.toLowerCase().indexOf(
    	
	    $('#cise').val().toLowerCase()) > -1) {
     	   $sel.append('<option>'+ v.cinemaName + '</option>');
			}
		  });
	     }
	   });
   	 });
	
	 var geocodingAPI = "http://maps.googleapis.com/maps/api/geocode/json?address="
	    				+ $("#adds li:eq(0)").text() + "&sensor=true";

	 $.getJSON(geocodingAPI, function(json) {
	 // Set the variables from the results array
	 var address = json.results[0].formatted_address;
	 var latitude = json.results[0].geometry.location.lat;
     var longitude = json.results[0].geometry.location.lng;
	 // Set the table td text
	 $('#address').text(address);
	 $('#latitude').text(latitude);
	 $('#longitude').text(longitude);
	 });

	 // Caching the link jquery object
	 var $myLink = $('a.myLink');
  	 // Set the links properties
	 $myLink.prop({
			href : geocodingAPI,
			title : 'Click on this link to open in a new window.'
	  }).click(function(e) {
		    e.preventDefault();
			window.open(this.href, '_blank');
	  });

	  arr = [];
	  arrName = [];
	  arrCity = [];
	  arrAddress = [];
	  var j = 0;
	  var i = 0;
	  var l = 0;
	  var k = 0;
	  $("#adds .butAd").each(function() {
		b = $(this).text();
		arr[i] = b;
		i = i + 1;
	  });
	  
	  $("#adds .infoName").each(function() {
		c = $(this).text();
		arrName[j] = c;
		j = j + 1;
	  });
	  
	  $("#adds .infoAddress").each(function() {
			f = $(this).text();
			arrAddress[k] = f;
			k = k + 1;
		});
	  
	  $("#adds .infoCity").each(function() {
		d = $(this).text();
		arrCity[l] = d;
		l = l + 1;
	   });
	  
	  l = 0;
	  i = 0;
	  j = 0;
	  k = 0;
	  $("#adds .lic").each(function() {
		var a = $(this).text();
		getAddress(a, arr[i], arrName[j], arrCity[l], arrAddress[k]);
		i = i + 1;
		j = j + 1;
		l = l + 1;
		k = k + 1;
	   });
	  });
	  
	  function getAddress(a, b, c, d, f) {
		jQuery.ajax({
			type : "GET",
			dataType : "json",
			url : "http://maps.googleapis.com/maps/api/geocode/json",
			data : {'address' : a,
				    'sensor' : false },
			success : function(data) {
				if (data.results.length) {
					$('#addies').append(
							data.results[0].geometry.location.lat + "     _   "
									+ data.results[0].geometry.location.lng
									+ "<br>");

					var markers = [];
					var marker = new Object();
					marker.lat = data.results[0].geometry.location.lat;
					marker.lng = data.results[0].geometry.location.lng;
					marker.options = new Object();
					marker.options.icon = new google.maps.MarkerImage(
							"http://maps.google.com/mapfiles/marker.png");
					markers.push(marker);
					plotmap(markers, b, c, d, f)
				} else {
					$('#addies').append('invalid address');
				}
			}
		});
	}

	
	
	function myFunc() {
		var found = document.querySelectorAll(".ci");
		for (i = 0; i < found.length; i++) {
			found[i].style.display = 'block';
		}
	}
		
	function displayShowMoreLabels() {
		$(".newsItemContent").each(function(index) {
			if ((parseInt($(this).css("height"))) > 80) {
				$(this).css("height", "80px");
				$(this).css("overflow", "hidden");
				$(this).next().css("display", "inline-block");
			}
		});
	}

	$(document).ready(function() {
		displayShowMoreLabels();
	});
	
	$(window).load(function() {
		$('#cise').click(function(e) {
			e.preventDefault();
			$("#alert").hide();
		});
	});
})($);