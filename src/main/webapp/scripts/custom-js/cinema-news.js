function validate(str){
	var trimmed = str.trim();
	if(trimmed==''){
		return null;
	}else return str;
}

function showNextNews(language, byCinema) {
//	if (cinemaForm.force().valid()) {
    	
//   }

	
	var currentPage;
	var cinemaId;
	var recordInputData = false;
	if(typeof byCinema === 'undefined'){
		if(parseInt($('#currentPageNumberWithCinema').val())>0){
			byCinema=true;
		}
	}else{
		recordInputData=true;
		}
	
	if(typeof byCinema === 'undefined'){
		$('#currentPageNumberWithCinema').val(0);
		currentPage = $('#currentPageNumber').val();
	}else{
		if(!isTheSameCinemaClause()){
			$('#currentPageNumberWithCinema').val(0);
		}
		$('#currentPageNumber').val(1);
		currentPage = $('#currentPageNumberWithCinema').val();
		cinemaCity = $('#cinema-city-input').val();
		cinemaName = $('#cinema-name-input').val();
	}
	var pageNumberToPass = parseInt(currentPage) + 1;
	var newsParameters = {
			page : pageNumberToPass
			};
	if(!(typeof byCinema === 'undefined')){
		newsParameters.cinemaId=cinemaId;
		newsParameters.cinemaCity=cinemaCity;
		newsParameters.cinemaName=cinemaName;
	}
	$.post('events_history',
					newsParameters,
					function(result) {
						
						if(recordInputData){
							$('#currentCity').val(newsParameters.cinemaCity);
							$('#currentCinemaName').val(newsParameters.cinemaName);
						}
						if((pageNumberToPass==1) && (!(typeof byCinema === 'undefined'))){
							var initials = '<div class="row">'+
							'<div id="colNews1" class="col-xs-4"></div>'+
							'<div id="colNews2" class="col-xs-4"></div>'+
							'<div id="colNews3" class="col-xs-4"></div></div>';
							$('#cinemaNewsContent').html(initials);
						}
						if (result.newsList.length > 0) {
							var i;							
							for (i = 0; i < result.newsList.length; ++i) {
								$('#cinemaNewsContent').append(
										$('.templateContainer .newNewsItem'));
								var clone = $('.newNewsItem').clone(true, true);
								$('.templateContainer').prepend(clone);

								// img src
								var pictureURLGot = result.newsList[i].mainInfo.pictureURL;
								var imgSource;
								if (pictureURLGot != null
										&& pictureURLGot != '') {
									imgSource = pictureURLGot;
								} else {
									imgSource = "images/defaultEvent.jpg";
								}

								// news info
								var info = '<ul><li><i class="fa fa-user"></i><a href="user?id='
										+ result.newsList[i].user.id + '">';
								info += '<span>' + result.newsList[i].user.firstName
										+ ' ' + result.newsList[i].user.lastName
										+ '</span></a></li>';
								info += '<li><i class="fa fa-calendar"></i><span>'
										+ result.newsList[i].mainInfo.addedDateTime
										+ '</span></li></ul>';

								// header
								var gotHeader = result.newsList[i].mainInfo.header;
								var header;
								if (gotHeader != null && gotHeader != '') {
									header = gotHeader;
								} else {
									header = 'Announcement';
								}

								// content
								var newsId = result.newsList[i].mainInfo.id;
								var content = '<input class="showMoreInput" id="ch'
										+ newsId + '"	type="checkbox">';
								content += '<p class="newsItemContent">'
										+ result.newsList[i].mainInfo.content + '</p>';
								content += '<label style="visibility: hidden;" class="showMoreLabel"	for="ch'
										+ newsId + '"></label>';

								// about cinema
								var aboutCinema = '<div class="locationPartTitle">';
								if(language=='uk'){
									aboutCinema+='Кінотеатр:';
								}else{
									aboutCinema+='Cinema:';
								}
								aboutCinema += '&nbsp;<a href="cinema-info?id='+result.newsList[i].cinema.id+'"><span>'
										+ result.newsList[i].cinema.cinemaName
										+ '</span></a></div><div class="clearfix"></div>';
								aboutCinema += '<div class="locationPartTitle">';
								if(language=='uk'){
									aboutCinema+='Адреса:';
								}else{
									aboutCinema+='Address:';
								}
								aboutCinema += '&nbsp;<span>'
										+ result.newsList[i].cinema.city + ', '
										+ result.newsList[i].cinema.address
										+ '</span>';
								aboutCinema += '</div>';

								$('#cinemaNewsContent .newNewsItem img').attr(
										'src', imgSource);
								$(
										'#cinemaNewsContent .newNewsItem .blog-poast-info')
										.html(info);
								$('#cinemaNewsContent .newNewsItem h4 label')
										.html(header);
								$('#cinemaNewsContent .newNewsItem .mainPart')
										.html(content);
								$('#cinemaNewsContent .newNewsItem .aboutCinema')
										.html(aboutCinema);
								$('#cinemaNewsContent .newNewsItem').css(
										"display", "block");
								$('#cinemaNewsContent *').removeClass(
										'newNewsItem');
							}
							var newItems = $('#cinemaNewsContent .newsItem:not([id])');
							newsItemsAllocate(newItems);
							displayShowMoreLabels();
							if(result.hasNextItems==false){
								$('#showMoreButton').css('visibility', 'hidden');
							}else{
								$('#showMoreButton').css('visibility', 'visible');
							}
							//if was search
							if(!(typeof byCinema === 'undefined')){
								$('#currentPageNumberWithCinema').val(pageNumberToPass);
							}else{
								$('#currentPageNumber').val(pageNumberToPass);
							}
						}
					});
}

function displayShowMoreLabels() {
	$(".newsItemContent").each(function(index) {
		if ((parseInt($(this).css("height"))) > 80) {
			$(this).css("height", "80px");
			$(this).css("overflow", "hidden");
			$(this).next().css("visibility", "visible");
		} else {
			$(this).css("height", "80px");
		}
	});
}

$(document).ready(function() {
	newsItemsAllocate();
	displayShowMoreLabels();
	$('#cinemasNews').css('visibility','visible');
});

$(document).ready(function () {
    var cinemaForm = $('#get-news-mf').initForm();
    
    cityInputLoad();
    cinemaNameInputLoad();
    
    $('#clear-filter').click(function () {
        $('#get-news-mf-id').val('');
        $('#chosenCity').val('');
        $('#chosenName').val('');
   });

    $('#cinema-city-input').on('keyup focus input', cityInputLoad);        
    
    $('#cinema-name-input').on('keyup focus input', cinemaNameInputLoad);        
    
    var inputs = $('#get-news-mf-id, #get-news-mf-event');
    inputs.on('change focus', function () {
        if ($('#get-news-mf-id').val().length && ($('#get-news-mf-eventText').val().length)) {
            $('#get-news-mf-submit').val('Save').showb();
        } else {
            
        }
    });
});

function newsItemsAllocate(newsList){
	if(typeof newsList=='undefined'){
	var newsItemsAmount = $("#cinemaNewsContent .newsItem").length;
	
	var currentColumn;
	var currentNewsItem;
	
	var containerId;
	var newsItemId;

	for(i=0;i<newsItemsAmount;++i){
		currentColumn = i%3+1;
		currentNewsItem = i+1;
		
		containerId='colNews'+currentColumn;
		newsItemId='newsItem'+currentNewsItem;
		
		if(!($.contains('#'+containerId,'#'+newsItemId))){
			$('#'+containerId).append($('#'+newsItemId));
		}
	}	
	}else{
		var newsItemsAmount = newsList.length;		
		var currentColumn;		
		var containerId;
		for(i=0;i<newsList.length;++i){
			currentColumn = i%3+1;
			containerId='colNews'+currentColumn;		
			$('#'+containerId).append(newsList[i]);
		}	

	}
}

function cityInputLoad () {
    var search = $('#cinema-city-input').val();
    var cinema = $('#cinema-name-input').val();
    $.get('data', {action: 'get', target: 'cinemas-cities', search: search, param: cinema}, function (json) {
        var content = $('#city-list').empty();
        var cities = JSON.parse(json);
        cities.forEach(function (city) {
            content.append('<option value="' + city + '"></option>');
        });
        showNewsButtonManage();
    });
}

function cinemaNameInputLoad(e) {
    var city = $('#cinema-city-input').val();
    var search = $('#cinema-name-input').val();
    $.get('data', {action: 'get', target: 'cinemas-names', search: search, param: city}, function (json) {
        var content = $('#cinema-list').empty();
        var cities = JSON.parse(json);
        cities.forEach(function (cinema) {
            content.append('<option value="' + cinema + '"></option>');                
        });
        showNewsButtonManage();
    });

}

function showNewsButtonManage(){
	var unlock=false;

		var cityToCheck = $('#cinema-city-input').val();
		var cinemaNameToCheck = $('#cinema-name-input').val();
	
	var optionValues=[];
	$('#city-list option').each(function(index){
		optionValues[index]=$(this).val();
	});
	if($.inArray(cityToCheck,optionValues)>=0||cityToCheck==''){
		optionValues=[];
		$('#cinema-list option').each(function(index){
			optionValues[index]=$(this).val();    			
		});
    	if($.inArray(cinemaNameToCheck,optionValues)>=0||cinemaNameToCheck==''){
    		var lastSelectedCity = $('#currentCity').val();
    		var lastSelectedCinemaName = $('#currentCinemaName').val();
    		if(!isTheSameCinemaClause()){
    			unlock=true;
    		}
    	}
		}
	
	if(unlock){
		$('#get-news-mf-submit').removeAttr('disabled');
		}
	else{
		$('#get-news-mf-submit').attr('disabled','disabled');
		}
	
	return unlock;
}

function isTheSameCinemaClause(){
	var lastSelectedCity = $('#currentCity').val();
	var lastSelectedCinemaName = $('#currentCinemaName').val();
		var cityToCheck = $('#cinema-city-input').val();
		var cinemaNameToCheck = $('#cinema-name-input').val();
		return ((lastSelectedCity==cityToCheck)&&(lastSelectedCinemaName==cinemaNameToCheck));
}


function tog(v){return v?'addClass':'removeClass';} 
$(document).on('input', '.clearable', function(){
    $(this)[tog(this.value)]('x');
}).on('mousemove', '.x', function( e ){
    $(this)[tog(this.offsetWidth-18 < e.clientX-this.getBoundingClientRect().left)]('onX');
}).on('touchstart click', '.onX', function( ev ){
    $(this).removeClass('x onX').val('').change();
    $(this).next().html('');
});