	function validate( sth){
		if(sth==null||sth=="")return 'N/A';
		else return sth
	}
	
function getRecommendation(id, language){
$.post("recommend", {
	  id:id,
	  language:language
	  }, function(result){
		  var msg;
		  if(language=='uk'){
			  msg='ВАША РЕКОМЕНДАЦІЯ НА СЬОГОДНІ';
		  }else{
			  msg='YOUR RECOMMENDATION FOR TODAY';
		  }
		  
		  if(result.statusMessage=='failure'){
			  $('#recommendationsHeader').text(msg);
			  $('#without_recommend').show();
			  $('#with_recommend').hide();
		  }else{
			  $('#without_recommend').hide();
			  $('#with_recommend').show();
			  $('#recommendationsHeader').text(msg);
			  var genres = "";
			  for (i = 0; i < result.genresAmount; i++) {
				  var curItem = 'genre'+i;
				    genres += result[curItem];
				    if(i!=result.genresAmount-1){
				    	genres+=', '
				    }
			}
		  $('#movieGenres').text(validate(genres));	
		  $('#movieName').text(validate(result.name));	
		  var duration = validate(result.duration);
		  if(duration!='N/A'){
		  $('#movieDuration').text(validate(duration)+(language=='uk'?' хвилин':' minutes'));
		  }
		  else{
			  $('#movieDuration').text('N/A');
		  }
		  $('#movieDescription').text(validate(result.description));
		  $('#movieActors').text(validate(result.actors));
		  $('#movieReleased').text(validate(result.released));
		  $('#movieRatingMovieMotion').text(validate(result.ratingMovieMotion));
		  $('#movieIMDBRating').text(validate(result.ratingIMDB));
		  if(result.posterURL=='N/A'){
			  $('#moviePoster').attr('src','images/defaultMoviePoster.jpg');
		  }
		  else{
			  $('#moviePoster').attr('src', result.posterURL);
		  }
		  $('.modal-body').show();
		  }
		  $("#myModal").modal({
				keyboard : true
			});
	  });
}

function subscribe(userId, lang){
	$.post("subscription", {
		  subscriptionAction:'subscribe',
		  userId:userId
		  }, function(result){
			  var msg;
			  if(lang=='uk'){
				  msg='Ви щойно додали нову підписку!';
			  }else{
				  msg='You have just subscribed!';				  
			  }
			  sweetAlert({
				  title: msg,   
				  timer: 1500,
				  type:'success',
				  showConfirmButton: false 
				  });
			  setTimeout(function(){
				  window.location.replace("friends");
			  }, 1500);
		  });
}


function unsubscribe(userId, lang){
	$.post("subscription", {
		  subscriptionAction:'unsubscribe',
		  userId:userId
		  }, function(result){
			  var msg;
			  if(lang=='uk'){
				  msg='Ви щойно видалили підписку!';
			  }else{
				  msg='You have just unsubscribed!';				  
			  }
			  sweetAlert({
				  title: msg,  
				  timer: 1500,
				  type:'success',
				  showConfirmButton: false 
				  });
			  setTimeout(function(){
				  window.location.replace("friends");
			  }, 1500);
			  });
}

function goToUser(userId){
	$('#form'+userId).submit();
}