(function() {
	$(document).ready(function() {

		addEventListener("load", function() {
			setTimeout(hideURLbar, 0);
		}, false);

		function hideURLbar() {
			window.scrollTo(0, 1);
		}

		$('#horizontalTab').easyResponsiveTabs({
			type : 'default', // Types: default, vertical, accordion
			width : 'auto', // auto or any width like 600px
			fit : true
		// 100% fit in a container
		});

		jQuery(document).ready(function($) {
			$(".scroll").click(function(event) {
				event.preventDefault();
				$('html,body').animate({
					scrollTop : $(this.hash).offset().top
				}, 1200);
			});
		});

		$('#slider').nivoSlider();

		//$(function() {
		//	SyntaxHighlighter.all();
		//});
		$(window).load(function() {
			$('.flexslider').flexslider({
				animation : "slide",
				start : function(slider) {
					$('body').removeClass('loading');
				}
			});
		});

		$("#flexiselDemo1").flexisel({
			visibleItems : 5,
			animationSpeed : 1000,
			autoPlay : true,
			autoPlaySpeed : 3000,
			pauseOnHover : true,
			enableResponsiveBreakpoints : true,
			responsiveBreakpoints : {
				portrait : {
					changePoint : 480,
					visibleItems : 2
				},
				landscape : {
					changePoint : 640,
					visibleItems : 3
				},
				tablet : {
					changePoint : 800,
					visibleItems : 4
				}
			}
		});
	});
})($);