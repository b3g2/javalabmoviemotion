
function validate(sth) {
	if (sth == null || sth == "")
		return 'N/A';
	else
		return sth
}

function initialView() {
	$('#genre').text(validate($('#genre').text()));
	$('#country').text(validate($('#country').text()));
	$('#runtime').text(validate($('#runtime').text()));
	$('#actors').text(validate($('#actors').text()));
	$('#director').text(validate($('#director').text()));
	$('#writer').text(validate($('#writer').text()));
	$('#year').text(validate($('#year').text()));
	$('#released').text(validate($('#released').text()));
	$('#plot').text(validate($('#plot').text()));
}

function changeFilmStatus(status, userId, filmId) {
	$.post("changeMovieStatus", {
		status : status,
		userId : userId,
		filmId : filmId
	}, function(result) {
		if (result.successfully == true)
			// BUTTONS
			// likes-dislikes
			if (result.likeValue == "LIKED") {
				$('#likeBtn').addClass('active');
				$('#dislikeBtn').removeClass('active');
			} else if (result.likeValue == "DISLIKED") {
				$('#dislikeBtn').addClass('active');
				$('#likeBtn').removeClass('active');
			} else if (result.likeValue == "NOTMARKED") {
				$('#dislikeBtn').removeClass('active');
				$('#likeBtn').removeClass('active');
			}
		// status
		if (result.status == "WATCHED") {
			$('#markAsWatchedBtn').addClass('resp-tab-active');
			$('#scheduleBtn').removeClass('resp-tab-active');
		} else if (result.status == "SCHEDULED") {
			$('#markAsWatchedBtn').removeClass('resp-tab-active');
			$('#scheduleBtn').addClass('resp-tab-active');
		} else if (result.status == "UNWATCHED") {
			$('#markAsWatchedBtn').removeClass('resp-tab-active');
			$('#scheduleBtn').removeClass('resp-tab-active');
		}
		// favourite
		if (result.favourite == false) {
			$('#favouriteBtn').removeClass('resp-tab-active');
		} else if (result.favourite == true) {
			$('#favouriteBtn').addClass('resp-tab-active');
		}

		// STATISTICS
		$('#likes').text(result.likes);
		$('#dislikes').text(result.dislikes);
		var customRating = validate(result.customRating);
		if (customRating != 'N/A')
			customRating += '/10';
		$('#customRating').text(customRating);
		var imdbRating = validate(result.imdbRating);
		if (imdbRating != 'N/A')
			imdbRating += '/10';
		$('#imdbRating').text(imdbRating);
		$('#reviewsAmount').text(result.reviews);

	});
}

function addComment(userId, filmId) {
	var content = $.trim($('#inputCommentArea').val());
	var sthWasInput = (content != '' && content
			.match(/<script[\s\S]*?>[\s\S]*?<\/script>/gi) == null);
	if (sthWasInput) {
		$
				.post(
						'add_comment',
						{
							userId : userId,
							filmId : filmId,
							content : content
						},
						function placeCommentHere(result) {
							if (result.successfully == true) {
								$('#allComments').prepend($('.newComment'));
								var clone = $('.newComment').clone(true, true);
								$('#template').prepend(clone);

								var link = "user?id=" + userId;
								var photoSrc = ("${user.photoURL}" == null || "${user.photoURL}" == "") ? "\"images/avatarStandard.png\""
										: "\"${user.photoURL}\"";
								var photo = '<img src=' + photoSrc
										+ ' alt="no photo">';
								var name = '${user.firstName}&nbsp;${user.lastName}';
								var time = result.dateTime;
								var comment = '<p><a href="' + link + '">'
										+ name + '</a>';
								comment += 'on&nbsp;' + time + '</p><h6>'
										+ content + '</h6>';
								$('#allComments .newComment').attr('id',
										'comment' + result.commentId);
								$('#allComments .newComment button').attr(
										'onclick',
										'deleteComment(' + result.commentId
												+ ')');
								$('#allComments .newCommentator').html(photo);
								$('#allComments .newCommentContent').html(
										comment);
								$('#allComments .newComment').css("display",
										"block");

								// клонувати, клон розмістити там же, а в нового
								// позабирати айдішки
								$('#inputCommentArea').val('');
								$('#allComments *').removeClass('newComment');
								$('#allComments *').removeClass(
										'newCommentator');
								$('#allComments *').removeClass(
										'newCommentContent');
								$("#allComments").load(location.href+" #allComments>*","");
							}
						});
	}
}

function getThisFilmShowingCinemas(event) {
	var filmId = event.data.filmId;
	var lang = event.data.lang;
	var check = $("#checkIfFirst").val();
	if (check == 'yes') {
		$("#checkIfFirst").val('no');
	}
	$
			.get(
					'get_cinemas',
					{
						filmId : filmId
					},
					function(result) {
						var length = result.length;
						if (length > 0) {
							var content = '';
							var item;
							for (var i = 0; i < length; ++i) {
								var clone = $('.location-template').clone(true,
										true);
								var currentId = "location" + i;
								clone.attr("id", currentId);
								clone.removeClass('location-template');
								$('#location-modal-body').append(clone);
								$('#' + currentId).css("display", "none");

								var location = result[i].cinemaPart.cinemaName
										+ ': ';
								location += result[i].cinemaPart.city + ', ';
								location += result[i].cinemaPart.address;
								$('#' + currentId + ' .location')
										.html(
												'<a href="cinema-info?id='
														+ result[i].cinemaPart.id
														+ '"><i class="fa fa-map-marker"></i>'
														+ location + '</a>');

								var dateLimits = transformDateFormat(result[i].showingPart.dateStart)
										+ '-';
								dateLimits += transformDateFormat(result[i].showingPart.dateFinish);
								$('#' + currentId + ' .date-limits').html(
										'<i class="fa fa-calendar"></i> '
												+ dateLimits);

								var priceLimits = result[i].showingPart.minPrice
										+ '-';
								priceLimits += result[i].showingPart.maxPrice
										+ ' UAH';
								$('#' + currentId + ' .price-limits').html(
										'<i class="fa fa-money"></i> '
												+ priceLimits);
								$('#' + currentId).css("display", "block");
							}

							// $('#location-modal-body').html(content);
							if (check == 'no') {
								$("#myModal").modal({
									keyboard : true
								});
							}
						} else {
							$('#getCinemasButton').attr("disabled", true);
							if(lang=="uk"){
								$('#getCinemasButton').text('Наразі показів немає');
							}else{
								$('#getCinemasButton').text('No cinemas available');
							}
						}
					});
}

function transformDateFormat(data) {
	var modifiedData = data.substr(5, 9);
	var day = modifiedData.substr(3, 5);
	var month = modifiedData.substr(0, 2);
	return day + "." + month;
}

function deleteComment(commentId) {
	$.post('delete_comment', {
		commentId : commentId
	}, function(result) {
		if (result.successfully) {
			$('#comment' + commentId).css("display", "none");
		}
	});
}

function getCheckValue() {
	return $("#checkIfFirst").val();
}
