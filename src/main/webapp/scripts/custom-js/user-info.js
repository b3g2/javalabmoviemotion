(function() {
	$(document).ready(function() {
		$('.profile_movies_table').DataTable();
	});

	subscribe = function(userId) {
		$.post("user", {
			subscriptionAction : 'subscribe',
			userId : userId
		}, function(result) {
			$("#friend-div").load(location.href+" #friend-div>*","");
		});
	}

	unsubscribe = function(userId) {
		$.post("user", {
			subscriptionAction : 'unsubscribe',
			userId : userId
		}, function(result) {
			$("#friend-div").load(location.href+" #friend-div>*","");
		});
	}
})($);
