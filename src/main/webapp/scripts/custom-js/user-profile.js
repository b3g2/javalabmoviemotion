(function() {
	
	$(document).ready(function() {
		$('.profile_movies_table').DataTable();
	});
	
	$(window).load(function() {
		
		var i = 0;
		$('#show-calendar').click(function(e) {
			if (i % 2 == 0) {
				$("#toggleText").show();
				$("#moviesForDayTag").show();
				$("#i1").hide();
				$("#review-slider").hide();
				$("#featured").hide();
				$("#i2").show();
			} else {
				$("#i2").hide();
				$("#moviesForDayTag").hide();
				$("#review-slider").show();
				$("#featured").show();
				$("#i1").show();
				$("#toggleText").hide();
			}
			i++;
		});

		var today = new Date();
		var month = today.getMonth() + 1;
		var year = today.getFullYear();

		if (month < 10) {
			month = '0' + month
		}

		today = year + '-' + month;
		
		$.ajax({
			type : "POST",
			url : "profile",
			data : { "calendar_visible" : true },
			dataType : "json",
			success : function(data) {
				var days = JSON.parse(data);
				$(".responsive-calendar").responsiveCalendar({
					time : today,
					events : days		
				});
			}
		});
		
		$.ajax({
			type : "POST",
			url : "profile",
			data : { "pie_chart" : true },
			dataType : "json",
			success : function(data) {
				var statistic = JSON.parse(data);
				var ctx = document.getElementById("chart-area").getContext("2d");
				window.myPie = new Chart(ctx).Pie(statistic);
			}
		});

		$('#show_all').click(function(e) {	
			$.ajax({
				type : "GET",
				url : "profile",
				data : { "all_movies" : true },
				dataType : "json",
				success : function(data) {
					$("#scrollbar_area").load(location.href+" #scrollbar_area>*","");
				}
			});
		});
		
		day_click = function(day, month, year) {
			
			if (day < 10) {
				day = '0' + day
			}
			
			if (month < 10) {
				month = '0' + month
			}
			
			$.ajax({
				type : "POST",
				url : "profile",
				data : { "day_click" : year+"-"+month+"-"+day },
				dataType : "json",
				success : function(data) {
					$("#scrollbar_area").load(location.href+"?refresh #scrollbar_area>*","");
				}
			});
		}
		
		$("#flexiselDemo1").flexisel({
			visibleItems : 4,
			animationSpeed : 1000,
			autoPlay : true,
			autoPlaySpeed : 3000,
			pauseOnHover : true,
			enableResponsiveBreakpoints : true,
			responsiveBreakpoints : {
				portrait : {
					changePoint : 480,
					visibleItems : 2
				},
				landscape : {
					changePoint : 640,
					visibleItems : 3
				},
				tablet : {
					changePoint : 800,
					visibleItems : 4
				}
			}
		});
	});
})($);
