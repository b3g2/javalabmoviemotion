(function () {
    Pager = function (pager, option) {
        var t = this;
        this.nextIndex = 0;
        this.page = 0;
        this.totalPages = option.totalPages;
        this.onPage = option.onPage;
        this.pageEl = $(pager).find('.page-number');
        this.prevEl = $(pager).find('.previous');
        this.get = function () {
            option.get();
        };
        this.nothing = function () {
            option.nothing();
        };
        this.checkPage = function () {
            if (this.page - 1 < 1) {
                this.prevEl.addClass('disabled');
            } else {
                this.prevEl.removeClass('disabled');
            }
            if (this.page + 1 > this.totalPages) {
                this.nextEl.addClass('disabled');
            } else {
                this.nextEl.removeClass('disabled');
            }

        };
        this.onPrev = function () {
            if (this.page > 1) {
                this.nextIndex -= this.onPage * 2;
                this.get();
                this.page--;
                this.nextIndex += this.onPage;
                this.pageEl.text(this.page);
                this.checkPage();
            }
        };
        this.prevEl.unbind('click');
        this.prevEl.click(function (e) {
            e.preventDefault();
            t.onPrev();
        });
        this.nextEl = $(pager).find('.next');
        this.onNext = function () {
            if (this.page < this.totalPages) {
                this.get();
                this.page++;
                this.nextIndex += this.onPage;
                this.pageEl.text(this.page);
                this.checkPage();
            }
        };
        this.nextEl.unbind('click');
        this.nextEl.click(function (e) {
            e.preventDefault();
            t.onNext();
        });
        this.reset = function () {
            this.page = 0;
            this.nextIndex = 0;
            this.onNext();
            this.pageEl.text(this.page);
            this.checkPage();
            if (this.totalPages == 0) {
                this.nothing()
            }
        };
        this.update = function () {
            if (this.page != 0) {
                this.nextIndex -= this.onPage;
                this.get();
                this.nextIndex += this.onPage;
            }
        };
    };
    //---bootstrap hide and show
    $.fn.hideb = function () {
        return $(this).addClass('hidden');
    };
    $.fn.showb = function () {
        return $(this).removeClass('hidden');
    };
    //---
    //submit form with ajax
    $.fn.ajaxSubmit = function (url, action, target, callback) {
        var data = {};
        var oldIDField = $(this).find('input.oldID');
        if (oldIDField) {
            var oldID = oldIDField.val();
        }
        var inputs = $(this).find('.form-control, input[ajax-submit="true"]');
        inputs.each(function () {
            if ($(this).attr('disabled') != 'disabled') {
                var name = $(this).attr('name');
                data[name] = $(this).val();
            }
        });
        var checkboxes = $(this).find('input[type="checkbox"]');
        checkboxes.each(function () {
            var value;
            if ($(this).attr('checked') == 'checked') {
                value = $(this).attr('checked-on');
            } else {
                value = $(this).attr('checked-off');
            }
            var name = $(this).attr('name');
            data[name] = value;
        });
        $.ajax({
            type: 'POST',
            url: url,
            data: {'action': action, 'target': target, 'data': JSON.stringify(data), 'oldID': oldID},
            success: callback
        });
    };
    //---tables---------------------------------------------------------------------------------------------------------
    $.fn.filterFields = function (table) {
        var all = $(this);
        $.each(this, function (i, el) {
            $(el).filterField(table, i + 1, all);
        });
    };
    $.fn.filterField = function (table, column_n, all) {
        this.on('change keyup', function () {
            var rows = table.find('tbody tr');
            var value = $(this).val();
            if (value) {
                rows.filter(function () {
                    return $(this).find('td:nth-child(' + column_n + ')').text().toLowerCase()
                            .indexOf(value.toLowerCase()) == -1;
                }).hide();
            } else {
                rows.show();
                $.each(all, function () {
                    if ($(this).val()) {
                        $(this).keyup();
                    }
                });
            }
        });
    };
    //---custom event---------------------------------------------------------------------------------------------------
    $.event.special.modify = {
        setup: function (data, namespaces) {
            var elem = this, $elem = $(elem);
            $elem.bind('keydown', $.event.special.modify.handler);
            $elem.keyup(function () {
                $(this).trigger('modify');
            });
        },

        teardown: function (namespaces) {
            var elem = this, $elem = $(elem);
            $elem.unbind('keydown', $.event.special.modify.handler);
        },

        handler: function (event) {
            var elem = this, $elem = $(elem), prevValue = $elem.data('value');
            var value = $elem.val();
            if (prevValue && value != prevValue) {
                event.type = "modify";
                $.event.dispatch.apply(this, arguments);
            }
            $elem.data('value', value);
        }
    };
})();
(function ($) {
    $.fn.goTo = function () {
        $('html, body').animate({
            scrollTop: $(this).offset().top + 'px'
        }, 'fast');
        return this;
    };
    $.fn.goSoftTo = function (fault) {
        $('html, body').animate({
            scrollTop: $(this).offset().top - fault + 'px'
        }, 'slow');
        return this;
    }
})(jQuery);