(function () {
    $.validator.methods.moreThan = function (value, element, param) {
        return parseInt($(element).val()) > parseInt($(param).val());
    };
    $.validator.methods.lessThan = function (value, element, param) {
        return parseInt($(element).val()) < parseInt($(param).val());
    };
    $.validator.methods.more = function (value, element, param) {
        return $(element).val() > param;
    };
    $.validator.methods.less = function (value, element, param) {
        return $(element).val() < param;
    };
    $.fn.lValidate = $.validator.methods.lValidate = function (value, element, param) {
        var patterns = {
            'name': /^[a-zа-яґєії`´ʼ’. -]{1,32}$/i,
            'email': /^[a-z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-z0-9.-]+$/i,
            'password': /^.{6,}$/,
            'title': /^[a-zа-яґєії`´ʼ’0-9,.!?'":; -]{1,64}$/i,
            'address': /^([a-zа-яґєії`´ʼ’0-9. -]{1,64}),([a-zа-яґєії`´ʼ’. -]{1,32}),([a-zа-яґєії`´ʼ’. -]{1,32})$/i,
            'city': /^[a-zа-яґєії`´ʼ’. -]{1,32}$/i,
            'street': /^[a-zа-яґєії`´ʼ’0-9. -]{1,64}$/i,
            'msg': /^.{1,12000}$/i,
            'url': /^(?:(?:https?|ftp):\/\/)?(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,}))\.?)(?::\d{2,5})?(?:[/?#]\S*)?$/i,
            'short': /^.{0,64}$/i
            //'date': /^$/ // todo: need implementation
            //'time': /^$/ // todo: need implementation
        };
        var type = param.type;
        var pattern = patterns[type];
        if (pattern) {
            var antiScript = !value.match(/<script\b[^>]*>([\s\S]*?)<\/script>/gim);
            var antiSql = !value.match(/(^|\s)(drop|alter|truncate)(\s)+(table|index|database)(\s|$)/gim);
            if (!antiScript || !antiSql) {
                swal({
                    title: t.validator.antiHack.title,
                    text: t.validator.antiHack.sub,
                    timer: 2000,
                    showConfirmButton: false,
                    type: 'warning'
                });
            }
            return !value.length || (!!value.match(pattern) && antiScript && antiSql);
        } else {
            return undefined;
        }
    };
    $.validator.methods.selected = function (value, element, param) {
        return value != param;
    };
    //---validator-defaults---------------------------------------------------------------------------------------------
    $.validator.setDefaults({
        errorPlacement: function (error, element) {
            var helpBlock = $(element).closest('.marker').find('.help-block');
            if (helpBlock.length) {
                helpBlock.text($(error).text()).showb();
            }
            $(element).closest('.marker').removeClass('has-success').addClass('has-error');
        },
        success: function (label, element) {
            var helpBlock = $(element).closest('.marker').find('.help-block');
            if (helpBlock.length) {
                helpBlock.hideb();
            }
            $(element).closest('.marker').removeClass('has-error').addClass('has-success');
        }
    });
    //---support-methods------------------------------------------------------------------------------------------------
    $.fn.initForm = function (withoutHelpBlock) {
        if (!withoutHelpBlock) {
            $.each($(this).find('.marker .form-control'), function () {
                    if ($(this).closest('.input-group').length) {
                        $(this).closest('.input-group').after('<span class="help-block hidden"></span>');
                    } else {
                        $(this).after('<span class="help-block hidden"></span>');
                    }
                }
            );
        }
        return $(this);
    };
    $.fn.initModal = function () {
        $(this).on('hidden.bs.modal', function () {
            $(this).find('form').clearForm();
        });
        return $(this);
    };
    $.fn.clearForm = function () {
        var helpBlock = $(this).find('.marker .help-block');
        if (helpBlock.length) {
            $(this).find('.marker .help-block').addClass('hidden');
        }
        $(this).find('.marker').removeClass('has-error');
        $(this).find('.marker').removeClass('has-success');
        $(this).find('.form-control').val('');
        $(this).find('select.form-control').val('-');
    };
    //---
    $.fn.force = function () {
        return $(this).find('.marker .form-control').trigger('keyup').end();
    };
    $.fn.focusInvalid = function () {
        $(this).find('.marker.has-error').first().find('.form-control').focus();
    };
})
();