// Load the SDK asynchronously
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s);
    js.id = id;
    js.src = "//connect.facebook.net/uk_UA/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));

window.fbAsyncInit = function () {
    FB.init({
        appId: '588979871252127',
        cookie: true,  // enable cookies to allow the server to access
                       // the session
        xfbml: true,  // parse social plugins on this page
        version: 'v2.2' // use version 2.2
    });

};

function fb_login() {
    FB.login(function (response) {
        if (response.authResponse) {
            access_token = response.authResponse.accessToken; //get access token
            user_id = response.authResponse.userID; //get FB UID
            FB.api('/me', {
                fields: 'id, first_name, last_name, email'
            }, function (response) {
                var userData = {
                    firstName: response.first_name,
                    lastName: response.last_name,
                    email: response.email,
                    photoURL: 'http://graph.facebook.com/' + response.id + '/picture\?type=square&height=200&width=200'
                };
                $.post("login", {
                    'action': 'signInWithSocial',
                    'data': JSON.stringify(userData)
                },onlogin);
                console.log(response.first_name, response.last_name, response.email);
            });
        } else {
            //user hit cancel button
        }
    });
}
