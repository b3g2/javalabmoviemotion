$(document).ready(function () {
    $('#footer-subscribe').click(function () {
        var value = $('#footer-subscribe-input').val();
        var valid = !!value.match(/^[a-z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-z0-9.-]+$/i);
        if (valid) {
            $.get('data', {action: 'add', target: 'subscribe', data: value}, function (json) {
                var res = JSON.parse(json);
                if (res) {
                    swal({
                        title: t.footerSubscribe.success.title,
                        text: t.footerSubscribe.success.sub,
                        timer: 2000,
                        showConfirmButton: false,
                        type: 'success'
                    });
                } else {
                    swal({
                        title: t.footerSubscribe.error.title,
                        text: t.footerSubscribe.error.sub,
                        timer: 2000,
                        showConfirmButton: false,
                        type: 'error'
                    });
                }
                $('#footer-subscribe-input').val('');
            })
        } else {
            swal(t.footerSubscribe.invalid.title, '', 'warning');
        }
    })
});
