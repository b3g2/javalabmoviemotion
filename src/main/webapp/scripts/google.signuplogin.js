var startGoogleApi = function () {
    gapi.load('auth2', function () {
        // Retrieve the singleton for the GoogleAuth library and set up the client.
        auth2 = gapi.auth2.init({
            client_id: '888869783874-56t9ml4lon48isa6m3ovkb0ul0s21vgm.apps.googleusercontent.com',
            cookiepolicy: 'single_host_origin',
            // Request scopes in addition to 'profile' and 'email'
            //scope: 'additional_scope'
        });
        attachSignIn(document.getElementById('signInWithGoogle'));
    });
};

function attachSignIn(element) {
    auth2.attachClickHandler(element, {},
        function (googleUser) {
            var first_name = googleUser.getBasicProfile().getGivenName();
            var last_name = googleUser.getBasicProfile().getFamilyName();
            var email = googleUser.getBasicProfile().getEmail();
            var id = googleUser.getBasicProfile().getId();
            var photoURL = null;
            var userData = {
                firstName: first_name,
                lastName: last_name,
                email: email,
                photoURL: photoURL

            };
            request = $.get('https://www.googleapis.com/plus/v1/people/' + id, {
                'fields': 'image', 'key': 'AIzaSyBRWE0Kno2v9ejSOBLQOe0Q-qFdgeK5YFM'
            }, function onSuccess(data) {
                photoURL = data.image.url;
                //get bigger picture - default size is 50.replace 50 with a 200
                var position = photoURL.lastIndexOf('50');
                if (position > -1) {
                    photoURL = photoURL.substring(0, position) + '200';
                }
                userData.photoURL = photoURL;
                $.post("login", {
                    'action': 'signInWithSocial',
                    'data': JSON.stringify(userData)
                }, onlogin);
            });
            request.error(function () {
                $.post("login", {
                    'action': 'signInWithSocial',
                    'data': JSON.stringify(userData)
                }, onlogin);
            });
        });
}