$(document).ready(function () {
    $.fn.highlight = function () {
        $(this).each(function () {
            var a = $(this);
            if (a.text() == document.title) {
                a.parent('li').addClass('highlight');
            }
        });
        return $(this);
    };
    $.fn.unhighlight = function () {
        $(this).each(function () {
            var a = $(this);
            a.parent('li').removeClass('highlight');
        });
        return $(this);
    };
    $('#header-menu').find('a').highlight().hover(
        function () {
            $('#header-menu').find('a').unhighlight();
        }, function () {
            $('#header-menu').find('a').highlight();
        });
    $('.header-menu-home').hover(
        function () {
            $('#header-menu').find('a').unhighlight();
        }, function () {
            $('#header-menu').find('a').highlight();
        });
});