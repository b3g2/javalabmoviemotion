$(document).ready(function () {
    $('#search-input').on('keyup focus', function (e) {
        if (e.which >= 37 && e.which <= 40) return;
        var search = $(this).val();
        if (search.length > 2) {
            $.get('data', {action: 'get', target: 'films', search: search, amount: 8}, function (json) {
                var films = JSON.parse(json);
                $('#search-list').empty().append('<input type="hidden" name="amount" value="' + films.length + '">');
                films.forEach(function (el, i) {
                    var title = (localeLanguage == 'uk' ? (el.titleUk ? el.titleUk : el.title) : el.title);
                    var plot = (localeLanguage == 'uk' ? (el.plotUk ? el.plotUk : el.plot) : el.plot);
                    var genres = (localeLanguage == 'uk' ? (el.genresUk ? el.genresUk : el.genres) : el.genres);
                    title = title.replace(new RegExp('(' + search + ')', 'gi'), '<span class="_highlight">$1</span>')
                    $('#search-list').append('<a href="movie_details?id=' + el.id + '"><li no="' + (i + 1) + '">' +
                        '<div><img src="' + (el.poster != 'N/A' ? el.poster : 'images/defaultMoviePoster.jpg') +
                        '" alt="poster"></div>' +
                        '<div><p>' +
                        '<b class="_title">' + (title ? title : "") + '&nbsp;' + (el.year ? '(' + el.year + ')' : '') + '</b>' +
                        '<b class="_rating">' + (el.ratingImdb ? el.ratingImdb : "N/A") + '&nbsp;<i class="fa fa-star"></i></b><br>' +
                        '<i class="_plot">' + (plot ? plot : "") + '</i>' +
                        '<b class="_genres"><i>' + (genres ? genres : "") + '</i></b>' +
                        '</p></div>' +
                        '</li></a>');
                });
                if (films.length) {
                    $('#search-list').showb();
                } else {
                    $('#search-list').hideb();
                }
            });
        } else {
            $('#search-list').hideb();
        }
    });

    function getFocusedElementNumber(def) {
        var no = def;
        var focusedElement = $('#header-search').find('li.focus');
        if (focusedElement.length) {
            no = parseInt(focusedElement.attr('no'));
        }
        return no;
    }

    function focusElement(no) {
        $('#header-search').find('li').removeClass('focus')
            .end().find('li[no="' + no + '"]').addClass('focus').goSoftTo(100);
    }

    function focusSearchInput() {
        $('#header-search').find('li').removeClass('focus');
        $('#search-input').focus().goSoftTo(100);
    }

    function hrefFocused() {
        var focused = false;
        var focusedElement = $('#header-search').find('li.focus');
        if (focusedElement.length) {
            focusedElement.click();
            focused = true;
        }
        return focused;
    }

    function hrefHovered() {
        var hovered = false;
        var hoveredElement = $('#header-search').find('li:hover');
        if (hoveredElement.length) {
            hovered = true;
        }
        return hovered;
    }

    $('#header-search').on('keydown', function (e) {
        if (e.which == 38 || e.which == 40) {//remove browser scroll on up and down
            e.preventDefault();
            return false;
        } else if (e.which == 13) {
            if (hrefFocused()) {
                e.preventDefault();
            }
        }
    }).on('keyup', function (e) {
        var amount;
        var no;
        if (e.which == 38) {//up
            amount = $(this).find('input[name="amount"]').val();
            no = getFocusedElementNumber(amount + 1) - 1;
        } else if (e.which == 40) {//down
            amount = $(this).find('input[name="amount"]').val();
            no = getFocusedElementNumber(0) + 1;
        }
        if (amount) {
            if (no >= 1 && no <= amount) {
                focusElement(no);
            } else {
                focusSearchInput();
            }
        }
    }).on('focusout', function () {
        debugger;
        if (!hrefHovered()) {
            $('#search-list').hideb();
        }
    });

    $('#search-input').on('input', function (e) {
        var $this = $(this);
        if (!$this.lValidate($this.val(), $this, {type: 'short'})) {
            e.preventDefault();
            return false;
        }
    }).on('keydown', function (e) {
        var $this = $(this);
        if (e.which == 13 && !$this.lValidate($this.val(), $this, {type: 'short'})) {
            e.preventDefault();
            return false;
        }
    });
});