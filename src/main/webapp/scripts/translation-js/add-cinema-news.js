var newsAddVocabulary={
		surenessCheck:'Are you sure?', 
		surenessAboutAnonymousNews:'Do you really want to add news without heading?',
		confirmationAddNews:'Yes, add it!',
		newsWasAddedSuccess:'Good job!',
		newsWasAdded:'The news was added!'
		}