(function () {
    t = {
        footerSubscribe: undefined,
        validator: undefined
    };
    if (localeLanguage == 'uk') {
        t.footerSubscribe = {
            success: {
                title: "Підписка",
                sub: "Ви успішно підписались на розсилку новин"
            },
            error: {
                title: "Упс...",
                sub: "Ви вже підписані на розсилку новин"
            },
            invalid: {
                title: "Некоректний email"
            }
        };
        t.validator = {
            antiHack: {
                title: "Хороша спроба :)",
                sub: "Виявлено недопустимий вираз"
            }
        }
    } else {
        t.footerSubscribe = {
            success: {
                title: "Subscribe",
                sub: "You have successfully subscribed for mailing"
            },
            error: {
                title: "Opps...",
                sub: "You are already subscribed for mailing"
            },
            invalid: {
                title: "Invalid email"
            }
        };
        t.validator = {
            antiHack: {
                title: "Nice try :)",
                sub: "Found invalid expression"
            }
        }
    }
})();
