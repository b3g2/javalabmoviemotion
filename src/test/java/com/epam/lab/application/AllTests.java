package com.epam.lab.application;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import com.epam.lab.connection.DataSourceTest;
import com.epam.lab.connection.JdbcConnectionPoolTest;
import com.epam.lab.service.DataServiceTest;
import com.epam.lab.service.UserServiceTest;
import com.epam.lab.web.servlet.page.UserProfilePageServletTest;
import com.epam.lab.web.servlet.page.UsersPageServletTest;

@RunWith(Suite.class)
@SuiteClasses({ DataSourceTest.class, JdbcConnectionPoolTest.class, DataServiceTest.class, UserServiceTest.class,
		UsersPageServletTest.class, UserProfilePageServletTest.class })
public class AllTests {

}
