package com.epam.lab.connection;

import static org.junit.Assert.*;

import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;

public class DataSourceTest {

	private Connection connection;
	private Class<?> classClass;
	private Class<?> classClassPool;

	@Before
	public void before() throws SQLException {
		classClass = JdbcConnectionPool.class;
		classClassPool = DataSource.class;
	}

	@Test
	public void notNullConnection() throws SQLException {
		connection = DataSource.getConnection();
		assertNotNull(connection);
	}

	@SuppressWarnings("unchecked")
	@Test
	public void sizeGetAndReturnConnectionPool() throws SQLException, NoSuchFieldException, SecurityException,
			IllegalArgumentException, IllegalAccessException {

		DataSource dataSource = new DataSource();
		List<Connection> connections = new ArrayList<>();
		Field poolField = classClassPool.getDeclaredField("pool");
		Field availableConnectionsField = classClass.getDeclaredField("availableConnections");

		poolField.setAccessible(true);
		availableConnectionsField.setAccessible(true);

		JdbcConnectionPool pool = (JdbcConnectionPool) poolField.get(dataSource);
		List<Connection> availableConnections = (List<Connection>) availableConnectionsField.get(pool);
		Integer beforeSizePool = availableConnections.size();

		for (int i = 0; i < 10; i++) {
			connections.add(DataSource.getConnection());
		}

		assertEquals(beforeSizePool - 10, availableConnections.size());

		for (Connection connection : connections) {
			DataSource.returnConnection(connection);
		}

		assertEquals(beforeSizePool + 0, availableConnections.size());
	}
}
