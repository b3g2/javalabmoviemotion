package com.epam.lab.connection;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;


public class JdbcConnectionPoolTest {

	private Connection connection;

    @Before
    public void before() throws SQLException {
        connection = DataSource.getConnection();
    }

    @After
    public void after() {
        DataSource.returnConnection(connection);
    }

    @Test
    public void returnConnectionShouldReturnConnection() throws SQLException {
        DataSource.returnConnection(connection);
        assertFalse(connection.isClosed());
    }

    @Test
    public void returnConnectionWithNullShouldNotThrow() {
        DataSource.returnConnection(null);
    }
}
