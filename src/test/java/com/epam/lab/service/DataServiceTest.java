package com.epam.lab.service;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.epam.lab.connection.DataSource;
import com.epam.lab.model.Cinema;
import com.epam.lab.model.User;

public class DataServiceTest {

	private static DataService dataService;
	private Connection connection;

	@BeforeClass
	public static void initializeClass() {
		dataService = new DataService();
	}

	@Before
	public void before() throws SQLException {
		connection = DataSource.getConnection();
	}

	@After
	public void after() {
		DataSource.returnConnection(connection);
	}

	@Test
	public void getCinemaTest() throws SQLException {

		Cinema cinemaTest = dataService.getCinema("Planeta");

		assertEquals(cinemaTest.getAddress(), "Зелена 113");
		assertEquals(cinemaTest.getCity(), "Львів");
		assertEquals(cinemaTest.getCountry(), "Україна");
		assertEquals(cinemaTest.getState(), "WORKING");
	}

	@Test
	public void getCinemaCitiesTest() throws SQLException {

		List<String> cities = new ArrayList<>();
		List<String> citiesTest = dataService.getCinemasCities(null);

		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT distinct city FROM cinema");

		while (rs.next()) {
			cities.add(rs.getString("city"));
		}

		assertArrayEquals(cities.toArray(), citiesTest.toArray());
	}

	@Test
	public void getCinemaNamesTest() throws SQLException {

		List<String> names = new ArrayList<>();
		List<String> namesTest = dataService.getCinemasNames(null, null);

		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT name FROM cinema");

		while (rs.next()) {
			names.add(rs.getString("name"));
		}

		assertArrayEquals(names.toArray(), namesTest.toArray());
	}

	@Test
	public void getUsersTest() throws SQLException {

		List<User> users = new ArrayList<>();
		DataService.GetParams params = new DataService.GetParams();
		params.setAll(true);
		List<User> usersTest = dataService.getUsers(null, params);
		System.out.println(usersTest);
		Statement stmt = connection.createStatement();
		ResultSet rs = stmt.executeQuery("SELECT * FROM user");

		if (rs.isBeforeFirst()) {
			while (!rs.isLast()) {
				if (rs.next()) {
					User result = new User();
					result.setId(rs.getInt("id"));
					result.setFirstName(rs.getString("fname"));
					result.setLastName(rs.getString("lname"));
					result.setRole(rs.getString("role"));
					result.setDuration(rs.getInt("duration"));
					result.setEmail(rs.getString("email"));
					result.setPassword(rs.getString("password"));
					result.setPhotoURL(rs.getString("photo_url"));
					result.setLanguage(rs.getString("language"));
					result.setSessionId(rs.getInt("session_id"));
					result.setTempPassword(rs.getString("temp_pass"));
					result.setBan(rs.getBoolean("ban"));
					result.setTempPasswordExpiry(rs.getString("temp_pass_expiry"));
					result.setAccessLevel(rs.getInt("access_level"));
					result.setMailing(rs.getBoolean("mailing"));
					
					users.add(result);
				}
			}
		}

		assertArrayEquals(users.toArray(), usersTest.toArray());
	}
}
