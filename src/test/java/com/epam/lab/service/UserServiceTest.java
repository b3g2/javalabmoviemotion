package com.epam.lab.service;

import static org.junit.Assert.*;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.json.simple.JSONArray;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mockito;

import com.epam.lab.connection.DataSource;
import com.epam.lab.dao.GeneralDAOService;
import com.epam.lab.model.Film;
import com.epam.lab.model.FilmUser;

public class UserServiceTest extends Mockito {

	private static UserService userService;
	private Connection connection;

	@BeforeClass
	public static void initializeClass() {
		userService = new UserService();
	}

	@Before
	public void before() throws SQLException {
		connection = DataSource.getConnection();
	}

	@After
	public void after() {
		DataSource.returnConnection(connection);
	}

	@Test
	public void getGenreStatisticTest() throws SQLException {

		HttpServletRequest request = mock(HttpServletRequest.class);
		List<Film> filmsWatched = new ArrayList<>();
		GeneralDAOService daoService = GeneralDAOService.getInstance();
		
		List<FilmUser> filmsWathedForUser = new ArrayList<>(
				daoService.getList("user_id='14' AND status='WATCHED'", FilmUser.class));
		
		for (FilmUser filmUser : filmsWathedForUser) {
			filmsWatched.add(daoService.get("id='" + filmUser.getFilmId() + "'", Film.class));
		}
		
		String expected = "["
			+ "{\"highlight\":\"#d8d8d8\",\"color\":\"#5CB85C\",\"label\":\"Екшин\",\"value\":12},"
			+ "{\"highlight\":\"#d8d8d8\",\"color\":\"#DA6034\",\"label\":\"Пригоди\",\"value\":10},"
			+ "{\"highlight\":\"#d8d8d8\",\"color\":\"#D9534F\",\"label\":\"Комедія\",\"value\":4},"
			+ "{\"highlight\":\"#d8d8d8\",\"color\":\"#F0AD4E\",\"label\":\"Драма\",\"value\":4},"
		    + "{\"highlight\":\"#d8d8d8\",\"color\":\"#003B64\",\"label\":\"Фантастика\",\"value\":8},"
		    + "{\"highlight\":\"#d8d8d8\",\"color\":\"#007DD3\",\"label\":\"Трилер\",\"value\":2}]";
		
		JSONArray statisticGenre = userService.getGenreStatistic(filmsWatched, request);
		assertEquals(expected, statisticGenre.toString());
  
	}
}
