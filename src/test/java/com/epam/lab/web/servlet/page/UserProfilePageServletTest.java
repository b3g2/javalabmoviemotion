package com.epam.lab.web.servlet.page;

import static org.junit.Assert.assertTrue;

import java.io.File;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mockito.Mockito;

import com.epam.lab.model.User;

public class UserProfilePageServletTest extends Mockito {

	@Test
	public void testServletDoPost() throws Exception {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);

		when(request.getParameter("calendar_visible")).thenReturn("true");
		when(request.getParameter("pie_chart")).thenReturn("true");
		when(request.getParameter("day_click")).thenReturn("true");
		PrintWriter writer = new PrintWriter("test.txt");
		when(response.getWriter()).thenReturn(writer);

		new UserProfilePageServlet().doPost(request, response);

		verify(request, atLeast(1)).getParameter("calendar_visible");
		writer.flush();
		assertTrue(FileUtils.readFileToString(new File("test.txt"), "UTF-8").contains(""));
	}
	
	@Test
	public void testServletDoGet() throws Exception {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);
		HttpSession session = mock(HttpSession.class);
		
		User user = new User();
		user.setFirstName("Marian");
		user.setLastName("Fediv");
		user.setEmail("marianfediv@gmail.com");
		user.setDuration(5000);
		user.setId(14);
		
		when(request.getSession()).thenReturn(session);
		when(session.getAttribute("user")).thenReturn(user);
		when(request.getParameter("all_movies")).thenReturn("true");
		PrintWriter writer = new PrintWriter("test.txt");
		when(response.getWriter()).thenReturn(writer);

		new UserProfilePageServlet().doGet(request, response);

		verify(request, atLeast(1)).getParameter("all_movies");
		writer.flush();
		assertTrue(FileUtils.readFileToString(new File("test.txt"), "UTF-8").contains(""));
	}
}
