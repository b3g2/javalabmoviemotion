package com.epam.lab.web.servlet.page;

import static org.junit.Assert.*;

import java.io.File;
import java.io.PrintWriter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.mockito.Mockito;

public class UsersPageServletTest extends Mockito {

	@Test
	public void testServlet() throws Exception {
		HttpServletRequest request = mock(HttpServletRequest.class);
		HttpServletResponse response = mock(HttpServletResponse.class);

		PrintWriter writer = new PrintWriter("test.txt");
		when(response.getWriter()).thenReturn(writer);

		new UsersPageServlet().populateSearch(request, 1);

		verify(request, atLeast(1)).getParameter("userSearch");
		writer.flush();
		assertTrue(FileUtils.readFileToString(new File("test.txt"), "UTF-8").contains(""));
	}
}